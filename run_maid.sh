#!/bin/bash
#
# run_maid.sh
#   Run the MAID model for a given scenario.
#
#########################################################################################
#
# Copyright notice:
#
# This file is part of MAID, "Model for Aerosol Ice Dynamics",
# Copyright (C) 2023, Tobias Schorr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# In the projects base directory you can find a copy of the
# GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
#
#########################################################################################

## catch errors occurring in this script
# '-e-: automatic error detection
# '-u': raise error when calling unbound variables
set -eu

# Default paths
SCRIPTPATH=$(readlink -f "$0")
BASEDIR=$(dirname "${SCRIPTPATH}")
PLOTLIBDIR=${BASEDIR}/tools/plotting

# Default config values
SOURCEPATH='src/maid.f90'
COMPILE=true
DEBUG=false
PLOT_TYPE="pdf"
SAVEPLOT=true
SHOWPLOT=false
SAVELOG=false
BINARYPATH=false
OUTPUTBASEDIR=false


#########################################################################################
# DEFINE COMMAND LINE INTERFACE

function show_usage(){
    # print help string
    printf "\n==================== run_maid.sh ====================\n"
    printf "Run the MAID model for a given scenario.\n\n"
    
    printf "Usage: $0 [scenario] [--option1] [value1] [--option2] [value2] ...\n"
    printf "\n"
    
    printf "Required argument: [scenario]\n"
    printf "  scenario:         string      –           Path to *.init file.\n\n"
    
    printf "Optional keyword arguments:\n"
    printf "Options             Type        Default         Description\n"
    echo   '-------             ----        -------         -----------'
    printf " -b|--bin           string      false           Select a binary of the model and skip its compilation.\n"
    printf " -c|--compile       bool/string true            Choose if compilation should be done normally or with a custom command.\n"
    printf "                                                The custom command must include '-o Maid'.\n"
    printf "                                                This option is omitted if you use the '-b|--bin' option.\n"
    printf " -g|--debug         bool        false           Choose if debug flag should be used for compilation.\n"
    printf " -h|--help          –           –               Shows this help.\n"
    printf " -l|--savelog       bool        false           Create logfiles of STDOUT and STDERR in the documentation directory.\n"
    printf " -o|--outputbasedir string      scenario/../    Define a custom path to save the output files.\n"
    printf " -p|--saveplot      bool        true            Save plots in a directory 'out/' parallel to the directory of your init file.\n"
    printf " sp|--showplot:     bool        false           Decide if interactive plot should open.\n"
    printf " -s|--source        string      ./maid.f90      Define a custom MAID source file. Defaults to 'src/maid.f90'.\n"
    
    printf "\n"
    printf "Note: Using '--debug true' generates the subfolder ./doc/ with all files being relevant for the latest model run.\n"

exit 1
}


# show help if no argument is passed or help is called with first argument
if [ $# -eq 0 ]; then
    show_usage
    exit 1
elif [ ${1} = "--help" ]; then
    show_usage
elif [ ${1} = "-h" ]; then
    show_usage
fi


# catch first argument defining the scenario
SCENARIOPATH=${1}
shift

# check if $SCENARIOPATH exists
if [ ! -e ${SCENARIOPATH} ]; then
    echo "ERROR: The specified scenario file doesn't exist! (${SCENARIOPATH})"
    exit
fi
    
    

# Catch all keyword arguments and update environment variables
# borrowed from https://www.golinuxcloud.com/beginners-guide-to-use-script-arguments-in-bash-with-examples/
while [ $# -gt 0 ]; do
      
    if [[ "$1" == "--bin" || "$1" == "-b" ]]; then
        BINARYPATH=${2}
        echo "BINARYPATH is set on '${BINARYPATH}'"
        [[ $COMPILE =~ ^- ]] && echo "'${BINARYPATH}' is not a proper value for -b|--bin" && show_usage
        shift
      
    elif [[ "$1" == "--compile" || "$1" == "-c" ]]; then
        COMPILE=${2}
        echo "COMPILE is set on '${COMPILE}'"
        [[ $COMPILE =~ ^- ]] && echo "'${COMPILE}' is not a proper value for -c|--compile" && show_usage
        shift
    
    elif [[ "$1" == "--debug" || "$1" == "-g" ]]; then
        DEBUG=${2}
        echo "DEBUG is set on '${DEBUG}'"
        [[ $DEBUG =~ ^- ]] && echo "'${DEBUG}' is not a proper value for -g|--debug" && show_usage
        shift
    
    elif [[ "$1" == "--help" || "$1" == "-h" ]]; then
        show_usage
    
    elif [[ "$1" == "--outputdir" || "$1" == "-o" ]]; then
        OUTPUTBASEDIR=${2}
        echo "OUTPUTBASEDIR is set on '${OUTPUTBASEDIR}'"
        [[ $OUTPUTBASEDIR =~ ^- ]] && echo "'${OUTPUTBASEDIR}' is not a proper value for -o|--outputdir" && show_usage
        shift
    
    elif [[ "$1" == "--savelog" || "$1" == "-l" ]]; then
        SAVELOG=${2}
        echo "SAVELOG is set on '${SAVELOG}'"
        [[ $SAVELOG =~ ^- ]] && echo "'${SAVELOG}' is not a proper value for -l|--savelog" && show_usage
        shift
    
    elif [[ "$1" == "--saveplot" || "$1" == "-p" ]]; then
        SAVEPLOT=${2}
        echo "SAVEPLOT is set on '${SAVEPLOT}'"
        [[ $SAVEPLOT =~ ^- ]] && echo "'${SAVEPLOT}' is not a proper value for -p|--saveplot" && show_usage
        shift
    
    elif [[ "$1" == "--showplot" || "$1" == "-sp" ]]; then
        SHOWPLOT=${2}
        echo "SHOWPLOT is set on '${SHOWPLOT}'"
        [[ $SHOWPLOT =~ ^- ]] && echo "'${SHOWPLOT}' is not a proper value for -sp|--showplot" && show_usage
        shift
        
    elif [[ "$1" == "--source" || "$1" == "-s" ]]; then
        SOURCEPATH=${2}
        echo "SOURCEPATH is set on '${SOURCEPATH}'"
        [[ $SOURCEPATH =~ ^- ]] && echo "'${SOURCEPATH}' is not a proper value for -s|--source" && show_usage
        shift
    
    else
        echo "Incorrect input provided! Showing --help."
        show_usage
    fi
    shift
done


#########################################################################################
## Setup paths and directories

# Substitute command `realpath` to run script also on macOS.
# On macOS the realpath command doesn't know optional arguments, like `--relative-to=...`,
# but with the coreutils package the command grealpath is provided, which behaves as on linux.
if [[ "$OSTYPE" == "darwin" ]]; then
    # MaxOS
    REALPATH_BIN=grealpath
elif [[ "$OSTYPE" == "linux-gnu" ]]; then
    REALPATH_BIN=realpath
else
    echo "Unrecognized operating system ${OSTYPE}."
    echo "Exiting."
    exit 1
fi

# transform relative paths to absolute paths
SCENARIOPATH=$($REALPATH_BIN ${SCENARIOPATH})
if [ $OUTPUTBASEDIR != false ]; then
    OUTPUTBASEDIR=$($REALPATH_BIN ${OUTPUTBASEDIR})
else
    # default to parent directory of scenario.init
    OUTPUTBASEDIR=$(dirname ${SCENARIOPATH%/*})
fi
if [ ${SOURCEPATH} != false ]; then
    SOURCEPATH=$($REALPATH_BIN ${SOURCEPATH})
fi
if [[ ${BINARYPATH} != false ]]; then
    if [ -e ${BINARYPATH} ]; then
        BINARYPATH=$($REALPATH_BIN ${BINARYPATH})
    else
        echo "Invalid binary specified with option '-b|--bin'! (${BINARYPATH})"
        show_usage
    fi
fi


# define all subsequent directories
DATADIR=${OUTPUTBASEDIR}/output
PLOTDIR=${OUTPUTBASEDIR}/fig
# Directory to save copies of input files and source code for documentation purposes
if [[ ${DEBUG} = true || ${SAVELOG} = true ]]; then DOCDIR=${OUTPUTBASEDIR}/doc; fi
# Temporary directory for files during compute time (deleted at the end)
COMPUTEDIR=/tmp/MAID_compute_PID$$
# Temporary path to be used during postprocessing
POSTPROCESSINGDIR=/tmp/MAID_postprocessing_PID$$

if [ ${DEBUG} = true ]; then 
    echo "SCRIPTPATH: $SCRIPTPATH"
    echo "BASEDIR: $BASEDIR"
    echo "PLOTLIBDIR: $PLOTLIBDIR"
    echo "COMPUTEDIR $COMPUTEDIR"
    echo "POSTPROCESSINGDIR: $POSTPROCESSINGDIR"
    echo "OUTPUTBASEDIR: $OUTPUTBASEDIR"
    echo "DATADIR: $DATADIR"
    echo "PLOTDIR: $PLOTDIR"
    echo "DOCDIR: $DOCDIR"
fi


# Determine TRAJECTORY from SCENARIO file
TRAJECTORYPATH=$(cat ${SCENARIOPATH} | grep '\[TRAJECTORY-FILE\]:' | awk -F: '{gsub(/ |"/,""); print $2}')

if [[ ${#TRAJECTORYPATH} > 0 ]]; then
    #     TRAJECTORYPATH=$($REALPATH_BIN ${TRAJECTORYPATH})
    
    # If given as relative path it must be relative to .init dirname
    # Translate relative .traj path to be relative to $BASEDIR
    case $TRAJECTORYPATH in
        /*)
            # absolute path, nothing to do
            ;;
        *)
            # .traj path given as relative path
            TRAJECTORYPATH=$($REALPATH_BIN --relative-to=$BASEDIR $(dirname $SCENARIOPATH)/$TRAJECTORYPATH)
            ;;
    esac
    
    if [ ! -e ${TRAJECTORYPATH} ]; then
       echo "ERROR: The .traj file specified in your .init file doesn't exist! (${TRAJECTORYPATH})"
       exit
    fi
        
    echo "TRAJECTORY is: ${TRAJECTORYPATH}"
else
    echo "No external .traj file specified."
fi


# Make directories
mkdir -p ${DATADIR}
mkdir -p ${PLOTDIR}
mkdir -p ${COMPUTEDIR}
mkdir -p ${POSTPROCESSINGDIR}
if [[ ${DEBUG} = true || ${SAVELOG} = true ]]; then mkdir -p ${DOCDIR}; fi
if [ ! -d ${BASEDIR}/bin/latest/ ]; then mkdir -p ${BASEDIR}/bin/latest/; fi

# extend directory structure if .init/.traj file is specified within subfolder structure
mkdir -p "${COMPUTEDIR}/$(dirname $($REALPATH_BIN --relative-to=${BASEDIR} ${SCENARIOPATH}))"
if [[ ${#TRAJECTORYPATH} > 0 ]]; then
    mkdir -p "${COMPUTEDIR}/$(dirname $($REALPATH_BIN --relative-to=${BASEDIR} ${TRAJECTORYPATH}))"
    mkdir -p "${POSTPROCESSINGDIR}/$(dirname $($REALPATH_BIN --relative-to=${BASEDIR} ${TRAJECTORYPATH}))"
fi


#########################################################################################
# Copy the model and the input data to the desired locations

# populate compute directory
if [ $BINARYPATH = false ]; then
    cp ${SOURCEPATH} ${COMPUTEDIR}
    if [[ ${SOURCEPATH} == *".f9"* ]]; then
        cp $(dirname ${SOURCEPATH})/constants.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/parameters.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/interpolation.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/size_distribution.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/water_properties.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/ice_properties.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/gas_properties.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/gas_kinetics.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/solution_particle.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/masstransport_gas_liquid.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/masstransport_gas_solid.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/sedimentation.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/atmosphere_dynamics.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/ice_formation_het.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/ice_formation_hom.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/ice_melting.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/lib_string.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/lib_path.f90 ${COMPUTEDIR}
        cp $(dirname ${SOURCEPATH})/lib_input.f90 ${COMPUTEDIR}
    fi
elif [ -e $BINARYPATH ]; then
    cp $BINARYPATH ${COMPUTEDIR}/$(basename ${BINARYPATH})
fi

if [[ ${#TRAJECTORYPATH} > 0 ]]; then
    cp ${TRAJECTORYPATH} ${COMPUTEDIR}/$($REALPATH_BIN --relative-to=${BASEDIR} ${TRAJECTORYPATH})
    
fi
cp ${SCENARIOPATH} ${COMPUTEDIR}/$($REALPATH_BIN --relative-to=${BASEDIR} ${SCENARIOPATH})


# populate postprocessing directory
if [[ ${#TRAJECTORYPATH} > 0 ]]; then
    cp ${TRAJECTORYPATH} ${POSTPROCESSINGDIR}/$($REALPATH_BIN --relative-to=${BASEDIR} ${TRAJECTORYPATH})
fi

if [ $SAVEPLOT = true ] || [ $SHOWPLOT = true ]; then
    cp ${PLOTLIBDIR}/plot_output_data.py ${POSTPROCESSINGDIR}
    cp ${PLOTLIBDIR}/plot_size_distributions.py ${POSTPROCESSINGDIR}
fi


# populate documentation directory
if [ ${DEBUG} = true ]; then
    cp ${SCENARIOPATH} ${DOCDIR}
    if [[ ${#TRAJECTORYPATH} > 0 ]]; then
        cp ${TRAJECTORYPATH} ${DOCDIR}
    fi
fi

if [[ ${DEBUG} = true && ${BINARYPATH} = false ]]; then
    cp ${SOURCEPATH} ${DOCDIR}
    if [[ ${SOURCEPATH} == *".f9"* ]]; then
        cp $(dirname ${SOURCEPATH})/constants.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/parameters.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/interpolation.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/size_distribution.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/water_properties.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/ice_properties.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/gas_properties.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/gas_kinetics.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/solution_particle.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/masstransport_gas_liquid.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/masstransport_gas_solid.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/sedimentation.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/atmosphere_dynamics.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/ice_formation_het.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/ice_formation_hom.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/ice_melting.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/lib_path.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/lib_string.f90 ${DOCDIR}
        cp $(dirname ${SOURCEPATH})/lib_input.f90 ${DOCDIR}
    fi
fi
    

##################################################
echo '-------------------------------------'
echo '-------------------------------------'
echo 'Welcome to the Aerosol Ice model MAID'
echo '                                     '
echo '-------------------------------------'

echo 'Using the following input files:     '
echo ">> SCENARIO: ${SCENARIOPATH}"
if [[ ${#TRAJECTORYPATH} > 0 ]]; then echo ">> TRAJECTORY: ${TRAJECTORYPATH}"; fi


##################################################
# Compile MAID or use already compiled binary
cd ${COMPUTEDIR}

if [ ${BINARYPATH} = false ]; then
    if [ "${COMPILE}" = true ]; then
        # default compile command
        if [ $(basename ${SOURCEPATH}) = 'maid.f90' ]; then
            COMPILE_COMMAND="gfortran -cpp lib_string.f90 lib_path.f90 parameters.f90 constants.f90 lib_input.f90 interpolation.f90 size_distribution.f90 ice_properties.f90 water_properties.f90 gas_properties.f90 gas_kinetics.f90 solution_particle.f90 masstransport_gas_liquid.f90 masstransport_gas_solid.f90 sedimentation.f90 atmosphere_dynamics.f90 ice_formation_het.f90 ice_formation_hom.f90 ice_melting.f90 $(basename ${SOURCEPATH}) -o Maid"
        elif [ $(basename ${SOURCEPATH}) = 'Maid.f' ]; then
            # legacy support for Maid version that came from FZJ
            COMPILE_COMMAND="gfortran -O2 -cpp $(basename ${SOURCEPATH}) -o Maid"
        fi
        
        if [ ${DEBUG} = true ]; then
            COMPILE_COMMAND="${COMPILE_COMMAND} -g -Wall -Wextra"
        fi
        
        ${COMPILE_COMMAND}
        cp Maid ${BASEDIR}/bin/latest/
        
    elif [ "${COMPILE}" != false ]; then
        # custom compile command
        ${COMPILE}
        cp Maid ${BASEDIR}/bin/latest/

    elif [ "${COMPILE}" = false ]; then
        # try to find a previously compiled binary and use it
        echo "INFO: You selected '--compile=false' and didn't specify '--bin'."
        echo "      The script will try to find a existing binary at '${BASEDIR}/bin/latest/Maid'."
        cp ${BASEDIR}/bin/latest/Maid ${COMPUTEDIR}
    fi
elif [ -e ${BINARYPATH} ]; then
    cp ${BINARYPATH} ${COMPUTEDIR}/Maid
fi


if [ ${DEBUG} = true ]; then
    cp Maid ${DOCDIR}
fi

##################################################
# Execute MAID
cd ${COMPUTEDIR}

echo '>> Starting MAID run ...'
if [[ ${SOURCEPATH} == *".f9"* ]]; then
    MAID_COMMAND="./Maid ${COMPUTEDIR}/$($REALPATH_BIN --relative-to=${BASEDIR} ${SCENARIOPATH})"
else
    # legacy support for Maid version that came from FZJ
    MAID_COMMAND="./Maid < ${COMPUTEDIR}/$($REALPATH_BIN --relative-to=${BASEDIR} ${SCENARIOPATH})"
fi

if [ ${SAVELOG} = true ]; then
    # save stdout and stderr in documentation directory
    LOG_STDOUT_PATH="$(basename ${SCENARIOPATH%.*})_stdout.log"
    LOG_STDERR_PATH="$(basename ${SCENARIOPATH%.*})_stderr.log"
    MAID_COMMAND+=" 2>${LOG_STDERR_PATH} 1>${LOG_STDOUT_PATH}"
fi

eval $MAID_COMMAND

echo ' '
echo '>> Finished Maid run.'


##################################################
# Move output to the desired location

cp ${COMPUTEDIR}/Maid.out               ${DATADIR}/$(basename ${SCENARIOPATH%.*})_data.out
mv ${COMPUTEDIR}/Maid.out               ${POSTPROCESSINGDIR}/

if [ $SAVELOG = true ]; then
    cp ${COMPUTEDIR}/*.log ${DOCDIR}
fi

if [ -f ${COMPUTEDIR}/Maid_SizeDist_Ice.out ]; then
    cp ${COMPUTEDIR}/Maid_SizeDist_Ice.out  ${DATADIR}/$(basename ${SCENARIOPATH%.*})_SizeDist_Ice.out
    mv ${COMPUTEDIR}/Maid_SizeDist_Ice.out  ${POSTPROCESSINGDIR}/
fi

if [ -f ${COMPUTEDIR}/Maid_SizeDist_Ptcl.out ]; then
    cp ${COMPUTEDIR}/Maid_SizeDist_Ptcl.out ${DATADIR}/$(basename ${SCENARIOPATH%.*})_SizeDist_Ptcl.out
    mv ${COMPUTEDIR}/Maid_SizeDist_Ptcl.out ${POSTPROCESSINGDIR}/
fi

if [ -f ${COMPUTEDIR}/Maid_SizeDist_IN.out ]; then
    cp ${COMPUTEDIR}/Maid_SizeDist_IN.out   ${DATADIR}/$(basename ${SCENARIOPATH%.*})_SizeDist_IN.out
    mv ${COMPUTEDIR}/Maid_SizeDist_IN.out   ${POSTPROCESSINGDIR}/
fi


##################################################
# Plotting
cd ${POSTPROCESSINGDIR}

if [ $SAVEPLOT = true ] || [ $SHOWPLOT = true ]; then
    
    echo '>> Starting plotting...'
    
    # default plot filenames
    if [ $SAVEPLOT = true ]; then
        PLOT_OVERVIEW_PATH="${PLOTDIR}/$(basename ${SCENARIOPATH%.*})_plot_overview.${PLOT_TYPE}"
        PLOT_SIZEDIST_PATH="${PLOTDIR}/$(basename ${SCENARIOPATH%.*})_plot_sd.${PLOT_TYPE}"
    # don't save plots
    elif [ $SAVEPLOT = false ]; then
        PLOT_OVERVIEW_PATH=""
        PLOT_SIZEDIST_PATH=""
    fi

    # create overview plot with comparison to AIDA experiment (if available)    
    python ${POSTPROCESSINGDIR}/plot_output_data.py \
    --data="${POSTPROCESSINGDIR}/Maid.out" \
    --plot=${PLOT_OVERVIEW_PATH} \
    --title=$'MAID data output:\n'"${SCENARIOPATH}"  \
    --showplot=${SHOWPLOT}
    
    # create plot of size distribution
    python ${POSTPROCESSINGDIR}/plot_size_distributions.py \
    --data_liq="${POSTPROCESSINGDIR}/Maid_SizeDist_Ptcl.out" \
    --data_ice="${POSTPROCESSINGDIR}/Maid_SizeDist_Ice.out" \
    --data_in="${POSTPROCESSINGDIR}/Maid_SizeDist_IN.out" \
    --plot=${PLOT_SIZEDIST_PATH} \
    --title=$'MAID size distribution output:\n'"${SCENARIOPATH}" \
    --showplot=${SHOWPLOT}
    
    echo ">> Finished plotting."
    
fi


# Remove temporary dirctories
cd ${BASEDIR}
rm -rf ${POSTPROCESSINGDIR}
rm -rf ${COMPUTEDIR}


echo '-------------------------------------------------------------'
echo ''
echo '      Ending '$0'. Have a nice day!'
echo ''
echo '-------------------------------------------------------------'

