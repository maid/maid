#!/bin/bash

# =======================================================================================
# Script: compare_maid_vs_aida.sh
#
# Description:
#    Compare the output of a MAID model run with an AIDA expansion.
#
#    Input arguments are described using the argument --help.
#
# ---------------------------------------------------------------------------------------
#
# Copyright notice:
#
# This file is part of MAID, "Model for Aerosol and Ice Dynamics",
# Copyright (C) 2023, Tobias Schorr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# In the projects base directory you can find a copy of the
# GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
#
# =======================================================================================


# HELP FUNCTION
function show_usage(){
    # print help string
    printf "\n=== Help function of compare_maid_vs_aida.sh ===\n"
    printf "Compare the output of a MAID model run with an AIDA expansion.\n\n"
    
    printf "Usage: $0 [--option1] [value1] [--option2] [value2]...\n"
    printf "\n"
    printf "Options:                Values:\n"
    printf "--------                -------"
    printf " -cf|--controlfile:     Path to the script running the model\n"
    printf " -i|--init:             Name of the .init/.traj file for the model\n"
    printf " -a|--aida_data:        Path to a .pkl file containing AIDA data\n"

exit 1
}


# ASSIGN INPUT ARGS TO VARIABLES
while [ $# -gt 0 ]; do
    if [[ "$1" == "--help" || "$1" == "-h" ]]; then
        show_usage
        
    elif [[ "$1" == "--controlfile" || "$1" == "-cf" ]]; then
        MODEL_CONTROL_FILE=$(realpath "$2")
        echo "MODEL_CONTROL_FILE is $MODEL_CONTROL_FILE"
        [[ $MODEL_CONTROL_FILE =~ ^- ]] && echo "$MODEL_CONTROL_FILE is not a proper value for -cf1|--controlfile1" && show_usage
        shift
        
    elif [[ "$1" == "--init" || "$1" == "-i" ]]; then
        TRAJECTORY="$2"
        echo "TRAJECTORY is $TRAJECTORY"
        [[ $TRAJECTORY =~ ^- ]] && echo "$TRAJECTORY is not a proper value for -i1|--init1" && show_usage
        shift
         
    elif [[ "$1" == "--aida_data" || "$1" == "-a" ]]; then
        AIDA_DATA="$2"
        echo "AIDA_DATA path is ${AIDA_DATA}"
        [[ ${AIDA_DATA} =~ ^- ]] && echo "${AIDA_DATA} is not a proper value for -a|--aida_data" && show_usage
        shift
    
    else
        echo "Incorrect input provided"
        show_usage
        exit 1
    fi
    shift
done


# define paths
SCRIPTPATH=$(readlink -f "$0")
BASEDIR=$(dirname $(dirname "$SCRIPTPATH"))
PLOTLIBDIR=${BASEDIR}/tools/plotting
OUTPUTBASEDIR=${BASEDIR}/scenarios/compare_maid_aida
DATADIR=${OUTPUTBASEDIR}/output

mkdir -p ${OUTPUTBASEDIR}/compare_maid_aida


# Run MAID model
${MODEL_CONTROL_FILE} ${TRAJECTORY} -o ${OUTPUTBASEDIR} -p false

# Generate coparison plot
python ${PLOTLIBDIR}/plot_timesteps_compare_maid_aida.py \
    --data ${DATADIR}/$(basename ${TRAJECTORY%.*})_data.out \
    --aida ${AIDA_DATA} \
    --outfile $OUTPUTBASEDIR/fig/compare_maid_aida_$(basename ${TRAJECTORY%.*}).png
