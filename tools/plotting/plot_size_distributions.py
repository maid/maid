#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot MAID size distribution data.


Copyright notice:

This file is part of MAID, "Model for Aerosol and Ice Dynamics",
Copyright (C) 2023, Tobias Schorr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

In the projects base directory you can find a copy of the
GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
"""

from pathlib import Path
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.cm import ScalarMappable


def plot_sd(ax, sd, norm, cmap):
    """Plot size distribution output from MAID."""
    for timestep, sd_ts in sd.groupby('ELAPSED_TIME_S'):
        ax.loglog(sd_ts['R_LOG_Q'], sd_ts['Z_LOG/dLOG_R'].replace(0, np.nan),
                  ls='-', lw=1,
                  color=cmap(norm(timestep)),
                  alpha=0.8)


def str2bool(v):
    """Transform str to bool."""
    # copied from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


# %% Main function

def main():
    """Plot size distribution from *SizeDist_*.out files."""
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-dliq", "--data_liq",
                        default="./output/Maid_SizeDist_Ptcl.out",
                        help="Path of MAID .out-file of particle size distribution.",
                        type=str)
    parser.add_argument("-dice", "--data_ice",
                        default="./output/Maid_SizeDist_Ice.out",
                        help="Path of MAID .out-file of ice size distribution.",
                        type=str)
    parser.add_argument("-din", "--data_in",
                        default="./output/Maid_SizeDist_IN.out",
                        help="Path of MAID .out-file of ice-nucleating particle size distribution.",
                        type=str)
    parser.add_argument("-p", "--plot",
                        default='',
                        help="Path of the plot to be saved.",
                        type=str)
    parser.add_argument("-sp",
                        "--showplot",
                        default='false',
                        help="Decide if a preview of the plot should be shown directly",
                        type=str)
    parser.add_argument("-t", "--title",
                        default='false',
                        help="Define custom plot title",
                        type=str)
    args = parser.parse_args()
    args.showplot = str2bool(args.showplot)
    
    cmap = plt.get_cmap('rainbow')
    column_names = ["ELAPSED_TIME_S", "K", "RAD", "Z", "R_LOG_Q", "Z_LOG", "Z_LOG/dLOG_R"]
    
    size_distributions = {'Solution particles': Path(args.data_liq),
                          'Ice-nucleating particles': Path(args.data_in),
                          'Ice particles': Path(args.data_ice)}
    
    # treat only size distributions with existing sd data files
    drop_sd_keys = list()
    for sd_name, sd_file in size_distributions.items():
        if not sd_file.exists():
            drop_sd_keys.append(sd_name)
    for sd_name in drop_sd_keys:
        del size_distributions[sd_name]
    n_subplots = len(size_distributions)
    
    if n_subplots == 0:
        print(f"Couldn't find the size distribution files. Stop {__file__}")
        exit()
    
    # Create figure
    fig, axes = plt.subplots(n_subplots, 1, sharey=True, sharex=True, figsize=[9, 10])
    if not isinstance(axes, np.ndarray):
        axes = [axes]
    
    if args.title == 'false':
        fig.suptitle(f"MAID size distribution output:\n{args.data_liq.rsplit('/', 1)[-1]},\n{args.data_ice.rsplit('/', 1)[-1]}",
                     fontsize=14, fontweight='bold')
    else:
        fig.suptitle(args.title, fontsize=14, fontweight='bold')
    
    
    # define cmap/colorbar boundaries
    sd_data = pd.read_csv(list(size_distributions.values())[0], sep=r"\s+", names=column_names, header=None, skiprows=1)
    vmin, vmax = (0, sd_data['ELAPSED_TIME_S'].unique().max())
    norm = plt.Normalize(vmin, vmax)
    
    for idx, (sd_name, sd_file) in enumerate(size_distributions.items()):
        axes[idx].set_title(sd_name)
        plot_sd(axes[idx], pd.read_csv(sd_file, sep=r"\s+", names=column_names, header=None, skiprows=1),
                norm, cmap)
    
    cbar_height = 0.05
    cax = fig.add_axes([0.1, cbar_height, 0.8, cbar_height])
    fig.colorbar(ScalarMappable(norm=norm, cmap=cmap),
                 cax=cax,
                 orientation='horizontal',
                 label="Time / (sec)")
    
    fig.subplots_adjust(bottom=cbar_height * 4)
        
    for ax in axes:
        ax.set_ylabel('dN / dlog(R)')
        ax.grid(ls='--', color='lightgrey')
        ax.set_ylim(bottom=1e-3)
    axes[-1].set_xlabel('Radius [µm]')
    
    if args.showplot:
        plt.show()
    
    if args.plot:
        plt.savefig(args.plot, dpi=200, bbox_inches='tight')


if __name__ == '__main__':
    main()
