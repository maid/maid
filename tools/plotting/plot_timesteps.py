#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot MAID <SCENARIONAME>_data.out data output files.
Limitation: MAID must be run with '[OUTPUT]: MAJOR_OUTPUT_STEP'.


Copyright notice:

This file is part of MAID, "Model for Aerosol and Ice Dynamics",
Copyright (C) 2023, Tobias Schorr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

In the projects base directory you can find a copy of the
GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
"""

import argparse
import pandas as pd
import matplotlib.pyplot as plt


# %% Utils

def load_pkl2dict(path, verbose=False):
    """Import .pkl file containing python dictionary.
    
    Parameters
    ----------
    path_of_file_pkl : :obj:`str`
        Path of file to be imported. Has to end with .pkl or .pickle
    
    Returns
    -------
    :obj:`dict`
        arbitrary content
    """
    import pickle
    
    with open(path, 'rb') as f:
        outfile = pickle.load(f)
        
        if verbose:
            print("load_pkl2dict: Imported data from", path)
            
        return outfile


def str2bool(v):
    """Transform str to bool."""
    # copied from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


# %% Plot MAID data output

def plot_maid_timesteps(axes, df, label=None):
    """Plot MAID *_data.out data output files with `[OUTPUT]=MAJOR_OUTPUT_STEP` specified."""
    if 'TIME' not in df.keys():
        raise KeyError("Expected column 'TIME' in data source.")
    
    # Temperature
    if 'TEMP' in df.keys():
        axes[0].set_ylabel('Temperature (K)')
        axes[0].plot(df['TIME'], df['TEMP'], label=label)
    
    # Pressure
    if 'PRESS' in df.keys():
        axes[1].set_ylabel('Pressure (hPa)', color='k')
        axes[1].plot(df['TIME'], df['PRESS'], label=label)
    
    # RHi
    if 'RHI' in df.keys():
        axes[2].set_ylabel('Rel. humidity w.r.t. ice (%)')
        axes[2].plot(df['TIME'], df['RHI'], label=label)
    
    # Ice water content
    if 'ICE(ppmv)' in df.keys():
        axes[3].set_ylabel('Ice water\ncontent (ppmv)')
        axes[3].plot(df['TIME'], df['ICE(ppmv)'], label=label)
       
    # Ice number concentration
    if 'NICE' in df.keys():
        axes[4].set_ylabel('Ice number\nconcentration (cm$^{-3}$)')
        axes[4].semilogy(df['TIME'], df['NICE'], label=label)
        axes[4].set_ylim(bottom=1e-3)
    
    # Ice particle radius
    if 'RICE' in df.keys():
        axes[5].set_ylabel('Ice radius (µm)')
        axes[5].plot(df['TIME'], df['RICE'], label=label)
    
    if 'hom=1,het=2' in df.keys():
        mask_het = df['hom=1,het=2'] == 2
        mask_hom = df['hom=1,het=2'] == 1
        
        for ax in axes:
            # ax.axvspan(df[mask_het]['TIME'].min(), df[mask_het]['TIME'].max(), facecolor='lightgrey', alpha=0.2)
            ax.axvspan(df[mask_hom]['TIME'].min(), df[mask_hom]['TIME'].max(), facecolor='cyan', alpha=0.2)
            
    axes[-1].set_xlabel('Time / (min)')
    
    return


# %% include AIDA data in MAID timesteps plot

def plot_aida_timesteps(axes, aida_data, label=None):
    """Plot AIDA data from .pkl file in the same format as plot_maid_timesteps()."""
    t0 = aida_data['t']['RefZeit']
        
    # Temperature
    data = aida_data['pT']['tg'].loc[t0:]
    
    axes[0].set_ylabel('Temperature (K)')
    axes[0].plot((data.index - t0).total_seconds() / 60, data, label=label)
    
    # Pressure
    data = aida_data['pT']['p'].loc[t0:]
    axes[1].set_ylabel('Pressure (hPa)', color='k')
    axes[1].plot((data.index - t0).total_seconds() / 60, data / 100)
    
    # RHi
    data = aida_data['h2o']['RHi_smooth'].loc[t0:]
    axes[2].set_ylabel('Rel. humidity w.r.t. ice ({{%}})')
    axes[2].plot((data.index - t0).total_seconds() / 60, data)
    
    # Ice number concentration
    data = aida_data['welas']['welas2']['cn'].loc[t0:]
    axes[4].set_ylabel('Ice number concentration (cm$^{-3}$)')
    axes[4].plot((data.index - t0).total_seconds() / 60, data)
    return


# %% CLI

def main():
    """Command line interface."""
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--data", default="./Maid_timesteps.out", help="Path of MAID .out-file", type=str)
    parser.add_argument("-a", "--aida", default="false", help="Path of AIDA .pkl-file", type=str)
    parser.add_argument("-p", "--plot", default="", help="Path of the plot to be saved", type=str)
    parser.add_argument("-sp", "--showplot", default='false', help="Decide if an interactive plot gets opened", type=str)
    parser.add_argument("-t", "--title", default='false', help="Define custom plot title", type=str)
    args = parser.parse_args()
    args.showplot = str2bool(args.showplot)
    
    # Create figure
    fig, axes = plt.subplots(6, 1, figsize=[10, 15], sharex=True)
    if args.title == 'false':
        fig.suptitle(f"MAID time series, \n{args.data.rsplit('/')[-1]}", fontsize=14, fontweight='bold')
    else:
        fig.suptitle(args.title, fontsize=14, fontweight='bold')
    plot_maid_timesteps(axes, pd.read_csv(args.data, sep=r"\s+"), label=f"MAID: {args.data.rsplit('/')[-1]}")
    
    if args.aida != 'false':
        aida_data = load_pkl2dict(args.aida)
        plot_aida_timesteps(axes, aida_data, label=f"AIDA: {args.aida.rsplit('/')[-1]}")
    
    axes[-1].set_xlabel('Time / minutes')
    for ax in axes:
        ax.grid(color='lightgrey', ls='--')
        
    axes[0].legend()
    
    if args.showplot:
        plt.show()
    
    if args.plot:
        fig.savefig(args.plot, dpi=200)


if __name__ == "__main__":
    main()
