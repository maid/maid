#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Compare AIDA data with MAID data outputs.
Limitation: MAID must be run with '[OUTPUT]: MAJOR_OUTPUT_STEP'.


Copyright notice:

This file is part of MAID, "Model for Aerosol and Ice Dynamics",
Copyright (C) 2023, Tobias Schorr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

In the projects base directory you can find a copy of the
GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
"""
import os
import sys
import argparse
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

sys.path.append(Path(__file__).parent.as_posix())
from plot_timesteps import load_pkl2dict
from plot_timesteps import plot_maid_timesteps
from plot_timesteps import plot_aida_timesteps


def plot_diff_maid_aida_timesteps(axes, maid, aida, mode='diff_abs', label_maid='unnamed1', label_aida='unnamed2'):
    r"""
    Plot the difference of datasets `maid` and `aida`.

    Parameters
    ----------
    axes : numpy.ndarray
        Array of axes generated by matplotlib.pyplot.
    maid : pd.DataFrame
        MAID data output imported with `pd.read_csv(path, sep=r'\s+')`.
    aida : TYPE
        AIDA data file imported with `load_pkl2dict(path/file.pkl)`.
    mode : str, optional
        Choose how to visualize the difference between `maid` and `aida` data.
        The default is 'diff_abs'.
    label_maid : str, optional
        Choose a label to be displayed in the legend for MAID data.
        DESCRIPTION. The default is 'unnamed1'.
    label_aida : TYPE, optional
        Choose a label to be displayed in the legend for AIDA data.
        DESCRIPTION. The default is 'unnamed2'.

    Returns
    -------
    NoneType
        None.

    """
    import numpy as np
    
    # different ways/"modes" to compare datasets
    if mode == 'ratio':
        def compare_ratio(data1, data2):
            return (data1 / data2).dropna()  # ratio
        comparison_fct = compare_ratio
        axes[0].set_title("Ratio (A / B)")
        
    elif mode == 'ratio_percentage':
        def compare_ratio_percentage(data1, data2):
            return ((data1 / data2).dropna() - 1) * 100  # deviation in percent
        comparison_fct = compare_ratio_percentage
        axes[0].set_title("Deviatioin in % (A / B - 1) * 100")
        
    elif mode == 'ratio_log10':
        def compare_ratio_log10(data1, data2):
            return np.log10(data1 / data2).dropna()
        comparison_fct = compare_ratio_log10
        axes[0].set_title("Compare as log$_{10}$(A / B)")
        
    elif mode == 'diff_abs':
        def compare_diff_abs(data1, data2):
            return (data1 - data2).dropna()
        comparison_fct = compare_diff_abs
        axes[0].set_title("Absolute difference (A - B)")
        
    elif mode == 'diff_rel':
        def compare_diff_relative(data1, data2):
            return ((data1 - data2) / data1 * 100).dropna()
        comparison_fct = compare_diff_relative
        axes[0].set_title("Relative difference ((A - B) / A) [%]")
        
    # use TIME as index
    maid = maid.set_index(maid['TIME'], drop=True)
    t0 = aida['experimentliste']['RefZeit']
    
    def interpolate_aida_on_maid_idx(aida_data, t0, maid_idx):
        aida_data.index = (aida_data.index - t0).total_seconds() / 60  # time index in minutes
        
        interpolated_aida = np.interp(maid_idx, aida_data.index, aida_data)
        return pd.Series(data=interpolated_aida, index=maid_idx)
    
    # Plot temperature
    axes[0].plot(comparison_fct(maid['TEMP'], interpolate_aida_on_maid_idx(aida['pT']['tg'].loc[t0:], t0, maid.index)))
    
    # Plot pressure
    axes[1].plot(comparison_fct(maid['PRESS'], interpolate_aida_on_maid_idx(aida['pT']['p'].loc[t0:], t0, maid.index) / 100))
    
    # Plot RHi
    axes[2].plot(comparison_fct(maid['RHI'], interpolate_aida_on_maid_idx(aida['h2o']['RHi_smooth'].loc[t0:], t0, maid.index)))
       
    # Plot ice number concentration
    axes[4].plot(comparison_fct(maid['NICE'], interpolate_aida_on_maid_idx(aida['welas']['welas2']['cn'].loc[t0:], t0, maid.index)))
        
    # adjust ylim symmetrically around 0
    for ax in axes:
        ax.hlines(0, 0, max([maid.index.max(), interpolate_aida_on_maid_idx(aida['pT']['tg'].loc[t0:], t0, maid.index).index.max()]), ls='--', color='k', lw=2)
        ymax = max([abs(y) for y in ax.get_ylim()])
        ax.set_ylim([- ymax, ymax])
    
    return


# %% Main function

def main():
    """Run main function via CLI."""
    # read cli arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-d1", "--data", default="./Maid_timesteps.out", help="Path of first MAID .out-file", type=str)
    parser.add_argument("-m", "--mode", default="diff_abs", help="Decide how to compare both datasets", type=str)
    parser.add_argument("-a", "--aida", default="false", help="Path of AIDA .pkl-file", type=str)
    parser.add_argument("-o", "--outfile", default="./compare_maid_aida.png", help="Directory of plot output file", type=str)
    args = parser.parse_args()
    
    label_maid = f"{args.data.rsplit('/', 1)[-1]}"
    label_aida = f"{args.aida.rsplit('/', 1)[-1]}"
    
    # generate figure
    fig, axes = plt.subplots(6, 2, figsize=[10, 15], sharex=True)
    ax_cols = axes.T
    fig.suptitle(f"Compare MAID vs AIDA\n{label_maid} vs {label_aida}", fontsize=14, fontweight='bold')
        
    if args.data:
        plot_maid_timesteps(ax_cols[0], pd.read_csv(args.data, sep=r"\s+"), label=label_maid)
    
    if args.aida:
        plot_aida_timesteps(ax_cols[0], load_pkl2dict(args.aida), label=label_aida)
    
    if args.data and args.aida:
        plot_diff_maid_aida_timesteps(ax_cols[1],
                                      pd.read_csv(args.data, sep=r"\s+"),
                                      load_pkl2dict(args.aida),
                                      mode=args.mode,
                                      label_maid=label_maid,
                                      label_aida=label_aida)
    
    for ax_col in ax_cols:
        ax_col[-1].set_xlabel('Time [minutes]')
        for ax in ax_col:
            ax.grid(color='lightgrey', ls='--')
    
    axes[0][0].legend()
    plt.show()
    
    fig.savefig(os.path.abspath(args.outfile), dpi=300)
    

if __name__ == "__main__":
    main()
