#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot MAID data output files, independent of the output format.


Copyright notice:

This file is part of MAID, "Model for Aerosol and Ice Dynamics",
Copyright (C) 2023, Tobias Schorr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

In the projects base directory you can find a copy of the
GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
"""
import argparse
from pathlib import Path

import pandas as pd
import matplotlib.pyplot as plt


def plot_output_data(axes, df, custom_title=None):
    """Plot MAID output data."""
    axes[-1].set_xlabel(df.index.name)
    
    for i, col in enumerate(df.columns):
        axes[i].set_yscale('log')
        axes[i].set_ylabel(col)
        axes[i].plot(df[col], label=col)
        
    for ax in axes:
        ax.grid(which='both', lw=0.2)
        ax.legend()
        for position in ['left', 'right', 'top']:
            ax.spines[position].set_visible(False)
    
    return None


# %% CLI

def str2bool(v):
    """Transform str to bool."""
    # copied from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main():
    """General function to plot MAID output data."""
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--data", help="Path of MAID data output file.", type=str)
    parser.add_argument("-p", "--plot", default="", help="Specify a path to save the plot.", type=str)
    parser.add_argument("-sp", "--showplot", default='false', help="Decide if an interactive plot gets opened", type=str)
    parser.add_argument("-t", "--title", default='false', help="Define custom plot title", type=str)
    args = parser.parse_args()
    args.showplot = str2bool(args.showplot)
    if args.title == 'false':
        args.title = str2bool(args.title)
    
    # load data
    df = pd.read_csv(args.data, sep=r"\s+", index_col=0)
    
    # Create figure
    plt.rcParams.update({'font.size': 8})  # reduce font size
    n_subplots = len(df.columns)
    fig, axes = plt.subplots(n_subplots, 1, sharex=True,
                           figsize=(4, 1 * n_subplots))
    
    if args.title:
        axes[0].set_title(args.title)
    else:
        axes[0].set_title("MAID data output:\n" + str(args.data))
    
    fig.tight_layout()
    fig.subplots_adjust(hspace=0)
    
    plot_output_data(axes, df, custom_title=args.title)
    
    if args.showplot:
        plt.show()
    
    if args.plot:
        fig.savefig(args.plot, dpi=200)
        
    return None

# %%

if __name__ == "__main__":
    main()
