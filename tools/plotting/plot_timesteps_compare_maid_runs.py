#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Compare two MAID model runs with one another.
Limitation: MAID must be run with '[OUTPUT]: MAJOR_OUTPUT_STEP'.


Copyright notice:

This file is part of MAID, "Model for Aerosol and Ice Dynamics",
Copyright (C) 2023, Tobias Schorr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

In the projects base directory you can find a copy of the
GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
"""
import os
import sys
import argparse
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

sys.path.append(Path(__file__).parent.as_posix())
from plot_timesteps import load_pkl2dict
from plot_timesteps import plot_maid_timesteps
from plot_timesteps import plot_aida_timesteps


def plot_diff_maid_maid_timesteps(axes, data1, data2, mode='compare_diff_relative', label1='unnamed1', label2='unnamed2'):
    r"""
    Plot the difference between two MAID runs.

    Parameters
    ----------
    axes : numpy.ndarray
        Array of axes generated by matplotlib.pyplot.
    data1 : pd.DataFrame
        First MAID data output imported with `pd.read_csv(path1, sep=r'\s+')`.
    data2 : pd.DataFrame
        Second MAID data output imported with `pd.read_csv(path2, sep=r'\s+')`.
    mode : str, optional
        Choose how to visualize the difference between `maid` and `aida` data.
        The default is 'compare_diff_relative'.
    label1 : str, optional
        Choose a label to be displayed in the legend for first MAID run.
        DESCRIPTION. The default is 'unnamed1'.
    label2 : str, optional
        Choose a label to be displayed in the legend for second MAID run.
        DESCRIPTION. The default is 'unnamed2'.

    Returns
    -------
    NoneType
        None.

    """
    import numpy as np
    
    # different ways/"modes" to compare datasets
    
    if mode == 'ratio':
        def compare_ratio(data1, data2):
            return (data1 / data2)  # ratio
        comparison_fct = compare_ratio
        axes[0].set_title("Ratio (A / B)")
        
    elif mode == 'ratio_percentage':
        def compare_ratio_percentage(data1, data2):
            return ((data1 / data2) - 1) * 100  # deviation in percent
        comparison_fct = compare_ratio_percentage
        axes[0].set_title("Deviatioin in % (A / B - 1) * 100")
        
    elif mode == 'ratio_log10':
        def compare_ratio_log10(data1, data2):
            return np.log10(data1 / data2)
        comparison_fct = compare_ratio_log10
        axes[0].set_title("Compare as log$_{10}$(A / B)")
        
    elif mode == 'diff_abs':
        def compare_diff_abs(data1, data2):
            return (data1 - data2)
        comparison_fct = compare_diff_abs
        axes[0].set_title("Absolute difference (A - B)")
        
    elif mode == 'diff_rel':
        def compare_diff_relative(data1, data2):
            return (data1 - data2) / data1
        comparison_fct = compare_diff_relative
        axes[0].set_title("Relative difference ((A - B) / A)")
        
    # use TIME as index
    data1 = data1.set_index(data1['TIME'])
    data2 = data2.set_index(data2['TIME'])
    
    # Temperature
    axes[0].set_ylabel('Temperature [K]')
    axes[0].plot(comparison_fct(data1['TEMP'], data2['TEMP']))
    
    # Pressure
    axes[1].set_ylabel('Pressure [hPa]', color='k')
    axes[1].plot(comparison_fct(data1['PRESS'], data2['PRESS']))
    
    # RHi
    axes[2].set_ylabel('Rel. humidity w.r.t. ice [%]')
    axes[2].plot(comparison_fct(data1['RHI'], data2['RHI']))
        
    # Ice water content
    axes[3].set_ylabel('Ice water content [ppmv]')
    axes[3].plot(comparison_fct(data1['ICE(ppmv)'], data2['ICE(ppmv)']))
       
    # Ice number concentration
    axes[4].set_ylabel('Ice number concentration [cm⁻³]')
    axes[4].plot(comparison_fct(data1['NICE'], data2['NICE']))
        
    # Ice particle radius
    axes[5].set_ylabel('Ice radius [µm]')
    axes[5].plot(comparison_fct(data1['RICE'], data2['RICE']))
        
    # adjust ylim symmetrically around 0
    for ax in axes:
        ax.hlines(0, 0, max([data1.index.max(), data2.index.max()]), ls='--', color='k', lw=2)
        ymax = max([abs(y) for y in ax.get_ylim()])
        ax.set_ylim([- ymax, ymax])
        
    return


# %% Main function

def main():
    """Run main function via CLI."""
    # read cli arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-d1", "--data1", default="./Maid_timesteps.out", help="Path of first MAID .out-file", type=str)
    parser.add_argument("-l1", "--label1", default="run A", help="Label for data1")
    parser.add_argument("-d2", "--data2", default="./Maid_timesteps.out", help="Path of second MAID .out-file", type=str)
    parser.add_argument("-l2", "--label2", default="run B", help="Label for data2")
    parser.add_argument("-m", "--mode", default="diff_abs", help="Decide how to compare both datasets", type=str)
    parser.add_argument("-a", "--aida", default="false", help="Path of AIDA .pkl-file", type=str)
    parser.add_argument("-o", "--outfile", default="./compare_maid_runs.png", help="Directory of plot output file", type=str)
    args = parser.parse_args()
    
    # generate figure
    fig, axes = plt.subplots(6, 2, figsize=[10, 15], sharex=True)
    ax_cols = axes.T
    fig.suptitle(f"Compare MAID runs\n({args.label1}) vs ({args.label2})", fontsize=14, fontweight='bold')
    
    if args.data1:
        plot_maid_timesteps(ax_cols[0], pd.read_csv(args.data1, sep=r"\s+"), args.label1)
        
    if args.data2:
        plot_maid_timesteps(ax_cols[0], pd.read_csv(args.data2, sep=r"\s+"), args.label2)
        
    if args.data1 and args.data2:
        plot_diff_maid_maid_timesteps(ax_cols[1], pd.read_csv(args.data1, sep=r"\s+"), pd.read_csv(args.data2, sep=r"\s+"),
                                      mode=args.mode, label1=args.label1, label2=args.label2)
    
    if args.aida != 'false':
        aida_data = load_pkl2dict(args.aida)
        plot_aida_timesteps(ax_cols[0], aida_data, label=f"AIDA: {args.aida.rsplit('/')[-1]}")
    
    for ax_col in ax_cols:
        ax_col[-1].set_xlabel('Time [minutes]')
        for ax in ax_col:
            ax.grid(color='lightgrey', ls='--')
    axes[0][0].legend()
    plt.show()
    
    fig.savefig(os.path.abspath(args.outfile), dpi=300)


if __name__ == "__main__":
    main()
