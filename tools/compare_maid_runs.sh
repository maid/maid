#!/bin/bash

# =======================================================================================
# Script: compare_maid_runs.sh
#
# Description:
#    Compare the output of two model runs performed with MAID.
#
#    Input arguments are described using the argument --help.
#
# ---------------------------------------------------------------------------------------
#
# Copyright notice:
#
# This file is part of MAID, "Model for Aerosol and Ice Dynamics",
# Copyright (C) 2023, Tobias Schorr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# In the projects base directory you can find a copy of the
# GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
#
# =======================================================================================


# HELP FUNCTION
function show_usage(){
    # print help string
    printf "\n=== Help function of compare_maid_runs.sh ===\n"
    printf "Compare the output of two model runs performed with MAID.\n\n"
    
    printf "Usage: $0 [--option1] [value1] [--option2] [value2]...\n"
    printf "\n"
    printf "Options:                  Values:\n"
    printf "  -cf1|--controlfile1:     Path to the script running the first model\n"
    printf "  -cf2|--controlfile2:     Path to the script running the second model\n"
    printf "  -i1|--init1:             Name of the .init/.traj file for first model\n"
    printf "  -i2|--init2:             Name of the .init/.traj file for second model\n\n"
    
    printf "Optional options:         Values:\n"
    printf "  -a|--aida_data:          Path to a .pkl file containing AIDA data\n"

exit 1
}


# ASSIGN INPUT ARGS TO VARIABLES
# borrowed from https://www.golinuxcloud.com/beginners-guide-to-use-script-arguments-in-bash-with-examples/
while [ $# -gt 0 ]; do
    if [[ "$1" == "--help" || "$1" == "-h" ]]; then
        show_usage
        
    elif [[ "$1" == "--controlfile1" || "$1" == "-cf1" ]]; then
        MODEL_CONTROL_FILE1=$(realpath "$2")
        echo "MODEL_CONTROL_FILE1 is $MODEL_CONTROL_FILE1"
        [[ $MODEL_CONTROL_FILE1 =~ ^- ]] && echo "$MODEL_CONTROL_FILE1 is not a proper value for -cf1|--controlfile1" && show_usage
        shift
        
    elif [[ "$1" == "--init1" || "$1" == "-i1" ]]; then
        TRAJECTORY1="$2"
        echo "TRAJECTORY1 is $TRAJECTORY1"
        [[ $TRAJECTORY1 =~ ^- ]] && echo "$TRAJECTORY1 is not a proper value for -i1|--init1" && show_usage
        shift
                
	elif [[ "$1" == "--controlfile2" || "$1" == "-cf2" ]]; then
        MODEL_CONTROL_FILE2=$(realpath "$2")
        echo "MODEL_CONTROL_FILE2 is $MODEL_CONTROL_FILE2"
        [[ $MODEL_CONTROL_FILE2 =~ ^- ]] && echo "$MODEL_CONTROL_FILE2 is not a proper value for -cf2|--controlfile2" && show_usage
        shift
    
    elif [[ "$1" == "--init2" || "$1" == "-i2" ]]; then
        TRAJECTORY2="$2"
        echo "TRAJECTORY2 is $TRAJECTORY2"
        [[ $TRAJECTORY2 =~ ^- ]] && echo "$TRAJECTORY2 is not a proper value for -i2|--init2" && show_usage
        shift
    
    elif [[ "$1" == "--aida_data" || "$1" == "-a" ]]; then
        AIDA_DATA="$2"
        echo "AIDA_DATA path is ${AIDA_DATA}"
        [[ ${AIDA_DATA} =~ ^- ]] && echo "${AIDA_DATA} is not a proper value for -a|--aida_data" && show_usage
        shift
    
    else
        echo "Incorrect input provided"
        show_usage
        exit 1
    fi
    shift
done

# ensure absolute paths
MODEL_CONTROL_FILE1=$(realpath ${MODEL_CONTROL_FILE1})
MODEL_CONTROL_FILE2=$(realpath ${MODEL_CONTROL_FILE2})
TRAJECTORY1=$(realpath ${TRAJECTORY1})
TRAJECTORY2=$(realpath ${TRAJECTORY2})

# define paths
SCRIPTPATH=$(readlink -f "$0")
BASEDIR=$(dirname $(dirname "$SCRIPTPATH"))
PLOTLIBDIR=${BASEDIR}/tools/plotting
OUTPUTBASEDIR=${BASEDIR}/scenarios/compare_maid_runs
DATADIR=${OUTPUTBASEDIR}/output

# Run MAID model 
${MODEL_CONTROL_FILE1} ${TRAJECTORY1} -o ${OUTPUTBASEDIR} -p false
${MODEL_CONTROL_FILE2} ${TRAJECTORY2} -o ${OUTPUTBASEDIR} -p false

# PLOT: compare timestep outputs
PY_PLOT_BASE='python ${PLOTLIBDIR}/plot_timesteps_compare_maid_runs.py \
--data1 "${DATADIR}/$(basename ${TRAJECTORY1%.*}_data.out)" \
--data2 "${DATADIR}/$(basename ${TRAJECTORY2%.*}_data.out)" \
--label1 "${TRAJECTORY1} @ $(basename ${MODEL_CONTROL_FILE1})" \
--label2 "${TRAJECTORY2} @ $(basename ${MODEL_CONTROL_FILE2})" \
--outfile "${OUTPUTBASEDIR}/fig/$(basename ${TRAJECTORY1%.*})_vs_$(basename ${TRAJECTORY2%.*}).png"'

# Plot AIDA data if argument is passed
if [ -z "$AIDA_DATA" ]; then
    eval "${PY_PLOT_BASE}"
else
    PY_PLOT_WITH_AIDA="$PY_PLOT_BASE --aida $(realpath ${AIDA_DATA})"
    eval "${PY_PLOT_WITH_AIDA}"
fi
