#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create initialization files and trajectory files for MAID runs.


Copyright notice:

This file is part of MAID, "Model for Aerosol and Ice Dynamics",
Copyright (C) 2023, Tobias Schorr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

In the projects base directory you can find a copy of the
GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
"""

# %% helper functions to be used by main functions


def p_sat_h2o(temp, over_surface, param='MurphyKoop2005'):
    """
    Calculate water vapor saturation pressure with respect to water or ice.

    Parameters
    ----------
    temp : int/float/list/np.array/pd.Series
        Temperature, in K.
    over_surface : str
        Define over which surface the saturation is parameterized. You can
        choose 'water' or 'ice'.
    param : str, optional
        Choose the parameterization.
        
        For over_surface = 'water':
            * HylandWexler1983 [HW1983]_
            * Sonntag1994 [Sonntag1994]_
            * Tabazadeh1997 [Tabazadeh1997]  # Add reference
            * MurphyKoop2005 [MK2005]_
            * Nachbar2019 [Nachbar2019]_
        
        for over_surface = 'ice':
            * MartiMauersberger1993 [MM1993]_
            * MurphyKoop2005 [MK2005]_
        
        The default is 'MurphyKoop2005'.

    Raises
    ------
    ValueError
        If invalid input for ´over_surface´ is entered.

    Returns
    -------
    float/np.array/pd.Series
        Water vapor saturation pressure or the specified surface (water/ice),
        in Pa.
    
    Refrences
    ---------
    .. [HW1983]
        Hyland, R. W. and A. Wexler, Formulations for the Thermodynamic
        Properties of the saturated Phases of H2O from 173.15K to 473.15K,
        ASHRAE Trans, 89(2A), 500-519, 1983.
    .. [MM1993]
        Marti, J. and K Mauersberger, A survey and new measurements of
        ice vapor pressure at temperatures between 170 and 250 K,
        GRL 20, 363-366, 1993
        https://doi.org/10.1029/93GL00105
    .. [Sonntag1994]
        Sonntag, D., Advancements in the field of hygrometry,
        Meteorol. Z., N. F., 3, 51-66, 1994.
    .. [Tabazadeh1997]
        Tabazadeh et al.;
        "The effect of particle size and nitric acid uptake on the homogeneous freezing of aqueous sulfuric acid particles";
        GRL Vol. 27, Iss. 8, p. 1111-1114, 2000
        https://doi.org/10.1029/1999gl010966
    .. [MK2005]
        Murphy, D. M. & Koop, T.;
        "Review of the vapour pressures of ice and supercooled water
        for atmospheric applications"; Quarterly Journal of the Royal
        Meteorological Society, Wiley, 2005 , 131 , 1539-1565
    .. [Nachbar2019]
        
    
    """
    import numpy as np
    import warnings
    
    # Check input temperature
    original_dtype = None  # init default value of variable
    if any([isinstance(temp, numeric_type) for numeric_type in [float, int]]):  # case for int/float inputs
        original_dtype = type(temp)
        temp = np.array([temp])
    elif isinstance(temp, list):  # case for array like inputs
        temp = np.array([temp])
        
    # Check input argument over_surface
    if not any(over_surface == np.array(['water', 'ice'])):
        raise ValueError(f"Argument `over_surface` needs to be specified as 'water' or 'ice'! The invalid input was '{over_surface}'")
        
    # Calculate water saturation pressure over water
    if over_surface == 'water':
        if param == 'HylandWexler1983':
            p_sat = np.exp(-5800.2206 / temp
                           + 1.3914993
                           - 0.048640239 * temp
                           + 0.000041764768 * temp**2
                           - 0.000000014452093 * temp**3
                           + 6.5459673 * np.log(temp))
        
        elif param == 'Sonntag1994':
            p_sat = (-6096.9385 / temp
                     + 16.635794
                     - 0.02711193 * temp
                     + 1.673952e-5 * temp**2
                     + 2.433502 * np.log(temp))
        
        elif param == 'Tabazadeh1997':
            p_sat = 100 * np.exp(((18.452406985 * temp - 3505.1578807)
                                  * temp - 330918.55082) * temp
                                 + 12725068.262 * (1 / temp**3))
        
        elif param == 'MurphyKoop2005':
            p_sat = np.exp(54.842763
                           - 6763.22 / temp
                           - 4.210 * np.log(temp)
                           + 0.000367 * temp
                           + np.tanh(0.0415 * (temp - 218.8))
                           * (53.878 - 1331.22 / temp - 9.44523
                              * np.log(temp) + 0.014025 * temp))
        
        elif param == 'Nachbar2019':
            # print warning if temperature is out of valid interval
            if any([(temp < 200) or (temp > 273.15) for temp in temp]):
                warn_msg = "Temperature exceeds valid range in wvsp_LSW_Nachbar2019()! \
                            Valid values are between valid between 200 and 273 K"
                warnings.warn(warn_msg, UserWarning)
            
            p_sat = np.exp(74.8727
                           - 7167.40548 / temp
                           - 7.77107 * np.log(temp)
                           + 0.00505 * temp)
    
    # Calculate water saturation pressure over ice
    elif over_surface == 'ice':
        
        if param == 'MartiMauersberger1993':
            p_sat = np.exp(-6132.935395 / temp + 28.867509)
        elif param == 'MurphyKoop2005':
            p_sat = np.exp(9.550426
                           - 5723.265 / temp
                           + 3.53068 * np.log(temp)
                           - 0.00728332 * temp)
        
    if original_dtype:  # convert to float if input type was float/int
        p_sat = float(p_sat)
    
    return p_sat  # in Pa


def scrape_H2SO4_eq_wt_AIM_model(temp, S, S_mode='water', verbose=False):
    """Run the Extended Aerosol Inorganic Model (E-AIM) II through its website
    to extract equilibrium composition of H2SO4/H2O solution droplets.
    
    Parameters
    ----------
    temp : int or float
        Temperature in K.
    S : int or float
        Saturation ratio.
    S_mode : str
        Saturation with respect to 'water' or 'ice'?
        The default is 'water'.
    verbose : bool, optional
        Decide if information should be printed during execution. The default is False.

    Returns
    -------
    wt_fraction : float
        Weight percentage of H2SO4 solution droplets.

    """
    import re
    import mechanize
    
    br = mechanize.Browser()
    if verbose:
        print(r"Open E-AIM Model II website to calculate wt% of H2SO4 at " + str(temp) + ' K...')
    br.open('http://www.aim.env.uea.ac.uk/aim/model2/model2a.php')
    
    br.select_form(id='mainForm')
    br.form.set_value(str(temp), 'temperature')
    br.form.set_value(str(S), 'water_var')
    if S_mode == 'ice':
        br.form.toggle_single('eql_ice')  # water_var as relative humidity w.r.t ice
    br.form.set_value(str(2), 'hydrogen')
    br.form.set_value(str(1), 'sulphate')
    
    # uncheck solit phases
    for checkbox in ['ice', 'h2so4_h2o', 'h2so4_3h2o', 'h2so4_2h2o',
                     'h2so4_3h2o', 'h2so4_4h2o', 'h2so4_4h2o', 'h2so4_65h2o']:
        br.form.toggle_single(checkbox)
    
    response = br.submit()
    output = response.read()
    br.close()
    
    regex_str = r"\w+\(aq\)\s+\d.\d+E[+-]\d\d\s+\d.\d+E[+-]\d\d"
    regex_pattern = re.compile(regex_str)
    findings = regex_pattern.findall(str(output))
    
    H2SO4_weight = 0
    for compound in findings[0:-1]:
        if verbose:
            print("Wight of compound %s: %s grams" % (compound.split(' ')[0], compound.split(' ')[-1]))
        H2SO4_weight += float(compound.split(' ')[-1])
    
    if verbose:
        print("Wight of compound %s: %s grams" % (findings[-1].split(' ')[0], findings[-1].split(' ')[-1]))
    H2O_weight = float(findings[-1].split(' ')[-1])
    
    wt_fraction = H2SO4_weight / (H2O_weight + H2SO4_weight)
    
    if verbose:
        print("Calculated weight percentage of %f." % wt_fraction)
    
    return wt_fraction


def solutes_saturation_vapor_pressures(T: float, X_HNO3: float, X_H2SO4: float,
                                       X_HCL: float, X_HBR: float) -> dict:
    """Calculate saturation vapor pressures P_SAT_SOLUTES of H2O, HNO3, HCl and HBr over solution, in Pa.
    
    Parameters
    ----------
    T : float
        Temperature in Kelvin.
    X_WT : float
        Mass fraction of solutes (X_HNO3, X_H2SO4, X_HCL, X_HBR).
    
    Literature
    ----------
    Luo et al. (1995), "Vapour pressures of H2SO4/HNO3/HCl/HBr/H2O solutions
    to low stratospheric temperatures",
    Geophysical research letters, vol. 22, no. 3, p. 247-250,
    doi:10.1029/94gl02988
    
    Notes
    -----
    Valid temperature range: 185K < T < 235K
    
    Returns
    -------
    dict
        Contains the saturation vapor pressures over solution in Pa with keys
        ['H2O', 'HNO3', 'HCL', 'HBR'].

    """
    import numpy as np
    
    A, B, A_SOLUTE, B_SOLUTE, P_SAT_SOLUTES = dict(), dict(), dict(), dict(), dict()
    
    A['HNO3'] = [22.74, 29., -0.457, -5.03, 1.447, -29.72, -13.9, 6.1]
    B['HNO3'] = [-7689.8, -2896.1, 2859.8, -274.2, -389.5, 7281., 6475., 801.]
    
    A['HCL'] = [21., 46.61, 4.069, -4.837, 2.186, -63., -40.17, -1.571]
    B['HCL'] = [-7437., -8327.8, 1300.9, 1087.2, -242.71, 18749., 18500., 5632.]
    
    A['HBR'] = [17.83, 1.02, -1.08, 3.9, 4.38, -8.87, -17., 3.73]
    B['HBR'] = [-8220.5, -362.76, 658.93, -914., -955.3, 9976.6, 19778.5, 7680.]
    
    # for P_SAT_SOLUTES['H2O'], see eq. 7
    C = [None, 23.306, -4.5261, -5.3465, 7.451, 12., -4., -8.19, -5814., 1033., 928.9, -2309., -1876.7]
    X_H = X_HNO3 + X_H2SO4 * 1.4408  # See Luo et al. (1995), below eq. 7
        
    # water vapor saturation pressure over solution surface
    P_SAT_SOLUTES['H2O'] = 100 * np.exp(C[1]
                                   + C[2] * X_HNO3
                                   + C[3] * X_H2SO4
                                   + X_H * (C[4] * X_HNO3 + C[5] * X_H2SO4)
                                   + X_H**2 * (C[6] * X_HNO3 + C[7] * X_H2SO4)
                                   + 1 / T * (C[8] + C[9] * X_HNO3 + C[10] * X_H2SO4
                                              + X_H * (C[11] * X_HNO3 + C[12] * X_H2SO4)))  # in Pa
    
    # vapor pressure of solute over solution surface
    for compound in A.keys():
        A_SOLUTE[compound] = (A[compound][0]
                              + A[compound][1] * X_HNO3
                              + A[compound][2] * X_H2SO4
                              + A[compound][3] * np.sqrt(X_HNO3)
                              + A[compound][4] * np.sqrt(X_H2SO4)
                              + A[compound][5] * X_HNO3**2
                              + A[compound][6] * X_HNO3 * X_H2SO4
                              + A[compound][7] * X_H2SO4**2)

        B_SOLUTE[compound] = (B[compound][0]
                              + B[compound][1] * X_HNO3
                              + B[compound][2] * X_H2SO4
                              + B[compound][3] * np.sqrt(X_HNO3)
                              + B[compound][4] * np.sqrt(X_H2SO4)
                              + B[compound][5] * X_HNO3**2
                              + B[compound][6] * X_HNO3 * X_H2SO4
                              + B[compound][7] * X_H2SO4**2)
                             
        P_SAT_SOLUTES[compound] = 100 * np.exp(A_SOLUTE[compound]
                                          + B_SOLUTE[compound] / T) * X_HNO3 * (X_HNO3 + 0.09 * X_H2SO4)  # in Pa
                             
    return P_SAT_SOLUTES


def fit_h2so4_wt_to_water_vapor_pressure(temp: float, p_h2o_ambient: float) -> float:
    """Get equilibrium mass fraction of H2SO4 to match water vapor pressure with ambient conditions."""
    from scipy.optimize import minimize
    
    def diff_pw_ambient_vs_p_H2O_solution(x_h2so4, temp, p_h2o_ambient):
        p_h2o_solution = solutes_saturation_vapor_pressures(temp, X_HNO3=0, X_H2SO4=x_h2so4, X_HCL=0, X_HBR=0)["H2O"]
        return abs(p_h2o_ambient - p_h2o_solution)
    
    res = minimize(diff_pw_ambient_vs_p_H2O_solution, [0.35], args=(temp, p_h2o_ambient),
                   bounds=[(0.0, 0.9)])
    
    return res.x[0]


# %% create .init file

def write_maid_init(init_path,
                    traj_type,
                    OUTPUT='MAJOR_OUTPUT_STEP',
                    OUTPUT_SD_LIQ='YES',
                    OUTPUT_SD_IN='YES',
                    OUTPUT_SD_ICE='YES',
                    TIME_RESOLUTION_MODEL=1,
                    TIME_RESOLUTION_OUTPUT=10,
                    FREEZING='HETEROGEN',
                    TIME_UNIT_TRAJ='SEKUNDEN',
                    PRESSURE_UNIT='MBAR',
                    PARTICLE='yes',
                    NUMBER_COMPONENTS=3,
                    NUMBER_BINS=60,
                    SD_RADIUS_MIN=0.01,
                    SD_RADIUS_MAX=25,
                    SD_RADIUS_UNIT='MYM',
                    AEROSOL_CONCENTRATION=0,
                    AEROSOL_CONCENTRATION_UNIT='P/CM**3',
                    AEROSOL_MEAN_RADIUS=0.25,  # default oriented at Martinas presentation at 2020-07-08
                    AEROSOL_MEAN_RADIUS_UNIT='MYM',
                    AEROSOL_SIGMA=1.5,  # default oriented at Martinas presentation at 2020-07-08
                    AEROSOL_AMOUNT_H2SO4=-1,
                    AEROSOL_COMPOSITION=[0, 1, 0],
                    IN_TYPE=110,  # for immersion freezing acc. to Kärcher & Lohmann (2003)
                    IN_CONCENTRATION=0,
                    IN_CONCENTRATION_UNIT='P/CM**3',
                    IN_MEAN_RADIUS=1,
                    IN_MEAN_RADIUS_UNIT='MYM',
                    IN_SIGMA=1.1,
                    IN_DENSITY=1,
                    ICE_CONCENTRATION=0,
                    ICE_CONCENTRATION_UNIT='P/CM**3',
                    ICE_MEAN_RADIUS=1,
                    ICE_MEAN_RADIUS_UNIT='MYM',
                    ICE_SIGMA=1.5,
                    ICE_AMOUNT_H2SO4=1.0,
                    ICE_COMPOSITION=[0, 1, 0],
                    COMP=-2,
                    COMP_UNIT='ppb',
                    H2O=0,
                    H2O_CONST=False,
                    H2O_UNIT='ppm',
                    HNO3=0,
                    HNO3_CONST=False,
                    HNO3_UNIT='ppb',
                    HCL=0,
                    HCL_CONST=False,
                    HCL_UNIT='ppb',
                    HBR=0,
                    HBR_CONST=False,
                    HBR_UNIT='ppb',
                    SEDIMENTATION='yes',
                    SEDFACTOR=0.57,
                    DZ=50,
                    T_START=None,
                    P_START=None,
                    UPDRAFT_VELOCITY=None,
                    T_END=None,
                    P_END=None,
                    DURATION=None,
                    RUNTIME_EXTENSION=0,
                    PAR_PSAT_WATER=3,
                    PAR_PSAT_ICE=2,
                    PAR_WATER_ACTIVITY=1,
                    PAR_HET_FREEZE='KL2003',
                    TRAJECTORY_FORMAT=[0, 3, 1, 2, 3, False],
                    TRAJECTORY_FILE='Maid_trajectory.dat',
                    ASSESSMENTS=[-1, -1, -1, -1],
                    USE_GRAVITY_WAVE_FORCING=False,
                    SIGMA_DELTA_W=15.0,
                    LATITUDE_ANGLE=None,
                    GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC=None,
                    SEED=None,
                    verbose=False):
    """Create MAID initialization file.
    
    Parameters
    ----------
    init_path : str
        Define path of *.init file to be generated.
    traj_type : str
        Define type of trajectory. Choose from ['external', 'internal_by_condition', 'internal_by_duration'].
        
        +-----------------------+-------------------------------------------------------------------------------+
        | traj_type value       | meaning                                                                       |
        +=======================+===============================================================================+
        | external              | Use a .traj file. The keyword TRAJECTORY_FILE must be set.                    |
        +-----------------------+-------------------------------------------------------------------------------+
        | internal_by_duration  | Calculate the trajectory model-internal. Ends after DURATION.                 |
        +-----------------------+-------------------------------------------------------------------------------+
        | internal_by_condition | Calculate the trajectory model-internal. Ends oce P-END ot T-END is reached.  |
        +-----------------------+-------------------------------------------------------------------------------+
        
    OUTPUT : str, optional
        Define MAID output structure.
        Choose between 'ZEIT_DRUCK_TABS', 'ANZAHL_PART', 'KRAEMER', 'MAJOR_OUTPUT', 'EFFIZIENZ', 'MAJOR_OUTPUT_STEP'.
        The default is 'MAJOR_OUTPUT_STEP'.
    OUTPUT_SD_LIQ : str, optional
        Chose if an output file of the aqueous aerosol size distribution will be created.
        The default is 'YES'.
    OUTPUT_SD_IN : str, optional
        Chose if an output file of the ice-nucleating particles size distribution will be created.
        The default is 'YES'.
    OUTPUT_SD_ICE : str, optional
        Chose if an output file of the ice crystal size distribution will be created.
        The default is 'YES'.
    TIME_RESOLUTION_MODEL : numeric, optional
        Time resolution of the model-intrinsic time loop in seconds.
        The default is 1.
    TIME_RESOLUTION_OUTPUT : numeric, optional
        Time resolution of the output data in seconds.
        The default is 10.
    FREEZING : str, optional
        Define expected freezing behaviour.
        Choose between 'HETEROGEN', 'HOMOGEN'.
        The default is 'HETEROGEN'.
        
        
    TIME_UNIT_TRAJ : str, optional
        Define time step scale.
        Choose from 'SEKUNDEN', 'MINUTEN', 'STUNDEN'.
        The default is 'SEKUNDEN'.
    PRESSURE_UNIT : str, optional
        Unit for pressure.
        Choose from ['BAR', 'MBAR', 'PA', 'TORR', 'PPB', 'PPM'].
        The default is 'MBAR'.
        
        
    PARTICLE : str, optional
        Flag if particles are present.
        Choose from ['yes', 'ja', 'no', 'nein'].
        The default is 'yes'.
    NUMBER_COMPONENTS : int, optional
        Define the number of soluable components (including water).
        The default is 3 (H2O + H2SO4 + HNO3).
    
    
    NUMBER_BINS : int, optional
        Define the number of bins in lognormal size distributions.
        The default is 60.
    SD_RADIUS_MIN : float, optional
        The radius of the smallest initial bin in the lognormal size distributions.
        The default is 0.01.
    SD_RADIUS_MAX : float, optional
        The radius of the biggest initial bin in the lognormal size distributions.
        The default is 25.
    SD_RADIUS_UNIT : str, optional
        Unit of the initial size distribution boundaries SD_RADIUS_MIN and SD_RADIUS_MAX.
        Choose from ['NM', 'MYM', 'CM', 'M'].
        The default is 'MYM'.
    EQUILIBRIUM : str, optional
        Decide for equilibrium calculations of kinetic.
        
        
    AEROSOL_CONCENTRATION : float, optional
        Number concentration of homogeneously freezing particles/droplets
        (e.g. H2SO4/H2O solution) in cm⁻³.
        The default is 0.
    AEROSOL_CONCENTRATION_UNIT : str, optional
        Unit of `AEROSOL_CONCENTRATION`.
        Choose from ['PTOT', 'P/CM**3'].
        The default is 'P/CM**3' (number concentration).
    AEROSOL_MEAN_RADIUS : float, optional
        Mean radius for lognormal size distribution of solution droplets in µm.
        The default is 0.25.
    AEROSOL_MEAN_RADIUS_UNIT : str, optional
        Unit of `AEROSOL_MEAN_RADIUS`.
        Choose from ['NM', 'MYM', 'CM', 'M'].
        The default is 'MYM'.
    AEROSOL_SIGMA : float, optional
        Geometric standard deviation for lognormal particle size distribution of solution droplets.
        The default is 1.5.
    AEROSOL_AMOUNT_H2SO4 : float, optional
        Weight percentage of sulfuric acid in aquaeous aerosol.
        For negative values the fraction will be read from keyword `AEROSOL_COMPOSITION`.
        The default is -1.
    AEROSOL_COMPOSITION : list of floats, optional
        Specify composition of aquaeous aerosol if `AEROSOL_AMOUNT_H2SO4` has a negative value.
        List contains the weight fraction for [H2SO4, H2O, HNO3].
        Note: Weight fraction, not weight percentage!
        The default is [0, 1, 0].
    
    IN_TYPE : str/int/float, optional
        Define INP freezing for immersion freezing according to Kärcher & Lohmann (2003).
        Choose from ['COATED_SOOT', 'SOOT', 'AMMONIUMSULFAT', 'MINERAL_DUST', 'SOOT_ACTIVATED']
        or enter a numeric value as a relative humidity (w.r.t. ice) freezing threshold.
        The default is 110.
    IN_CONCENTRATION : float, optional
        Number concentration of INPs in cm⁻³.
        The default is None.
    IN_CONCENTRATION_UNIT : str, optional
        Unit of `IN_CONCENTRATION`.
        Choose from ['PTOT', 'P/CM**3'].
        The default is 'P/CM**3' (number concentration).
    IN_MEAN_RADIUS : float, optional
        Mean radius for lognormal size distribution of ice-nucleating particles.
        The default is 0.25.
    IN_MEAN_RADIUS_UNIT : str, optional
        Unit of `IN_MEAN_RADIUS`.
        Choose from ['NM', 'MYM', 'CM', 'M'].
        The default is 'MYM'.
    IN_SIGMA : float, optional
        Geometric standard deviation for lognormal particle size distribution of ice-nucleating particles.
        The default is 1.1.
    IN_DENSITY : float, optional
        Density of the given INP type in g/cm³.
        The default is None.
    
    ICE_CONCENTRATION : float, optional
        Number concentration of preexisting ice in cm⁻³.
        The default is 0.
    ICE_CONCENTRATION_UNIT : str, optional
        Unit of `ICE_CONCENTRATION_UNIT`.
        Choose from ['PTOT', 'P/CM**3'].
        The default is 'P/CM**3' (number concentration).
    ICE_MEAN_RADIUS : float, optional
        Mean radius for lognormal size distribution of preexisting ice in µm.
        The default is 0.05.
    ICE_MEAN_RADIUS_UNIT : str, optional
        Unit of `ICE_MEAN_RADIUS`.
        Choose from ['NM', 'MYM', 'CM', 'M'].
        The default is 'MYM'.
    ICE_SIGMA : float, optional
        Geometric standard deviation for lognormal particle size distribution of pre-existing ice.
        The default is 1.5.
    ICE_AMOUNT_H2SO4 : float, optional
        Weight percentage of sulfuric acid in pre-existing ice.
        For negative values the fraction will be read from keyword `ICE_COMPOSITION`.
        The default is 1.0.
    ICE_COMPOSITION : list of floats, optional
        Specify composition of pre-existing ice if `ICE_AMOUNT_H2SO4` has a negative value.
        List contains the weight fraction for [H2SO4, H2O, HNO3].
        Note: Weight fraction, not weight percentage!
        The default is [0, 1, 0].
        
        
    COMP : int, optional
        Specifies the number of trace gas components.
        A Negative value means to read values.
        
        +------+---------+-------+-----+------+-----+-----+
        | COMP | N_PCOND | H2SO4 | H2O | HNO3 | HCL | HBR |
        +======+=========+=======+=====+======+=====+=====+
        | -1   | 2       |   x   |  x  |      |     |     |
        +------+---------+-------+-----+------+-----+-----+
        | -2   | 3       |   x   |  x  |  x   |     |     |
        +------+---------+-------+-----+------+-----+-----+
        | -3   | 4       |   x   |  x  |  x   |  x  |     |
        +------+---------+-------+-----+------+-----+-----+
        | -4   | 5       |   x   |  x  |  x   |  x  |  x  |
        +------+---------+-------+-----+------+-----+-----+
        Note: N_PCOND is the model-internal variable which defines the number of
        present trace gas components.
    COMP_UNIT : str, optional
        Choose the unit for the vapor pressure of condensable components.
        Choose from ['PA', 'BAR', 'MBAR', 'TORR', 'PPM', 'PPB']
        The default is 'PPB'.
        
    H2O : float, optional
        Water vapor content at experiment start.
        The default is 0.
    H2O_CONST : bool, optional
        Decide if the water vapor pressure is constant.
        The default is False.
    H2O_UNIT : str, optional
        Unit of `H2O`.
        Choose from ['PA', 'BAR', 'MBAR', 'TORR', 'PPM', 'PPB']
        The default is 'PPM'.
        
    HNO3 : float, optional
        Nitric acid vapor content at experiment start.
        The default is 0.
    HNO3_CONST : bool, optional
        Decide if the nitric acid vapor pressure is constant.
        The default is False.
    HNO3_UNIT : str, optional
        Unit of `HNO3`.
        Choose from ['PA', 'BAR', 'MBAR', 'TORR', 'PPM', 'PPB']
        The default is 'PPB'.
        
    HCL : float, optional
        Hydrochloric acid vapor content at experiment start.
        The default is 0.
    HCL_CONST : bool, optional
        Decide if the hydrochloric acid vapor pressure is constant.
        The default is False.
    HCL_UNIT : str, optional
        Unit of `HCL`.
        Choose from ['PA', 'BAR', 'MBAR', 'TORR', 'PPM', 'PPB']
        The default is 'PPB'.
        
    HBR : float, optional
        Hydrobromic acid vapor content at experiment start.
        The default is 0.
    HBR_CONST : bool, optional
        Decide if the hydrobromic acid vapor pressure is constant.
        The default is False.
    HBR_UNIT : str, optional
        Unit of `HBR`.
        Choose from ['PA', 'BAR', 'MBAR', 'TORR', 'PPM', 'PPB']
        The default is 'PPB'.
        
        
    SEDIMENTATION : str, optional
        Switch for sedimentation.
        Choose 'ja'/'yes' or something else for 'no'.
        The default is 'yes'.
    SEDFACTOR : float, optional
        Flux through top / flux through bottom of the air parcel.
        
        +-----------+---------------------------------------------------------+
        | SEDFACTOR | Interpretation                                          |
        +===========+=========================================================+
        | 0         | No sedimentation input in the box (top of cirrus cloud).|
        +-----------+---------------------------------------------------------+
        | 0 < x 0.6 | Regions with continuous and strong nucleation.          |
        +-----------+---------------------------------------------------------+
        | 1         | Net zero sedimentation.                                 |
        +-----------+---------------------------------------------------------+
        The default is 0.57.
    DZ : int/float, optional
        Height of the air parcel in m. The default is 50.
        
        
    T_START : float, optional
        Start temperature in K .
        Must be defined if `traj_type` is 'internal_by_condition' or 'internal_by_duration'.
        The default is None.
    P_START : float, optional
        Start pressure in Pa.
        Must be defined if `traj_type` is 'internal_by_condition' or 'internal_by_duration'.
        A negative values is the flag for to read the pressure from a trajectory file.
        The default is None.
    UPDRAFT_VELOCITY : float, optional
        Updraft velocity in cm/s.
        Must be defined if `traj_type` is 'internal_by_condition' or 'internal_by_duration'.
        The default is None.
    T_END : float, optional
        The final air temperature in K.
        Only used when `traj_type` is 'internal_by_condition'.
        Optional for `traj_type='internal_by_condition'` if P_END is also defined.
        The default is None.
    P_END : float, optional
        The final pressure in Pa.
        Only used when `traj_type` is 'internal_by_condition'.
        Optional for `traj_type='internal_by_condition'` if T_END is also defined.
        The default is None.
    DURATION : int, optional
        Duration of the scenario in seconds.
        Only used when `traj_type` is 'internal_by_duration'.
        The default is None.
    RUNTIME_EXTENSION : int/float, optional
        Additional model runtime, in seconds, after the trajectory reached its end.
        During this period the pressure is kept constant to give the particles 
        more time reaching equilibrium in their current environment.
        The default is 0.
        
        
    PAR_PSAT_WATER : int, optional
        Choose the parameterization for the water vapor saturation pressure over water.
        
        +----------------+-------------------------------------------------------------------------------------------------------------------------------+
        | PAR_PSAT_WATER | Literature                                                                                                                    |
        +================+===============================================================================================================================+
        | 1              | Tabazadeh et al. (2000), https://doi.org/10.1029/1999gl010966                                                                 |
        +----------------+-------------------------------------------------------------------------------------------------------------------------------+
        | 2              | Murphy & Koop (2005), https://doi.org/10.1256/qj.04.94                                                                        |
        +----------------+-------------------------------------------------------------------------------------------------------------------------------+
        | 3 (default)    | Nachbar et al. (2021), https://doi.org/10.1063/1.5100364                                                                      |
        +----------------+-------------------------------------------------------------------------------------------------------------------------------+
        | 4              | Sonntag (1994), https://doi.org/10.1127/metz/3/1994/51                                                                        |
        +----------------+-------------------------------------------------------------------------------------------------------------------------------+
        | 5              | Schneider et al. (2021), https://doi.org/10.5194/acp-21-14403-2021                                                            |
        +----------------+-------------------------------------------------------------------------------------------------------------------------------+
        | 6              | Hyland & Wexler (1983), "Formulation for the thermodynamic properties of the saturated phases of H2O from 173.15K to 473.15K" |
        +----------------+-------------------------------------------------------------------------------------------------------------------------------+
        The default is 3 (Nachbar et al.).
    PAR_PSAT_ICE : int, optional
        Choose the parameterization for the water vapor saturation pressure over ice.
        
        +--------------+------------------------------------------------------------------+
        | PAR_PSAT_ICE | Literature                                                       |
        +==============+==================================================================+
        | 1            | Marti and Mauersberger (1995), https://doi.org/10.1029/93gl00105 |
        +--------------+------------------------------------------------------------------+
        | 2 (default)  | Murphy & Koop (2005), https://doi.org/10.1256/qj.04.94           |
        +--------------+------------------------------------------------------------------+
        1: Marti and Mauersberger; 2: Murphy and Koop.
        The default is 2 (Murphy and Koop)
    PAR_WATER_ACTIVITY : int, optional
        Choose the parameterization for water activity.
        
        +--------------------+----------------------------------------------------------+
        | PAR_WATER_ACTIVITY | Literature                                               |
        +====================+==========================================================+
        | 1 (default)        | Carslaw et al. (1995), https://doi.org/10.1029/95gl01668 |
        +--------------------+----------------------------------------------------------+
        | 2                  | Shi et al. (2001), https://doi.org/10.1029/2000JD000181  |
        +--------------------+----------------------------------------------------------+
        The default is Carslaw (1995).
    PAR_HET_FREEZE : str, optional
        Choose the parameterization for heterogeneous freezing.
        
        +----------------------------+------------------------------------------------------------------+
        | PAR_HET_FREEZE             | Literature                                                       |
        +============================+==================================================================+
        | KL2003                     | Immersion freezing; Uses `IN_TYPE` for aerosol specification.    |
        |                            | Kärcher & Lohmann (2003), https://doi.org/10.1029/2002jd003220   |
        +----------------------------+------------------------------------------------------------------+
        | dust_Ullrich2017(_updated) | INAS density appraoch for dust; only implemented in depos. freez.|
        |                            | Ullrich et al. (2017), https://doi.org/10.1175/jas-d-16-0074.1   |
        +----------------------------+------------------------------------------------------------------+
        | soot_Ullrich2017           | INAS density appraoch for soot; only implemented in depos. freez.|
        |                            | Ullrich et al. (2017), https://doi.org/10.1175/jas-d-16-0074.1   |
        +----------------------------+------------------------------------------------------------------+
        | atd_steinke2015            | INAS density appraoch for ATD; only implemented in depos. freez. |
        |                            | Steinke et al. (2015), https://doi.org/10.5194/acp-15-3703-2015  |
        +----------------------------+------------------------------------------------------------------+
        | QUARTZ_SCHORR              | INAS density appraoch for quartz; only implemented in depos.     |
        |                            | freez.; preliminary parameterization based on PhD work!          |
        +----------------------------+------------------------------------------------------------------+
        | FUMED_SILICA_SCHORR        | INAS density appraoch for fumed silica; only implemented in      |
        |                            | depos. freez.; preliminary parameterization based on PhD work!   |
        +----------------------------+------------------------------------------------------------------+
        | CALCIUM_CARBONATE_SCHORR   | INAS density appraoch for quartz; only implemented in depos.     |
        |                            | freez.; preliminary parameterization based on PhD work!          |
        +----------------------------+------------------------------------------------------------------+
        The default is 'KL2003'.
    TRAJECTORY_FILE : str, optional
        Filename of corresponding .traj file.
        Must be defined if traj_type == 'external'.
        The default is 'Maid_trajectory.dat'.
    TRAJECTORY_FORMAT : list, optional
        List of keys to specify the trajectory file structure .
        Must be defined if traj_type == 'external'.
        
        +----------+---------+---------------------------------------------+
        | List idx | default | meaning                                     |
        +==========+=========+=============================================+
        | 0        | 0       | Number of initial comment lines.            |
        +----------+---------+---------------------------------------------+
        | 1        | 3       | Total number of columns.                    |
        +----------+---------+---------------------------------------------+
        | 2        | 1       | Column number of pressure.                  |
        +----------+---------+---------------------------------------------+
        | 3        | 2       | Column of water vapor source (wall flux)    |
        +----------+---------+---------------------------------------------+
        | 4        | 3       | Column of temperature.                      |
        +----------+---------+---------------------------------------------+
        | 5        | False   | Force temperature to adiabatically follow   |
        |          |         | pressure trajectory (no latent heat effect).|
        +----------+---------+---------------------------------------------+
        The default is [0, 3, 1, 2, 3, False].
    ASSESSMENTS : list, optional
        If concentrations of condensable compounds (H2O, HNO3, HCl, HBr) are given
        in the .traj file you have to specify their column indices here.
        The lengths of this list must match the number of condensable comounds.
        Negative values indicate, that the compound is not read from the .traj file,
        whereas `-1` means 'atmospheric calculation'.
        The column of water vapor wall flux must be flagged with `-2`.
        The default is [-1, -1, -1, -1]
    
    USE_GRAVITY_WAVE_FORCING : bool, optional
        Switch to activate gravity wave driven fluctuations in vertical wind speed,
        which forces temperature fluctuations.
        Implementation is based on Kärcher & Podglajen, JGR Atmos., 2019, doi: 10.1029/2019jd030680.
        The default is False.
    SIGMA_DELTA_W : float, optional
        The standard deviation of vertical wind speed fluctuations in cm/s.
        
        Related literature:
            * Podglajen et al., GRL, 2016, doi: 10.1002/2016gl068148 
            * Barahona et al., Scientific Reports, 2017, doi: 10.1038/s41598-017-07038-6
            * Kärcher & Podglajen, JGR Atmos., 2019, doi: 10.1029/2019jd030680
            
        The default is 15.0 cm/s.
    LATITUDE_ANGLE : float, optional.
        Latitude angle. Used to derive coriolis frequency, which contributes to
        a daming factor of temperature fluctuations.
        A finite value has to be assigned when USE_GRAVITY_WAVE_FORCING is True.
        The default is None.
    GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC : float, optional
        Autocorrelation time in seconds, which specifies the time interval in which
        the updraft fluctuation changes.
        The default is None.
    SEED : list, optional
        Seed for the pseudo-random number generator.
        Random numbers are used for the updraft fluctuations if USE_GRAVITY_WAVE_FORCING is True.
        SEED must be a list of 8 integers, which have to be in the interval (-2147483648, 2147483648). This limitation is due to their representation as signed 32-bit integer variables in Fortran.
        
    verbose : bool, optional
        Decide if information should be printed during execution.
        The default is False.

    Returns
    -------
    None.
    Creates a file `init_path` to be used for initialization of the MAID model.

    """
    if not IN_CONCENTRATION:
        IN_CONCENTRATION = 0.0
    if not IN_MEAN_RADIUS:
        IN_MEAN_RADIUS = 0.0
    if not IN_SIGMA:
        IN_SIGMA = 1.0
    if not IN_DENSITY:
        IN_DENSITY = 0.0
        
    if USE_GRAVITY_WAVE_FORCING and not LATITUDE_ANGLE:
        raise ValueError("You have to specify LATITUDE_ANGLE when using gravity wave forcing.")
    
    def format_section_headline(headline: str, level: int) -> str:
        total_length = 75  # character limit of a line
        
        if level == 0:
            line_character = '#'
        elif level == 1:
            line_character = '='
        elif level == 2:
            line_character = '-'
            
        formatted_line = '# ' + line_character * 12 + ' ' + headline + ' ' + line_character * (total_length - len(headline)) + '\n'
        return formatted_line
    
    with open(init_path, 'w+') as f:
        
        # GENERNAL STATEMENTS
        f.write(format_section_headline('GENERNAL STATEMENTS', level=1))
        f.write("[OUTPUT]: %s (ZEIT_DRUCK_TABS, ANZAHL_PART, KRAEMER, MAJOR_OUTPUT, EFFIZIENZ, MAJOR_OUTPUT_STEP)\n" % OUTPUT)
        f.write("[OUTPUT-SD-LIQ]: %s (YES or NO)\n" % OUTPUT_SD_LIQ)
        f.write("[OUTPUT-SD-IN]: %s (YES or NO)\n" % OUTPUT_SD_IN)
        f.write("[OUTPUT-SD-ICE]: %s (YES or NO)\n" % OUTPUT_SD_ICE)
        f.write("[TIME-RESOLUTION]: %s (in seconds, model time step)\n" % TIME_RESOLUTION_MODEL)
        f.write("[OUTPUT-TIME-RESOLUTION]: %s (in seconds, output time step)\n" % TIME_RESOLUTION_OUTPUT)
        f.write("[PARTICLE]: %s\n" % PARTICLE)
        f.write("[NUMBER-BINS]: %i\n" % NUMBER_BINS)
        f.write("[RADIUS]:  %2.6f %2.6f %s (RMIN, RMAX, Unit: NM, MYM, CM, M)\n" % (SD_RADIUS_MIN, SD_RADIUS_MAX, SD_RADIUS_UNIT))
        f.write("[FREEZING]: %s (HETEROGEN, HOMOGEN)\n" % FREEZING)
        f.write("[EQUILIBRIUM]: NO  (JA, YES, EQUI, GLEI, NO)\n")
        if SEED:
            f.write("[SEED]: %s\n" % ' '.join(map(str, SEED)))
        f.write("\n")
        
        # Condensable Components
        f.write(format_section_headline('Condensable Components', level=1))
        f.write("[COMP]: %i %s (neg. values= read data, # condensable components (2= H2O, HNO3))\n" % (COMP, COMP_UNIT))
        f.write("[H2O]: %.6f .%s. %s (Concentration, True / False value constant , Unit, H2O, HNO3, HCL, HBR)\n" % (H2O, H2O_CONST, H2O_UNIT))
        f.write("[HNO3]: %.6f .%s. %s\n" % (HNO3, HNO3_CONST, HNO3_UNIT))
        f.write("[HCL]: %.6f .%s. %s\n" % (HCL, HCL_CONST, HCL_UNIT))
        f.write("[HBR]: %.6f .%s. %s\n" % (HBR, HBR_CONST, HBR_UNIT))
        f.write("\n")
        
        # TRAJECTORY
        f.write(format_section_headline('TRAJECTORY', level=1))
        f.write(f"# Trajectory type is: {traj_type}\n")
        f.write("\n")
        
        if traj_type.lower() == 'external':
            if (not TRAJECTORY_FILE
                or not TRAJECTORY_FORMAT
                or not ASSESSMENTS
                or not TIME_UNIT_TRAJ
                or not PRESSURE_UNIT):
                raise ValueError(f"Not all variables defined, which are necessary for '{traj_type}'.")
            assert(P_START < 0)  # this works as a flag in MAID to read a .traj file
            
            f.write("[P-ANFANG]: %.2f\n" % P_START)
            f.write("[TIME-UNIT]: %s (SEKUNDEN, MINUTEN, STUNDEN)\n" % TIME_UNIT_TRAJ)
            f.write("[PRESSURE-UNIT]: %s (BAR, MBAR, PA, TORR, PPB, PPM)\n" % PRESSURE_UNIT)
            
            f.write('[TRAJECTORY-FILE]: "%s"\n' % TRAJECTORY_FILE)
            f.write("[TRAJECTORY-FORMAT]: %i %i %i %i %i .%s. (Comentline, #colums, colum. pressure, colum. wflux, colum temp., T/P forced)\n"
                    % (TRAJECTORY_FORMAT[0], TRAJECTORY_FORMAT[1], TRAJECTORY_FORMAT[2], TRAJECTORY_FORMAT[3], TRAJECTORY_FORMAT[4], TRAJECTORY_FORMAT[5]))
            f.write("[ASSESSMENTS]: %s \n\n" % (' '.join(map(str, ASSESSMENTS))))
        
        elif 'internal_' in traj_type.lower():
            if (not T_START
                or not P_START
                or not UPDRAFT_VELOCITY):
                raise ValueError(
                    "Not all variables defined, which are necessary "
                    "for internal trajectories. You have to define \n"
                    "T_START, P_START, UPDRAFT_VELOCITY "
                    "and either DURATION or P_END.")
                
            f.write("[T-ANFANG]: %.2f (in K)\n" % T_START)
            f.write("[P-ANFANG]: %.2f (in Pa)\n" % P_START)
            f.write("[UPDRAFT-VELOCITY]: %s (in cm sec-1)\n" % UPDRAFT_VELOCITY)
            f.write("[RUNTIME_EXTENSION]: %s (in seconds)\n" % RUNTIME_EXTENSION)
                        
            if 'internal_by_duration' in  traj_type.lower():
                assert(isinstance(DURATION, int))
                assert(DURATION > 0)
                f.write("[DURATION]: %i (in sec)\n" % DURATION)
            elif 'internal_by_condition' in traj_type.lower():
                if P_END:
                    f.write("[P-END]: %.2f (in Pa)\n" % P_END)
                if T_END:
                    f.write("[T-END]: %.2f (in K)\n" % T_END)
            else:
                raise ValueError("Invalid specification of keyword traj_type. You entered: ", traj_type)
        
        else:
            raise ValueError("Invalid specification of keyword traj_type. You entered: ", traj_type)
        f.write("\n")
        
        # Particle Properties
        f.write(format_section_headline('PARTICLE PROPERTIES', level=1))
        f.write("\n")
        
        f.write(format_section_headline('Soluable Aerosol Particles', level=2))
        f.write("[NUMBER-COMPONENTS]: %i\n" % NUMBER_COMPONENTS)
        f.write('[AEROSOL-CONCENTRATION]: %.4f "%s"\n' % (AEROSOL_CONCENTRATION, AEROSOL_CONCENTRATION_UNIT))
        f.write("[AEROSOL-MEAN-RADIUS]: %.4f %s\n" % (AEROSOL_MEAN_RADIUS, AEROSOL_MEAN_RADIUS_UNIT))
        f.write("[AEROSOL-SIGMA]: %.4f\n" % AEROSOL_SIGMA)
        f.write("[AEROSOL-AMOUNT-H2SO4]: %.6f (-1: Read Value)\n" % AEROSOL_AMOUNT_H2SO4)
        f.write("[AEROSOL-COMPOSITION]: %.6f %.6f %.6f  (weight fraction of H2SO4, H20, HNO3)\n"
                % (AEROSOL_COMPOSITION[0], AEROSOL_COMPOSITION[1], AEROSOL_COMPOSITION[2]))
        f.write("\n")
        
        f.write(format_section_headline('Ice-nucleating Particles', level=2))
        f.write("[IN_TYPE]: %s (COATED_SOOT, SOOT, AMMONIUMSULFAT, MINERAL_DUST, SOOT_ACTIVATED, defined by freezing treshold)\n" % IN_TYPE)
        f.write('[IN-CONCENTRATION]: %.4f "P/CM**3"\n' % IN_CONCENTRATION)
        f.write('[IN]: %.4f (LEGACY: Field used by old Fortran77 code)\n' % IN_CONCENTRATION)
        f.write('[IN-MEAN-RADIUS]: %.4f "%s"\n' % (IN_MEAN_RADIUS, IN_MEAN_RADIUS_UNIT))
        f.write("[IN-SIGMA]: %.4f\n" % IN_SIGMA)
        f.write("[SIGMA_IN]: %.4f(LEGACY: Field used by old Fortran77 code)\n" % IN_SIGMA)
        f.write("[IN-DENSITY]: %.4f (in g/cm**3)\n" % IN_DENSITY)
        f.write("\n")
        
        f.write(format_section_headline('Ice Particles', level=2))
        f.write('[ICE-CONCENTRATION]: %.4f  "%s"\n' % (ICE_CONCENTRATION, ICE_CONCENTRATION_UNIT))
        f.write("[ICE-MEAN-RADIUS]: %.4f  %s\n" % (ICE_MEAN_RADIUS, ICE_MEAN_RADIUS_UNIT))
        f.write("[ICE-SIGMA]: %.4f \n" % ICE_SIGMA)
        f.write("[ICE-AMOUNT-H2SO4]: %.4f \n" % ICE_AMOUNT_H2SO4)
        f.write("[ICE-COMPOSITION]: %.6f %.6f %.6f  (weight fraction of H2SO4, H20, HNO3)\n" % (ICE_COMPOSITION[0], ICE_COMPOSITION[1], ICE_COMPOSITION[2]))
        f.write("\n")
        
        # SEDIMENTATION
        f.write(format_section_headline('SEDIMENTATION', level=1))
        f.write("[SEDIMENTATION]: %s\n" % SEDIMENTATION)
        f.write("[SEDFACTOR]: %f (1= no sed., 0=full sed. cloud top)\n" % SEDFACTOR)
        f.write("[DZ]: %s (Box Height (m))\n" % DZ)
        f.write("\n")
        
        # GRAVITY WAVE FORCING
        if USE_GRAVITY_WAVE_FORCING:
            f.write(format_section_headline('GRAVITY WAVE FORCING', level=1))
            f.write("[USE_GRAVITY_WAVE_FORCING]: yes\n")
            f.write("[SIGMA_DELTA_W]: %.2f\n" % SIGMA_DELTA_W)
            f.write("[LATITUDE_ANGLE]: %.2f\n" % LATITUDE_ANGLE)
            if GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC:
                f.write("[GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC]: %.2f\n" % GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC)
            f.write("\n")

        # PARAMETERIZATIONS
        f.write(format_section_headline('PARAMETERIZATIONS', level=1))
        f.write("[PAR-PSAT-WATER]: %i (1: Tabazadeh; 2: Murphy and Koop; 3: Nachbar; 4 : Sonntag; 5 : Schneider; 6 : Hyland and Wexler)\n" % PAR_PSAT_WATER)
        f.write("[PAR-PSAT-ICE]: %i       (1: Marti and Mauersberger; 2: Murphy and Koop)\n" % PAR_PSAT_ICE)
        f.write("[PAR-WATER-ACTIVITY]: %i (1: Carslaw; 2: Shi; 3: Schneider)\n" % PAR_WATER_ACTIVITY)
        f.write("[PAR-HET-FREEZE]: %s (KL2003, dust_Ullrich2017, dust_Ullrich2017_updated, soot_Ullrich2017, atd_steinke2015, QUARTZ_SCHORR, FUMED_SILICA_SCHORR, CALCIUM_CARBONATE_SCHORR)\n\n" % PAR_HET_FREEZE)
        
        
# %% Create .init and .traj files to reproduce AIDA experiments

def load_pkl2dict(path, verbose=False):
    """Import .pkl file containing python dictionary.
    
    Parameters
    ----------
    path_of_file_pkl : :obj:`str`
        Path of file to be imported. Has to end with .pkl or .pickle
    
    Returns
    -------
    :obj:`dict`
        arbitrary content
    """
    import pickle
    
    with open(path, 'rb') as f:
        outfile = pickle.load(f)
        
        if verbose:
            print("load_pkl2dict: Imported data from", path)
            
        return outfile
    

def aida_to_maid_trajectory(traj_path, aida_file_path, time_resolution='sec', mode='calc'):
    """Create MAID trajectories for a given AIDA experiment.

    Parameters
    ----------
    traj_path : str
        Path to the .traj file to be generated.
    aida_file_path : str
        Path to the AIDA data input file.
    time_resolution : str, optional
        Time resolution for the output.
        Choose between 'hour' or 'min' or 'sec'.
        The default is 'sec'.
    mode : str, optional
        Decide how the wall flux is determined.
        Choose between 'calc' or 'mbw' or 'off'/None.
        The default is 'calc'.
    
    Returns
    -------
    None.
    Creates a .traj file.

    """
    import pandas as pd
    import warnings
    
    # read AIDA experiment data
    data = load_pkl2dict(aida_file_path)
    
    # determine water wall flux
    if mode == 'calc':
        if 'pw' in data['wall_flux'].keys():
            # Legacy: This is for an outdated data structure in the .pkl file
            wall_flux_ppmv = data['wall_flux']['pw']
        else:
            # This is the standard data structure as of 2022-04
            wall_flux_ppmv = data['wall_flux']['dataset'].wall_flux_h2o
        
    elif mode == 'mbw':  # calculate wall flux from MBW
        wall_flux_ppmv = data['h2o_total']['MBW_VMR_ppm'][data['pT'].index].diff().fillna(0)
    elif (mode == 'off') or (mode is None):
        wall_flux_ppmv = 0
    
    if any(wall_flux_ppmv.isna()):
        warnings.warn(("Encountered NaNs in water wall flux. NaNs are replaced with '0' to avoid malfunction of MAID. "
                       "Please check if a wall flux is to be expected for the affected time interval!"), UserWarning)
        wall_flux_ppmv = wall_flux_ppmv.fillna(0)
    
    # combine the relevant data into a single dataframe
    data_traj = pd.concat([data['pT']['p'] / 100,  # in hPa
                           pd.DataFrame({'wall_flux': wall_flux_ppmv}, index=data['pT'].index, dtype='float'),  # water flux in ppmv
                           data['pT']['tg']], axis=1)  # in K
    
    if any(data_traj.isna()):
        warnings.warn("Encountered NaNs in traj-DataFrame. "
                      "Probably one of the datasets ends earlier. "
                      "All rows with NaNs will be dropped!")
        data_traj = data_traj.dropna(axis='rows')
    
    # truncate dataset to relevant time interval
    t_start = data['experimentliste']['RefZeit']
    t_end = data['experimentliste']['ExpEnde']
    data_traj = data_traj.loc[t_start: t_end]

    if time_resolution == 'sec':
        time_axis = (data_traj.index - data['experimentliste']['RefZeit']).total_seconds().astype('int')
        time_resolution = 'sec'  # use as default time resolution
        
    if time_resolution == 'min':
        time_axis_sec = (data_traj.index - data['experimentliste']['RefZeit']).total_seconds().astype('int')
        time_axis = (time_axis_sec // 60)
    
    data_traj.index = time_axis  # replace index column with absolute integer wrt time resolution
    data_traj = data_traj.loc[~data_traj.index.duplicated()]  # remove entries with duplicated index, only relevant for minute resolution
    
    # write data to .traj file
    spacer = ' ' * 6
    with open(traj_path, 'w+') as f:
        for line in pd.DataFrame.itertuples(data_traj):
            newline = ('%.2f' + spacer + '%.2f' + spacer + '%.8f' + spacer + '%.2f') % (line.Index, line.p, line.wall_flux, line.tg)
            f.write(newline + '\n')


def aida_to_maid_init(init_path,
                      aida_pkl_path,
                      AEROSOL_MEAN_RADIUS=0.25,
                      AEROSOL_MEAN_RADIUS_UNIT='MYM',
                      AEROSOL_SIGMA=1.1,
                      IN_MEAN_RADIUS=None,
                      IN_MEAN_RADIUS_UNIT='MYM',
                      IN_SIGMA=None,
                      IN_DENSITY=None,
                      IN_TYPE=110,  # only for PAR_HET_FREEZE=='KL2003'
                      PAR_HET_FREEZE='dust_Ullrich2017_updated',
                      TRAJECTORY_FILE='',
                      OUTPUT='AWICIT',
                      OUTPUT_SD_LIQ='NO',
                      OUTPUT_SD_IN='NO',
                      OUTPUT_SD_ICE='NO',
                      TIME_RESOLUTION_OUTPUT=1,
                      TRAJECTORY_FORMAT=[0, 3, 1, 2, 3, True],
                      ASSESSMENTS=[-2, -1, -1, -1],
                      verbose=False):
    """Create MAID scenario (*.init + *.traj) from AIDA data.
    
    For a description of input arguments check the docstring of `write_maid_init()`.
    """
    import pandas as pd
    
    if not TRAJECTORY_FILE:
        TRAJECTORY_FILE = init_path.replace('.init', '.traj')
    
    # read AIDA data
    data = pd.read_pickle(aida_pkl_path)
    t_start = data['experimentliste']['RefZeit']
    average_interval_start = t_start - pd.Timedelta(seconds=30)
    
    temp_start = data['pT']['tg'].loc[average_interval_start: t_start].mean()  # in K
    p_start = data['pT']['p'].loc[average_interval_start: t_start].mean()  # in Pa
    p_end = data['pT']['p'].min()  # in Pa
    h2o_start = data['h2o']['pw_corr'].loc[average_interval_start: t_start].mean()  # in Pa
    duration = int((data['pT']['p'].idxmin() - t_start).total_seconds())  # in seconds
    
    try:  # Soluable particles
        drop_conc = data['n_d']  # in cm⁻³
    except KeyError:
        drop_conc = 0
    if drop_conc:  # calculate wt% of H2SO4 solution droplets
        # RHi_start = data['h2o']['RHi_smooth'][average_interval_start: t_start].mean()  # in %
        # wt_fraction_H2SO4_eqilib = scrape_H2SO4_eq_wt_AIM_model(temp_start, S=RHi_start / 100, S_mode='ice', verbose=verbose)
        
        p_h2o_ambient_start = (data['h2o']['VMR_H2O_ppm'] * 1e-6 * data['h2o']['p_AIDA_hPa'] * 100).loc[average_interval_start: t_start].mean()
        wt_fraction_H2SO4_eqilib = fit_h2so4_wt_to_water_vapor_pressure(temp_start, p_h2o_ambient_start)
        
    else:
        wt_fraction_H2SO4_eqilib = 0
        
    try:  # INPs
        inp_conc = data['n_ae']  # in cm⁻³
    except KeyError:
        inp_conc = 0
            
    # Create .init file
    # Note: unmodified default arguments are commented out
    write_maid_init(init_path,
                    traj_type='external',
                    OUTPUT=OUTPUT,
                    OUTPUT_SD_LIQ=OUTPUT_SD_LIQ,
                    OUTPUT_SD_IN=OUTPUT_SD_IN,
                    OUTPUT_SD_ICE=OUTPUT_SD_ICE,
                    TIME_RESOLUTION_OUTPUT=TIME_RESOLUTION_OUTPUT,
                    # FREEZING='HETEROGEN',
                    # TIME_UNIT_TRAJ='SEKUNDEN',
                    # PRESSURE_UNIT='MBAR',
                    # PARTICLE='yes',
                    # NUMBER_COMPONENTS=3,
                    # NUMBER_BINS=60,
                    # SD_RADIUS_MIN=0.01,
                    # SD_RADIUS_MAX=25,
                    # SD_RADIUS_UNIT='MYM',
                    AEROSOL_CONCENTRATION=drop_conc,
                    # AEROSOL_CONCENTRATION_UNIT='P/CM**3',
                    AEROSOL_MEAN_RADIUS=AEROSOL_MEAN_RADIUS,
                    AEROSOL_MEAN_RADIUS_UNIT=AEROSOL_MEAN_RADIUS_UNIT,
                    AEROSOL_SIGMA=AEROSOL_SIGMA,  # default oriented at Martinas presentation at 2020-07-08
                    AEROSOL_AMOUNT_H2SO4=wt_fraction_H2SO4_eqilib * 100,
                    # AEROSOL_COMPOSITION=[0, 1 , 0],
                    IN_TYPE=IN_TYPE,  # for immersion freezing acc. to Kärcher & Lohmann (2003)
                    IN_CONCENTRATION=inp_conc,
                    # IN_CONCENTRATION_UNIT='P/CM**3',
                    IN_MEAN_RADIUS=IN_MEAN_RADIUS,
                    IN_MEAN_RADIUS_UNIT=IN_MEAN_RADIUS_UNIT,
                    IN_SIGMA=IN_SIGMA,
                    IN_DENSITY=IN_DENSITY,
                    # ICE_CONCENTRATION=0,
                    # ICE_CONCENTRATION_UNIT='P/CM**3',
                    # ICE_MEAN_RADIUS=0.05,
                    # ICE_MEAN_RADIUS_UNIT='MYM',
                    # ICE_SIGMA=0.6,
                    # ICE_AMOUNT_H2SO4=1.0,
                    # ICE_COMPOSITION=[0, 1, 0],
                    COMP=-1,
                    # COMP_UNIT='ppb',
                    H2O=h2o_start,
                    H2O_UNIT='PA',
                    # H2O_CONST=False,
                    # HNO3=0,
                    # HNO3_CONST=False,
                    # HNO3_UNIT='ppb',
                    # HCL=0,
                    # HCL_CONST=False,
                    # HCL_UNIT='ppb',
                    # HBR=0,
                    # HBR_CONST=False,
                    # HBR_UNIT='ppb',
                    # SEDIMENTATION='yes',
                    # SEDFACTOR=0.57,
                    # DZ=50,
                    T_START=temp_start,
                    P_START=-p_start,
                    P_END=p_end,
                    DURATION=duration,
                    # PAR_PSAT_WATER=3,
                    # PAR_PSAT_ICE=2,
                    # PAR_WATER_ACTIVITY=1,
                    PAR_HET_FREEZE=PAR_HET_FREEZE,
                    TRAJECTORY_FORMAT=TRAJECTORY_FORMAT,
                    TRAJECTORY_FILE=TRAJECTORY_FILE,
                    ASSESSMENTS=ASSESSMENTS,
                    verbose=verbose)


# %%  Example call:
    
# import os
# from pathlib import Path
# scenario_prefix = 'test_function_call'
# campaign = 'AWICIT05'
# exp_num = '07'
# aida_pkl_path = f'/home/tobias/phd-sync/py_aida/DataSets/calc_aida_all/{campaign}/{campaign}_{exp_num}_aida.pkl'
# traj_path = Path(f'/home/tobias/phd-sync/model/MAID/MAID/scenarios/{scenario_prefix}/traj/{campaign}_{exp_num}_aida_lowtemp.traj')
# init_path = Path(f'/home/tobias/phd-sync/model/MAID/MAID/scenarios/{scenario_prefix}/{campaign}_{exp_num}_aida_lowtemp.init')
# for p in [traj_path, init_path]:
#     p.parent.mkdir(parents=True, exist_ok=True)
# aida_to_maid_trajectory(traj_path,
#                         aida_pkl_path,
#                         time_resolution='sec',
#                         mode='calc')
# aida_to_maid_init(str(init_path), aida_pkl_path, os.path.relpath(traj_path, init_path))
