# Media directory for MAID documentation


### General recommendation
Please don't add common image formats to the repository. They will bloat the repository.
Try to use svg files which can be converted to png through terminal commands.
You can run the conversion in the CI pipeline to create the png files on the fly while building the documentation.


### MAID Logo
The MAID logo will is exported as png and added to the repository to use it also in the README.md file.

The file `maid_logo.png` can be converted from svg to have the right size:
```shell
apt install imagemagick
HEIGHT=800
DENSITY=1200
convert -density $DENSITY -resize $HEIGHT -background none maid_logo.svg maid_logo.png
```


