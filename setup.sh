#!/bin/bash
#
# -----------------------------------------------------
#
# Install dependencies for MAID:
#   - system dependencies if run with root permissions
#   - python dependencies in (new) virtual environment
#
# This script needs the following dependencies:
#   - apt
#   - dpkg
#   - sudo (to install missing system packages)
#
# -----------------------------------------------------

# '-e-: automatic error detection
# '-u': raise error when calling unbound variables
set -eu

# check if run by root
if [[ $(whoami) == "root" ]]; then
    echo "Running other peoples script with root is dangerous!"
    echo "Please use a non-privileged user account."
    echo "Exiting."
    exit
fi

# define variables
VENV_DIR="${HOME}/.virtualenvs/MAID"
PY_VERSION=$(python3 --version | cut -d '.' -f 1-2 | cut -d " " -f 2)


# check for missing system dependencies.
REQUIRED_PKGS="git gfortran python${PY_VERSION}-venv"
MISSING_PKGS=""
for REQUIRED_PKG in $REQUIRED_PKGS; do
    echo "Checking for ${REQUIRED_PKG}..."
    PKG_VERSION=$(dpkg-query -W $REQUIRED_PKG | sed 's/\s\s*/ /g' | cut -d ' ' -f 2)
    if [ "" = "$PKG_VERSION" ]; then
        MISSING_PKGS+="$REQUIRED_PKG"
    fi
    unset PKG_VERSION
done

# inform user about possibly missing system dependencies
if [[ $(echo $MISSING_PKGS | wc -w ) == 0 ]]; then
    echo "All system dependencies fulfilled."
elif [[ $(echo $MISSING_PKGS | wc -w ) -gt 0 ]]; then
    echo "ERROR: Missing system dependencies!"
    echo "Please install the required packages first by running"
    echo "    sudo apt install ${MISSING_PKGS}"
    echo "Exiting."
    exit
fi


# create and activate virtual python environment
echo "Installing python dependencies in virtual python environment..."
if [ ! -d "$VENV_DIR" ]; then
    echo "Create new virtual python environment in ${VENV_DIR}."
    python3 -m venv $VENV_DIR
fi
source ${VENV_DIR}/bin/activate
pip install -r requirements.txt

echo ""
echo "Setup complete."
echo "Activate the python environment by running 'source ${VENV_DIR}/bin/activate'."
