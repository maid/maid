---
title: Implementation references
---

The following publications were used to implement different processes in the model:

* Sedimentation:
    - P. Spichtinger and D. J. Cziczo, “Impact of heterogeneous ice nuclei on homogeneous freezing events in cirrus clouds”, Journal of Geophysical Research, vol. 115, no. D14208, 2010, doi: 10.1029/2009jd012168.
* Aqueous solution particles
    - K. S. Carslaw, B. Luo, and T. Peter, “An analytic expression for the composition of aqueous HNO3-H2SO4 stratospheric aerosols including gas phase removal of HNO3”, Geophysical Research Letters, vol. 22, no. 14, pp. 1877–1880, Jul. 1995, doi: 10.1029/95gl01668.
    - B. Luo, K. S. Carslaw, T. Peter, and S. L. Clegg, “Vapour pressures of H2SO4/HNO3/HCl/HBr/H2O solutions to low stratospheric temperatures”, Geophysical Research Letters, vol. 22, no. 3, pp. 247–250, Feb. 1995, doi: 10.1029/94gl02988.
    - A. Tabazadeh, O. B. Toon, S. L. Clegg, and P. Hamill, “A new parameterization of H2SO4/H2O aerosol composition: Atmospheric implications”, Geophysical Research Letters, vol. 24, no. 15, pp. 1931–1934, Aug. 1997, doi: 10.1029/97gl01879.
    - Q. Shi, J. T. Jayne, C. E. Kolb, D. R. Worsnop, and P. Davidovits, “Kinetic model for reaction of ClONO2with H2O and HCl and HOCl with HCl in sulfuric acid solutions”, Journal of Geophysical Research: Atmospheres, vol. 106, no. D20, pp. 24259–24274, Oct. 2001, doi: 10.1029/2000jd000181.
* Mass transport gas/liquid (condensation):
    - B. Dahneke, “Simple Kinetic Theory of Brownian Diffusion in Vapors and Aerosols”, in Theory of Dispersed Multiphase Flow, Elsevier, 1983, pp. 97–133. doi: 10.1016/b978-0-12-493120-6.50011-8.
* HNO3 trapping:
    - B. Kärcher and M. M. Basko, “Trapping of trace gases in growing ice crystals”, Journal of Geophysical Research: Atmospheres, vol. 109, no. D22, p. n, Nov. 2004, doi: 10.1029/2004jd005254.
    - B. Kärcher and C. Voigt, “Formation of nitric acid/water ice particles in cirrus clouds”, Geophysical Research Letters, vol. 33, no. 8, 2006, doi: 10.1029/2006gl025927.
* Water vapor saturation pressure w.r.t. liquid water
    - R. W. Hyland and A. Wexler, “Formulation for the thermodynamic properties of the saturated phases of H2O from 173.15K to 473.15K”, in ASHRAE Trans., 1983, vol. 89(2A), pp. 520–535. 
    - D. M. Murphy and T. Koop, “Review of the vapour pressures of ice and supercooled water for atmospheric applications”, Quarterly Journal of the Royal Meteorological Society, vol. 131, no. 608, pp. 1539–1565, Apr. 2005, doi: 10.1256/qj.04.94.
    - M. Nachbar, D. Duft, and T. Leisner, “The vapor pressure of liquid and solid water phases at conditions relevant to the atmosphere”, The Journal of Chemical Physics, vol. 151, no. 6, p. 64504, Aug. 2019, doi: 10.1063/1.5100364.
    - J. Schneider et al., “High homogeneous freezing onsets of sulfuric acid aerosol at cirrus temperatures”, vol. 21, no. 18, pp. 14403–14425, Sep. 2021, doi: 10.5194/acp-21-14403-2021.
    - D. Sonntag, “Advancements in the field of hygrometry”, Meteorologische Zeitschrift, vol. 3, no. 2, pp. 51–66, May 1994, doi: 10.1127/metz/3/1994/51.
* Water vapor saturation pressure w.r.t. ice:
    - J. Marti and K. Mauersberger, “A survey and new measurements of ice vapor pressure at temperatures between 170 and 250K”, Geophysical Research Letters, vol. 20, no. 5, pp. 363–366, Mar. 1993, doi: 10.1029/93gl00105.
    - D. M. Murphy and T. Koop, “Review of the vapour pressures of ice and supercooled water for atmospheric applications”, Quarterly Journal of the Royal Meteorological Society, vol. 131, no. 608, pp. 1539–1565, Apr. 2005, doi: 10.1256/qj.04.94.
* Ice nucleation:
    - B. Kärcher and U. Lohmann, “A parameterization of cirrus cloud formation: Heterogeneous freezing”, Journal of Geophysical Research, vol. 108, no. D14, 2003, doi: 10.1029/2002jd003220.
