---
title: Documentation
ordered_subpage: installation.md
ordered_subpage: guides
ordered_subpage: files
ordered_subpage: tools
ordered_subpage: implementation_references.md
ordered_subpage: publications.md
ordered_subpage: history.md
---

# MAID Documentation

Welcome to the documentation of the process model MAID, which stands for "Model for Aerosol and Ice Dynamics".
It is a Lagrangian process model to simulate cloud formation in the upper troposphere / lower stratosphere (UTLS).

The sections shown on the left provide additional information about usage, data structures and additional tooling. 
If you're looking for the documentation of the [source files](|url|/lists/files.html) you should check the [modules](|url|/lists/modules.html) and [procedures](|url|/lists/procedures.html) overviews, as well as the [program page](|url|/program/maid.html).


## Usage

Read the section [Installation](installation.html) which includes instructions for a [Quick Start](|page|/guides/quick_start.html).
If that works you can go on to the [User Guide](|page|/guides/user_guide.html).
