---
title: run_maid.sh
---

# run_maid.sh
This script `run_maid.sh` is a wrapper to run the MAID model conveniently.
It creates temporary directories with each call, so a batch of multiple model runs can be executed in parallel.
A basic command line interface (CLI) makes the script suited for batch processing with reduced overhead as well as for single runs with a direct interactive data preview.


## Basic usage
The script needs one obligatory argument representing the path to an initialization file.  
Example:
```bash
./run_maid.sh ./scenarios/your_scenario_set/init/filename_X.init
```


## Command line options
A description of the command line options can be found in the help string of the script.
You can print this information by running `run_maid.sh --help` or `run_maid.sh -h`.

```text
./run_maid.sh --help                                                                                                          

==================== run_maid.sh ====================
Run the MAID model for a given scenario.

Usage: ./run_maid.sh [scenario] [--option1] [value1] [--option2] [value2] ...

Required argument: [scenario]
  scenario:         string      –           Path to *.init file.

Optional keyword arguments:
Options             Type        Default         Description
-------             ----        -------         -----------
 -b|--bin           string      false           Select a binary of the model and skip its compilation.
 -c|--compile       bool/string true            Choose if compilation should be done normally or with a custom command.
                                                The custom command must include '-o Maid'.
                                                This option is omitted if you use the '-b|--bin' option.
 -g|--debug         bool        false           Choose if debug flag should be used for compilation.
 -h|--help          –           –               Shows this help.
 -l|--savelog       bool        false           Create logfiles of STDOUT and STDERR in the documentation directory.
 -o|--outputbasedir string      scenario/../    Define a custom path to save the output files.
 -p|--saveplot      bool        true            Save plots in a directory 'out/' parallel to the directory of your init file.
 sp|--showplot:     bool        false           Decide if interactive plot should open.
 -s|--source        string      ./maid.f90      Define a custom MAID source file. Defaults to 'src/maid.f90'.

Note: Using '--debug true' generates the subfolder ./doc/ with all files being relevant for the latest model run.
```
Please call the help string in your own terminal to ensure you see the right output for your current version.

@todo
Update path definitions in help string. Some of them are outdated.
@endtodo


## Usage examples

### Call run_maid.sh for single MAID runs 
Single MAID run with defaults (model compilation, save output, no plots):
```shell
./run_maid.sh path/to/scenario.init
```

Single MAID run with generation of plots (true by default), saved to default directory:
```shell
./run_maid.sh path/to/scenario.init -p true
# or
./run_maid.sh path/to/scenario.init --saveplot true
```

Single MAID run and show plots afterwards (without saving the plots):
```shell
./run_maid.sh path/to/scenario.init -sp true -p false
./run_maid.sh path/to/scenario.init --showplot true --saveplot false
```

Single MAID run in debug mode (modified compile options and infos in stdout)
```shell
./run_maid.sh path/to/scenario.init -g true
./run_maid.sh path/to/scenario.init --debug true
```

Additionally to the debug mode you can save the stdout/stderr streams:

```shell
./run_maid.sh path/to/scenario.init -g true -l true
./run_maid.sh path/to/scenario.init --debug true --savelog true
```

### Call run_maid.sh for batch MAID runs
To run MAID with a set of scenarios you should reduce the overhead of singel runs.

This command doesn't compile the model and creates no plots. It checks at the default directory for an existing compiled model binary:
```shell
./run_maid.sh path/to/scenario.init -p false -c false
./run_maid.sh path/to/scenario.init --saveplot false --compile false
```

If you want to use a specific binary, you should use:

```shell
./run_maid.sh path/to/scenario.init -p false -c false -b path/to/precompiled/MAID
./run_maid.sh path/to/scenario.init --saveplot false --compile false --bin path/to/precompiled/MAID
```
