---
title: Scenario from AIDA exp.
---

# Create scenarios from AIDA datasets
Once you have an AIDA dataset (in the pickle format) you can use provided tools to create an initialization file and a trajectory file directly from this dataset.

The dedicated python functions reside in the file [tools/create_scenarios.py](|url|/sourcefile/create_scenarios.py.html).
To import them just move to the projects base directory and run
```python
# python
from tools import create_scenarios
```


## tools.create_scenarios.aida_to_maid_init()
The function `aida_to_maid_init()` creates initialization files from a pickled AIDA dataset (*.pkl).
However, you should **double check if all default values fit your setup** and adjust those if necessary.


## tools.create_scenarios.aida_to_maid_trajectory()
The function `aida_to_maid_trajectory()` creates trajectory files from a pickled AIDA dataset (*.pkl).
You have the option to include also a water flux between an iced surface on the wall and the gas phase.
You should **double check** if your dataset was even created with a calculated wall flux, which is not the case by default.


## Usage and documentation
Details on the preceeding functions are described in their docstring.
Please either look into the docstring contained in [the source code](|url|/sourcefile/create_scenarios.py.html) or view its content by running
```python
# python
from tools import create_scenarios
help(create_scenarios.aida_to_maid_init)
help(create_scenarios.aida_to_maid_trajectory)
```
For the sake of maintainability the docstrings won't be replicated here.
