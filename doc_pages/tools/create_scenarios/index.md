---
title: Create Scenarios
---

# Tools: Create Scenarios

You are provided with a python tool to create initialization files for MAID.  
Especially once you create large scenario sets this tool becomes increasingly helpful.

* [Create initialization files](|page|/tools/create_scenarios/create_init.html)

If you want to intercompare AIDA cloud chamber experiments with the MAID model you can also create initialization files and trajectory files directly from an AIDA dataset:

* [Create scenarios from AIDA datasets](|page|/tools/create_scenarios/scenario_from_aida.html)
