---
title: Create .init files
---

# Create initialization files
For the automated creation of input files [a python file](|url|/sourcefile/create_scenarios.py.html) is provided.

When your current working directory is the projects base directory, you can import all contained functions by running
```python
# python
import tools.create_scenarios
```
in your python file or interpreter.

## tools.create_scenarios.write_maid_init()
The function `write_maid_init()` to creates initialization files. It has an optional keyword argument for every key that can be included into the initialization file.
Even though most arguments have a meaningful default value, **you should always double check all default values** before creating an initialization file.

The function contains a number of checks to ensure constency between certain variables.

For details regarding the keyword arguments please either look into the docstring contained in [the source code](|url|/sourcefile/create_scenarios.py.html) or look into the docstring by running
```python
# python
import tools.create_scenarios
help(tools.create_scenarios.write_maid_init)
```
