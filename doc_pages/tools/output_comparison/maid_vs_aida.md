---
title: Compare MAID vs AIDA
---

# Compare MAID vs AIDA
You are provided with tools to compare a MAID run with an AIDA dataset.

Therefore two tools are available:

1. [python] [tools/plotting/plot_timesteps_compare_maid_aida.py](|url|/sourcefile/plot_timesteps_compare_maid_aida.py.html)
2. [shell] [tools/compare_maid_aida.sh](|url|/sourcefile/compare_maid_aida.sh.html)

The python script takes an existing model output file and an AIDA dataset as input arguments.
The shell script extends the python script functionality, so you can

* compile the model
* execute the model run
* and create the comparison plots

within a single command.


## Usage of the shell script
The help function of the shell script can be viewed via
```text
$ ./tools/compare_maid_aida.sh --help

=== Help function of compare_maid_vs_aida.sh ===
Compare the output of a MAID model run with an AIDA expansion.

Usage: ./tools/compare_maid_aida.sh [--option1] [value1] [--option2] [value2]...

Options:                Values:
./tools/compare_maid_aida.sh: Zeile 24: printf: --: Ungültige Option.
printf: Aufruf: printf [-v var] Format [Argumente]
 -cf|--controlfile:     Path to the script running the model
 -i|--init:             Name of the .init/.traj file for the model
 -a|--aida_data:        Path to a .pkl file containing AIDA data

```
The control file is typically `run_maid.sh`, but can also be another modified script.  


## Usage of the python script
The python script has also a CLI and its help function can be viewed via:
```text
$ python tools/plotting/plot_timesteps_compare_maid_aida.py --help                                                                                                                                                                                                                                                                              
usage: plot_timesteps_compare_maid_aida.py [-h] [-d1 DATA] [-m MODE] [-a AIDA] [-o OUTFILE]

options:
  -h, --help            show this help message and exit
  -d1 DATA, --data DATA
                        Path of first MAID .out-file
  -m MODE, --mode MODE  Decide how to compare both datasets
  -a AIDA, --aida AIDA  Path of AIDA .pkl-file
  -o OUTFILE, --outfile OUTFILE
                        Directory of plot output file
```
For details on the available `MODE` values, please check the [source file](|url|/sourcefile/plot_timesteps_compare_maid_runs.py.html).
