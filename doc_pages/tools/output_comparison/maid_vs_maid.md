---
title: Compare MAID runs
---

# Compare MAID runs
You are provided with tools to compare two MAID runs with one another.

Therefore two tools are available:

1. [python] [tools/plotting/plot_timesteps_compare_maid_runs.py](|url|/sourcefile/plot_timesteps_compare_maid_runs.py.html)
2. [shell] [tools/compare_maid_runs.sh](|url|/sourcefile/compare_maid_runs.sh.html)

The python script takes existing output files as input arguments.
The shell script extends the python script functionality, so you can

* compile the model
* execute the model runs
* and create the comparison plots

within a single command.


## Usage of the shell script
The help function of the shell script can be viewed via
```text
$ ./tools/compare_maid_runs.sh --help                                                                                                                                                                                                                                                                                                                  

=== Help function of compare_maid_runs.sh ===
Compare the output of two model runs performed with MAID.

Usage: ./tools/compare_maid_runs.sh [--option1] [value1] [--option2] [value2]...

Options:                  Values:
  -cf1|--controlfile1:     Path to the script running the first model
  -cf2|--controlfile2:     Path to the script running the second model
  -i1|--init1:             Name of the .init/.traj file for first model
  -i2|--init2:             Name of the .init/.traj file for second model

Optional options:         Values:
  -a|--aida_data:          Path to a .pkl file containing AIDA data

```
The control file is typically `run_maid.sh`, but can also be another modified script.  
As an extra feature you can also add AIDA data to the plot. However the AIDA data will not be part of the comparison.
If you want to compare MAID runs to AIDA data, please [read here](|page|/tools/output_comparison/maid_vs_aida.html).


## Usage of the python script
The python script has also a CLI and its help function can be viewed via:
```text
$ python tools/plotting/plot_timesteps_compare_maid_runs.py --help                                                                                                                                                                                                                                                                                     
usage: plot_timesteps_compare_maid_runs.py [-h] [-d1 DATA1] [-l1 LABEL1] [-d2 DATA2] [-l2 LABEL2] [-m MODE] [-a AIDA] [-o OUTFILE]

options:
  -h, --help            show this help message and exit
  -d1 DATA1, --data1 DATA1
                        Path of first MAID .out-file
  -l1 LABEL1, --label1 LABEL1
                        Label for data1
  -d2 DATA2, --data2 DATA2
                        Path of second MAID .out-file
  -l2 LABEL2, --label2 LABEL2
                        Label for data2
  -m MODE, --mode MODE  Decide how to compare both datasets
  -a AIDA, --aida AIDA  Path of AIDA .pkl-file
  -o OUTFILE, --outfile OUTFILE
                        Directory of plot output file
```
For details on the available `MODE` values, please check the [source file](|url|/sourcefile/plot_timesteps_compare_maid_runs.py.html).
