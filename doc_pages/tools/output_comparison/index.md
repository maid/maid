---
title: Output comparison
ordered_subpage: maid_vs_maid.md
ordered_subpage: maid_vs_aida.md
---

# Output comparison
You are provided with some basic scripts to compare the output of a MAID run to another dataset.  

You have the two possibilities:

* [Compare two MAID runs with one another](|page|/tools/output_comparison/maid_vs_maid.html)
* [Compare a MAID run with an AIDA dataset](|page|/tools/output_comparison/maid_vs_aida.html)

The scripts can compare the variables in the dataset with respect to different metrics:

* absolute difference
* relative difference
* ratio
* log10(ratio)
* ratio, in percentage

The entry point for the comparison is either a shell script or a python script, which take different input arguments:

* [shell] Takes initialization files as input, compiles and runs the model, then creates comparison plots
* [python] Takes existing output files as input to create the comparison plots

Note, internally the shell script runs the model and calls the python script afterwards using the freshly generated output files.
