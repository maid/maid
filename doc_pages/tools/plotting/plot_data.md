---
title: Plot data
---

# Plot data files
The format of output data files can vary, depending on the `[OUTPUT]` key in the initialization file.
No matter which output format you chose you can use the basic plot functionality. The first column is used as index column and all other columns get plotted on a logarithmic y-scale.

## CLI usage example
The python plotting scripts provide a command line interface (CLI), too.
You can check the help function to get a brief introduction about the command line options:

```text
$ python tools/plotting/plot_output_data.py --help
usage: plot_output_data.py [-h] [-d DATA] [-p PLOT] [-sp SHOWPLOT] [-t TITLE]

options:
  -h, --help            show this help message and exit
  -d DATA, --data DATA  Path of MAID data output file.
  -p PLOT, --plot PLOT  Specify a path to save the plot.
  -sp SHOWPLOT, --showplot SHOWPLOT
                        Decide if an interactive plot gets opened
  -t TITLE, --title TITLE
                        Define custom plot title
```

Example call to plot a MAID data output:
```shell
python tools/plotting/plot_output_data.py /
    -d scenarios/testing/output/run_42_step.out /
    -sp true /
    -t 'MAID run 42'
```

To plot AIDA data with your MAID output together in the same graph, you can use `tools/plotting/plot_timesteps.py`, which is only supported for `[OUTPUT]: MAJOR_OUTPUT_STEP`.
```shell
python tools/plotting/plot_timesteps.py /
    -d scenarios/testing/output/run_42_step.out /
    -sp true /
    -t 'MAID run 42 with AIDA' /
    -a path/to/AIDA_data/run_42.pkl
```


## Direct python usage example
The CLI is based on some python functions which can also be called independently.  
A result similar to the CLI example can be achieved with the following python code:

```python
import matplotlib.pyplot as plt
from tools.plotting import plot_timesteps

maid_data = "scenarios/testing/output/run_42_step.out"
aida_data = 'path/to/AIDA_data/run_42.pkl'

fig, axes = plt.subplots(6, 1, figsize=[10, 15], sharex=True)
plt.grid(color='lightgrey', ls='--')
plot_timesteps.plot_maid_timesteps(axes,
                                   pd.read_csv(maid_data, sep=r"\s+"),
                                   label=f"MAID run '{maid_data}'")
plot_timesteps.plot_aida_timesteps(axes,
                                   tools.plotting.load_pkl2dict(aida_data),
                                   label=f"AIDA data '{aida_data}'")
axes[-1].set_xlabel('Time / minutes')
axes[0].legend()
plt.show()
```

The [source code](|url|/sourcefile/plot_timesteps.py.html) of the python file `tools/plotting/plot_timesteps.py` contains also docstrings for the python functions, which might be helpful.
