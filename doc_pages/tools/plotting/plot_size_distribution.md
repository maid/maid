---
title: Plot size distribution
---

# Plot size distribution
For the visualization of size distribution output files a python file [tools/plotting/plot_size_distributions.py](|url|/sourcefile/plot_size_distributions.py.html) is provided.  
You can use a command line interface (CLI) or use the contained functions separately through python.

## CLI usage example
The command line options can be viewd by running the python file with the `--help` option:
```text
$ python tools/plotting/plot_size_distributions.py --help
usage: plot_size_distributions.py [-h] [-dliq DATA_LIQ] [-dice DATA_ICE] [-din DATA_IN] [-p PLOT] [-sp SHOWPLOT] [-t TITLE]

options:
  -h, --help            show this help message and exit
  -dliq DATA_LIQ, --data_liq DATA_LIQ
                        Path of MAID .out-file of particle size distribution.
  -dice DATA_ICE, --data_ice DATA_ICE
                        Path of MAID .out-file of ice size distribution.
  -din DATA_IN, --data_in DATA_IN
                        Path of MAID .out-file of ice-nucleating particle size distribution.
  -p PLOT, --plot PLOT  Path of the plot to be saved.
  -sp SHOWPLOT, --showplot SHOWPLOT
                        Decide if a preview of the plot should be shown directly
  -t TITLE, --title TITLE
                        Define custom plot title
```

You can combine size distributions of up to three aerosol types in one figure: liquid particles (liq), ice-nucleating particles (in) and ice crystals (ice).

Example usage:
```bash
# plot only the size distribution of liquid aerosol particles
python tools/plotting/plot_size_distributions.py -sp true -dliq scenarios/SET_NAME/output/run_X_sd_liq.out

# plot size distributions of all aerosol types
python tools/plotting/plot_size_distributions.py -sp true /
    -dliq scenarios/SET_NAME/output/run_X_sd_liq.out /
    -dice scenarios/SET_NAME/output/run_X_sd_ice.out /
    -din scenarios/SET_NAME/output/run_X_sd_in.out /
    -t "Just an example for the documentation"
```

## Direct python usage example
The CLI is based on some python functions which can also be called independently.  

Therefore import the functions with
```python
from tools.plotting import plot_size_distributions
```

Inspiration how to handle the data files can be taken from the function `plot_sd()`, whereas the main functionaly of the CLI is located in `main()`.
Please check the [source code](|url|/sourcefile/plot_size_distributions.py.html) for details.
