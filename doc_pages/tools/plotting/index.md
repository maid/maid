---
title: Plotting
---

# Plotting
The plotting support is ment to provide the basic functionality for a quick look on the output data.
You can

* [Plot data output files](|page|/tools/plotting/plot_data.html)
* [Plot size distribution files](|page|/tools/plotting/plot_size_distribution.html)

It is recommended that users create their own scripts to plot model outputs to fit their own specific needs!

@note
The plotting scripts are based on pythons [matplotlib](https://matplotlib.org/) package.
Their documentation provides high quality tutorials and examples.
@endnote
