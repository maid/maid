---
title: Tools
ordered_subpage: run_maid.md
ordered_subpage: create_scenarios
ordered_subpage: plotting
---

# Tools

This project comes with a couple of handy tools to facilitate some tasks

* [bash] [run_maid.sh - a shell wrapper with a CLI to run the model conveniently](|page|/tools/run_maid.html)
* [python] [generation of initialization files](|page|/tools/create_scenarios/create_init.html)
* [python] [visualization of output data](|page|/tools/plotting/index.html)
* [bash] [comparison of model runs](|page|/tools/output_comparison/maid_vs_maid.html)
* [bash] [comparison of model runs with AIDA datasets](|page|/tools/output_comparison/maid_vs_aida.html)
