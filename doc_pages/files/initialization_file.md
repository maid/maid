---
title: Input: Initialization file (*.init)
---

# Initialization file (*.init)
Each model run has to specified by a initialization file providing certain values and parameters.
The syntax of such files is described here.

## File syntax
The way initialization files are currently written (and interpreted) follows an unstandardized way.
However, the syntax is relatively close to a .ini file.

The main purpose of the file is to map a `VALUE` to a `KEY`.__

The sytax follows the following rules:

* Map a value to a key: `[KEY]: [VALUE] (optional comment)`
* Map a list of values to a key: `[KEY]: [VALUE1] [VALUE2] [VALUE3] (optional comment)`
* Lines starting with `#` are ignored (comments)
* The order is arbirary
* There are no hierarchy elements

## Reading initialization files
The import of initialization files is handles by the subroutine `READ_INI` contained in [src/lib_input.f90](|url|/sourcefile/lib_input.f90.html).

## Generation of initialization files
The easiest wayd to create new initialization files are to

* [manually] copy an existing initialization file and modify it to your needs
* [automatically] use the function `write_maid_init()` in the python file [tools/create_scenarios](|url|/sourcefile/create_scenarios.py.html)

The latter is [described here](|url|/page/tools/create_scenarios/create_init.html).


## Description of all available keys
You can find an extensive description of all keys and their possible values in the docstring of `write_maid_init()` in [tools/create_scenarios](|url|/sourcefile/create_scenarios.py.html).

You can either look up the docstring directly in the source code or call it in a python interpreter with
```python
from tools import create_scenarios
help(create_scenarios.write_maid_init)
```
Often IDEs have a nicer visualization of the docstrings.

@todo
Here should be a separate table describing the `KEY` and `VALUE` options.
@endtodo
