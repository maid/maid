---
title: Input: Trajectory file (*.traj)
---

# Trajectory file (*.traj)
The trajectory file is needed if you want to run a pre-defined trajectory of ambient conditions.

### Relevant keys in the .init file
The syntax of the file must be specified in the initialization file of your run.
Those keys determine the 

* number of header lines
* unit of time index column
* number of columns
* meaning of columns
* units of columns

### Key: [TRAJECTORY-FILE]
With `[TRAJECTORY-FILE]` you specify the path to your trajectory file.
The Path can be either absolute or relative to your .init file.

@note
If the key `[TRAJECTORY-FILE]` is present in your initialization file it is assumed that you want to run the model with an external trajectory.
Thus, the keys specifying internal trajectories are ignored.
@endnote
    
Examples:
```text
[TRAJECTORY-FILE]: "../traj/my_trajectory.traj"
# or
[TRAJECTORY-FILE]: "/home/path_to_maid/scenarios/set_name/traj/my_trajectory.traj"
```

### Key: [TIME-UNIT]
With `[TIME-UNIT]` you specify the unit of your index column (column index 0).
The value must be an character expression

value | interpreted time unit
----- | ---------------------------------
sec   | seconds
min   | minutes
hour  | hours
time  | timestamps with format `HH:MM:SS`

@note
Only the first 3-4 characters of the value are interpreted, so if you have a time column with data in minutes you can write `min` or `minute` or `minutes`.
Likewise, if your time index column contains timestamps you can write `time` as well as `timestamps`.
@endnote

### Key: [PRESSURE-UNIT]
With `[PRESSURE-UNIT]` you specify the unit of the values in your pressure column.

@note
Such a key doesn't exist for the temperature column. If the temperature column starts with a value smaller than 100 it's interpreted to be in °C and internally converted to Kelvin.
@endnote

### Key: [P-ANFANG] (Legacy)
If you use a trajectory file `[P-ANFANG]` should specify a negative number.
In previous model versions this key was used to distinguish between internal and external trajectories.
This functionality is already covered by checking if the key `[TRAJECTORY-FILE]` is present.
However, not all code dependencies are removed, yet, so please just set a negative value for `[P-ANFANG]` when using external trajectories.

Note: *Anfang* is German and means *Start* or *Beginning*, so this key is ment to represent the initial pressure value.

### Key: [TRAJECTORY-FORMAT]
With `[TRAJECTORY-FORMAT]` you specify the structure and the content of your trajectory file.
The value is a space-separated list of six entries with the following meaning

list index | type    | meaning
---------- | ------- | ------------------------------------------------------------------
1          | integer | number of comment lines at the file beginning
2          | integer | number of data columns (time index column not counted in)
3          | integer | column index of air pressure
4          | integer | column index of water vapor pressure source
5          | integer | column of air temperature
6          | boolean | .true./.false. to enforce trajectory (overwrite diabatic effects)

@bug
List index 6 to enforce air temperature/pressure might be meaningless.
With a trajectory file the diabatic effects are overwritten in any case.
@endbug

Example:
```text
[TRAJECTORY-FORMAT]: 0 3 1 2 3 .False. (optional comment in brackets)
```

### Key: [ASSESSMENTS]
With `[ASSESSMENTS]` you specify if the vapor pressure of condensable components is specified in the trajectory file.
It is a space-sparated list with as many entries as condensable components are available

components | list length
-----------| -----------
H2O (pure) | 1
 + H2SO4   | 2
 + HNO3    | 3
 + HCl     | 4
 + HBr     | 5

value of list entry | meaning
------------------- | ----------------------------------------------------------
\> 0                | use value as column index to read value
-1                  | don't read from trajectory file
-2                  | read flux (souce/sink) of partial pressure from trajectory

@bug
The flux (source/sink) is currently only implemented for water.
This should be generally implemented for all condensable components.
@endbug

Example:
```text
# read water wall flux, ignore H2SO4 and HNO3
[ASSESSMENTS]: -2 -1 -1 
```
