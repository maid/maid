---
title: Output: Data files
---

# Output: data files
The structure of an output file is determined by the value of the key `[OUTPUT]` in the .init file.
Possible values are

* ZEIT_DRUCK_TABS
* ANZAHL_PART
* AEROSOL
* KRAEMER
* MAJOR
* MAJOR_OUTPUT_STEP

@todo
Remove legacy entries from this list.
@endtodo

## [OUTPUT]: MAJOR_OUTPUT_STEP
The value *MAJOR_OUTPUT_STEP* is considered the default and you can find plotting routines in `tools/plotting/`.

Column separator is multiple spaces ([\s]+).
The single header line is `TIME  TEMP  PRESS  NICE  RICE  RHI  ICE(ppmv) hom=1,het=2`.

column index | variable                                 | unit
------------ | ---------------------------------------- | -------
1 (index)    | elapsed time                             | minutes
2            | air temperature                          | Kelvin
3            | air pressure                             | hPa
4            | current total ice number concentration   | cm⁻³
5            | ice gemetric mean radius                 | µm
6            | relative humidity w.r.t. ice             | %
7            | ice water path                           | ppmv
8            | ice formation mode (1=hom, 2=het)        | -

@todo
Create similar overview tables for the other output formats.
@endtodo
