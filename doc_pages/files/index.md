---
title: File specifications
ordered_subpage: initialization_file.md
ordered_subpage: trajectory_file.md
---

# File type specifications
The MAID model interacts with some input/output files.
The requirements for those files is described here.

Those files include

* Initialization files `*.init` (obligatory)
* Trajectory files `*.traj` (optional)
* Output files (various formats possible)
