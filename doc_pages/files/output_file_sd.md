---
title: Output: Size distribution files
---

# Output: Size distribution files

The size distribution (SD) output files contain time resolved size distributions.
You can create such an output for liquid aerosol particles, solid aerosol particles and ice crystals, respectively.

### Control creation of SD output files
With the keys `[OUTPUT-SD-LIQ]`, `[OUTPUT-SD-IN]` and `[OUTPUT-SD-ICE]` you can control wheather to create the output files

Example:
```text
[OUTPUT-SD-LIQ]: yes
[OUTPUT-SD-IN]: no
[OUTPUT-SD-ICE]: yes
```

### Data structure
The SD data output files doesn't have a header line and consists of 7 columns.
The content of those collumns is as follows:

column index | variable meaning                         | unit
------------ | ---------------------------------------- | -------
1 (index)    | elapsed time                             | minutes
2            | size bin                                 | Kelvin
3            | bin radius                               | µm
4            | number concentration for bin             | cm⁻³
5            | R_H2O_LOG_Q                              | µm
6            | Z_H2O_LOG                                | cm⁻³
7            | (Z_H2O_LOG / dLOG_R_H2O)                 | ??? TODO

@note
Every time a new entry is added a full set of all bins is written into the file.
So when you specified the size distributions to be splitted in 60 size bins and the output interval to be 10 seconds, you will write 21.600 lines for a simulated hour.
@endnote

@todo
Check variable meaning and units for column index 5, 6 & 7.
@endtodo
