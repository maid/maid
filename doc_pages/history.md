---
title: MAID History
---

# MAID History
In the following, the temporal evolution of the model is wrapped up.
Due to a lack of documentation it is hard to attribute specific parts of the code to individual authors.
Therefore the following list is probably incomplete, maybe partially wrong, but describes currently the best guess.

---

* The MAID model originates from an existing model described in a paper from Bunz & Dlugi from the year 1991 [^Bunz1991]. This model focused on chamber experiments and the code was further developed by Helmut Bunz at Forschungszentrum Karlsruhe (predecessor of the later KIT).
* Following it seems like Monika Niemand worked on the code, however the actual code contribution remains unclear. Back then the file was called `taida_eis_hom+het.for` and "MAID" didn't have its current name yet.
* Along the PhD work of Iulia Gensch at Forschungszentrum Jülich (FZJ) the model was largely overworked, including
    - condensation and freezing of UT aerosol particles
    - microphysics and partitioning of water and nitric acid in ice clouds
    - adaption to Lagrangian atmospheric cirrus cloud calculations
    - heterogeneous freezing of liquid aerosol particles, implemented as immersion freezing
    - nitric acid uptake of growing ice crystals (’trapping’)
* In 2008 a paper called "MAID: a model to simulate UT/LS aerosols and ice clouds" was published describing the new model. [^Bunz2008]
* An accompaning paper by Iulia Gensch presented her PhD results. [^Gensch2008]
* In 2008 in a work by Robert Wagner et al. MAID was used to calculate the condensational growth of sulfuric acid solution droplets [^Wagner2008].
* In 2010 the model was used for a diploma thesis called "Untersuchungen zur Klimatologie der Bildung von Eiswolken mit Hilfe des kinetischen Modells MAID" by Margit Hildebrandt. However, except of some nowadays removed if-statements, there was probably no actual code contribution.
* Around 2010 the main creator, Helmut Bunz, had retired and at KIT the model was abandoned in favor of another model (ACPIM) [^Conolly2009][^Dearden2009].
Unfortunately neither structured sources nor documentation (if it even existed) were preserved, which led to the loss of additional tooling, e.g. a Windows-based graphical user interface.
* In 2012 Christian Rolf finished his PhD at FZJ utilizing and extending the MAID model.
    - New sedimentation routine
* His PhD results were also published in 2012 [^Rolf2012].
* In 2016 the model was used by Martina Krämer for different cirrus cloud formation scenarios in ther Cirrus Guide Pt. 1 [^Kraemer2016].
* In 2019 Manuel Baumgartner implemented a basic netCDF support (which was later removed again)
* Starting in February 2021 Tobias Schorr picked up the model and reworked it extensively:
    - Conversion of the FORTRAN77 code to modern Fortran.
    - Restructuring of the code base
    - Removal of abandoned code blocks
    - Add many comments, documentation and references
    - Add a new aerosol type: solid aerosol particles
    - Add heterogeneous deposition freezing of solid aerosol particles based on ice-nucleating active site density parameterizations
    - Add Mesoscale vertical wind speed fluctuations based on Kärcher et al. (2019)
    - Reimplementation of intrinsic atmospheric trajectories (no external trajectory)
    - Repaired equilibrium calculations
    - Multiple bug fixes
    - shell script as a CLI
    - adding python wrappers for
        * plotting output data and size distributions
        * automatic generation of initialization files
    - usage of git versioning
    - Writing a documentation and creating a website using [ford](https://forddocs.readthedocs.io/en/latest/index.html)
* In September 2023 the MAID model was put under an open license.
    - The previous code contributors Helmut Bunz, Iulia Gensch, Martina Krämer and Christian Rolf were contacted regarding the copyright of their code. Monika Niemand couldn't be reached, however if there was any contribution, it remains unclear.
    - The mentioned authors agreed to put the model under an open license.
    - The model is now accessible under an [AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html) license at [https://codebase.helmholtz.cloud/maid/maid](https://codebase.helmholtz.cloud/maid/maid).
    
[^Bunz1991]: H. Bunz and R. Dlugi, “Numerical studies on the behaviour of aerosols in smog chambers,” Journal of Aerosol Science, vol. 22, no. 4, pp. 441–465, Jan. 1991, doi: 10.1016/0021-8502(91)90004-2.
[^Bunz2008]: H. Bunz, S. Benz, I. Gensch, and M. Krämer, “MAID: a model to simulate UT/LS aerosols and ice clouds,” Environmental Research Letters, vol. 3, no. 3, p. 35001, Jul. 2008, doi: 10.1088/1748-9326/3/3/035001.
[^Gensch2008]: I. V. Gensch et al., “Supersaturations, microphysics and nitric acid partitioning in a cold cirrus cloud observed during CR-AVE 2006: an observation–modelling intercomparison study,” Environmental Research Letters, vol. 3, no. 3, p. 35003, Jul. 2008, doi: 10.1088/1748-9326/3/3/035003.
[^Wagner2008]: R. Wagner et al., “Infrared Optical Constants of Highly Diluted Sulfuric Acid Solution Droplets at Cirrus Temperatures,” The Journal of Physical Chemistry A, vol. 112, no. 46, pp. 11661–11676, Nov. 2008, doi: 10.1021/jp8066102.
[^Conolly2009]: Connolly, P. J., M¨ohler, O., Field, P. R., Saathoff, H., Burgess, R.,
Gallagher, M. W., and Choularton, T. W.: Studies of ice nucleation on three different types of dust particle, Atmos. Chem. Phys., 9, 2805–2824, 2009, doi:10.5194/acp-9-2805-2009
[^Dearden2009]: Dearden, C.: Investigating the simulation of cloud microphysical processes in numerical models using a one-dimensional dynamical frameword., Atmos. Sci. Lett., 10, 207–214, 2009, doi: 10.1002/asl.239
[^Rolf2012]: C. Rolf, M. Krämer, C. Schiller, M. Hildebrandt, and M. Riese, “Lidar observation and model simulation of a volcanic-ash-induced cirrus cloud during the Eyjafjallajökull eruption,” Atmospheric Chemistry and Physics, vol. 12, no. 21, pp. 10281–10294, Nov. 2012, doi: 10.5194/acp-12-10281-2012.
[^Kraemer2016]: M. Krämer et al., “A microphysics guide to cirrus clouds – Part 1: Cirrus types,” Atmospheric Chemistry and Physics, vol. 16, no. 5, pp. 3463–3483, Mar. 2016, doi: 10.5194/acp-16-3463-2016.
