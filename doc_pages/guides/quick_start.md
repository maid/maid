---
title: Quick Start
---

## Quick Start

With the following steps you can do a first test run, to check if the model runs on your system.

1. Install the dependencies as described in the [installation instructions](|url|/page/installation.html).
2. Activate the virtual python environment, which you should have created during installation, via `source $YOUR_ENV_PATH/bin/activate`. `$YOUR_ENV_PATH` must be replaced with the local path to your virtual python environment.
3. Run `./run_maid.sh -sp true ./scenarios/examples/$SELETED_EXAMPLE.init`, where the option `-sp true` indicates that you want to *show plots*.

If your're on a system with a graphical interface you should see interactive plots, showing the output data of your model run.
If you're on a headless system use the option `-p true` to save the plots, so you can copy them from `./scenarios/examples/fig/` to another system with a graphical interface.
