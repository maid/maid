---
title: Fortran Resources
---

# Fortran Resources
If you're new to Fortran the following links may give you an orientation to facilitate your start with the language.

## Fortran learning resources
In the last couple of years a community formed around the website [fortran-lang.org](https://fortran-lang.org/) to be a (modern!) website covering Fortran.
Instead of listing too many additional resources here, please check [their info material](https://fortran-lang.org/en/learn/#).

In addition, the [Fortran Wiki](https://fortranwiki.org/fortran/show/HomePage) is also an excellent ressource.


## Fortran tooling
 to provide modern tooling to Fortran.  
Some of the related projects are

* [fpm](https://github.com/fortran-lang/fpm): Fortran Package Manager (similar to Rust's *cargo*)
* [stdlib](https://github.com/fortran-lang/stdlib): A standard library for Fortran[^inofficialStdlib]
* [fortls](https://github.com/fortran-lang/fortls): A Fortran language server (code autocompletion, suggestions, syntax highlighting)

[^inofficialStdlib]: The Fortran standard doesn't have a standard library. The project is community driven.
Another important modern Fortran tool is

* [ford](https://github.com/Fortran-FOSS-Programmers/ford): An automatic documentation generator for Fortran programs (as used here).


## Fortran compiler

This project is only tested for use with [gfortran](https://gcc.gnu.org/fortran/) as a compiler.
The latest documentation can be found [here](https://gcc.gnu.org/onlinedocs/) and is a helpful ressource.
