---
title: User Guide with AIDA
---

# User Guide with AIDA

This page extends the section [User Guide](user_guide.html) with additional options when working with data from the aerosol cloud chamber [AIDA](https://www.imk-aaf.kit.edu/AIDA_facilities.php), which is located at the department "Atmospheric Aerosol Research" in the "Institute of Meteorology and Climate Research" at the Karlsruhe Institute of Technology.

If you have a pickled AIDA dataset you can

* create trajectories from AIDA data
* include water flux between gas phase and iced chamber walls
* compare MAID outputs with AIDA data

### Getting an AIDA dataset
When performing an experiment in AIDA the data is logged into a database. With the project [aida_experiment_analysis](https://codebase.helmholtz.cloud/aida/aida_experiment_analysis) (originally called *py_aida*) you can collect all relevant data and metadata into a single object, which can be stored as pickle file.
If you have such a pickle you can create initialization files and trajectory files from the contained information.


### Creating an initialization file
To create an initialization file based on AIDA data you can use the function `aida_to_maid_init()` provided in [tools/create_scenarios.py](|url|/sourcefile/create_scenarios.py.html).
For a closer description of the functionality check the docstring of this function and the respective [documentation section](|url|/page/tools/create_scenarios/scenario_from_aida.html).

### Creating a trajectory file
The create a trajectory file based on AIDA data you can use the function `aida_to_maid_traj()` also provided in [tools/create_scenarios.py](|url|/sourcefile/create_scenarios.py.html).
Depending on the settings while creating the pickle file you can also add a column describing a flux of water vapor between ice on the chamber wall and the gas phase.
For a closer description of the functionality check the docstring of this function and the respective [documentation section](|url|/page/tools/create_scenarios/scenario_from_aida.html).

### Compare a MAID run with an AIDA dataset
If you want to compare your model run with the experimental dataset you can use the script [tools/compare_maid_aida.sh](|url|/sourcefile/compare_maid_aida.sh.html)
The script comes with a command line interface, which provides usage instructions when being run as `./tools/compare_maid_aida.sh --help`.
Also check the related [documentation section](|url|/page/tools/output_comparison/index.html).
