---
title: Guides
ordered_subpage: quick_start.md
ordered_subpage: user_guide.md
ordered_subpage: user_guide_aida.md
ordered_subpage: developer_guide.md
---

This page provides you with two guides.

##  Quick Start
The [Quick Start](|page|/guides/quick_start.html) instructions provide an easy entry to the usage of the MAID model.
It serves as a check if the model runs on your system and to get a first impression about its capabilities and performance.

# User Guide
The [user guide](|page|/guides/user_guide.html) provides an introduction on how to work with the model in more detail.
It covers how to create new initialization files and trajectory files. Moreover it shows how to handle the command line interface of the shell wrapper `run_maid.sh`.

# Developer Guide
The [developer guide](|page|/guides/developer_guide.html) provides guidelines in order to do changes on the model and the associated files.
It includes a description of the code style choices and suggestions how to contribute to the code repository.
