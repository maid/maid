---
title: Installation
---

# Installation

## Prerequisites
The model and its tools are mainly developed and tested on linux systems, but should also work on macOS.
As a Windows user you can easily setup a virtual machine with a debian-based linux distribution ([example](https://www.wikihow.com/Install-Debian-in-Virtualbox)).
Make sure you have a bash-like terminal (bash/zsh/fish/...) available, which should already be the default for most linux distributions.

@note
**Limitation for Windows users (1):**

Currently `*.traj` files can't reside in a subdirectory of the `*.init` file location.
Help to improve Windows support by extending `src/lib_path.f90` for Windows paths!
@endnote

@note
**Limitation for Windows users (2):**

The model gets actually called by a shell wrapper `run_maid.sh`, which also manages temporary directories and the compilation of the model.
Help to improve Windows support by translating `run_maid.sh` into a platform independent python script!
@endnote

## Cloning the repository
It is recommended to clone the project using git:
```sh
# option 1: via https
git clone https://codebase.helmholtz.cloud/maid/maid.git
# option 2: via ssh
git clone git@codebase.helmholtz.cloud:maid/maid.git
```

Please do not simply download the project as a zip file, since it will be more difficult to receive the latest changes or to contribute your changes to the remote repository.

## Dependency installation
After cloning the repository you can move to the project directory and run
```sh
./setup.sh
```
to check if the required system packages are installed and to 

@note
**Additional dependency for macOS users:**

You also have to install `coreutils` (e.g. with `brew`) to provide the command `grealpath`.
@endnote

The script `setup.sh`

* checks for missing system dependencies (suggests installation of missing packages)
* ensures a virtual python environment is present under `$HOME/.virtualenvs/MAID`
* installs the python dependencies from `requirements.txt` in the virtual environment
* informs you how to activate the virtual environment

As long as this project experiences active development you should be fine by sticking to the latest available versions (Tested with gfortran 12.2 and python 3.11, as of 02/2024).
