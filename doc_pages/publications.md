---
title: Publications using MAID
---

In the following publications MAID was used:

* H. Bunz, S. Benz, I. Gensch, and M. Krämer, “MAID: a model to simulate UT/LS aerosols and ice clouds,” Environmental Research Letters, vol. 3, no. 3, p. 35001, Jul. 2008, [doi: 10.1088/1748-9326/3/3/035001](https://doi.org/10.1088/1748-9326/3/3/035001).
* I. V. Gensch et al., “Supersaturations, microphysics and nitric acid partitioning in a cold cirrus cloud observed during CR-AVE 2006: an observation–modelling intercomparison study,” Environmental Research Letters, vol. 3, no. 3, p. 35003, Jul. 2008, [doi: 10.1088/1748-9326/3/3/035003](https://doi.org/10.1088/1748-9326/3/3/035003).
* R. Wagner et al., “Infrared Optical Constants of Highly Diluted Sulfuric Acid Solution Droplets at Cirrus Temperatures,” The Journal of Physical Chemistry A, vol. 112, no. 46, pp. 11661–11676, Nov. 2008, [doi: 10.1021/jp8066102](https://doi.org/10.1021/jp8066102).
* C. Rolf, M. Krämer, C. Schiller, M. Hildebrandt, and M. Riese, “Lidar observation and model simulation of a volcanic-ash-induced cirrus cloud during the Eyjafjallajökull eruption,” Atmospheric Chemistry and Physics, vol. 12, no. 21, pp. 10281–10294, Nov. 2012, [doi: 10.5194/acp-12-10281-2012](https://doi.org/10.5194/acp-12-10281-2012).
* M. Krämer et al., “A microphysics guide to cirrus clouds – Part 1: Cirrus types,” Atmospheric Chemistry and Physics, vol. 16, no. 5, pp. 3463–3483, Mar. 2016, [doi: 10.5194/acp-16-3463-2016](https://doi.org/10.5194/acp-16-3463-2016).

The predecessor of MAID was described in

* H. Bunz and R. Dlugi, “Numerical studies on the behaviour of aerosols in smog chambers,” Journal of Aerosol Science, vol. 22, no. 4, pp. 441–465, Jan. 1991, [doi: 10.1016/0021-8502(91)90004-2](https://www.doi.org/10.1016/0021-8502(91)90004-2).
