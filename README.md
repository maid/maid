![MAID logo](./doc_media/maid_logo.png)

# MAID: Model for Aerosol and Ice Dynamics
This is the online git repository for MAID.  
Please check the [documentation](https://maid.pages.hzdr.de/maid/), where most model characteristics are described.

Moreover the documentation provides you with certain guides to give you an easy access into the project:

* For a first impression you can follow the [quick start guide](https://maid.pages.hzdr.de/maid/page/guides/quick_start.html) and play around with the provided example files.
* To run the model with your own scenarios follow the [user guide](https://maid.pages.hzdr.de/maid/page/guides/user_guide.html).
* If you want to improve things and help to improve the model please read the [developer guide](https://maid.pages.hzdr.de/maid/page/guides/developer_guide.html).


## Model features
The model key features cover

* Atmospheric thermodynamics in the upper troposphere / lower stratosphere (UTLS)
* Balancing of trace gases, such as H2O, HNO3, HBr, HCl
* Three simultaneous aerosol types:
    - aqueous solution particles (incl. H2SO4)
    - solid aerosol (e.g. dust)
    - ice crystals
* Ice nucleation:
    - heterogeneous deposition freezing of solid aerosol particles
    - heterogeneous immersion freezing of aqueous solution particles
    - homogeneous freezing of soluble droplets
* Condensation / evaporation of aqueous solution particles
* Growth / sublimation of ice crystals
* Sedimentation of ice crystals
* Different trajectory types
    - internally calculated adiabatic updraft trajectories
    - external trajectories (read from file)
* Fluctuations of updraft velocity driven by gravity waves


## Contributing
This model was open-sourced in 2023 to provide an open cirrus box model for the scientific community.
It is free to use under the constrains of the AGPL license (see LICENSE file).
Everybody is welcome use and improve the model.
Please check the [developer guide](https://maid.pages.hzdr.de/maid/page/guides/developer_guide.html) if you plan to contribute to the code base.
