module masstransport_gas_solid
  !! Procedures describing the mass transport between solid and gas phase (HNO3).
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: HNO3_TRAP
  

contains


  subroutine HNO3_TRAP(RAD, PMASS, ZAHL, ZERO, EVAP, DMASS_EVAP, &
      &                TABS, P_HNO3, LATENT_HEAT_PROCESS, DT_STEP, KULIM, KOLIM, N_SOLUTES, DMASS_TRAP_SUM, &
      &                DIFF_COEFS, AV_VEL, DC_OUT)
  !! Calculate nitric acid trapping.
  !!
  !! ### Literature
  !!
  !! * B. Kärcher and M. M. Basko, “Trapping of trace gases in growing ice crystals”,
  !!   Journal of Geophysical Research: Atmospheres, vol. 109, no. D22, p. n, Nov. 2004, doi: 10.1029/2004jd005254.
  !! * B. Kärcher and C. Voigt, “Formation of nitric acid/water ice particles in cirrus clouds”, Geophysical Research Letters, vol. 33, no. 8, 2006, doi: 10.1029/2006gl025927.
  !! * TODO: Is this the right literature? In the old code was only a hint to Kärcher, but no specific paper mentioned.
  !!
  use PARAMETERS, only: N_BINS, N_COMPONENTS
  use solution_particle, only: get_solution_density, get_solution_surface_tension, get_p_sat_solutes
  use CONSTANTS, only: RGAS_CGS, PI, MOLAR_MASS_SOLUTES, ACCOMODATION_COEF_SOLUTES
  implicit none
  
  real(dp), intent(inout) :: RAD(*)
  real(dp), intent(inout) :: PMASS(0:N_COMPONENTS, *)
  real(dp), intent(in) :: ZAHL(*)
  logical, intent(in) :: ZERO(*)
  logical, intent(inout) :: EVAP(N_BINS)
  real(dp), intent(inout) :: DMASS_EVAP(N_BINS)
  real(dp), intent(in) :: TABS
  real(dp), intent(in) :: P_HNO3
  real(dp), intent(out) :: LATENT_HEAT_PROCESS  !! in J/cm³, latent heat from this procedure
  real(dp), intent(in) :: DT_STEP
  integer, intent(in) :: KULIM, KOLIM
  integer, intent(in) :: N_SOLUTES
  real(dp), intent(inout) :: DMASS_TRAP_SUM(*)
  real(dp), intent(in) :: DIFF_COEFS(N_COMPONENTS)  !! in cm²/s, diffusion coefficients of condensable components in gas
  real(dp), intent(in) :: AV_VEL(N_COMPONENTS)  !! in cm/s, mean thermal velocity of a gas phase molecule, acc. to Maxwells theory
  real(dp), intent(inout) :: DC_OUT(N_COMPONENTS)  !! in g/cm³, total change of mass concentration of condensable components in the gas phase
  
  integer :: K, KK
  integer :: NZERO
  real(dp), parameter :: EPS = 5e-3_dp
  real(dp) :: DMASS_TRAP_PARTICLE(N_BINS)
  real(dp) :: DMASS_TRAP_BIN(N_BINS)
  real(dp) :: X_WT(N_COMPONENTS)
  real(dp) :: SPECIFIC_LATENT_HEAT_SOLUTES(N_COMPONENTS)
  real(dp) :: SPECIFIC_LATENT_HEAT_HNO3(N_BINS)
  real(dp) :: P_SAT_SOLUTES(N_COMPONENTS)
  real(dp) :: DMASS_EVAP_HNO3(N_BINS)
  real(dp) :: PMASS_EVAP(0:N_COMPONENTS, N_BINS)
  real(dp) :: TIME_COND
  real(dp) :: DT_COND
  real(dp) :: C_FAKT  !! Factor to transform pressure to concentration
  real(dp) :: C_FAKTI
  real(dp) :: P_AKT
  real(dp) :: DC_OUT_EVAP
  real(dp) :: LATENT_HEAT_DT
  real(dp) :: CND0
  real(dp) :: CND
  real(dp) :: BETA
  real(dp) :: UPTAKE_COEFF
  real(dp) :: DC
  real(dp) :: DC_DT
  real(dp) :: DPHNO3
  real(dp) :: P_CHANGE
  real(dp) :: RCOND0
  real(dp) :: RCOND
  real(dp) :: RHO_PART
  real(dp) :: SIG_PART
  real(dp) :: TRAPP_EFF
  real(dp) :: VAP_PART0
  real(dp) :: VAP_PART
  real(dp) :: YYY
  
  ! Note: PA IN G/CM**3
  
  ! ------------------------------------------------------------------
  !
  ! PURE WATER:
  !
  ! ------------------------------------------------------------------
  !
  C_FAKT = 10.0_dp * MOLAR_MASS_SOLUTES(3) / (RGAS_CGS * TABS)
  C_FAKTI = 1.0_dp / C_FAKT
  P_AKT = P_HNO3
  DC_OUT_EVAP = DC_OUT(3)
  do K = KULIM, KOLIM
    DMASS_TRAP_BIN(K) = 0.0_dp
    PMASS_EVAP(0, K) = PMASS(0, K)
    PMASS_EVAP(3, K) = PMASS(3, K)
  end do
  !
  ! ------------------------------------------------------------------
  !
  ! ANNOTATION: TIME_COND=OWN TIME STEP (SMALLER!!!, DT_STEP IS THE
  !             GREATER STEP); SEE DT_COND=MIN(DT_COND,EPS*
  !             ABS(PMASS(0,K)/DMASS_TRAP_PARTICLE(K)),DT_STEP-TIME_COND); THIS
  !             MEANS, WENN THE CONDENSED MASS LOW, THEN IT
  !             COMES OUT FROM THE DO LOOP
  !
  ! ------------------------------------------------------------------
  !
  TIME_COND = 0.0_dp
  LATENT_HEAT_PROCESS = 0.0_dp
  !
  ! ------------------------------------------------------------------
  !
  ! CND=KNUDSEN NUMBER=LAMBDA/RP (DAHNEKE APPROACH 1983)
  ! LAMBDA=MEAN FREE PATH =2*D/_C
  ! RP=PARTICLE RADIUS
  ! D=DIFFUSION FACTOR
  ! _C=ROOT MEAN SQUARE VELOCITY= (3*R*T)**1/2
  ! VAP_PART0=EXPONENT FOR KELVIN FORMULA (P=P0*EXP(VAP_PART0/RP))
  !
  ! ------------------------------------------------------------------

  CND0 = 2.0_dp * DIFF_COEFS(3) / AV_VEL(3)
  RCOND0 = 4.0_dp * PI * DIFF_COEFS(3)
  VAP_PART0 = 2.0_dp * MOLAR_MASS_SOLUTES(3) / (RGAS_CGS * TABS)
  do while(TIME_COND .LT. DT_STEP)
    DT_COND = DT_STEP
    NZERO = 0
    do K = KULIM, KOLIM
      if (.not. ZERO(K)) then
        do KK = 1, N_SOLUTES
          X_WT(KK) = PMASS(KK, K) / PMASS(0, K)
          if (X_WT(KK) .LT. 0.0_dp) print*, '3273', K, KK, X_WT(KK), PMASS(KK, K), PMASS(0, K)
        end do
        RHO_PART = get_solution_density(TABS, X_WT)
        SIG_PART = get_solution_surface_tension(TABS, X_WT)
        call get_p_sat_solutes(TABS, X_WT, P_SAT_SOLUTES, SPECIFIC_LATENT_HEAT_SOLUTES)
        RAD(K) = (PMASS(0, K) / (RHO_PART * 4.0_dp / 3.0_dp * PI))**(1.0_dp / 3.0_dp)
        CND = CND0 / RAD(K)

        UPTAKE_COEFF = 0.635_dp - 0.00275_dp * TABS  ! TODO: where does this parametrization come from?
        BETA = ACCOMODATION_COEF_SOLUTES(3) * (CND + 1.0_dp)  &
          &    / (2.0_dp * CND * (CND + 1.0_dp) + ACCOMODATION_COEF_SOLUTES(3))
        TRAPP_EFF = UPTAKE_COEFF * BETA

        RCOND = RCOND0 * RAD(K) * TRAPP_EFF
        SPECIFIC_LATENT_HEAT_HNO3(K) = SPECIFIC_LATENT_HEAT_SOLUTES(3)
        if (VAP_PART0*SIG_PART / (RHO_PART * RAD(K)) .GT. 69.0_dp) then
          print*, 'HNO3 ', K, VAP_PART0, SIG_PART, RHO_PART, RAD(K), VAP_PART0 * SIG_PART / (RHO_PART * RAD(K))
        end if
        VAP_PART = P_SAT_SOLUTES(3) * exp(VAP_PART0 * SIG_PART / (RHO_PART * RAD(K)))
        !
        ! ------------------------------------------------------------------
        !
        ! ANNOTATION: KELVIN EFFECT FOR VAPOR PRESSURE OVER CURVED SURFACES
        !               DPH2O=DIFF. BULK PRESSURE
        !               DMASS_TRAP_PARTICLE(K)=MASS GROWTH
        !               DC_DT=CONCENTRATION CHANGE BECAUSE OF MASS GROWTH
        !
        ! ------------------------------------------------------------------
        !
        ! DC=P_AKT*C_FAKT
        DPHNO3 = P_AKT - VAP_PART
        DC = DPHNO3 * C_FAKT
        DMASS_TRAP_PARTICLE(K) = RCOND * DC
        DT_COND=min(DT_COND, &
        &           EPS * abs(PMASS(0, K) / DMASS_TRAP_PARTICLE(K)), &
        &           DT_STEP - TIME_COND)
      end if
    end do
    TIME_COND = TIME_COND + DT_COND
    DC_DT = 0.0_dp
    LATENT_HEAT_DT = 0.0_dp
    
    do K = KULIM, KOLIM
    !
    ! ------------------------------------------------------------------
    !
    ! ANNOTATION: PREPARES THE CALCULATION OF LN(R)
    !
    ! ------------------------------------------------------------------
    !
      DMASS_TRAP_BIN(K) = 0.0_dp
      !           RAD(K)=1.0_dp
      if (.not. ZERO(K)) then
        DMASS_TRAP_PARTICLE(K) = DMASS_TRAP_PARTICLE(K) * DT_COND
        YYY = PMASS(3, K)
        PMASS(3, K) = PMASS(3, K) + DMASS_TRAP_PARTICLE(K)
        if (PMASS(3, K) .LT. 0.0_dp) then
          DMASS_TRAP_PARTICLE(K) = -YYY
          PMASS(3, K) = 0.0_dp
          EVAP(K) = .FALSE.
        end if
        LATENT_HEAT_DT = LATENT_HEAT_DT + DMASS_TRAP_PARTICLE(K) * ZAHL(K) * SPECIFIC_LATENT_HEAT_HNO3(K)
        DC_DT = DC_DT + DMASS_TRAP_PARTICLE(K) * ZAHL(K)
        !
        ! ------------------------------------------------------------------
        !
        ! ANNOTATION: IF THE WATER EVAPORATES COMPLETLY FROM THE ICE
        !               PARTICLES, THEN ALL THE OTHER COMPONENTS (HNO3)
        !               SUPPOSE TO EVAPORATE ALSO 100%, EXCEPT H2SO4
        !
        ! ------------------------------------------------------------------
        !
        PMASS(0, K) = PMASS(0, K) + DMASS_TRAP_PARTICLE(K)
        DMASS_TRAP_BIN(K) = DMASS_TRAP_BIN(K) + DMASS_TRAP_PARTICLE(K) * ZAHL(K)
        do KK = 1, N_SOLUTES
          X_WT(KK) = PMASS(KK, K) / PMASS(0, K)
        end do
        RHO_PART = get_solution_density(TABS, X_WT)
        SIG_PART = get_solution_surface_tension(TABS, X_WT)
        RAD(K) = (PMASS(0, K) / (RHO_PART * 4.0_dp / 3.0_dp * PI))**(1.0_dp / 3.0_dp)
        call get_p_sat_solutes(TABS, X_WT, P_SAT_SOLUTES, SPECIFIC_LATENT_HEAT_SOLUTES)
      end if
    end do
    
    ! ------------------------------------------------------------------
    !
    ! ANNOTATION: IN THIS CASE: EVAPORATION OF THE WHOLE PARTICLE
    !
    ! ------------------------------------------------------------------
    DC_OUT(3) = DC_OUT(3) + DC_DT
    P_CHANGE = DC_DT * C_FAKTI
    P_AKT = P_AKT - P_CHANGE
    LATENT_HEAT_PROCESS = LATENT_HEAT_PROCESS + LATENT_HEAT_DT
  end do
  
  do K = KULIM, KOLIM
    DMASS_TRAP_SUM(K) = DMASS_TRAP_SUM(K) + DMASS_TRAP_BIN(K)
    DMASS_EVAP_HNO3(K) = 0.0_dp
    if (EVAP(K)) then
      if (DMASS_TRAP_SUM(K) .LE. 0.0_dp) then
        DMASS_EVAP_HNO3(K) = 0.0_dp
      else
        DMASS_EVAP_HNO3(K) = DMASS_TRAP_SUM(K) * DMASS_EVAP(K) / ZAHL(K)
      end if
      
      if ((PMASS_EVAP(3, K) + DMASS_EVAP_HNO3(K)) .LT. 0.0_dp) DMASS_EVAP_HNO3(K) = -PMASS_EVAP(3, K)
      LATENT_HEAT_PROCESS = LATENT_HEAT_PROCESS + DMASS_EVAP_HNO3(K) * ZAHL(K) * SPECIFIC_LATENT_HEAT_HNO3(K)
      PMASS(3, K) = PMASS_EVAP(3, K) + DMASS_EVAP_HNO3(K)
      PMASS(0, K) = PMASS_EVAP(0, K) + DMASS_EVAP_HNO3(K)
      do KK = 1, N_SOLUTES
        X_WT(KK) = PMASS(KK, K) / PMASS(0, K)
      end do
      RHO_PART = get_solution_density(TABS, X_WT)
      SIG_PART = get_solution_surface_tension(TABS, X_WT)
      RAD(K) = (PMASS(0, K) / RHO_PART * (4.0_dp / 3.0_dp * PI))**(1.0_dp / 3.0_dp)
      DC_OUT_EVAP = DC_OUT_EVAP + DMASS_EVAP_HNO3(K)*ZAHL(K)
      DC_OUT(3) = DC_OUT_EVAP
      P_CHANGE = DC_DT * C_FAKTI
      P_AKT = P_AKT - P_CHANGE
      call get_p_sat_solutes(TABS, X_WT, P_SAT_SOLUTES, SPECIFIC_LATENT_HEAT_SOLUTES)
    end if
    DMASS_TRAP_SUM(K) = DMASS_TRAP_SUM(K) + DMASS_EVAP_HNO3(K) * ZAHL(K)
  end do
  !     PRINT*, EVAP(1),' ',DMASS_TRAP_SUM(1),' ',DMASS_EVAP(1),' '
  !    &     ,PMASS_EVAP(3,1)
  LATENT_HEAT_PROCESS = LATENT_HEAT_PROCESS / MOLAR_MASS_SOLUTES(3)
  return
  end subroutine HNO3_TRAP

end module masstransport_gas_solid
