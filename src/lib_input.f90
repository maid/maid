module lib_input
  !! Procedures to parse input files.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: READ_INI
  
  
contains


  function get_value_start_index(LINE) result(IDX_START)
    !! Returns first index where a value is expected.
    implicit none
    character(len=*) :: LINE  !! current line read from initialization file
    integer :: IDX_START  !! starting index position in LINE where a value is expected
    
    ! assume value always comes after a double colon
    IDX_START = index(LINE, ':') + 1  
  end function get_value_start_index


  function get_value_end_index(LINE) result(IDX_END)
    !! Returns last index where a value is expected.
    implicit none
    character(len=*) :: LINE  !! current line read from initialization file
    integer :: IDX_END  !! ending index position in LINE where a value is expected
    
    ! assume value can be followed by a comment in brackets or is end of line
    if (index(LINE, '(') .ne. 0) then  ! last index of value
      IDX_END = index(LINE, '(')
      IDX_END = IDX_END - 1
    else
      IDX_END = len(LINE)
    end if
  end function get_value_end_index


  function read_real_value_from_line(LINE, VAL_FORMAT) result(VAL)
    !! Read value with type real from a given line.
    implicit none
    character(len=*) :: LINE  !! current line read from initialization file
    character(len=*), optional :: VAL_FORMAT  !! specification of the format in which the value is expected
    real(dp) :: VAL  !! value to be extracted from LINE
    
    integer :: IDX_START  !! starting index position in LINE where a value is expected
    integer :: IDX_END  !! ending index position in LINE where a value is expected
    
    IDX_START = get_value_start_index(LINE)
    IDX_END = get_value_end_index(LINE)
    
    if (present(VAL_FORMAT)) then
      read(LINE(IDX_START : IDX_END), VAL_FORMAT) VAL
    else
      read(LINE(IDX_START : IDX_END), *) VAL
    end if
  end function read_real_value_from_line


  function read_integer_value_from_line(LINE, VAL_FORMAT) result(VAL)
    !! Read value with type integer from a given line.
    implicit none
    character(len=*) :: LINE  !! current line read from initialization file
    character(len=*), optional :: VAL_FORMAT  !! specification of the format in which the value is expected
    integer :: VAL  !! value to be extracted from LINE
    
    integer :: IDX_START  !! starting index position in LINE where a value is expected
    integer :: IDX_END  !! ending index position in LINE where a value is expected
    
    IDX_START = get_value_start_index(LINE)
    IDX_END = get_value_end_index(LINE)
    
    if (present(VAL_FORMAT)) then
      read(LINE(IDX_START : IDX_END), VAL_FORMAT) VAL
    else 
      read(LINE(IDX_START : IDX_END), *) VAL
    end if
  end function read_integer_value_from_line


  function read_logical_value_from_line(LINE, VAL_FORMAT) result(VAL)
    !! Read value with type logical from a given line.
    implicit none
    character(len=*) :: LINE  !! current line read from initialization file
    character(len=*), optional :: VAL_FORMAT  !! specification of the format in which the value is expected
    logical :: VAL  !! value to be extracted from LINE
    
    integer :: IDX_START  !! starting index position in LINE where a value is expected
    integer :: IDX_END  !! ending index position in LINE where a value is expected
    
    IDX_START = get_value_start_index(LINE)
    IDX_END = get_value_end_index(LINE)
    
    if (present(VAL_FORMAT)) then
      read(LINE(IDX_START : IDX_END), VAL_FORMAT) VAL
    else
      read(LINE(IDX_START : IDX_END), *) VAL
    end if
  end function read_logical_value_from_line


  function read_character_value_from_line(LINE, VAL_FORMAT) result(VAL)
    !! Read value with type character from a given line.
    implicit none
    character(len=*) :: LINE  !! current line read from initialization file
    character(len=*), optional :: VAL_FORMAT  !! specification of the format in which the value is expected
    character(len=:), allocatable :: VAL  !! value to be extracted from LINE
    
    integer :: VAL_LENGTH  !! character length of VAL
    integer :: IDX_START  !! starting index position in LINE where a value is expected
    integer :: IDX_END  !! ending index position in LINE where a value is expected
    
    IDX_START = get_value_start_index(LINE)
    IDX_END = get_value_end_index(LINE)
    VAL_LENGTH = IDX_END - IDX_START + 1
    
    allocate(character(len=VAL_LENGTH) :: VAL)
    
    if (present(VAL_FORMAT)) then
      read(LINE(IDX_START : IDX_END), VAL_FORMAT) VAL
    else
      read(LINE(IDX_START : IDX_END), *) VAL
    end if
    
    ! in case of leading/trailing white space, repeat with trimmed length
    if (len(trim(VAL)) .ne. VAL_LENGTH) then
      IDX_START = index(LINE, trim(VAL))
      IDX_END = IDX_START + len(trim(VAL)) - 1
      VAL_LENGTH = len(trim(VAL))
      
      deallocate(VAL)
      allocate(character(len=VAL_LENGTH) :: VAL)
      
      if (present(VAL_FORMAT)) then
        read(LINE(IDX_START : IDX_END), VAL_FORMAT) VAL
      else
        read(LINE(IDX_START : IDX_END), '(A)') VAL
      end if
    end if
  end function read_character_value_from_line


  ! ------------------------------------------------------------------
  !
  ! S18:
  !
  ! READ INIT FILE
  !
  ! ------------------------------------------------------------------
  subroutine READ_INI(FILEUNIT_INI, FILENAME_INI, DT_STEP, DT_OUT, TEMP_START, TEMP_FINAL,  &
      &               PRESS_START, PRESS_FINAL, DURATION, RUNTIME_EXTENSION, UPDRAFT, &
      &               OUTPUT_FORMAT_DATA, OUTPUT_FORMAT_SD_LIQ, OUTPUT_FORMAT_SD_IN, OUTPUT_FORMAT_SD_ICE,  &
      &               FORM_NUCL, TIME_FORM, P_UNIT, TRAJ_HEADER_COUNT,  &
      &               TRAJ_COLUMN_COUNT, TRAJ_COLIDX_P, TRAJ_COLIDX_TGAS, FORCED_T, FILENAME_TRAJ,  &
      &               PART_PRESENT, RMIN, RMAX, RMINMAX_UNIT, N_SOLUTES, CALC_EQUIL,  &
      &               Z_H2O_TOT, Z_H2O_UNIT, R_H2O, R_H2O_UNIT, SIG_H2O, XWT_H2SO4_H2O, MASS_FRACTIONS_H2O,  &
      &               Z_ICE_TOT, Z_ICE_UNIT, R_ICE, R_ICE_UNIT, SIG_ICE, XWT_H2SO4_ICE, MASS_FRACTIONS_ICE,  &
      &               N_PCOND, PCOND_UNIT, PCOND_INI, IS_PCOND_CONST, PCOND_INI_UNIT,  &
      &               IS_PCOND_INI_DEFINED, TRAJ_COLIDX_PCOND, IN_TYPE, Z_IN_TOT, Z_IN_UNIT,  &
      &               R_IN, R_IN_UNIT, SIGMA_IN, RHO_IN, DZ,  &
      &               SEDFACTOR, DO_SEDIMENTATION, &
      &               USE_GRAVITY_WAVE_FORCING, SIGMA_DELTA_W, LATITUDE_ANGLE, GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC, &
      &               TRAJ_COLIDX_WALLFLUX_PCOND,  &
      &               KPAR_H2OS, KPAR_AW_P, KPAR_ICE, PAR_HET_FREEZE, &
      &               SEED, COMPILED_WITH_G)
    !! Read input file which specifies a MAID scenario.
    !!
    !! **Note:** The file format doesn't follow an official standard.
    !! To bind a VALUE to a KEY use the format
    !! ```text
    !! [KEY]: VALUE (OPTIONAL_COMMENT)
    !! ```
    !!
    use PARAMETERS, only: N_COMPONENTS, KMAX
    use lib_string, only: to_uppercase, character_to_logical
    use lib_path, only: convert_abspath_to_relpath, is_relative_path
    implicit none
    
    ! general
    integer, intent(in) :: FILEUNIT_INI  !! file index for .init file
    character(len=*) :: FILENAME_INI  !! filename of .init file, which specifies parameters for the model run
    character(len=30), intent(out) :: OUTPUT_FORMAT_DATA  !! specifies the main data output format
    character(len=30), intent(out) :: OUTPUT_FORMAT_SD_LIQ  !! specifies the liquid size distribution output format
    character(len=30), intent(out) :: OUTPUT_FORMAT_SD_IN  !! specifies the ice-nucleating particle size distribution output format
    character(len=30), intent(out) :: OUTPUT_FORMAT_SD_ICE  !! specifies the ice crystal size distribution output format
    character(len=15), intent(out) :: FORM_NUCL  !! heterogeneous or homogeneous freezing
    character(len=4), intent(out) :: CALC_EQUIL  !! specifies if equilibrium calculation is applied on solution particles
    real(dp), intent(out) :: DT_STEP  !! in sec, time increment for model-internal time loop
    real(dp), intent(out) :: DT_OUT  !! in sec, time increment for output values
    character(len=255) :: CWD  !! current working directory
    integer :: SEED(:)  !! Seed for pseudorandom number generation
    
    ! parameterizations
    integer, intent(out) :: KPAR_H2OS  !! Parametrization identifier for water vapor saturation pressure with respect to water, defined through [PAR-PSAT-WATER]
    integer, intent(out) :: KPAR_AW_P  !! Parametrization identifier for water activity, defined through [PAR-WATER-ACTIVITY]
    integer, intent(out) :: KPAR_ICE  !! Parametrization identifier for water vapor saturation pressure with respect to ice, defined through [PAR-PSAT-ICE]
    character(len=40), intent(out) :: PAR_HET_FREEZE  !! paramtrization for heterogeneous ice formation
    
    ! internal trajectory
    real(dp), intent(out) :: TEMP_START  !! in K or °C, interpretation of unit depends if temperature is bigger/smaller than 100
    real(dp), intent(out) :: TEMP_FINAL  !! in K, final air temperature
    real(dp), intent(out) :: PRESS_START  !! in Pa
    real(dp), intent(inout) :: PRESS_FINAL  !! in Pa
    real(dp), intent(inout) :: UPDRAFT  !! in cm/s, updraft velocity
    integer, intent(inout) :: DURATION  !! in sec
    real(dp), intent(inout) :: RUNTIME_EXTENSION  !! in sec
    
    ! particles
    character(len=4), intent(out) :: RMINMAX_UNIT  !! unit for the biggest and smallest radius (e.g. m, mym, ...)
    real(dp), intent(out) :: RMIN, RMAX  !! in units of RMINMAX_UNIT, boundaries for lognormal size distributions
    
    ! liquid particles
    integer, intent(out) :: N_SOLUTES  !! number of compounds in the solution particle (1 = H2SO4, 2 =  + H2O, 3 =  + HNO3...)
    character(len=8), intent(out) :: Z_H2O_UNIT  !! unit of the total number concentration of liquid particle size distribution
    real(dp), intent(out) :: Z_H2O_TOT  !! in #/cm³, total number concentration of liquid particles
    character(len=4), intent(out) :: R_H2O_UNIT  !! unit of the geometric mean radius of liquid particle size distribution
    real(dp), intent(out) :: R_H2O  !! in cm, median radius for the lognormal size distribution of initial liquid particles
    real(dp), intent(out) :: SIG_H2O  !! width parameter for the lognormal size distribution of initial liquid particles
    real(dp), intent(out) :: XWT_H2SO4_H2O  !! mass fraction of sulfuric acid in solution particles
    real(dp), intent(out) :: MASS_FRACTIONS_H2O(N_COMPONENTS)  !! mass fraction of component KK in water
    
    ! ice particles
    character(len=8), intent(out) :: Z_ICE_UNIT  !! unit of the total number concentration of ice crystal size distribution
    real(dp), intent(out) :: Z_ICE_TOT  !! in #/cm³, total number concentration of ice crystals
    character(len=4), intent(out) :: R_ICE_UNIT  !! unit of the geometric mean radius of ice crystal size distribution
    real(dp), intent(out) :: R_ICE  !! in cm, median radius for the lognormal size distribution of initial ice crystals
    real(dp), intent(out) :: SIG_ICE  !! width parameter for the lognormal size distribution of initial ice crystals
    real(dp), intent(out) :: XWT_H2SO4_ICE  !! mass fraction of sulfuric acid in ice crystals
    
    ! ice-nucleating particles
    character(len=20), intent(out) :: IN_TYPE  !! specifies the freezing behavior for immersion freezing with PAR_HET_FREEZE=KL2003
    character(len=8), intent(out) :: Z_IN_UNIT  !! unit of the total number concentration of ice-nucleating particle size distribution
    real(dp), intent(out) :: Z_IN_TOT  !! in #/cm³, total number concentration of ice-nucleating particles
    character(len=4), intent(out) :: R_IN_UNIT  !! unit of the geometric mean radius of ice-nucleating particle size distribution
    real(dp), intent(out) :: R_IN  !! in cm, median radius for the lognormal size distribution of initial ice-nucleating particles
    real(dp), intent(out) :: SIGMA_IN  !! width parameter for the lognormal size distribution of initial ice-nucleating particles
    real(dp), intent(out) :: RHO_IN  !! in g/cm³, density of ice-nucleating particles
    
    ! gases
    integer, intent(out) :: N_PCOND  !! number of condensable components
    character(len=4), intent(out) :: PCOND_INI_UNIT(N_COMPONENTS)  !! unit for the initial vapor pressure concentration
    real(dp), intent(out) :: PCOND_INI(N_COMPONENTS)  !! in PCOND_INI_UNIT, initial values of P_COND(KK)
    
    ! .traj content
    character(len=200), intent(inout) :: FILENAME_TRAJ  !! Filename of .traj file, which can be used to pass external trajectories
    integer, intent(out) :: TRAJ_HEADER_COUNT  !! number of header rows in .traj file
    integer, intent(out) :: TRAJ_COLUMN_COUNT  !! number of columns in .traj file
    character(len=10), intent(out) :: TIME_FORM  !! format of the time column in .traj file
    character(len=4), intent(out) :: P_UNIT  !! pressure unit (PA, HPA, BAR...)
    integer, intent(out) :: TRAJ_COLIDX_P  !! column index for pressure in the .traj file
    integer, intent(out) :: TRAJ_COLIDX_WALLFLUX_PCOND  !! column index for water wall flux in the .traj file
    integer, intent(out) :: TRAJ_COLIDX_TGAS  !! column index for temperature in the .traj file
    character(len=4), intent(out) :: PCOND_UNIT  !! unit for the vapor pressure concentration
    integer, intent(inout) :: TRAJ_COLIDX_PCOND(N_COMPONENTS)  !! column indices for vapor pressures in the .traj file
    
    ! sedimentation
    real(dp), intent(out) :: DZ  !! height ot the air parcel
    real(dp), intent(out) :: SEDFACTOR  !! dimensionless, strength of sedimentation; 0<SEDFACTOR<1
    
    logical, intent(inout) :: FORCED_T  !! if .true. an adiabatic temperature trend is enforced, hence latent heat effects are neglected
    logical, intent(inout) :: PART_PRESENT  !! flag to indicate if any particles are present
    logical, intent(inout) :: IS_PCOND_CONST(N_COMPONENTS)  !! .true. if a vapor pressure of a condensable component must be constant
    logical, intent(inout) :: IS_PCOND_INI_DEFINED(N_COMPONENTS)  !! .true. if a valid initial value was specified in .init file
    logical, intent(out) :: DO_SEDIMENTATION  !! flag indicates if sedimentation is calculated
    
    ! Gravity wave driven mesoscale fluctuations
    logical, intent(inout) :: USE_GRAVITY_WAVE_FORCING  !! Determine if gavity wave forcing gets applied
    real(dp), intent(out) :: SIGMA_DELTA_W  !! in cm s⁻¹, standard deviation of the vertical wind speed fluctuations, width parameter for the probability density function
    real(dp), intent(out) :: LATITUDE_ANGLE  !! in degrees, latitude angle, specified as 0° at north pole, 180° at south pole.
    real(dp), intent(inout) :: GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC  !! if specified value gets used as static autocorrelation time of updraft fluctuations
    
    logical, intent(in) :: COMPILED_WITH_G  !! debug flag
    
    integer :: K  !! index for size channels, 0<=K<=(N_BINS-1)
    real(dp) :: MASS_FRACTIONS_ICE(N_COMPONENTS)  !! mass fraction of component KK in the ice phase
    integer :: FILESTATUS  !! variable to catch io status when reading/writing files
    integer :: II  !! character index in LINE
    integer :: IND1, IND2, IND3  !! character index in LINE, used to catch the value of a key-value pair
    character(len=200) :: LINE  !! current line read from initialization file
    
    
    if (COMPILED_WITH_G) print*, "Start reading of initialization file: ", trim(FILENAME_INI)
    
    lineloop: do
      read(FILEUNIT_INI, '(A200)', iostat=FILESTATUS) LINE
      if (FILESTATUS .ne. 0) exit lineloop
      
      if ((index(LINE, '[T-ANFANG]') .ne. 0) .or. (index(LINE, '[T-START]') .ne. 0)) then
        TEMP_START = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'Start temperature: ', TEMP_START
        
      else if (index(LINE, '[T-END]') .ne. 0) then
        TEMP_FINAL = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'Final temperature: ', TEMP_FINAL
        
      else if ((index(LINE, '[P-ANFANG]') .ne. 0) .or. (index(LINE, '[P-START]') .ne. 0)) then
        PRESS_START = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'Start pressure: ', PRESS_START
        
      else if ((index(LINE, '[P-ENDE]') .ne. 0) .or. (index(LINE, '[P-END]') .ne. 0)) then
        PRESS_FINAL = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'Final pressure: ',  PRESS_FINAL
        
      else if ((index(LINE, '[DAUER]') .ne. 0) .or. (index(LINE, '[DURATION]') .ne. 0)) then
        DURATION = read_integer_value_from_line(LINE, val_format='(I7)')
        if (COMPILED_WITH_G) print*, 'Duration: ', DURATION
        
      else if (index(LINE, '[RUNTIME_EXTENSION]') .ne. 0) then
        RUNTIME_EXTENSION = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'RUNTIME_EXTENSION: ', RUNTIME_EXTENSION
        
      else if (index(LINE, '[TIME-RESOLUTION]') .ne. 0) then
        DT_STEP = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'TIME-RESOLUTION', DT_STEP
        
      else if (index(LINE, '[OUTPUT-TIME-RESOLUTION]') .ne. 0) then
        DT_OUT = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'OUTPUT-TIME-RESOLUTION', DT_OUT
        
      else if (index(LINE, '[UPDRAFT-VELOCITY]') .ne. 0) then
        UPDRAFT = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'UPDRAFT-VELOCITY', UPDRAFT
        
      else if (index(LINE, '[PAR-PSAT-WATER]') .ne. 0) then
        KPAR_H2OS = read_integer_value_from_line(LINE, val_format='(I7)')
        if (COMPILED_WITH_G) print*, 'Parameterization psat-water: ', KPAR_H2OS
        
      else if (index(LINE, '[PAR-PSAT-ICE]') .ne. 0) then
        KPAR_ICE = read_integer_value_from_line(LINE, val_format='(I7)')
        if (COMPILED_WITH_G) print*, 'Parameterization psat-ice: ', KPAR_ICE
        
      else if (index(LINE, '[PAR-WATER-ACTIVITY]') .ne. 0) then
        KPAR_AW_P = read_integer_value_from_line(LINE, val_format='(I7)')
        if (COMPILED_WITH_G) print*, 'Parameterization water activity: ', KPAR_AW_P
        
      else if (index(LINE, '[PAR-HET-FREEZE]') .ne. 0) then
        PAR_HET_FREEZE = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'Parameterization het. freezing: ', PAR_HET_FREEZE
        
      else if (index(LINE, '[OUTPUT]') .ne. 0) then
        OUTPUT_FORMAT_DATA = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'OUTPUT_FORMAT_DATA: ',  OUTPUT_FORMAT_DATA
        
      else if (index(LINE, '[OUTPUT-SD-LIQ]') .ne. 0) then
        OUTPUT_FORMAT_SD_LIQ = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'OUTPUT_FORMAT_SD_LIQ: ', OUTPUT_FORMAT_SD_LIQ
        
      else if (index(LINE, '[OUTPUT-SD-IN]') .ne. 0) then
        OUTPUT_FORMAT_SD_IN = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'OUTPUT_FORMAT_SD_IN: ', OUTPUT_FORMAT_SD_IN
        
      else if (index(LINE, '[OUTPUT-SD-ICE]') .ne. 0) then
        OUTPUT_FORMAT_SD_ICE = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'OUTPUT_FORMAT_SD_ICE: ', OUTPUT_FORMAT_SD_ICE
        
      else if (index(LINE, '[FREEZING]') .ne. 0) then
        FORM_NUCL = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'FREEZING: ', FORM_NUCL
        
      else if (index(LINE, '[TIME-UNIT]') .ne. 0) then
        TIME_FORM = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'TIME-UNIT: ', TIME_FORM
        
      else if (index(LINE, '[PRESSURE-UNIT]') .ne. 0) then
        P_UNIT = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'PRESSURE-UNIT: ',  P_UNIT
        
      else if (index(LINE, '[TRAJECTORY-FORMAT]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 10
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) TRAJ_HEADER_COUNT, TRAJ_COLUMN_COUNT, TRAJ_COLIDX_P,  &
          &                      TRAJ_COLIDX_WALLFLUX_PCOND, TRAJ_COLIDX_TGAS, FORCED_T
        if (COMPILED_WITH_G) print*, 'TRAJECTORY-FORMAT: ', TRAJ_HEADER_COUNT, TRAJ_COLUMN_COUNT, TRAJ_COLIDX_P, &
        &                             TRAJ_COLIDX_WALLFLUX_PCOND, TRAJ_COLIDX_TGAS, FORCED_T
        
      else if (index(LINE, '[TRAJECTORY-FILE]') .ne. 0) then
        FILENAME_TRAJ = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'TRAJECTORY-FILE: ', FILENAME_TRAJ
        
      else if (index(LINE, '[PARTICLE]') .ne. 0) then
        PART_PRESENT = character_to_logical(read_character_value_from_line(LINE))
        if (COMPILED_WITH_G) print*, 'PARTICLE: ', PART_PRESENT
        
      else if (index(LINE, '[NUMBER-COMPONENTS]') .ne. 0) then
        N_SOLUTES = read_integer_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'NUMBER-COMPONENTS: ', N_SOLUTES
        
      else if (index(LINE, '[NUMBER-BINS]') .ne. 0) then
        KMAX = read_integer_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'NUMBER-BINS: ', KMAX
        
      else if (index(LINE, '[RADIUS]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 15
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) RMIN, RMAX, RMINMAX_UNIT
        if (COMPILED_WITH_G) print*, 'RADIUS: ',  RMIN, RMAX, RMINMAX_UNIT
        
      else if (index(LINE, '[EQUILIBRIUM]') .ne. 0) then
        CALC_EQUIL = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'EQUILIBRIUM: ', CALC_EQUIL
        
      else if (index(LINE, '[AEROSOL-CONCENTRATION]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 20
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) Z_H2O_TOT, Z_H2O_UNIT
        if (COMPILED_WITH_G) print*, 'AEROSOL-CONCENTRATION: ', Z_H2O_TOT, Z_H2O_UNIT
        
      else if (index(LINE, '[AEROSOL-MEAN-RADIUS]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 15
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) R_H2O, R_H2O_UNIT
        if (COMPILED_WITH_G) print*, 'AEROSOL-MEAN-RADIUS: ', R_H2O, R_H2O_UNIT
        
      else if (index(LINE, '[AEROSOL-SIGMA]') .ne. 0) then
        SIG_H2O = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'AEROSOL-SIGMA: ',  SIG_H2O
        
      else if (index(LINE, '[AEROSOL-AMOUNT-H2SO4]') .ne. 0) then
        XWT_H2SO4_H2O = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'AEROSOL-AMOUNT-H2SO4: ', XWT_H2SO4_H2O
        
      else if ((index(LINE, '[AEROSOL-COMPOSITION]') .ne. 0) .and. (XWT_H2SO4_H2O.LT.0)) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 30
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) (MASS_FRACTIONS_H2O(K), K = 1, N_SOLUTES)
        if (COMPILED_WITH_G) print*, 'AEROSOL-COMPOSITION: ', (MASS_FRACTIONS_H2O(K), K = 1, N_SOLUTES)
        
      else if (index(LINE, '[ICE-CONCENTRATION]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 20
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) Z_ICE_TOT, Z_ICE_UNIT
        if (COMPILED_WITH_G) print*, 'ICE-CONCENTRATION: ', Z_ICE_TOT, Z_ICE_UNIT
        
      else if (index(LINE, '[ICE-MEAN-RADIUS]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 15
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) R_ICE, R_ICE_UNIT
        if (COMPILED_WITH_G) print*, 'ICE-MEAN-RADIUS: ', R_ICE, R_ICE_UNIT
        
      else if (index(LINE, '[ICE-SIGMA]') .ne. 0) then
        SIG_ICE = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'ICE-SIGMA: ',  SIG_ICE
        
      else if (index(LINE, '[ICE-AMOUNT-H2SO4]') .ne. 0) then
        XWT_H2SO4_ICE = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'ICE-AEROSOL-AMOUNT-H2SO4: ', XWT_H2SO4_ICE
        
      else if (index(LINE, '[ICE-COMPOSITION]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 15
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) (MASS_FRACTIONS_ICE(K), K = 1, N_SOLUTES)
        if (COMPILED_WITH_G) print*, 'ICE-COMPOSITION: ', (MASS_FRACTIONS_ICE(K), K = 1, N_SOLUTES)
        
      else if (index(LINE, '[COMP]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 30
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) N_PCOND, PCOND_UNIT
        if (COMPILED_WITH_G) print*, 'COMP: ', N_PCOND, PCOND_UNIT
        
      else if ((index(LINE, '[H2O]') .ne. 0) .and. ((-1*N_PCOND + 1) .ge. 2)) then
        IND2 = 0
        K = 2
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 30
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) PCOND_INI(K), IS_PCOND_CONST(K), PCOND_INI_UNIT(K)
        IS_PCOND_INI_DEFINED(K) = PCOND_INI(K) .ge. 0.0_dp
        if (IS_PCOND_INI_DEFINED(K)) then
          PCOND_INI_UNIT(K) = to_uppercase(PCOND_INI_UNIT(K))
        else
          IS_PCOND_CONST(K) = .FALSE.
        end if
        if (COMPILED_WITH_G) print*, 'H2O: ', PCOND_INI(2), IS_PCOND_CONST(2), PCOND_INI_UNIT(2), IS_PCOND_INI_DEFINED(2)
        
      else if ((index(LINE, '[HNO3]') .ne. 0) .and. ((-1*N_PCOND + 1) .ge. 3)) then
        IND2 = 0
        K = 3
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 30
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) PCOND_INI(K), IS_PCOND_CONST(K), PCOND_INI_UNIT(K)
        IS_PCOND_INI_DEFINED(K) = PCOND_INI(K) .ge. 0.0_dp
        if (IS_PCOND_INI_DEFINED(K)) then
          PCOND_INI_UNIT(K) = to_uppercase(PCOND_INI_UNIT(K))
        else
          IS_PCOND_CONST(K) = .FALSE.
        end if
        if (COMPILED_WITH_G) print*, 'HNO3: ', PCOND_INI(3), IS_PCOND_CONST(3), PCOND_INI_UNIT(3), IS_PCOND_INI_DEFINED(3)
        
      else if ((index(LINE, '[HCL]') .ne. 0) .and. ((-1*N_PCOND + 1) .ge. 4)) then
        IND2 = 0
        K = 4
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 30
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) PCOND_INI(K), IS_PCOND_CONST(K), PCOND_INI_UNIT(K)
        IS_PCOND_INI_DEFINED(K) = PCOND_INI(K) .ge. 0.0_dp
        if (IS_PCOND_INI_DEFINED(K)) then
          PCOND_INI_UNIT(K) = to_uppercase(PCOND_INI_UNIT(K))
        else
          IS_PCOND_CONST(K) = .FALSE.
        end if
        if (COMPILED_WITH_G) print*, 'HCL: ', PCOND_INI(4), IS_PCOND_CONST(4), PCOND_INI_UNIT(4)
        
      else if ((index(LINE, '[HBR]') .ne. 0) .and. ((-1*N_PCOND + 1) .ge. 5)) then
        IND2 = 0
        K = 5
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 30
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) PCOND_INI(K), IS_PCOND_CONST(K), PCOND_INI_UNIT(K)
        IS_PCOND_INI_DEFINED(K) = PCOND_INI(K) .ge. 0.0_dp
        if (IS_PCOND_INI_DEFINED(K)) then
          PCOND_INI_UNIT(K) = to_uppercase(PCOND_INI_UNIT(K))
        else
          IS_PCOND_CONST(K) = .FALSE.
        end if
        if (COMPILED_WITH_G) print*, 'HBR: ', PCOND_INI(5), IS_PCOND_CONST(5), PCOND_INI_UNIT(5)
        
      else if ((index(LINE, '[ASSESSMENTS]') .ne. 0) .and. ((-1*N_PCOND + 1) .ge. 2)) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 20
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) (TRAJ_COLIDX_PCOND(K), K = 2, -1*N_PCOND + 1)
        if (COMPILED_WITH_G) print*, 'ASSESSMENTS: ', (TRAJ_COLIDX_PCOND(K), K = 2, -1*N_PCOND + 1)
        
      else if (index(LINE, '[IN_TYPE]') .ne. 0) then
        IN_TYPE = read_character_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'IN_TYPE: ', IN_TYPE
        
      else if (index(LINE, '[IN-CONCENTRATION]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 20
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) Z_IN_TOT, Z_IN_UNIT
        if (COMPILED_WITH_G) print*, 'IN-CONCENTRATION: ',  Z_IN_TOT, Z_IN_UNIT
        
      else if (index(LINE, '[IN-MEAN-RADIUS]') .ne. 0) then
        IND2 = 0
        do ii = index(LINE, ':') + 1, index(LINE, ':') + 10
          if (LINE(ii:ii) .ne. ' ' .and. IND2 .eq. 0) then
            IND2 = ii
          end if
        end do
        IND3 = IND2 + 15
        if (index(LINE(IND2:), '(') .gt. 0) IND3 = IND2 + index(LINE(IND2:), '(') - 2
        read(LINE(IND2:IND3), *) R_IN, R_IN_UNIT
        if (COMPILED_WITH_G) print*, 'IN-MEAN-RADIUS: ',  R_IN, R_IN_UNIT
        
      else if (index(LINE, '[IN-SIGMA]') .ne. 0) then
        SIGMA_IN = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'IN-SIGMA: ',  SIGMA_IN
        
      else if (index(LINE, '[IN-DENSITY]') .ne. 0) then
        RHO_IN = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'IN-DENSITY: ', RHO_IN
        
      else if (index(LINE, '[SEDIMENTATION]') .ne. 0) then
        DO_SEDIMENTATION = character_to_logical(read_character_value_from_line(LINE))
        if (COMPILED_WITH_G) print*, 'SEDIMENTATION: ', DO_SEDIMENTATION
        
      else if (index(LINE, '[SEDFACTOR]') .ne. 0) then
        SEDFACTOR = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'SEDFACTOR: ',  SEDFACTOR
        
      else if (index(LINE, '[DZ]') .ne. 0) then
        DZ = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, 'DZ: ',  DZ
        
      else if (index(LINE, '[USE_GRAVITY_WAVE_FORCING]') .ne. 0) then
        USE_GRAVITY_WAVE_FORCING = character_to_logical(read_character_value_from_line(LINE))
        if (COMPILED_WITH_G) print*, "USE_GRAVITY_WAVE_FORCING: ", USE_GRAVITY_WAVE_FORCING
        
      else if (index(LINE, '[SIGMA_DELTA_W]') .ne. 0) then
        SIGMA_DELTA_W = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, "SIGMA_DELTA_W: ", SIGMA_DELTA_W
        
      else if (index(LINE, '[LATITUDE_ANGLE]') .ne. 0) then
        LATITUDE_ANGLE = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, "LATITUDE_ANGLE: ", LATITUDE_ANGLE
        
      else if (index(LINE, '[GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC]') .ne. 0) then
        GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC = read_real_value_from_line(LINE)
        if (COMPILED_WITH_G) print*, "GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC: ", GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC
   
      else if (index(LINE, '[SEED]') .ne. 0) then
        IND1 = get_value_start_index(LINE)
        IND2 = get_value_end_index(trim(LINE))
        read(LINE(IND1:IND2), *) SEED(1:size(SEED))
        if (COMPILED_WITH_G) print*, 'SEED: ', SEED
        
      end if
    end do lineloop
    
    print*, ''
    print*, 'Finished reading of initialization file.'
    print*, ''
    
    ! translate .traj paths which are relative to .init path to be relative to current working directory
    if (len_trim(FILENAME_TRAJ) .gt. 0 .and. is_relative_path(FILENAME_TRAJ)) then
      ! relative .traj paths in .ini file must be relative to the .ini file
      ! → translate the relative path to an absolute path
      
      ! convert relative .traj file as absolute path
      FILENAME_TRAJ = FILENAME_INI(: index(FILENAME_INI, '/', back=.true.)) // trim(FILENAME_TRAJ)
      
      ! convert absolute .traj path to relative path, relative to CWD
      call getcwd(CWD)
      CWD = trim(CWD) // '/'  ! mark CWD as path by adding trailing path separator
      FILENAME_TRAJ = convert_abspath_to_relpath(FILENAME_TRAJ, relative_to = trim(CWD))
    end if
  end subroutine READ_INI


end module lib_input
