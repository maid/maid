program MAID
!! 
!! # MAID: Model for Aerosol and Ice Dynamics
!!
!! ---
!! Copyright notice:
!!
!! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
!! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
!!
!! This program is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Affero General Public License as
!! published by the Free Software Foundation, either version 3 of the
!! License, or (at your option) any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Affero General Public License for more details.
!!
!! In the projects base directory you can find a copy of the
!! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
!! ---
!!
!! ### Selectable parametrizations from the .init file
!!
!! KPAR_AW_P: Water activity in sulfuric acid aqueous solutions.
!!
!! KPAR_AW_P | Literature reference                                                                            
!! ----------|-------------------------------------------------------------------------------------------------
!! 1         | K. S. Carslaw, B. Luo, and T. Peter, “An analytic expression for the composition of             
!!           | aqueous HNO3-H2SO4 stratospheric aerosols including gas phase removal of HNO3”,                 
!!           | Geophysical Research Letters, vol. 22, no. 14, pp. 1877–1880, Jul. 1995, doi: 10.1029/95gl01668.
!! 2         | Q. Shi, J. T. Jayne, C. E. Kolb, D. R. Worsnop, and P. Davidovits, “Kinetic model for reaction  
!!           | of ClONO2with H2O and HCl and HOCl with HCl in sulfuric acid solutions,” Journal of Geophysical 
!!           | Research: Atmospheres, vol. 106, no. D20, pp. 24259–24274, Oct. 2001, doi: 10.1029/2000jd000181.
!! 3         | J. Schneider et al., “High homogeneous freezing onsets of sulfuric acid aerosol at cirrus
!!           | temperatures,” vol. 21, no. 18, pp. 14403–14425, Sep. 2021, doi: 10.5194/acp-21-14403-2021.
!!
!! ---
!! KPAR_H2OS: Water vapor saturation pressure over (supercooled) liquid water
!!
!! KPAR_H2OS | Literature reference
!! ----------|-------------------------------------------------------------------------------------------------
!! 1         | A. Tabazadeh, O. B. Toon, S. L. Clegg, and P. Hamill, “A new parameterization of H2SO4/H2O
!!           | aerosol composition: Atmospheric implications,” Geophysical Research Letters, vol. 24,
!!           | no. 15, pp. 1931–1934, Aug. 1997, doi: 10.1029/97gl01879.
!! 2         | D. M. Murphy and T. Koop, “Review of the vapour pressures of ice and supercooled water for
!!           | atmospheric applications,” Quarterly Journal of the Royal Meteorological Society, vol. 131,
!!           | no. 608, pp. 1539–1565, Apr. 2005, doi: 10.1256/qj.04.94.
!! 3         | M. Nachbar, D. Duft, and T. Leisner, “The vapor pressure of liquid and solid water phases at
!!           | conditions relevant to the atmosphere,” The Journal of Chemical Physics, vol. 151, no. 6,
!!           | p. 64504, Aug. 2019, doi: 10.1063/1.5100364.
!! 4         | D. Sonntag, “Advancements in the field of hygrometry,” Meteorologische Zeitschrift, vol. 3,
!!           | no. 2, pp. 51–66, May 1994, doi: 10.1127/metz/3/1994/51.
!! 5         | SCHNEIDER (unpublished, based on DEHS (Di-Ethyl-Hexyl-Sebacat) aerosol)
!! 6         | R. W. Hyland and A. Wexler, “Formulation for the thermodynamic properties of the saturated
!!           | phases of H2O from 173.15K to 473.15K,” in ASHRAE Trans., 1983, vol. 89(2A), pp. 520–535. 
!!
!! ---
!! KPAR_ICE: Water vapor saturation pressure over ice
!!
!! KPAR_ICE  | Literature reference
!! ----------|-------------------------------------------------------------------------------------------------
!! 1         | J. Marti and K. Mauersberger, “A survey and new measurements of ice vapor pressure at 
!!           | temperatures between 170 and 250K,” Geophysical Research Letters, vol. 20, no. 5,
!!           | pp. 363–366, Mar. 1993, doi: 10.1029/93gl00105.
!! 2         | D. M. Murphy and T. Koop, “Review of the vapour pressures of ice and supercooled water for
!!           | atmospheric applications,” Quarterly Journal of the Royal Meteorological Society, vol. 131,
!!           | no. 608, pp. 1539–1565, Apr. 2005, doi: 10.1256/qj.04.94.
!!
!! ---
!! PAR-HET-FREEZE: Heterogeneous freezing parameterization
!!
!! PAR-HET-FREEZE           | Literature reference
!! ------------------------ | ---------------------------------------------------------------------------------
!! KL2003                   | B. Kärcher and U. Lohmann, “A parameterization of cirrus cloud formation:
!!                          | Heterogeneous freezing,” Journal of Geophysical Research, vol. 108, no. D14,
!!                          | 2003, doi: 10.1029/2002jd003220.
!! SOOT_ULLRICH2017         | R. Ullrich et al., “A New Ice Nucleation Active Site Parameterization for Desert
!!                          | Dust and Soot,” Journal of the Atmospheric Sciences, vol. 74, no. 3,
!!                          | pp. 699–717, Mar. 2017, doi: 10.1175/jas-d-16-0074.1.
!! DUST_ULLRICH2017         | see above
!! DUST_ULLRICH2017_UPDATED | (unpublished) Updated parameterization on dust
!! ATD_STEINKE2015          | R. Ullrich et al., “A New Ice Nucleation Active Site Parameterization for Desert
!!                          | Dust and Soot,” Journal of the Atmospheric Sciences, vol. 74, no. 3,
!!                          | pp. 699–717, Mar. 2017, doi: 10.1175/jas-d-16-0074.1.
!! QUARTZ_SCHORR            | (unpublished) INAS density parameterization for quartz
!! FUMED_SILICA_SCHORR      | (unpublished) INAS density parameterization for fumed/amorphous silica
!! CALCIUM_CARBONATE_SCHORR | (unpublished) INAS density parameterization for calcium carbonate
!!
!! ---
!! Components of aqueous solutions are numbered as follows:
!!
!!  Index | Name
!! -------|---------
!!  0     | 'TOTAL'
!!  1     | 'H2SO4'
!!  2     | 'H2O'
!!  3     | 'HNO3'
!!  4     | 'HCL'
!!  5     | 'HBR'
!!
use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
use, intrinsic :: iso_fortran_env, only: compiler_version, compiler_options
use, intrinsic :: ieee_arithmetic, only: ieee_is_normal

use lib_string, only: to_uppercase
use lib_input, only: READ_INI

use PARAMETERS
use parameters, only: GRAVITY_WAVE_UPDRAFT_FORCING_SAMPLE_SIZE, DELTA_W_PDF_N_BINS
use CONSTANTS

use gas_kinetics, only: diff_coef_h2o, diff_coef_hno3
use gas_properties, only: specific_heat_dry_air_const_pres
use water_properties, only: get_p_h2o_sat_liq
use ice_properties, only: get_p_h2o_sat_ice

use size_distribution, only: get_geometric_mean_radius, get_logspace_1D, initialize_psd
use solution_particle, only: get_solution_density, water_activity_sol_shi2001, water_activity_sol_schneider2021
use ice_formation_het, only: ice_form_het_deposition_ns, ice_form_het_immersion_KL2003
use ice_formation_hom, only: ice_form_hom_Koop2000
use ice_melting, only: apply_ice_melting
use masstransport_gas_solid, only: HNO3_TRAP
use masstransport_gas_liquid, only: condensation_water_particle, condensation_solution_particle
use sedimentation, only: apply_sedimentation
use atmosphere_dynamics, only: get_dry_adiabatic_lapse_rate, &
  &                            get_buoyancy_frequency, &
  &                            get_coriolis_frequency, &
  &                            get_gravity_wave_forcing_autocorrelation_time, &
  &                            get_gravity_wave_updraft_forcing_sample, &
  &                            get_gravity_wave_fluctuations


implicit none


! General variables
character(len=4) :: CALC_EQUIL  !! specifies if equilibrium calculation is applied on solution particles
logical :: DO_NOT_CALC_EQUILIBRIUM  !! inverse flag to state if equilibrium calculation is appied on solution particles
logical :: COMPILED_WITH_G  !! debug flag
logical :: TLOOP  !! if .true. the model-internal time loop continues
integer :: N_PCOND  !! number of condensable components
integer :: N_SOLUTES  !! number of compounds in the solution particle (1=H2SO4, 2=+H2O, 3=+HNO3...)
integer :: N_SOLUTES_LBOUND  !! first value to iterate to N_SOLUTES; default value is 1, only in case of pure water it is N_SOLUTES_LBOUND=N_SOLUTES=2
integer :: DURATION  !! in seconds, duration of trajectory
real(dp) :: DT_STEP  !! in seconds, model-internal time step during TLOOP
real(dp) :: UPDRAFT  !! in cm/s, updraft velocity
real(dp) :: DP_STEP  !! in Pa, pressure change for a step in TLOOP
real(dp) :: DELTA_TEMP_ADIABATIC_STEP  !! in K, temperature change for a step in TLOOP
character(len=9) :: TRAJECTORY_END_CRITERIA  !! distinguish between end criteria of built-in updraft or external .traj file
real(dp) :: RUNTIME_EXTENSION  !! in seconds, extend model runtime at constant pressure after internal trajectory has finished
logical :: IS_DURING_RUNTIME_EXTENSION  !! flag if currently in phase of duration extension
integer :: FILESTATUS  !! variable to catch io status when reading/writing files
integer, allocatable :: SEED(:)  !! Seed for pseudorandom number generation
integer :: SEED_SIZE  !! size of SEED, defaults to 8

! .ini file
integer :: FILEUNIT_INI  !! file index for .init file
character(len=256) :: FILENAME_INI  !! filename of .init file, which specifies parameters for the model run
character(len=10) :: TIME_FORM  !! format of the time column in .traj file
character(len=4) :: R_H2O_UNIT  !! unit of the geometric mean radius of liquid particle size distribution
character(len=4) :: R_IN_UNIT  !! unit of the geometric mean radius of ice-nucleating particle size distribution
character(len=4) :: R_ICE_UNIT  !! unit of the geometric mean radius of ice crystal size distribution
character(len=8) :: Z_H2O_UNIT  !! unit of the total number concentration of liquid particle size distribution
character(len=8) :: Z_IN_UNIT  !! unit of the total number concentration of ice-nucleating particle size distribution
character(len=8) :: Z_ICE_UNIT  !! unit of the total number concentration of ice crystal size distribution
character(len=4) :: P_UNIT  !! pressure unit (PA, HPA, BAR...)
character(len=4) :: PCOND_UNIT  !! unit for the vapor pressure concentration
character(len=4) :: PCOND_INI_UNIT(N_COMPONENTS)  !! unit for the initial vapor pressure concentration
integer :: IND_PINI(N_COMPONENTS)  !! index representing type of PCOND_INI_UNIT; Pressure/mixing ratio/RICE/RH2O
character(len=15) :: FORM_NUCL  !! specifies if heterogeneous freezing is enabled (N_NUCL=1: hom, N_NUCL=2: hom+het)
character(len=20) :: IN_TYPE  !! specifies the freezing behavior for immersion freezing with PAR_HET_FREEZE=KL2003

! .traj file
logical :: READ_PRES  !! flag if trajectory file should be considered
integer :: FILEUNIT_TRAJ  !! file index for .traj file
character(len=255) :: FILENAME_TRAJ  !! Filename of .traj file, which can be used to pass external trajectories
integer :: NREAD  !! index corresponding to expression in TIME_FORM, defining unit of time column
integer :: TRAJ_TEMP_COLUMN_COUNT  !! -TRAJ_COLIDX_TGAS, number of read values. Finally, the average of the read values (TRAJ_COLIDX_TGASM) will be taken as gas temperature.
integer :: TRAJ_HEADER_COUNT  !! number of header rows in .traj file
integer :: TRAJ_COLUMN_COUNT  !! number of columns in .traj file
integer :: TRAJ_COLIDX_P  !! column index for pressure in the .traj file
integer :: TRAJ_COLIDX_TGAS  !! column index for temperature in the .traj file
integer :: TRAJ_COLIDX_WALLFLUX_PCOND  !! column index for water wall flux in the .traj file
integer :: TRAJ_COLIDX_TGASM(20)  !! column indices for multiple temperatures in the .traj file
integer :: TRAJ_COLIDX_PCOND(N_COMPONENTS)  !! column indices for vapor pressures in the .traj file
real(dp) :: TRAJ_ROW(80)  !! current value set from the .traj file
real(dp) :: TRAJ_ROW_OLD(80)  !! previous value set from the .traj file
real(dp) :: TRAJ_TIMESTAMP_LATEST  !! in seconds, row in .traj file with latest timestamp before model timestamp
real(dp) :: TRAJ_TIMESTAMP_LAST_CHECKED  !! in seconds, last timestamp checked when searching next value of TRAJ_TIMESTAMP_NEXT
real(dp) :: TRAJ_TIMESTAMP_NEXT  !! in seconds, row in .traj file with next timestamp after model timestamp
real(dp) :: TRAJ_TIMESTAMP_NEXT_S  !! in seconds, latest timestamp being read from .traj file
real(dp) :: TRAJ_TIMESTAMP_NEXT_M  !! in minutes, latest timestamp being read from .traj file
real(dp) :: TRAJ_TIMESTAMP_NEXT_H  !! in hours, latest timestamp being read from .traj file
real(dp) :: TRAJ_TIMESTAMP_INI_S  !! in seconds, initial timestamp
real(dp) :: TRAJ_TIMESTAMP_INI_M  !! in minutes, initial timestamp
real(dp) :: TRAJ_TIMESTAMP_INI_H  !! in hours, initial timestamp
real(dp) :: TRAJ_TIME_OFFSET_S  !! in seconds, time offset from first row in .traj file
real(dp) :: TRAJ_TIME_OFFSET_M  !! in minutes, time offset from first row in .traj file
real(dp) :: TRAJ_TEMP_GAS_LATEST  !! in K, temperature at TRAJ_TIMESTAMP_LATEST
real(dp) :: TRAJ_TEMP_GAS_NEXT  !! in K, temperature at TRAJ_TIMESTAMP_NEXT
real(dp) :: TRAJ_TEMP_GAS_LATEST_MIN  !! in K, lowest temperature contributing to TRAJ_TEMP_GAS_LATEST, if multiple temperatures are given
real(dp) :: TRAJ_TEMP_GAS_LATEST_MAX  !! in K, highest temperature contributing to TRAJ_TEMP_GAS_LATEST, if multiple temperatures are given
real(dp) :: TRAJ_TEMP_GAS_NEXT_MIN  !! in K, lowest temperature contributing to TRAJ_TEMP_GAS_NEXT, if multiple temperatures are given
real(dp) :: TRAJ_TEMP_GAS_NEXT_MAX  !! in K, highest temperature contributing to TRAJ_TEMP_GAS_NEXT, if multiple temperatures are given
real(dp) :: TRAJ_PRESS_LATEST  !! in Pa, air pressure from .traj file at TRAJ_TIMESTAMP_LATEST
real(dp) :: TRAJ_PRESS_LAST_CHECKED  !! in Pa, air pressure from .traj file at TRAJ_TIMESTAMP_LAST_CHECKED
real(dp) :: TRAJ_PRESS_NEXT  !! in Pa, air pressure from .traj file at TRAJ_TIMESTAMP_NEXT
real(dp) :: TRAJ_PCOND(3, N_COMPONENTS)  !! in Pa, vapor pressure of condensable components derived from .traj file; 1:TRAJ_TIMESTAMP_LATEST, 2:TRAJ_TIMESTAMP_NEXT, 3:interpolated to ELAPSED_TIME_S
real(dp) :: TRAJ_WALLFLUX_PCOND(N_BINS)  !! wall fluxes, values must be read from .traj file

! Output
real(dp) :: DT_OUT  !! in sec, time increment for output values
character(len=30) :: OUTPUT_FORMAT_DATA  !! specifies the main data output format
character(len=30) :: OUTPUT_FORMAT_SD_LIQ  !! specifies the liquid size distribution output format
character(len=30) :: OUTPUT_FORMAT_SD_IN  !! specifies the ice-nucleating particle size distribution output format
character(len=30) :: OUTPUT_FORMAT_SD_ICE  !! specifies the ice crystal size distribution output format
character(len=60) :: FILENAME_OUTPUT_DATA  !! filename of the main output file
character(len=60) :: FILENAME_OUTPUT_SD_LIQ  !! filename of the liquid size distribution output file
character(len=60) :: FILENAME_OUTPUT_SD_IN  !! filename of the ice-nucleating particle size distribution output file
character(len=60) :: FILENAME_OUTPUT_SD_ICE  !! filename of the ice crystal size distribution output file
integer :: LINES_WRITTEN_OUT  !! counter for lines already written to data output file

integer :: FILEUNIT_OUTPUT_DATA  !! file index for main output file
integer :: FILEUNIT_OUTPUT_SD_ICE  !! file index for ice crystal size distribution output file
integer :: FILEUNIT_OUTPUT_SD_LIQ  !! file index for liquid particle size distribution output file
integer :: FILEUNIT_OUTPUT_SD_IN  !! file index for ice-nucleating particle size distribution output file

! time
real(dp) :: ELAPSED_TIME_S  !! in seconds, elapsed model time
real(dp) :: ELAPSED_TIME_M  !! elapsed time in minutes

! Gas temperature and pressure
real(dp) :: TGAS  !! in °C, air temperature
real(dp) :: TGAS_START  !! in °C, initial air temperature
real(dp) :: TABS_FINAL  !! in K, final air temperature
real(dp) :: TABS  !! in K, air temperature
real(dp) :: TABS_OLD  !! in K, air temperature in previous time step
real(dp) :: TGAS_MIN  !! in °C, interpolated value between TRAJ_TEMP_GAS_LATEST_MIN and TRAJ_TEMP_GAS_NEXT_MIN for current time stamp
real(dp) :: TGAS_MAX  !! in °C, interpolated value between TRAJ_TEMP_GAS_LATEST_MAX and TRAJ_TEMP_GAS_NEXT_MIN for current time stamp
logical :: FORCED_T  !! if .true. an adiabatic temperature trend is enforced, hence latent heat effects are neglected
real(dp) :: TEMP_POTENTIAL_START  !! in K, potential temperature at trajectory start, used to suppress adiabatic effects
real(dp) :: P  !! in Pa, air pressure
real(dp) :: PRESS_START  !! in Pa, initial air pressure
real(dp) :: PRESS_FINAL  !! in Pa, final air pressure
real(dp) :: P_OLD  !! in Pa, air pressure in current time step

real(dp) :: CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL  !! factor to scale input data of ambient pressure to Pascal
real(dp) :: CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL  !! factor to scale input data of condensable component vapore pressure to Pascal

real(dp) :: P_H2O_SAT_LIQ  !! in Pa, water vapor saturation pressure with respect to water
real(dp) :: P_H2O_SAT_ICE  !! in Pa, water vapor saturation pressure with respect to ice
real(dp) :: SAT_ICE  !! water vapor saturation ratio with respect to ice

real(dp) :: DENSITY_AIR  !! in kg/m³, density of air
real(dp) :: CP_DRY_AIR  !! in kJ/kg/K, heat capacity of air
real(dp) :: C_COND(N_COMPONENTS)  !! in g/cm³, concentration of condensable gases
real(dp) :: P_COND(N_COMPONENTS)  !! in Pa, vapor pressures of condensable components
real(dp) :: PCOND_INI(N_COMPONENTS)  !! in PCOND_INI_UNIT, initial values of P_COND(KK)
real(dp) :: P_SAT_SOLUTE_H2O(N_BINS)  !! in Pa, water vapor saturation pressure over the particle surface
logical :: IS_PCOND_CONST(N_COMPONENTS)  !! .true. if a vapor pressure of a condensable component must be constant
logical :: IS_PCOND_INI_DEFINED(N_COMPONENTS)  !! .true. if a valid initial value was specified in .init file

! Particles (general)
logical :: PART_PRESENT  !! flag to indicate if any particles are present
character(len=4) :: RMINMAX_UNIT  !! unit for the biggest and smallest radius (e.g. m, mym, ...)
real(dp) :: RMIN, RMAX  !! in units of RMINMAX_UNIT, boundaries for lognormal size distributions
real(dp) :: DELX  !!  ln(width) of a size channel
real(dp) :: X(N_BINS)  !! abscissa values for the generation of a particle size distribution
real(dp) :: MASS_FRACTIONS(N_COMPONENTS)  !! mass fraction of component KK in either liquid or ice particles
integer :: KK  !! index for soluble components
integer :: K  !! index for size channels, 0<=K<=(N_BINS-1)
integer :: KU, KO  !! boundary to iterate through size channels
integer :: KULIM, KOLIM  !! boundary to iterate through size channels
integer :: KULIM_H2O, KOLIM_H2O  !! boundaries of filled liquid channels
integer :: KULIM_H2O_INI, KOLIM_H2O_INI  !! initial boundaries of filled liquid channels
integer :: KULIM_ICE, KOLIM_ICE  !! boundaries of filled ice channels
integer :: KULIM_ICE_INI, KOLIM_ICE_INI  !! initial boundaries of filled ice channels
integer :: KULIM_IN, KOLIM_IN  !! boundaries of filled IN channels
logical :: FOUND_POPULATED_BIN_H2O, FOUND_POPULATED_BIN_ICE  !! mark if a channel is populated
real(dp), parameter :: VOLI = 1.0_dp / 1e6_dp  !! Factor to scale values relative to m³ to be relative to cm³

! Liquid particles
real(dp) :: RHO_PART  !! in g/cm³, density of solution particles
real(dp) :: MASS_FRACTIONS_H2O(N_COMPONENTS)  !! mass fraction of component KK in water
real(dp) :: SIG_H2O  !! width parameter for the lognormal size distribution of initial liquid particles
real(dp) :: R_H2O  !! in cm, median radius for the lognormal size distribution of initial liquid particles
real(dp) :: X_WT(N_COMPONENTS)  !! mass fraction of soluble components in the liquid particles
real(dp) :: Z_H2O_INI(N_BINS)  !! in #/cm³, initial number concentration of liquid particles per channel
real(dp) :: Z_H2O(N_BINS)  !! in #/cm³, number concentration of liquid particles per channel
real(dp) :: Z_H2O_TOT  !! in #/cm³, total number concentration of liquid particles
real(dp) :: PMASS_H2O(0: N_COMPONENTS, N_BINS)  !! in g, mass of component KK in a single liquid particle within channel K
real(dp) :: PMASS_H2O_INI(0: N_COMPONENTS, N_BINS)  !! in g, initial mass of component KK in a liquid particle within channel K
real(dp) :: PMASS_EQ(0:N_COMPONENTS,N_BINS)  !! equilibrium mixing ratio of the liquid particles
real(dp) :: RAD_H2O(N_BINS)  !! in cm, radius of the liquid particles
real(dp) :: RAD_H2O_INI(N_BINS)  !! in cm, initial radius of the liquid particles
real(dp) :: RAD_EQ(N_BINS)  !! in cm, radius after equilibrium calculation
real(dp) :: VOL_PART(N_BINS)  !! in cm³, volume of a liquid particle per channel
real(dp) :: WT_AVH2O(N_COMPONENTS) !! total mass of component kk in the liquid particle in the channel K
real(dp) :: WT_AVICE(N_COMPONENTS)  !! total mass of component kk in the ice particle in the channel K

real(dp) :: H2SO4_1  !! molar fraction of H2SO4 in solution with water.
real(dp) :: XWT_H2SO4_H2O_INI, XWT_H2SO4_H2O  !! mass fraction of sulfuric acid in solution particles
logical :: IS_H2O_PURE  !! flag is .true. if no sulfuric acid is present in the liquid particle phase
logical :: IS_H2O_PRESENT  !! flag indicates if any liquid particles are present
logical :: ZERO_H2O(N_BINS)  !! flag indicates if a channel in Z_H2O is empty

! Ice nuclei
real(dp) :: RHO_IN  !! in g/cm³, density of ice-nucleating particles
real(dp) :: Z_IN_TOT  !! in #/cm³, total number concentration of ice-nucleating particles
real(dp) :: SIGMA_IN  !! width parameter for the lognormal size distribution of initial ice-nucleating particles
real(dp) :: R_IN  !! in cm, median radius for the lognormal size distribution of initial ice-nucleating particles
real(dp) :: Z_IN_INI(N_BINS)  !! in #/cm³, initial number concentration of ice-nucleating particles per channel
real(dp) :: Z_IN(N_BINS)  !! in #/cm³, number concentration of ice-nucleating particles per channel
real(dp) :: RAD_IN(N_BINS)  !! in cm, radius of ab ice-nucleating particle in channel K
real(dp) :: SURF_IN(N_BINS)  !! in cm², surface of an ice-nucleating particle in channel K
real(dp) :: PMASS_IN(N_BINS)  !! in g, mass of an ice-nucleating particle in channel K
logical :: ZERO_IN(N_BINS)  !! true if Z_IN is empty
character(len=40) :: PAR_HET_FREEZE  !! paramtrization for heterogeneous ice formation

! Ice crystals
real(dp) :: A1, A2, DEL_K, PMSSKU, PMSSKO, PNEU  !! Used while creating initial ice crystal size distribution
integer :: KDEL_INT  !! value of DEL_K as int, int(DEL_K)
real(dp) :: MASS_FRACTIONS_ICE(N_COMPONENTS)  !! mass fraction of component KK in the ice phase
real(dp) :: SIG_ICE  !! width parameter for the lognormal size distribution of initial ice crystals
real(dp) :: R_ICE  !! in cm, median radius for the lognormal size distribution of initial ice crystals
real(dp) :: Z_ICE(N_BINS)  !! in #/cm³, number concentration of ice crystals per channel
real(dp) :: Z_ICE_INI(N_BINS)  !! in #/cm³, initial number concentration of ice crystals per channel
real(dp) :: Z_ICE_TOT  !! in #/cm³, total number concentration of ice crystals
real(dp) :: Z_ICE_OLD  !! in #/cm³, store previous value of Z_ICE(K)
real(dp) :: PMASS_ICE(0: N_COMPONENTS, N_BINS)  !! in g, mass of component KK in a single ice crystal within channel K
real(dp) :: PMASS_ICE_INI(0: N_COMPONENTS, N_BINS)  !! in g, initial mass of component KK in a single ice crystal within channel K
real(dp) :: RAD_ICE(N_BINS)  !! in cm, radius of the ice crystals
real(dp) :: RAD_ICE_INI(N_BINS)  !! in cm, initial radius of the ice crystals
real(dp) :: XWT_H2SO4_ICE_INI, XWT_H2SO4_ICE  !! mass fraction of sulfuric acid in ice crystals
logical :: SUPPRESS_ICE  !! .true. if negative ice number concentration at initialization
logical :: IS_ICE_PURE  !! flag is .true. if no sulfuric acid is present in the ice crystal phase
logical :: IS_ICE_PRESENT  !! flag indicates if any ice crystals are present
logical :: ZERO_ICE(N_BINS)  !! flag indicates if a channel in Z_ICE is empty

! Condensation/evaporation
real(dp) :: DELTA_P_COND(N_COMPONENTS)  !! in Pa, change of vapor pressure caused by wall flux
real(dp) :: DMASS_TRAP_SUM(N_BINS)  !! in ??, ?? (to be checked)
logical :: EVAP(N_BINS)  !! in ??, ?? (to be checked)
real(dp) :: DMASS_EVAP(N_BINS)  !! in ??, ?? (to be checked)
real(dp) :: DIFF_COEFS(N_COMPONENTS)  !! in cm²/s, diffusion coefficients of condensable components in gas
real(dp) :: AV_VEL(N_COMPONENTS)  !! in cm/s, mean thermal velocity of a gas phase molecule, acc. to Maxwells theory
real(dp) :: DC_OUT(N_COMPONENTS)  !! in g/cm³, total change of mass concentration of condensable components in the gas phase

! Ice formation
real(dp) :: WATER_ACTIVITY_ICE  !! ice activity, ratio of water vapor saturation pressure over ice and water vapor saturation pressure over water
real(dp) :: WATER_ACTIVITY_SOL(N_BINS)  !! water activity in the solution particles for each size channel
real(dp) :: DELTA_AW(N_BINS)  !! difference of water activity in solution and pure water
real(dp) :: VOLRATIO  !! Scaling ratio for nucleation rates in mixed freezing approach according to Kärcher & Lohmann (2003)
real(dp) :: STAY_LIQUID_PROBABILITY(N_BINS)  !! Probability of liquid particles to .not. freeze
real(dp) :: Z_ICE_NEW_STEP  !! in #/cm³, number concentration of ice crystals formed in the current time step
real(dp) :: Z_ICE_NEW(N_BINS)  !! in #/cm³, number of new ice crystals per channel.
real(dp) :: Z_ICE_NEW_HET(N_BINS)  !! in #/cm³, number of new heterogeneously formed ice crystals per channel.
real(dp) :: Z_ICE_NEW_HOM(N_BINS)  !! in #/cm³, number of new homogeneously formed ice crystals per channel.
real(dp) :: Z_ICE_NEW_HET_STEP  !! in #/cm³, number concentration of heterogeneously formed ice crystals in the current time step
real(dp) :: Z_ICE_NEW_HOM_STEP  !! in #/cm³, number concentration of heterogeneously formed ice crystals in the current time step
real(dp) :: Z_ICE_FORMED_SUM  !! in cm⁻³, sum of all nucleated ice crystals
real(dp) :: Z_ICE_HET_SUM  !! in cm⁻³, sum of all heterogeneously nucleated ice crystals
real(dp) :: Z_ICE_HOM_SUM  !! in cm⁻³, sum of all homogeneously nucleated ice crystals
character(len=4) :: ICE_NUCLEATION_MODE  !!  describes the ice-nucleation mode that happened during a timestep, 'het'/'hom'/'mix'/'none'
integer :: N_NUCL  !! index to distinguish between heterogeneous and homogeneous ice mode; het=2, hom=1
integer :: N_NUCL_INI  !! initial value of N_NUCL

! INAS het. ice variables
real(dp) :: NS  !! in #/m², ice nucleating active site density
integer :: NEW_HET_ICE_BIN  !! index of bin to be filled with new heterogeneous ice
real(dp) :: RAD_THRESHOLD_NEW_HET_ICE  !! in cm, size threshold at which `NEW_HET_ICE_BIN` should increase +1
real(dp) :: FROZEN_INP(N_BINS)  !! in #/cm³, number concentration from initial INPs to freeze heterogeneously at current time step
real(dp) :: FROZEN_INP_OLD(N_BINS)  !! in #/cm³, number concentration from initial INPs to be frozen heterogeneously at previous time step

! latent heat
real(dp) :: LATENT_HEAT_PROCESS  !! in J/cm³, latent heat effect from a certain process
real(dp) :: LATENT_HEAT_STEP  !! in J/cm³, sum of the latent heat effects during a time loop
real(dp) :: LATENT_HEAT_SUM  !! in J/cm³, sum of the latent heat effects during the whole trajectory
real(dp) :: DELTA_TEMP_LATENT_HEAT  !! in K, temperature change due to latent heat effects

! sedimentation
logical :: DO_SEDIMENTATION  !! flag indicates if sedimentation is calculated
real(dp) :: DZ  !! height ot the air parcel
real(dp) :: SEDFACTOR  !! dimensionless, strength of sedimentation; 0<SEDFACTOR<1

! Output-specific variables
! OUTPUT_FORMAT_DATA .eq. 'KRAEMER', 'MAJOR_OUTPUT' or 'MAJOR_OUTPUT_STEP'
real(dp) :: RG_H2O  !! in cm, geometric mean radius for liquid particle size distribution
real(dp) :: RG_ICE  !! in cm, geometric mean radius for ice crystal size distribution
real(dp) :: SIGMA  !! dimensionless, geometric standard deviation of the liquid particle size distribution, assuming an lognormal size distribution
real(dp) :: SIGMA_ICE  !! dimensionless, geometric standard deviation of the ice crystal size distribution, assuming an lognormal size distribution
real(dp) :: TOTAL1  !! in g/cm⁻³, variable used to store the total mass of a particle size distribution
real(dp) :: P_P2  !! in ppmv, volume mixing ratio of water in soluble solution particles
real(dp) :: P_P3  !! in ppbv, volume mixing ratio of HNO3 in soluble solution particles
real(dp) :: P_P2_ICE  !! in ppmv, volume mixing ratio of water in ice crystals
real(dp) :: P_P3_ICE  !! in ppbv, volume mixing ratio of HNO3 in ice crystals
real(dp) :: R_ice_eff  !! in cm, effective radius as ratio of volume mean radius and surface mean radius
real(dp) :: R_ice_mm  !! in cm, mass mean radius
real(dp) :: WC_ICE_ABS  !! in ??, ice water content, variable only used to calculate EXT_ICE
real(dp) :: EXT_ICE  !! dimensionless, extinction after Gayet et al. (2006)
real(dp) :: ALNRG  !! helper varliable during calculation, [sum_K(ln(RAD(K)) * Z(K))) / sum(Z(K))]
real(dp) :: sum_N_R_2  !! helper variable to calculate sum_K(Z_ICE(K) * RAD_ICE(K)**2)
real(dp) :: sum_N_R_3  !! helper variable to calculate sum_K(Z_ICE(K) * RAD_ICE(K)**3)
real(dp) :: sum_N_R_4  !! helper variable to calculate sum_K(Z_ICE(K) * RAD_ICE(K)**4)

! OUTPUT_FORMAT_DATA .eq. 'AWICIT'
real(dp) :: GWC  !! in ppmv, gas water content
real(dp) :: LWC  !! in ppmv, liquid water content
real(dp) :: IWC  !! in ppmv, ice water content

! Size distribution output
real(dp) :: RAD_CENTER_LOG  !! log10(RAD_center) for a bin between (K-1) and K
real(dp) :: RAD_CENTER_LOG_TO_LINEAR  !! in cm, radius where the center of a bin between (K-1) and K would be in the logarithmic space 
real(dp) :: RAD_LEFT_LOG, RAD_RIGHT_LOG  !! boundary diameters in logarithmic space of a bin between (K-1) and K
real(dp) :: Z_BIN_AVG  !! in cm⁻³, average particle number concentration of a bin between (K-1) and K

! Gravity wave driven mesoscale fluctuations
logical :: USE_GRAVITY_WAVE_FORCING  !! Determine if gavity wave forcing gets applied
real(dp) :: SIGMA_DELTA_W  !! in cm s⁻¹, standard deviation of the vertical wind speed fluctuations, width parameter for the probability density function
real(dp), allocatable :: GRAVITY_WAVE_UPDRAFT_FORCING_SAMPLE(:)  !! in cm s⁻¹, values of DELTA_W sampled from probability density function
real(dp) :: BUOYANCY_FREQ  !! in s⁻¹, Brunt-Väisälä frequency, which determines the autocorrelation timescale for updraft fluctuations
real(dp) :: GRAVITY_WAVE_AUTOCORRELATION_TIME  !! in sec, autocorrelation timescale of updraft fluctuations, determines how often the updraft gets updated
real(dp) :: GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC  !! if specified value gets used as static autocorrelation time of updraft fluctuations
integer :: COUNT_GRAVITY_WAVE_UPDRAFT_REFRESHS  !! Count how often the updraft velocity got updated with respect to updraft fluctuations
real(dp) :: LATITUDE_ANGLE  !! in degrees, latitude angle, specified as 0° at north pole, 180° at south pole.
real(dp) :: CORIOLIS_FREQUENCY  !! in s⁻¹, coriolis frequency
real(dp) :: DELTA_W  !! in cm s⁻¹, Variation in updraft velocity due to gravity wave forcing
real(dp) :: DELTA_TEMP_GRAVITY_WAVE_UPDRAFT_FLUCT  !! in K, temperature variation due to DELTA_W
real(dp) :: DELTA_P_GRAVITY_WAVE_UPDRAFT_FLUCT  !! in Pa, adiabatic pressure change caused by gravity wave forcing on temperature

! ==================================================================

! Check compile options
print*, "Info: This program was compiled by '", compiler_version(), "' using the following options:"
print*, "    ", compiler_options()

COMPILED_WITH_G =  index(compiler_options(), "-g ") > 0

! prepare seed for pseudo-random number generation
call random_seed(size=SEED_SIZE)
allocate(SEED(SEED_SIZE))
call random_seed(get=SEED)  ! initialize seed, can be overwritten with given seed from .init file

! Initialize variables of pressure, concentration and mass fraction for all components
do K = 1, N_COMPONENTS
  TRAJ_COLIDX_PCOND(K) = -1
  IS_PCOND_CONST(K) = .false.
  IS_PCOND_INI_DEFINED(K) = .false.
  TRAJ_PCOND(1, K) = 0.0_dp
  TRAJ_PCOND(2, K) = 0.0_dp
  TRAJ_PCOND(3, K) = 0.0_dp
  X_WT(K) = 0.0_dp
  MASS_FRACTIONS(K) = 0.0_dp
  WT_AVH2O(K) = 0.0_dp
  WT_AVICE(K) = 0.0_dp
  P_COND(K) = 0.0_dp
  DC_OUT(K) = 0.0_dp
  DELTA_P_COND(K) = 0.0_dp
end do

! Initialize further variables, not component-specific
NS = 0.0_dp
Z_ICE_NEW_STEP = 0.0_dp
Z_ICE_NEW_HET_STEP = 0.0_dp
Z_ICE_NEW_HOM_STEP = 0.0_dp
Z_ICE_HET_SUM = 0.0_dp
Z_ICE_HOM_SUM = 0.0_dp
Z_ICE_FORMED_SUM = 0.0_dp
XWT_H2SO4_H2O_INI = 0.0_dp
LATENT_HEAT_STEP = 0.0_dp
LATENT_HEAT_SUM = 0.0_dp
KULIM_ICE = 1
KOLIM_ICE = 1
KULIM_H2O = 1
KOLIM_H2O = 1
TRAJ_TEMP_COLUMN_COUNT = 0
LINES_WRITTEN_OUT = 0
IS_H2O_PURE = .true.
IS_ICE_PURE = .true.
SUPPRESS_ICE = .false.
IS_H2O_PRESENT = .false.
IS_ICE_PRESENT = .false.
IS_DURING_RUNTIME_EXTENSION = .false.

! Initialize addtional default values for variables to be read from .init file
USE_GRAVITY_WAVE_FORCING = .false.
GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC = 0.0
FORCED_T = .false.
PART_PRESENT = .false.
FILENAME_TRAJ = ''  ! default to be used if TRAJECTORY-FILE is not defined
DT_STEP = 0.0_dp
DT_OUT = 1.0_dp
DURATION = 0
RUNTIME_EXTENSION = 0.0_dp
PRESS_FINAL = 0.0_dp
TABS_FINAL = 0.0_dp
UPDRAFT = 0.0_dp


! Read variable values from .init file
call get_command_argument(1, FILENAME_INI)  ! Get filename from first command line argument
open(newunit=FILEUNIT_INI, file=FILENAME_INI, status='OLD', action='READ')
call READ_INI(FILEUNIT_INI, FILENAME_INI, DT_STEP, DT_OUT, TGAS_START, TABS_FINAL, &
  &           PRESS_START, PRESS_FINAL, DURATION, RUNTIME_EXTENSION, UPDRAFT,  &
  &           OUTPUT_FORMAT_DATA, OUTPUT_FORMAT_SD_LIQ, OUTPUT_FORMAT_SD_IN, OUTPUT_FORMAT_SD_ICE,  &
  &           FORM_NUCL, TIME_FORM, P_UNIT, TRAJ_HEADER_COUNT,  &
  &           TRAJ_COLUMN_COUNT, TRAJ_COLIDX_P, TRAJ_COLIDX_TGAS, FORCED_T, FILENAME_TRAJ, &
  &           PART_PRESENT, RMIN, RMAX, RMINMAX_UNIT, N_SOLUTES, CALC_EQUIL,  &
  &           Z_H2O_TOT, Z_H2O_UNIT, R_H2O, R_H2O_UNIT, SIG_H2O, XWT_H2SO4_H2O, MASS_FRACTIONS_H2O,  &
  &           Z_ICE_TOT, Z_ICE_UNIT, R_ICE, R_ICE_UNIT, SIG_ICE, XWT_H2SO4_ICE, MASS_FRACTIONS_ICE, N_PCOND,  &
  &           PCOND_UNIT, PCOND_INI, IS_PCOND_CONST, PCOND_INI_UNIT, IS_PCOND_INI_DEFINED,  &
  &           TRAJ_COLIDX_PCOND, IN_TYPE, Z_IN_TOT, Z_IN_UNIT, R_IN, R_IN_UNIT, SIGMA_IN, RHO_IN, DZ,  &
  &           SEDFACTOR, DO_SEDIMENTATION, &
  &           USE_GRAVITY_WAVE_FORCING, SIGMA_DELTA_W, LATITUDE_ANGLE, GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC, &
  &           TRAJ_COLIDX_WALLFLUX_PCOND,  &
  &           KPAR_H2OS, KPAR_AW_P, KPAR_ICE, PAR_HET_FREEZE, &
  &           SEED, COMPILED_WITH_G)
close(FILEUNIT_INI)

call random_seed(put=SEED)
if (COMPILED_WITH_G) print*, "Seed for pseudorandom numbers:", SEED

! Ensure the given start temperature is represented in °C
if (TGAS_START .gt. 100.0_dp) TGAS_START = TGAS_START - T0


if (COMPILED_WITH_G) then
  ! print which parameterizations were selected in the .init file
  print*, 'INFO: Using the following parameterizations:'

  select case (KPAR_ICE)
    case (1)
      print*, '    Saturation water vaopor pressure over ice: Marti & Mauersberger (1993).'
    case (2)
      print*, '    Saturation water vaopor pressure over ice: Murphy & Koop (2005)'
    case default
      print*, 'WARNING: Wrong or non-existent value for switch `[PAR-PSAT-ICE]`: ', KPAR_ICE
      stop 'Abort due to unknown parameterization input.'
  end select

  select case (KPAR_H2OS)
    case (1)
      print*, '    Saturation pressure water: Tabazadeh et al. (1997)'
    case (2)
      print*, '    Saturation pressure water: Murphy & Koop (2005)'
    case (3)
      print*, '    Saturation pressure water: Nachbar et al. (2019)'
    case (4)
      print*, '    Saturation pressure water: Sonntag et al. (1994)'
    case (5)
      print*, '    Saturation pressure water: Schneider et al. (2021)'
    case (6)
      print*, '    Saturation pressure water: Hyland & Wexler (1983)'
    case default
      print*, 'WARNING: Wrong or non-existent value for switch `[PAR-PSAT-WATER]`: ', KPAR_H2OS
      stop 'Abort due to unknown parameterization input.'
  end select

  select case (KPAR_AW_P)
    case (1)
      print*, '    Water activity: Carslaw et al. (1995)'
    case (2)
      print*, '    Water activity: Shi et al. (2001)'
    case (3)
      print*, '    Water activity: derived from Schneider et al. (2021)'
    case default
      print*, 'WARNING: Wrong or non-existent value for switch `[PAR-WATER-ACTIVITY]`: ', KPAR_AW_P
      stop 'Abort due to unknown parameterization input.'
  end select

  PAR_HET_FREEZE = to_uppercase(PAR_HET_FREEZE)
  select case (PAR_HET_FREEZE)
    case ('KL2003')
      print*, '    Heterogeneous freezing: Immersion freezing acc. to Kärcher & Lohmann (2003), specified by `IN_TYPE`'
      print*, '    IN_TYPE = ', IN_TYPE
    case ('ATD_STEINKE2015')
      print*, '    Heterogeneous freezing: Arizona test dust (ATD), Steinke et al. (2015) '
    case ('SOOT_ULLRICH2017', 'DUST_ULLRICH2017', 'DUST_ULLRICH2017_UPDATED')
      print*, '    Heterogeneous freezing: Ullrich et al. (2017): ', PAR_HET_FREEZE
    case ('FUMED_SILICA_SCHORR', 'QUARTZ_SCHORR', 'CALCIUM_CARBONATE_SCHORR')
      print*, '    Using a preliminary freezing freezing parameterization from Schorr: ', PAR_HET_FREEZE
    case default
      print*, 'WARNING: Wrong or non-existent value for switch `[PAR_HET_FREEZE]`: ', PAR_HET_FREEZE
      stop 'Abort due to unknown parameterization input.'
  end select

  ! Print info if sedimentation is switched on
  select case (DO_SEDIMENTATION)
    case (.true.)
      print*, 'Sedimentation is switched ON (Spichtinger & Cziczo, 2010).'
    case (.false.)
      print*, 'Sedimentation is switched OFF.'
  end select  
  
  print*, ""
end if  ! (COMPILED_WITH_G)


! Translate variables from .init file from character to integer variables
FORM_NUCL = to_uppercase(FORM_NUCL)
select case (FORM_NUCL)
  case ('HOMOGENEOUS', 'HOMOGEN')
    N_NUCL = 1
    if (COMPILED_WITH_G) print*, 'Pure homogeneous freezing enabled.'
  case ('HETEROGENEOUS', 'HETEROGEN')
    N_NUCL = 2
    if (COMPILED_WITH_G) print*, 'Heterogeneous AND homogeneous freezing enabled.'
  case default
    N_NUCL = 3
end select


! Open the output files

! Data output file
FILENAME_OUTPUT_DATA = 'Maid.out'
open(newunit=FILEUNIT_OUTPUT_DATA, file=FILENAME_OUTPUT_DATA, status='REPLACE', action='WRITE')

! Liquid solution particle size distribution
OUTPUT_FORMAT_SD_LIQ = to_uppercase(OUTPUT_FORMAT_SD_LIQ)
select case (OUTPUT_FORMAT_SD_LIQ)
  case ('YES')
    FILENAME_OUTPUT_SD_LIQ = 'Maid_SizeDist_Ptcl.out'
    open(newunit=FILEUNIT_OUTPUT_SD_LIQ, file=FILENAME_OUTPUT_SD_LIQ, status='REPLACE', action='WRITE')
    if (COMPILED_WITH_G) print*, "Creating output for size distribution of liquid particles."
    
    ! Write header
    write(FILEUNIT_OUTPUT_SD_LIQ, *) &
      & '#ELAPSED_TIME_S ', &
      & 'BIN_IDX ', &
      & 'RAD_UM ', &
      & 'Z_PCM3 ', &
      & 'RAD_BIN_CENTER_UM ', &
      & 'Z_BIN_AVG_PCM3 ', &
      & 'Z_BIN_AVG_PCM3_PER_LOGINTERVAL'
    
  case ('NO')
    if (COMPILED_WITH_G) print*, "You chose no output for size distribution of soluble particles."
  case default
    print*, "WARNING: You entered an invalid value for OUTPUT_FORMAT_SD_LIQ: ", OUTPUT_FORMAT_SD_LIQ
    print*, "The respective size distribution will not be saved."
end select

! Ice crystal particle size distribution
OUTPUT_FORMAT_SD_ICE = to_uppercase(OUTPUT_FORMAT_SD_ICE)
select case (OUTPUT_FORMAT_SD_ICE)
  case ('YES')
    FILENAME_OUTPUT_SD_ICE = 'Maid_SizeDist_Ice.out'
    open(newunit=FILEUNIT_OUTPUT_SD_ICE, file=FILENAME_OUTPUT_SD_ICE, status='REPLACE', action='WRITE')
    if (COMPILED_WITH_G) print*, "Creating output for size distribution of ice crystals."
    
    ! Write header
    write(FILEUNIT_OUTPUT_SD_ICE, *) &
      & 'ELAPSED_TIME_S ', &
      & 'BIN_IDX ', &
      & 'RAD_UM ', &
      & 'Z_PCM3 ', &
      & 'RAD_BIN_CENTER_UM ', &
      & 'Z_BIN_AVG_PCM3 ', &
      & 'Z_BIN_AVG_PCM3_PER_LOGINTERVAL'
          
  case ('NO')
    if (COMPILED_WITH_G) print*, "You chose no output for size distribution of ice particles."
  case default
    print*, "WARNING: You entered an invalid value for OUTPUT_FORMAT_SD_ICE: ", OUTPUT_FORMAT_SD_ICE
    print*, "The respective size distribution will not be saved."
end select

! Ice-nucleating particle size distribution
OUTPUT_FORMAT_SD_IN = to_uppercase(OUTPUT_FORMAT_SD_IN)
select case (OUTPUT_FORMAT_SD_IN)
  case ('YES')
    FILENAME_OUTPUT_SD_IN = 'Maid_SizeDist_IN.out'
    open(newunit=FILEUNIT_OUTPUT_SD_IN, file=FILENAME_OUTPUT_SD_IN, status='REPLACE', action='WRITE')
    if (COMPILED_WITH_G) print*, "Creating output for size distribution of ice-nucleating particles."
    
    ! Write header
    write(FILEUNIT_OUTPUT_SD_IN, *) &
      & '#ELAPSED_TIME_S ', &
      & 'BIN_IDX ', &
      & 'RAD_UM ', &
      & 'Z_PCM3 ', &
      & 'RAD_BIN_CENTER_UM ', &
      & 'Z_BIN_AVG_PCM3 ', &
      & 'Z_BIN_AVG_PCM3_PER_LOGINTERVAL'
    
  case ('NO')
    if (COMPILED_WITH_G) print*, "You chose no output for size distribution of solid ice-nucleating particles."
  case default
    print*, "WARNING: You entered an invalid value for OUTPUT_FORMAT_SD_IN: ", OUTPUT_FORMAT_SD_IN
    print*, "The respective size distribution will not be saved."
end select


! Determine trajectory type
if (PRESS_START .lt. 0.0_dp) then
  READ_PRES = .true.  ! use .traj file located at FILENAME_TRAJ
else
  READ_PRES = .false.  ! don't use external .traj file
end if

! Determine end of trajectory
if (.not. READ_PRES .and. .not. len_trim(FILENAME_TRAJ) .gt. 0) then
  ! using .traj file
  if (PRESS_START .lt. 0.0_dp) then
    ! PRESS_START < 0 is the indicator to use external .traj files
    stop "For built-in trajectories [P-ANFANG] must contain a positive number!"
  end if
  
  ! Check against invalid variable combinations given in .init file
  if (.not. UPDRAFT .gt. 0.0_dp) then
    ! UPDRAFT is not defined
    stop "For built-in trajectories you must define [UPDRAFT-VELOCITY] in your .init file!"
  else if (DURATION .gt. 0 .and. ((PRESS_FINAL .gt. 0.0_dp) .or. (TABS_FINAL .gt. 0.0_dp))) then
    ! UPDRAFT is defined, but also DURATION !AND! PRESS_FINAL
    stop "For built-in trajectories you may only define either [DURATION] or ([P-END] and/or [T-END]) in your .init file!"
  end if
  
  ! Determine trajectory end criterium from valid constellations of variables given in .init file
  if ((DURATION .gt. 0) .and. (.not. PRESS_FINAL .gt. 0.0_dp) .and. (.not. TABS_FINAL .gt. 0.0_dp)) then
    ! UPDRAFT and DURATION are defined
    if (COMPILED_WITH_G) print*, "INFO: End of built-in adiabatic trajectory is defined by duration."
    TRAJECTORY_END_CRITERIA = 'DURATION'
  else if ((.not. DURATION .gt. 0) .and. ((PRESS_FINAL .gt. 0.0) .or. TABS_FINAL .gt. 0.0)) then
    ! UPDRAFT and PRESS_FINAL are defined
    if (COMPILED_WITH_G) print*, "INFO: End of built-in adiabatic trajectory is defined by final condition."
    TRAJECTORY_END_CRITERIA = 'CONDITION'
  end if
else if (READ_PRES) then
  TRAJECTORY_END_CRITERIA = 'FROMFILE'
end if

! If .traj file is used, prepare certain variables
if (READ_PRES) then
  TIME_FORM = to_uppercase(TIME_FORM)
  P_UNIT = to_uppercase(P_UNIT)
  
  if (TRAJ_COLIDX_TGAS .lt. 0) then
    ! Note, negative TRAJ_COLIDX_TGAS means, that multiple temperature columns are given.
    ! The number of temperature columns are given by -TRAJ_COLIDX_TGAS
    TRAJ_TEMP_COLUMN_COUNT = -TRAJ_COLIDX_TGAS
  end if
end if

N_PCOND = -1 * N_PCOND + 1  ! Note, increment N_PCOND because 1=H2SO4, but condensable components start at 2=WATER
PCOND_UNIT = to_uppercase(PCOND_UNIT)
select case (PCOND_UNIT)
  case ('PA')
    CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL = 1.0_dp
  case ('BAR')
    CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL = 1e5_dp
  case('MBAR', 'HPA')
    CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL = 1e2_dp
  case ('TORR')
    CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL = 133.32_dp
  case ('PPB')
    CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL = -1e-9_dp
  case ('PPM')
    CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL = -1e-6_dp
  case default
    if (N_PCOND .gt. 2) then
      print*, "ERROR: Invalid unit for pressure of condensable components PCOND_UNIT: ", PCOND_UNIT
      stop
    end if
end select


! Initialize particle distributions
if (PART_PRESENT) then
  
  ! The following block is necessary due to a 'special' naming scheme:
  ! When reading from .init file N_SOLUTES=1 means pure water, however
  ! in the component ranking water has the index 2.
  if (N_SOLUTES .eq. 1) then
    N_SOLUTES_LBOUND = 2
  else
    N_SOLUTES_LBOUND = 1
  end if
  if (N_SOLUTES .eq. 1) N_SOLUTES = 2
  
  
  ! Convert unit of given upper and lower size distribution boundaries to cm
  select case (RMINMAX_UNIT)
    case ('NM')
      RMIN = RMIN * 1e-7_dp
      RMAX = RMAX * 1e-7_dp
    case ('MYM')
      RMIN = RMIN * 1e-4_dp
      RMAX = RMAX * 1e-4_dp
    case ('CM')
      ! no conversion necessary
    case ('M')
      RMIN = RMIN * 100.0_dp
      RMAX = RMAX * 100.0_dp
    case default
      print*, "Invalid unit for min/max radius: ", RMINMAX_UNIT
      stop
  end select
  
  ! Check flag to make particles equilibrate with environment in each timestep
  DO_NOT_CALC_EQUILIBRIUM = .not. ((CALC_EQUIL .eq. 'EQUI') &
    &                              .or. (CALC_EQUIL .eq. 'GLEI') &
    &                              .or. (CALC_EQUIL .eq. 'JA') &
    &                              .or. (CALC_EQUIL .eq. 'YES'))
  
  ! Check consistency between variables KMAX and N_BINS
  if (KMAX .gt. N_BINS) then
    print*, 'WARNING: selected number of bins KMAX (', KMAX, &
      &     ') is bigger than array size N_BINS (', N_BINS, ').'
    print*, 'This conflict gets now avoided by setting `KMAX = N_BINS`.'
    KMAX = N_BINS
  end if
  
  ! ------------------------------------------------------------------
  !
  ! Initialize particle size distributions (PSDs)
  !
  ! ------------------------------------------------------------------
  
  ! Define log10-spaced bin radii for size distributions
  X = get_logspace_1D(RMIN, RMAX, KMAX)
  
  ! initialize default values for size distribution arrays
  RAD_H2O(:) = X(:)
  Z_H2O(:) = 0.0_dp
  ZERO_H2O(:) = .true.
  PMASS_H2O(0, :) = 4.0_dp / 3.0_dp * PI * X**3.0_dp * RHO_H2O
  PMASS_H2O(1:N_COMPONENTS, :) = 0.0_dp
  STAY_LIQUID_PROBABILITY(:) = 1.0_dp
  RAD_EQ(:) = 1.0_dp

  RAD_IN(:) = X(:)
  Z_IN(:) = 0.0_dp
  ZERO_IN(:) = .true.
  PMASS_IN(:) = 4.0_dp / 3.0_dp * PI * X**3.0_dp * RHO_IN
  
  RAD_ICE(:) = X(:)
  Z_ICE(:) = 0.0_dp
  ZERO_ICE(:) = .true.
  PMASS_ICE(0, :) = 4.0_dp / 3.0_dp * PI * X**3.0_dp * RHO_ICE
  PMASS_ICE(1:N_COMPONENTS, :) = 0.0_dp
  
  
  ! ------------------------------------------------------------------
  !
  ! Initialize liquid PSD
  !
  ! ------------------------------------------------------------------
  
  ! Convert unit of total concentration for lognormal size distribution of liquid particles to cm⁻³
  ! and distinguish if number concentration or mass concentration
  Z_H2O_UNIT = to_uppercase(Z_H2O_UNIT)
  select case (Z_H2O_UNIT)
    case ('PTOT')
      Z_H2O_TOT = Z_H2O_TOT * 1e-6_dp
    case ('P/CM**3')
      ! already the right unit, nothing to do here
    case default
      print*, "ERROR: Invalid unit for initial liquid particles: ", Z_H2O_UNIT
      stop
  end select
  
  ! Convert unit of median radius for lognormal size distribution of liquid particles
  R_H2O_UNIT = to_uppercase(R_H2O_UNIT)
  select case (R_H2O_UNIT)
    case ('NM')
      R_H2O = R_H2O * 1e-7_dp
    case ('MYM')
      R_H2O = R_H2O * 1e-4_dp
    case ('CM')
      R_H2O = R_H2O
    case ('M')
      R_H2O = R_H2O * 100.0_dp
    case default
      print*, "ERROR: Invalit unit for radius of liquid particles: ", R_H2O_UNIT
      stop
  end select
  
  ! Create size distribution of liquid particles, if present
  IS_H2O_PRESENT = Z_H2O_TOT .gt. CUTOFF_NUMBER_CONC
  select case (IS_H2O_PRESENT)
    
    case (.true.)  ! (IS_H2O_PRESENT)
      X_WT(1) = 0.01_dp * XWT_H2SO4_H2O
      X_WT(2) = 1.0_dp - 0.01_dp * XWT_H2SO4_H2O
      X_WT(3) = 0.0_dp
      
      TABS = TGAS_START + T0
      RHO_PART = get_solution_density(TABS, X_WT)
      
      IS_H2O_PURE = .FALSE.
      if (XWT_H2SO4_H2O .lt. 0.01_dp .and. XWT_H2SO4_H2O .ge. 0.0_dp) then
        ! solution particles with less than 0.01% H2SO4 are considere pure water drops
        XWT_H2SO4_H2O = 0.0_dp
        IS_H2O_PURE = .TRUE.
        RHO_PART = RHO_H2O
      end if
      
      ! From here on convert XWT_H2SO4_H2O from percentage to mass fraction
      XWT_H2SO4_H2O = XWT_H2SO4_H2O * 0.01_dp
      
      if (XWT_H2SO4_H2O .lt. 0.0_dp) then
        do K = 1, N_SOLUTES
          MASS_FRACTIONS(K) = MASS_FRACTIONS_H2O(K)
        end do
        XWT_H2SO4_H2O = MASS_FRACTIONS(1)
      else
        MASS_FRACTIONS(1) = XWT_H2SO4_H2O
        MASS_FRACTIONS(2) = 1.0_dp - XWT_H2SO4_H2O
        
        if (N_SOLUTES .eq. N_SOLUTES_LBOUND) then  ! In case of pure water
          do K = 1, N_COMPONENTS
            MASS_FRACTIONS(K) = 0.0_dp
          end do
          MASS_FRACTIONS(2) = 1.0_dp
          XWT_H2SO4_H2O = 0.0_dp
        end if
      end if
      
      Z_H2O = initialize_psd(RAD_H2O, Z_H2O_TOT, R_H2O, SIG_H2O, KMAX)
      
      ZERO_H2O = (.not. Z_H2O .gt. 0.0_dp)
      KULIM_H2O = findloc(ZERO_H2O, .false., dim=1)
      KOLIM_H2O = findloc(ZERO_H2O, .false., back=.true., dim=1)
      PMASS_H2O(0, :) = 4.0_dp / 3.0_dp * PI * RAD_H2O(:)**3.0_dp * RHO_PART
      do KK = 1, N_SOLUTES
        PMASS_H2O(1, :) = PMASS_H2O(0, :) * XWT_H2SO4_H2O
        PMASS_H2O(2, :) = PMASS_H2O(0, :) * (1.0_dp - XWT_H2SO4_H2O)
      end do
      
    case (.false.)
      Z_H2O_TOT = 0.0_dp
  end select  ! (IS_H2O_PRESENT)
  
  ! ------------------------------------------------------------------
  !
  ! Initialize ice crystals
  !
  ! ------------------------------------------------------------------
  Z_ICE_UNIT = to_uppercase(Z_ICE_UNIT)
  select case (Z_ICE_UNIT)
    case ('G')
      Z_ICE_TOT = Z_ICE_TOT * VOLI
      Z_ICE_UNIT = 'MASS'
    case ('MYG')
      Z_ICE_TOT = Z_ICE_TOT * 1e-6_dp * VOLI
      Z_ICE_UNIT = 'MASS'
    case ('MG')
      Z_ICE_TOT = Z_ICE_TOT * 1e-3_dp * VOLI
      Z_ICE_UNIT = 'MASS'
    case ('KG')
      Z_ICE_TOT = Z_ICE_TOT * 1e3_dp * VOLI
      Z_ICE_UNIT = 'MASS'
    case ('G/CM**3')
      Z_ICE_UNIT = 'MASS'
    case ('G/M**3')
      Z_ICE_TOT = Z_ICE_TOT * 1e-6_dp
      Z_ICE_UNIT = 'MASS'
    case ('MYG/M**3')
      Z_ICE_TOT = Z_ICE_TOT * 1e-12_dp
      Z_ICE_UNIT = 'MASS'
    case ('MG/M**3')
      Z_ICE_TOT = Z_ICE_TOT * 1e-9_dp
      Z_ICE_UNIT = 'MASS'
    case ('PTOT')
      Z_ICE_TOT = Z_ICE_TOT * VOLI
      Z_ICE_UNIT = 'NUMBER'
    case ('P/CM**3')
      Z_ICE_UNIT = 'NUMBER'
    case default
      print*, 'ERROR: Invalid unit for initial ice crystals: ', Z_ICE_UNIT
      stop
  end select
  
  ! Check if initial ice crystals are present
  IS_ICE_PRESENT = Z_ICE_TOT .gt. CUTOFF_NUMBER_CONC
  
  ! Note, negative Z_ICE_TOT means to suppress ice formation!
  if (Z_ICE_TOT .lt. -CUTOFF_NUMBER_CONC) then
    SUPPRESS_ICE = .true.
    Z_ICE_TOT = 0.0_dp
  end if
  
  if (IS_ICE_PRESENT .or. SUPPRESS_ICE) then
    
    ! Convert unit of geometric mean radius for lognormal size distribution of ice crystals
    R_ICE_UNIT = to_uppercase(R_ICE_UNIT)
    select case (R_ICE_UNIT)
      case ('NM')
        R_ICE = R_ICE * 1e-7_dp
      case ('MYM')
        R_ICE = R_ICE * 1e-4_dp
      case ('CM')
        R_ICE = R_ICE
      case ('M')
        R_ICE = R_ICE * 100.0_dp
      case default
        print*, 'ERROR: Invalid unit for radius of ice crystals: ', R_ICE_UNIT
        stop
    end select
    
    ! ice crystals with less than 0.01% H2SO4 are considere pure water
    if (XWT_H2SO4_ICE .lt. 0.01_dp .and. XWT_H2SO4_ICE .ge. 0.0_dp) then
      XWT_H2SO4_ICE = 0.0_dp
      IS_ICE_PURE = .true.
      RHO_PART = RHO_ICE
    end if
    
    ! From here on convert XWT_H2SO4_ICE from percentage to mass fraction
    XWT_H2SO4_ICE = XWT_H2SO4_ICE * 0.01_dp
    
    do K = 1, N_COMPONENTS
      MASS_FRACTIONS(K) = 0.0_dp
    end do
    
    if (XWT_H2SO4_ICE .lt. 0.0_dp) then  ! use mass fractions from MASS_FRACTIONS_ICE
      do K = 1, N_SOLUTES
        MASS_FRACTIONS(K) = MASS_FRACTIONS_ICE(K)
      end do
      
      XWT_H2SO4_ICE = MASS_FRACTIONS(1)
    else  ! use mass fraction from XWT_H2SO4_ICE
      MASS_FRACTIONS(1) = XWT_H2SO4_ICE
      MASS_FRACTIONS(2) = 1.0_dp - XWT_H2SO4_ICE
      
      if (N_SOLUTES .eq. N_SOLUTES_LBOUND) then
        ! in case of pure ice water
        do K = 1, N_COMPONENTS
          MASS_FRACTIONS(K) = 0.0_dp
        end do
        
        MASS_FRACTIONS(2) = 1.0_dp
        XWT_H2SO4_ICE = 0.0_dp
      end if
    end if
    
    Z_ICE = initialize_psd(RAD_ICE, Z_ICE_TOT, R_ICE, SIG_ICE, KMAX)
    Z_ICE_INI(:) = Z_ICE(:)
    
    ZERO_ICE = (.not. Z_ICE .gt. 0.0_dp)
    KULIM_ICE = findloc(ZERO_ICE, .false., dim=1)
    KOLIM_ICE = findloc(ZERO_ICE, .false., back=.true., dim=1)
    RHO_PART = RHO_ICE * (1.0_dp - XWT_H2SO4_ICE) + DENSITY_H2SO4 * XWT_H2SO4_ICE
    PMASS_ICE(0, :) = 4.0_dp / 3.0_dp * PI * RAD_ICE(:)**3.0_dp * RHO_PART
    do KK = 1, N_SOLUTES
      PMASS_ICE(1, :) = PMASS_ICE(0, :) * XWT_H2SO4_ICE
      PMASS_ICE(2, :) = PMASS_ICE(0, :) * (1.0_dp - XWT_H2SO4_ICE)
    end do
    
  else
    Z_ICE_TOT = 0.0_dp
    XWT_H2SO4_ICE = 0.0_dp
  end if  ! (IS_ICE_PRESENT .or. SUPPRESS_ICE)
  
  do K = 1, KMAX
    Z_H2O_INI(K) = Z_H2O(K)
    Z_ICE_INI(K) = Z_ICE(K)
    RAD_H2O_INI(K) = RAD_H2O(K)
    RAD_ICE_INI(K) = RAD_ICE(K)
    do KK = 0, N_SOLUTES
      PMASS_H2O_INI(KK, K) = PMASS_H2O(KK, K)
      PMASS_ICE_INI(KK, K) = PMASS_ICE(KK, K)
    end do
  end do
  
  XWT_H2SO4_H2O_INI = XWT_H2SO4_H2O
  XWT_H2SO4_ICE_INI = XWT_H2SO4_ICE
  KULIM_H2O_INI = KULIM_H2O
  KOLIM_H2O_INI = KOLIM_H2O
  KULIM_ICE_INI = KULIM_ICE
  KOLIM_ICE_INI = KOLIM_ICE
end if  ! (PART_PRESENT)
  
! ------------------------------------------------------------------
!
! Initialize ice-nucleating particle size distribution
!
! ------------------------------------------------------------------
Z_IN_UNIT = to_uppercase(Z_IN_UNIT)
select case (Z_IN_UNIT)
  case ('PTOT')
    Z_IN_TOT = Z_IN_TOT * VOLI
    Z_IN_UNIT = 'NUMBER'
  case ('P/CM**3')
    Z_IN_UNIT = 'NUMBER'
  case default
    print*, "ERROR: Invalid unit for initial ice nuclei: ", Z_IN_UNIT
    stop
end select

R_IN_UNIT = to_uppercase(R_IN_UNIT)
select case (R_IN_UNIT)
  case ('NM')
    R_IN = R_IN * 1e-7_dp
  case ('MYM')
    R_IN = R_IN * 1e-4_dp
  case ('CM')
    R_IN = R_IN
  case('M')
    R_IN = R_IN * 100.0_dp
  case default
    print*, "ERROR: Invalid unit for initial radius of ice nuclei: ", R_IN_UNIT
    stop
end select

Z_IN = initialize_psd(RAD_IN, Z_IN_TOT, R_IN, SIGMA_IN, KMAX)
PMASS_IN(:) = 4.0_dp / 3.0_dp * PI * RAD_IN(:)**3 * RHO_IN
SURF_IN(:) = 4.0_dp * PI * RAD_IN(:)**2
ZERO_IN = (.not. Z_IN .gt. 0.0_dp)
KULIM_IN = findloc(ZERO_IN, .false., dim=1)
KOLIM_IN = findloc(ZERO_IN, .false., back=.true., dim=1)
Z_IN_INI = Z_IN


! Initialize additional ice bin to be used for het. depositional ice according to INAS density approach
NEW_HET_ICE_BIN = KMAX + 1

! populate new bin when the ice volume reached the tenfold of INP reometric mean volume (ca RAD*2.15)
RAD_THRESHOLD_NEW_HET_ICE =  4.0_dp * 1e-4  ! Note: This threshold is empirically chosen. Needs validation!

Z_ICE(NEW_HET_ICE_BIN) = 0.0_dp
ZERO_ICE(NEW_HET_ICE_BIN) = .true.
RAD_ICE(NEW_HET_ICE_BIN) = R_IN
PMASS_ICE(0, NEW_HET_ICE_BIN) = 4.0_dp / 3.0_dp * PI * R_IN**3 * RHO_ICE
PMASS_ICE(1, NEW_HET_ICE_BIN) = 0.0_dp
PMASS_ICE(2, NEW_HET_ICE_BIN) = 4.0_dp / 3.0_dp * PI * R_IN**3 * RHO_ICE
PMASS_ICE(3, NEW_HET_ICE_BIN) = 0.0_dp
PMASS_ICE(4, NEW_HET_ICE_BIN) = 0.0_dp

Z_H2O(NEW_HET_ICE_BIN) = 0.0_dp
ZERO_H2O(NEW_HET_ICE_BIN) = .true.
RAD_H2O(NEW_HET_ICE_BIN) = R_IN
PMASS_H2O(0, NEW_HET_ICE_BIN) = 4.0_dp / 3.0_dp * PI * R_IN**3 * RHO_H2O
PMASS_H2O(1, NEW_HET_ICE_BIN) = 0.0_dp
PMASS_H2O(2, NEW_HET_ICE_BIN) = 4.0_dp / 3.0_dp * PI * R_IN**3 * RHO_H2O
PMASS_H2O(3, NEW_HET_ICE_BIN) = 0.0_dp
PMASS_H2O(4, NEW_HET_ICE_BIN) = 0.0_dp
STAY_LIQUID_PROBABILITY(NEW_HET_ICE_BIN) = 1.0_dp


! Initialize variables for gravity wave forcing
if (USE_GRAVITY_WAVE_FORCING) then
  BUOYANCY_FREQ = 1.0_dp / 80.0_dp  ! a somewhat reasonable placeholder to have a finite value for first loop
  COUNT_GRAVITY_WAVE_UPDRAFT_REFRESHS = 0
  allocate(GRAVITY_WAVE_UPDRAFT_FORCING_SAMPLE(GRAVITY_WAVE_UPDRAFT_FORCING_SAMPLE_SIZE))
  GRAVITY_WAVE_UPDRAFT_FORCING_SAMPLE = get_gravity_wave_updraft_forcing_sample( &
    & SIGMA_DELTA_W, GRAVITY_WAVE_UPDRAFT_FORCING_SAMPLE_SIZE, DELTA_W_PDF_N_BINS)
  CORIOLIS_FREQUENCY = get_coriolis_frequency(LATITUDE_ANGLE)  ! used as damping factor for DELTA_TEMP_GRAVITY_WAVE_UPDRAFT_FLUCT
  DELTA_TEMP_GRAVITY_WAVE_UPDRAFT_FLUCT = 0.0_dp
  DELTA_P_GRAVITY_WAVE_UPDRAFT_FLUCT = 0.0_dp
  DELTA_W = 0.0_dp
  
  if (.not. GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC .gt. 0.0) then
    GRAVITY_WAVE_AUTOCORRELATION_TIME = get_gravity_wave_forcing_autocorrelation_time(BUOYANCY_FREQ)
  else if (GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC .gt. 0.0) then
    GRAVITY_WAVE_AUTOCORRELATION_TIME = GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC
  else
    stop "Encountered invalid value for GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC"
  end if
end if


select case (READ_PRES)
  
  case (.true.)
  ! If external .traj file is used
    
    ! Read time unit for .traj file, which must be specfied in .init file
    if ((TIME_FORM(1:3) .eq. 'UHR') .or. (TIME_FORM(1:4) .eq. 'TIME')) then
      NREAD = 1
    else if ((TIME_FORM(1:3) .eq. 'SEK') .or. (TIME_FORM(1:3) .eq. 'SEC')) then
      NREAD = 2
    else if (TIME_FORM(1:3) .eq. 'MIN') then
      NREAD = 3
    else if ((TIME_FORM(1:3) .eq. 'STU') .or. (TIME_FORM(1:3) .eq. 'HOU')) then
      NREAD = 4
    else
      NREAD = 0
      print*, 'ERROR: Invalid time format: ', TIME_FORM
      stop
    end if

    ! Factor to convert pressure from .traj file to pascal
    select case (P_UNIT)
      case ('PA')
        CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL = 1.0_dp
      case ('BAR')
        CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL = 1e5_dp
      case ('MBAR', 'HPA')
        CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL = 1e2_dp
      case ('TORR')
        CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL = 133.32_dp
      case default
        print*, "ERROR: Invalid unit for system pressure: ", CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL
        stop
    end select
    
    
    ! open TRAJECTORY file
    open(newunit=FILEUNIT_TRAJ, file=FILENAME_TRAJ)
    
    ! skip header lines
    do K = 1, TRAJ_HEADER_COUNT
      read(FILEUNIT_TRAJ, *)
    end do
    
    ! Read first row of .traj file, determine time offset from time column
    select case (NREAD)
      case (1)
        read(FILEUNIT_TRAJ, *) TRAJ_TIMESTAMP_INI_H, TRAJ_TIMESTAMP_INI_M, &
          &                    TRAJ_TIMESTAMP_INI_S, (TRAJ_ROW(K), K=1, TRAJ_COLUMN_COUNT)
        TRAJ_TIME_OFFSET_S = TRAJ_TIMESTAMP_INI_H * 3600.0_dp &
          &                  + TRAJ_TIMESTAMP_INI_M * 60.0_dp &
          &                  + TRAJ_TIMESTAMP_INI_S
      case (2)
        read(FILEUNIT_TRAJ, *) TRAJ_TIMESTAMP_INI_S, (TRAJ_ROW(K), K=1, TRAJ_COLUMN_COUNT)
        TRAJ_TIME_OFFSET_S = TRAJ_TIMESTAMP_INI_S
      case (3)
        read(FILEUNIT_TRAJ, *) TRAJ_TIMESTAMP_INI_M, (TRAJ_ROW(K), K=1, TRAJ_COLUMN_COUNT)
        TRAJ_TIME_OFFSET_S = TRAJ_TIMESTAMP_INI_M * 60.0_dp
      case (4)
        read(FILEUNIT_TRAJ, *) TRAJ_TIMESTAMP_INI_H, (TRAJ_ROW(K), K=1, TRAJ_COLUMN_COUNT)
        TRAJ_TIME_OFFSET_S = TRAJ_TIMESTAMP_INI_H * 3600.0_dp
      case default
        print*, "ERROR: invalid value for variable NREAD: ", NREAD
        stop
    end select
    
    do K = 1, TRAJ_COLUMN_COUNT
      TRAJ_ROW_OLD(K) = TRAJ_ROW(K)
    end do
    
    TRAJ_TIME_OFFSET_M = TRAJ_TIME_OFFSET_S / 60.0_dp
    PRESS_START = TRAJ_ROW(TRAJ_COLIDX_P) * CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL
    TRAJ_TIMESTAMP_LATEST = 0.0_dp
    TRAJ_PRESS_LATEST = PRESS_START
    
    ! Read vapor pressure of condensable gases
    !
    ! Specification for condensable gases from [ASSESSMENTS] in .init file:
    !    N: TRAJ_COLIDX_PCOND(KK) for values of condensable component vapor pressure N>0
    !   -1: THEN CALCULATING OF PRESSURE; NO READ VALUE FOR PARTIAL PRESSURE
    !   -X: read wall flux values from .traj file
    !
    ! For (X .GE. 2): X=2 --> H2O; X=3 --> HNO3; X=4 --> HCL: X=5 --> HBR
    do KK = 2, N_PCOND
      if (TRAJ_COLIDX_PCOND(KK) .lt. 0) cycle
      TRAJ_PCOND(1, KK) = TRAJ_ROW(TRAJ_COLIDX_PCOND(KK))
      TRAJ_PCOND(3, KK) = TRAJ_PCOND(1, KK)
    end do
    
    
    ! Read temperature
    select case (TRAJ_COLIDX_TGAS)
    
      case (1 : )  ! (TRAJ_COLIDX_TGAS .gt. 0), scalar
        TRAJ_TEMP_GAS_LATEST = TRAJ_ROW(TRAJ_COLIDX_TGAS)
        if (TRAJ_TEMP_GAS_LATEST .gt. 100.0_dp) TRAJ_TEMP_GAS_LATEST = TRAJ_TEMP_GAS_LATEST - T0
        
      case ( : -1)  ! (TRAJ_COLIDX_TGAS .lt. 0), multiple temperatures given
        TRAJ_TEMP_GAS_LATEST = 0.0_dp
        TRAJ_TEMP_GAS_LATEST_MIN = TRAJ_ROW(TRAJ_COLIDX_TGASM(1))
        TRAJ_TEMP_GAS_LATEST_MAX = TRAJ_ROW(TRAJ_COLIDX_TGASM(1))
        
        do K = 1, TRAJ_TEMP_COLUMN_COUNT
          TRAJ_TEMP_GAS_LATEST = TRAJ_TEMP_GAS_LATEST + TRAJ_ROW(TRAJ_COLIDX_TGASM(K))
          TRAJ_TEMP_GAS_LATEST_MIN = min(TRAJ_TEMP_GAS_LATEST_MIN, TRAJ_ROW(TRAJ_COLIDX_TGASM(K)))
          TRAJ_TEMP_GAS_LATEST_MAX = max(TRAJ_TEMP_GAS_LATEST_MAX, TRAJ_ROW(TRAJ_COLIDX_TGASM(K)))
        end do
        TRAJ_TEMP_GAS_LATEST = TRAJ_TEMP_GAS_LATEST / real(TRAJ_TEMP_COLUMN_COUNT, kind=dp)
        
        if (TRAJ_TEMP_GAS_LATEST .gt. 100.0_dp) then
          TRAJ_TEMP_GAS_LATEST = TRAJ_TEMP_GAS_LATEST - T0
          TRAJ_TEMP_GAS_LATEST_MIN = TRAJ_TEMP_GAS_LATEST_MIN - T0
          TRAJ_TEMP_GAS_LATEST_MAX = TRAJ_TEMP_GAS_LATEST_MAX - T0
        end if
    end select  ! (TRAJ_COLIDX_TGAS)
    
    TGAS_START = TRAJ_TEMP_GAS_LATEST
    
    
    ! Read second row of .traj file
    select case (NREAD)
      case (1)
        read(FILEUNIT_TRAJ, *) TRAJ_TIMESTAMP_NEXT_H, TRAJ_TIMESTAMP_NEXT_M, TRAJ_TIMESTAMP_NEXT_S, &
          &                    (TRAJ_ROW(K), K=1, TRAJ_COLUMN_COUNT)
        TRAJ_TIMESTAMP_NEXT = (TRAJ_TIMESTAMP_NEXT_H - TRAJ_TIMESTAMP_INI_H) * 3600.0_dp &
          &                   + (TRAJ_TIMESTAMP_NEXT_M - TRAJ_TIMESTAMP_INI_M) * 60.0_dp &
          &                   + TRAJ_TIMESTAMP_NEXT_S - TRAJ_TIMESTAMP_INI_S
      case (2)
        read(FILEUNIT_TRAJ, *) TRAJ_TIMESTAMP_NEXT_S, (TRAJ_ROW(K), K=1, TRAJ_COLUMN_COUNT)
        TRAJ_TIMESTAMP_NEXT = TRAJ_TIMESTAMP_NEXT_S - TRAJ_TIMESTAMP_INI_S
      case (3)
        read(FILEUNIT_TRAJ, *) TRAJ_TIMESTAMP_NEXT_M, (TRAJ_ROW(K), K=1, TRAJ_COLUMN_COUNT)
        TRAJ_TIMESTAMP_NEXT = (TRAJ_TIMESTAMP_NEXT_M - TRAJ_TIMESTAMP_INI_M) * 60.0_dp
      case (4)
        read(FILEUNIT_TRAJ, *) TRAJ_TIMESTAMP_NEXT_H, (TRAJ_ROW(K), K=1, TRAJ_COLUMN_COUNT)
        TRAJ_TIMESTAMP_NEXT = (TRAJ_TIMESTAMP_NEXT_H - TRAJ_TIMESTAMP_INI_H) * 3600.0_dp
      case default
        print*, "ERROR: invalid value for variable NREAD: ", NREAD
        stop
    end select
    
    do K = 1, TRAJ_COLUMN_COUNT
      TRAJ_ROW_OLD(K) = TRAJ_ROW(K)
    end do
    
    ! Convert pressure to Pascal
    TRAJ_PRESS_NEXT = TRAJ_ROW(TRAJ_COLIDX_P) * CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL
    
    ! Read vapor pressure of condensable components, in case a column index is specified
    do K = 2, N_PCOND
      if (TRAJ_COLIDX_PCOND(K) .lt. 0) cycle
      TRAJ_PCOND(2, K) = TRAJ_ROW(TRAJ_COLIDX_PCOND(K))
    end do
    
    ! Read temperature
    select case (TRAJ_COLIDX_TGAS)
      case (1 : )  ! (TRAJ_COLIDX_TGAS .gt. 0), scalar
        TRAJ_TEMP_GAS_NEXT = TRAJ_ROW(TRAJ_COLIDX_TGAS)
        if (TRAJ_TEMP_GAS_NEXT .gt. 100.0_dp) TRAJ_TEMP_GAS_NEXT = TRAJ_TEMP_GAS_NEXT - T0  ! Convert temperature to !C
      case ( : -1) ! (TRAJ_COLIDX_TGAS .lt. 0), multiple temperatures
        TRAJ_TEMP_GAS_NEXT = 0.0_dp
        TRAJ_TEMP_GAS_NEXT_MIN = TRAJ_ROW(TRAJ_COLIDX_TGASM(1))
        TRAJ_TEMP_GAS_NEXT_MAX = TRAJ_ROW(TRAJ_COLIDX_TGASM(1))
        
        do K = 1, TRAJ_TEMP_COLUMN_COUNT
          TRAJ_TEMP_GAS_NEXT = TRAJ_TEMP_GAS_NEXT + TRAJ_ROW(TRAJ_COLIDX_TGASM(K))
          TRAJ_TEMP_GAS_NEXT_MIN = min(TRAJ_TEMP_GAS_NEXT_MIN, TRAJ_ROW(TRAJ_COLIDX_TGASM(K)))
          TRAJ_TEMP_GAS_NEXT_MAX = max(TRAJ_TEMP_GAS_NEXT_MAX, TRAJ_ROW(TRAJ_COLIDX_TGASM(K)))
        end do
        
        TRAJ_TEMP_GAS_NEXT = TRAJ_TEMP_GAS_NEXT / real(TRAJ_TEMP_COLUMN_COUNT, kind=dp)
        
        if (TRAJ_TEMP_GAS_NEXT .gt. 100.0_dp) then
          TRAJ_TEMP_GAS_NEXT = TRAJ_TEMP_GAS_NEXT - T0
          TRAJ_TEMP_GAS_NEXT_MIN = TRAJ_TEMP_GAS_NEXT_MIN - T0
          TRAJ_TEMP_GAS_NEXT_MAX = TRAJ_TEMP_GAS_NEXT_MAX - T0
        end if
      case default
        print*, "ERROR: invalid value for variable TRAJ_COLIDX_TGAS: ", TRAJ_COLIDX_TGAS
        print*, "The only implemented options for TRAJ_COLIDX_TGAS are smaller or bigger than zero!"
        stop
    end select  ! (TRAJ_COLIDX_TGAS)
end select ! (READ_PRES)

P = PRESS_START
TGAS = TGAS_START
TABS = TGAS_START + T0

! Initialize vapor pressures of condensable components and convert to Pascal
P_COND(2) = get_p_h2o_sat_ice(TABS)  ! initialize fallback water vapor pressure to be saturated w.r.t. ice
do K = 2, N_PCOND
  if (IS_PCOND_INI_DEFINED(K)) then
    select case (PCOND_INI_UNIT(K))
      case ('PA')
        IND_PINI(K) = 1
        P_COND(K) = PCOND_INI(K)
        continue
      case ('BAR')
        IND_PINI(K) = 1
        PCOND_INI(K) = 1e5_dp * PCOND_INI(K)
      case ('MBAR', 'HPA')
        IND_PINI(K) = 1
        PCOND_INI(K) = 1e2_dp * PCOND_INI(K)
        P_COND(K) = PCOND_INI(K)
      case ('TORR')
        IND_PINI(K) = 1
        PCOND_INI(K) = 133.32_dp * PCOND_INI(K)
        P_COND(K) = PCOND_INI(K)
      case ('RICE')
        IND_PINI(K) = 2
        P_COND(K) = PCOND_INI(K) * get_p_h2o_sat_ice(TABS)
      case ('PPB')
        IND_PINI(K) = 4
        PCOND_INI(K) = 1e-9_dp * PCOND_INI(K)
        P_COND(K) = PCOND_INI(K) * P
      case ('PPM')
        IND_PINI(K) = 4
        PCOND_INI(K) = 1e-6_dp * PCOND_INI(K)
        P_COND(K) = PCOND_INI(K) * P
      case ('RH2O')
        if (K .eq. 2) then
          IND_PINI(K) = 3
          P_COND(K) = PCOND_INI(K) * get_p_h2o_sat_liq(TABS)
        end if
      case default
        print*, "ERROR: Invalid unit for initial partial pressure: ", PCOND_INI_UNIT(K)
        stop
    end select ! (PCOND_INI_UNIT)
  end if
end do


! Initialize air properties
TEMP_POTENTIAL_START = (TGAS + T0)  &
  &                    * (P0 / P)**(  &
  &                                 RGAS_SPECIFIC_DRY_AIR  &
  &                                 / (specific_heat_dry_air_const_pres(TGAS + T0) * 1e3_dp)  &
  &                                )

  
! Specify format of the data output and write header information
OUTPUT_FORMAT_DATA = to_uppercase(OUTPUT_FORMAT_DATA)
select case (OUTPUT_FORMAT_DATA)
  case ('TIME_PRESSURE_TABS')
    write(FILEUNIT_OUTPUT_DATA, *) ' TIME', ' P', ' TABS', ' RHI', ' ZI_NEU'
  case ('ANZAHL_PART')
    write(FILEUNIT_OUTPUT_DATA, *) '    TIME', '       K', '    NUMBER_LIQ', &
      &                           '    R_LIQ', '    NUMBER_ICE', '    R_ICE'
  case ('KRAEMER')
    write(FILEUNIT_OUTPUT_DATA, '(1X, A98)') '    TIME[MIN]  HNO3GAS[PPB] HNO3LIQ[PPB] HNO3ICE[PPB]' //  &
      &                                     '    H2OGAS[PPM] H2OLIQ[PPM] H2OICE[PPM]'
  case ('MAJOR_OUTPUT')
    write(FILEUNIT_OUTPUT_DATA, *)  &
      & 'Time(min)  T(K)     P(hPa)  Np(1/ccm)  Rp(um)  Ni(1/ccm) ' //  &
      & ' Ri(um) Ri_eff(um) Ri_mm(um) RHi(ratio) HNO3_GAS LIQ  ' //  &
      & '   ICE(pptv)    H2O_GAS      LIQ     ICE(ppmv)  Ext(1/m)' //  &
      & ' hom=1,het=2'
  case ('MAJOR_OUTPUT_STEP')
    write(FILEUNIT_OUTPUT_DATA, *) '  TIME  TEMP  PRESS  NICE  RICE  RHI  ICE(ppmv) hom=1,het=2'
  case('AWICIT')
    write(FILEUNIT_OUTPUT_DATA, *) &
      & 'TIME_SEC TEMP_K PRESS_PA ', &
      & 'S_ICE GWC_PPMV LWC_PPMV IWC_PPMV ', &
      & 'R_GEOM_MEAN_ICE_UM N_ICE_PCM3 N_ICE_SUM_HET_PCM3 N_ICE_SUM_HOM_PCM3 ', &
      & 'ICE_NUCL_MODE'
end select  ! (OUTPUT_FORMAT_DATA)


! Start time loop
P_OLD = P
ELAPSED_TIME_S = 0.0_dp
ELAPSED_TIME_M = 0.0_dp
TLOOP = .true.
do while (TLOOP)
  TABS_OLD = TABS
  P_OLD = P
  ELAPSED_TIME_S = ELAPSED_TIME_S + DT_STEP
  ELAPSED_TIME_M = ELAPSED_TIME_S / 60.0_dp
  
  ! Get new values for pressure and temperature
  select case (READ_PRES)
    case (.true.)
    ! read from .traj file
      do while (ELAPSED_TIME_S .gt. TRAJ_TIMESTAMP_NEXT)
      ! iterate throgh rows of .traj file, until previous and current timestamp enclose the model timestamp ELAPSED_TIME_S
        TRAJ_TIMESTAMP_LAST_CHECKED = TRAJ_TIMESTAMP_NEXT
        TRAJ_PRESS_LAST_CHECKED = TRAJ_PRESS_NEXT
        
        select case (NREAD)
          case (1)
            read(FILEUNIT_TRAJ, *, iostat=FILESTATUS) TRAJ_TIMESTAMP_NEXT_H, TRAJ_TIMESTAMP_NEXT_M,  &
              &                                   TRAJ_TIMESTAMP_NEXT_S, (TRAJ_ROW(KK), KK=1, TRAJ_COLUMN_COUNT)
            TRAJ_TIMESTAMP_NEXT = (TRAJ_TIMESTAMP_NEXT_H - TRAJ_TIMESTAMP_INI_H) * 3600.0_dp &
              &                   + (TRAJ_TIMESTAMP_NEXT_M - TRAJ_TIMESTAMP_INI_M) * 60.0_dp &
              &                   + TRAJ_TIMESTAMP_NEXT_S - TRAJ_TIMESTAMP_INI_S
          case (2)
            read(FILEUNIT_TRAJ, *, iostat=FILESTATUS) TRAJ_TIMESTAMP_NEXT_S, (TRAJ_ROW(KK), KK=1, TRAJ_COLUMN_COUNT)
            TRAJ_TIMESTAMP_NEXT = TRAJ_TIMESTAMP_NEXT_S - TRAJ_TIMESTAMP_INI_S
          case (3)
            read(FILEUNIT_TRAJ, *, iostat=FILESTATUS) TRAJ_TIMESTAMP_NEXT_M, (TRAJ_ROW(KK), KK=1, TRAJ_COLUMN_COUNT)
            TRAJ_TIMESTAMP_NEXT = (TRAJ_TIMESTAMP_NEXT_M - TRAJ_TIMESTAMP_INI_M) * 60.0_dp
          case (4)
            read(FILEUNIT_TRAJ, *, iostat=FILESTATUS) TRAJ_TIMESTAMP_NEXT_H, (TRAJ_ROW(KK), KK=1, TRAJ_COLUMN_COUNT)
            TRAJ_TIMESTAMP_NEXT = (TRAJ_TIMESTAMP_NEXT_H - TRAJ_TIMESTAMP_INI_H) * 3600.0_dp
        end select
        
        do K = 1, TRAJ_COLUMN_COUNT
          if (TRAJ_ROW(K) .lt. -1e20_dp) TRAJ_ROW(K) = TRAJ_ROW_OLD(K)
        end do
        
        do K = 1, TRAJ_COLUMN_COUNT
          TRAJ_ROW_OLD(K) = TRAJ_ROW(K)
        end do
        
        TRAJ_PRESS_NEXT = TRAJ_ROW(TRAJ_COLIDX_P) * CONVERSION_FACTOR_TRAJ_PRESS_TO_PASCAL
        
        if (FILESTATUS .eq. -1) then
        ! If end of .traj file is reached
          TRAJ_PRESS_NEXT = -1000.0_dp
          close(FILEUNIT_TRAJ)
        end if
        
        if (TRAJ_PRESS_NEXT .gt. 0.0_dp) then
          TRAJ_PRESS_LATEST = TRAJ_PRESS_LAST_CHECKED
          TRAJ_TIMESTAMP_LATEST = TRAJ_TIMESTAMP_LAST_CHECKED
          do K = 2, N_PCOND
            if (TRAJ_COLIDX_PCOND(K) .lt. 0) cycle
            TRAJ_PCOND(1, K) = TRAJ_PCOND(2, K)
            TRAJ_PCOND(2, K) = TRAJ_ROW(TRAJ_COLIDX_PCOND(K))
          end do
          
          select case (TRAJ_COLIDX_TGAS)
            case (1:)
              TRAJ_TEMP_GAS_LATEST = TRAJ_TEMP_GAS_NEXT
              TRAJ_TEMP_GAS_NEXT = TRAJ_ROW(TRAJ_COLIDX_TGAS)
              if (TRAJ_TEMP_GAS_NEXT .gt. 100.0_dp) TRAJ_TEMP_GAS_NEXT = TRAJ_TEMP_GAS_NEXT - T0
              
            case (:-1)
              TRAJ_TEMP_GAS_LATEST = TRAJ_TEMP_GAS_NEXT
              TRAJ_TEMP_GAS_LATEST_MIN = TRAJ_TEMP_GAS_NEXT_MIN
              TRAJ_TEMP_GAS_LATEST_MAX = TRAJ_TEMP_GAS_NEXT_MAX
              TRAJ_TEMP_GAS_NEXT = 0.0_dp
              TRAJ_TEMP_GAS_NEXT_MIN = TRAJ_ROW(TRAJ_COLIDX_TGASM(1))
              TRAJ_TEMP_GAS_NEXT_MAX = TRAJ_ROW(TRAJ_COLIDX_TGASM(1))
              do K = 1, TRAJ_TEMP_COLUMN_COUNT
                TRAJ_TEMP_GAS_NEXT = TRAJ_TEMP_GAS_NEXT + TRAJ_ROW(TRAJ_COLIDX_TGASM(K))
                TRAJ_TEMP_GAS_NEXT_MIN = min(TRAJ_TEMP_GAS_NEXT_MIN, TRAJ_ROW(TRAJ_COLIDX_TGASM(K)))
                TRAJ_TEMP_GAS_NEXT_MAX = max(TRAJ_TEMP_GAS_NEXT_MAX, TRAJ_ROW(TRAJ_COLIDX_TGASM(K)))
              end do
              
              TRAJ_TEMP_GAS_NEXT = TRAJ_TEMP_GAS_NEXT / real(TRAJ_TEMP_COLUMN_COUNT, kind=dp)
              if (TRAJ_TEMP_GAS_NEXT .gt. 100.0_dp) then
                TRAJ_TEMP_GAS_NEXT = TRAJ_TEMP_GAS_NEXT - T0
                TRAJ_TEMP_GAS_NEXT_MIN = TRAJ_TEMP_GAS_NEXT_MIN - T0
                TRAJ_TEMP_GAS_NEXT_MAX = TRAJ_TEMP_GAS_NEXT_MAX - T0
              end if
            case default
              print*, "ERROR: invalid value for variable TRAJ_COLIDX_TGAS: ", TRAJ_COLIDX_TGAS
              print*, "The only implemented options for TRAJ_COLIDX_TGAS are smaller or bigger than zero!"
              stop
          end select ! case (TRAJ_COLIDX_TGAS)
          
        else
          P = TRAJ_PRESS_LAST_CHECKED
          do K = 2, N_PCOND
            if (TRAJ_COLIDX_PCOND(K) .lt. 0) cycle
            TRAJ_PCOND(3, K) = TRAJ_PCOND(2, K)
          end do
          
          if (TRAJ_COLIDX_TGAS .gt. 0) then
            TGAS = TRAJ_TEMP_GAS_NEXT
          else if (TRAJ_COLIDX_TGAS .lt. 0) then
            TGAS = TRAJ_TEMP_GAS_NEXT
            TGAS_MIN = TRAJ_TEMP_GAS_NEXT_MIN
            TGAS_MAX = TRAJ_TEMP_GAS_NEXT_MAX
          end if
          
          TLOOP = .false.
          TRAJ_TIMESTAMP_NEXT = ELAPSED_TIME_S + DT_STEP
        end if  ! (TRAJ_PRESS_NEXT .gt. 0.0_dp)
      end do  ! WHILE (ELAPSED_TIME_S .gt. TRAJ_TIMESTAMP_NEXT)
      
      ! Linear interpolation of values at ELAPSED_TIME_S between TRAJ_TIMESTAMP_LATEST and TRAJ_TIMESTAMP_NEXT
      if (TLOOP) then
      ! interpolate pressure
        P = ((TRAJ_TIMESTAMP_NEXT - ELAPSED_TIME_S) * TRAJ_PRESS_LATEST &
        &    + (ELAPSED_TIME_S - TRAJ_TIMESTAMP_LATEST) * TRAJ_PRESS_NEXT) &
        &   / (TRAJ_TIMESTAMP_NEXT - TRAJ_TIMESTAMP_LATEST)
      end if
      
      do K = 2, N_PCOND
      ! Interpolate vapor pressures of condensable components
        if (TRAJ_COLIDX_PCOND(K) .lt. 0) cycle
        TRAJ_PCOND(3, K) = ((TRAJ_TIMESTAMP_NEXT - ELAPSED_TIME_S) * TRAJ_PCOND(1, K) &
          &                 + (ELAPSED_TIME_S - TRAJ_TIMESTAMP_LATEST) * TRAJ_PCOND(2, K)) &
          &                / (TRAJ_TIMESTAMP_NEXT - TRAJ_TIMESTAMP_LATEST)
      end do
      
      select case (TRAJ_COLIDX_TGAS)
      ! Interpolate gas temperature
        case (1 : )  ! TRAJ_COLIDX_TGAS.gt.0
          TGAS = ((TRAJ_TIMESTAMP_NEXT - ELAPSED_TIME_S) * TRAJ_TEMP_GAS_LATEST &
            &     + (ELAPSED_TIME_S - TRAJ_TIMESTAMP_LATEST) * TRAJ_TEMP_GAS_NEXT) &
            &    / (TRAJ_TIMESTAMP_NEXT - TRAJ_TIMESTAMP_LATEST)
        case ( : -1)  ! TRAJ_COLIDX_TGAS.lt.0
          TGAS = ((TRAJ_TIMESTAMP_NEXT - ELAPSED_TIME_S) * TRAJ_TEMP_GAS_LATEST &
            &     + (ELAPSED_TIME_S - TRAJ_TIMESTAMP_LATEST) * TRAJ_TEMP_GAS_NEXT) &
            &    / (TRAJ_TIMESTAMP_NEXT - TRAJ_TIMESTAMP_LATEST)
          TGAS_MIN = ((TRAJ_TIMESTAMP_NEXT - ELAPSED_TIME_S) * TRAJ_TEMP_GAS_LATEST_MIN &
            &         + (ELAPSED_TIME_S - TRAJ_TIMESTAMP_LATEST) * TRAJ_TEMP_GAS_NEXT_MIN) &
            &        / (TRAJ_TIMESTAMP_NEXT - TRAJ_TIMESTAMP_LATEST)
          TGAS_MAX = ((TRAJ_TIMESTAMP_NEXT - ELAPSED_TIME_S) * TRAJ_TEMP_GAS_LATEST_MAX &
            &         + (ELAPSED_TIME_S - TRAJ_TIMESTAMP_LATEST) * TRAJ_TEMP_GAS_NEXT_MAX) &
            &        / (TRAJ_TIMESTAMP_NEXT - TRAJ_TIMESTAMP_LATEST)
      end select
      
      if (FORCED_T) then
      ! Force temperature to follow adiabatic relation, overwrites all diabatic effects (e.g. latent heat)
        TGAS = TEMP_POTENTIAL_START  &
          &    * (P0 / P)**(-RGAS_SPECIFIC_DRY_AIR  &
          &                 / (specific_heat_dry_air_const_pres(TGAS + T0) * 1e3_dp))  &
          &    - T0
      end if
      
      
      ! Read vapor flux of condensable components (e.g. wall flux in cloud chambers)
      do K = 2, N_PCOND
        if ((TRAJ_COLIDX_PCOND(K) .eq. -2) .and. (TRAJ_COLIDX_WALLFLUX_PCOND .NE. 0)) then
          TRAJ_WALLFLUX_PCOND(K) = TRAJ_ROW(TRAJ_COLIDX_WALLFLUX_PCOND)  ! WALL FLUX AMOUNT
        end if
        
        ! wall flux must be passed in the same unit as given in [H2O] field in init file
        select case (PCOND_INI_UNIT(K))
          case ('PPM')
            TRAJ_WALLFLUX_PCOND(K) = 1e-6_dp * TRAJ_WALLFLUX_PCOND(K) * P
          case ('PPB')
            TRAJ_WALLFLUX_PCOND(K) = 1e-9_dp * TRAJ_WALLFLUX_PCOND(K) * P
          case ('MBAR', 'HPA')
            TRAJ_WALLFLUX_PCOND(K) = 100.0_dp * TRAJ_WALLFLUX_PCOND(K)
          case ('PA')
            ! default case, nothing to do here.
          case default
            print*, "ERROR: probably unsupported unit for wall flux of condensable component!"
            stop
        end select
                
        DELTA_P_COND(K) = 0.0_dp
        if (TRAJ_COLIDX_PCOND(K) .eq. -2) then
          DELTA_P_COND(K) = TRAJ_WALLFLUX_PCOND(K) * (DT_STEP / (TRAJ_TIMESTAMP_NEXT - TRAJ_TIMESTAMP_LATEST))
        end if
      end do
      
      TABS = TGAS + T0
      
    case (.false.)  ! (READ_PRES)
    ! Built-in adiabatic updraft
    
      if (.not. IS_DURING_RUNTIME_EXTENSION) then
        CP_DRY_AIR = specific_heat_dry_air_const_pres(TABS)  ! in kJ/kg/K
        DELTA_TEMP_ADIABATIC_STEP = - get_dry_adiabatic_lapse_rate(TABS) * UPDRAFT * DT_STEP
        DP_STEP = P * ((TABS + DELTA_TEMP_ADIABATIC_STEP) / TABS)**((CP_DRY_AIR * 1e3_dp) / RGAS_SPECIFIC_DRY_AIR) - P
      else
        DELTA_TEMP_ADIABATIC_STEP = 0.0_dp
        DP_STEP = 0.0_dp
      end if
      
      P = P + DP_STEP  ! pressure change due to expansion of air / updraft
      TABS = TABS_OLD + DELTA_TEMP_ADIABATIC_STEP

      ! gravity wave driven updraft velocity fluctuations
      if (USE_GRAVITY_WAVE_FORCING) then
        
        ! update only after each integer multiple of GRAVITY_WAVE_AUTOCORRELATION_TIME
        if (floor(ELAPSED_TIME_S / GRAVITY_WAVE_AUTOCORRELATION_TIME) .ge. COUNT_GRAVITY_WAVE_UPDRAFT_REFRESHS) then
          call get_gravity_wave_fluctuations(DELTA_W, DELTA_TEMP_GRAVITY_WAVE_UPDRAFT_FLUCT, GRAVITY_WAVE_UPDRAFT_FORCING_SAMPLE, &
            &                                TABS, CORIOLIS_FREQUENCY, DT_STEP)
          COUNT_GRAVITY_WAVE_UPDRAFT_REFRESHS = COUNT_GRAVITY_WAVE_UPDRAFT_REFRESHS + 1
        end if
        
        ! get (adiabatic) pressure change due to forced temperature fluctuation
        DELTA_P_GRAVITY_WAVE_UPDRAFT_FLUCT = P * ((TABS + DELTA_TEMP_GRAVITY_WAVE_UPDRAFT_FLUCT) / TABS)**( &
          & (CP_DRY_AIR * 1e3_dp) / RGAS_SPECIFIC_DRY_AIR) - P
          
        P = P + DELTA_P_GRAVITY_WAVE_UPDRAFT_FLUCT
        TABS = TABS + DELTA_TEMP_GRAVITY_WAVE_UPDRAFT_FLUCT
      end if
      
      TGAS = TABS - T0
  end select  ! (READ_PRES)
  
  
  ! Update autocorrelation timescale of gavity wave driven updraft fluctuations with updated ambient concitions (and derived gradients)
  if (USE_GRAVITY_WAVE_FORCING .and. (.not. GRAVITY_WAVE_AUTOCORRELATION_TIME_STATIC .gt. 0.0)) then
    BUOYANCY_FREQ = get_buoyancy_frequency((TABS_OLD + TABS) / 2.0_dp, &
      &                                    (P + P_OLD) / 2.0_dp, &
      &                                    TEMP_GRADIENT= (TABS - TABS_OLD) / ((UPDRAFT + DELTA_W) * DT_STEP),  &
      &                                    PRESS_GRADIENT= (P - P_OLD) / ((UPDRAFT + DELTA_W) * DT_STEP))

    if (.not. ieee_is_normal(BUOYANCY_FREQ)) then
      print*, "ERROR: BUOYANCY_FREQ = ", BUOYANCY_FREQ
      stop "Exit due to unnormal BUOYANCY_FREQ"
    end if
    
    GRAVITY_WAVE_AUTOCORRELATION_TIME = get_gravity_wave_forcing_autocorrelation_time(BUOYANCY_FREQ)
  end if
  
  
  ! Apply diabatic effects from last time step
  DENSITY_AIR = DENSITY_AIR_273K * (P / P0) * (T0 / TABS)
  CP_DRY_AIR = specific_heat_dry_air_const_pres(TABS)
  select case (READ_PRES)
    case (.true.)  ! use external trajectory file
      ! Use accumulated diabatic effect, because the temperatures from .traj file do not include the
      ! model-intrinsic diabatic effects from previous timesteps.
      LATENT_HEAT_SUM = LATENT_HEAT_SUM + LATENT_HEAT_STEP
      DELTA_TEMP_LATENT_HEAT = LATENT_HEAT_SUM * 1e3_dp / (CP_DRY_AIR * DENSITY_AIR)  ! temperature change due to latent heat effects from last loop
    case (.false.)  ! internal trajectory
      ! Use latent heat from last timestep, because the previous temperatures were already corrected wrt adiabatic effects
      DELTA_TEMP_LATENT_HEAT = LATENT_HEAT_STEP * 1e3_dp / (CP_DRY_AIR * DENSITY_AIR)  ! temperature change due to latent heat effects from last loop
  end select
  
  if (.not. FORCED_T) then
    P = P * (TABS + DELTA_TEMP_LATENT_HEAT) / TABS  ! scale as isochoric change (constant volume)
    TABS = TABS + DELTA_TEMP_LATENT_HEAT
    TGAS = TABS - T0
    DENSITY_AIR = DENSITY_AIR_273K * (P / P0) * (T0 / TABS)  ! update variable to include also the diabatic contribution
    CP_DRY_AIR = specific_heat_dry_air_const_pres(TABS)  ! update variable to include also the diabatic contribution
  end if
  
  
  ! Update variables for the new timestamp
  P_H2O_SAT_LIQ = get_p_h2o_sat_liq(TABS)
  P_H2O_SAT_ICE = get_p_h2o_sat_ice(TABS)
  WATER_ACTIVITY_ICE = P_H2O_SAT_ICE / P_H2O_SAT_LIQ
  SAT_ICE = P_COND(2) / P_H2O_SAT_ICE
  DIFF_COEFS(2) = diff_coef_h2o(TABS, P)
  DIFF_COEFS(3) = diff_coef_hno3(TABS, P)
  DIFF_COEFS(4) = DIFF_COEFS(3)
  DIFF_COEFS(5) = DIFF_COEFS(3)
  LATENT_HEAT_STEP = 0.0_dp
  ICE_NUCLEATION_MODE = 'none'
  
  
  ! Scale partial pressures according to change of absolute pressure
  do K = 2, N_PCOND
  ! scale partial pressures / concetrations of condensable components
    if (IS_PCOND_CONST(K)) then
    ! If vapor mixing ratios are kept constant
      select case (IND_PINI(K))
        case (1)  ! PCOND_INI given in Pa
          P_COND(K) = PCOND_INI(K)
        case (2)  ! PCOND_INI given as relative humidity w.r.t. ice
          P_COND(K) = PCOND_INI(K) * P_H2O_SAT_ICE
        case (3)  ! PCOND_INI given as relative humidity w.r.t. water
          P_COND(K) = PCOND_INI(K) * P_H2O_SAT_LIQ
        case (4)  ! PCOND_INI given as volume concentration
          P_COND(K) = PCOND_INI(K) * P
        case default
          print*, "ERROR: invalid index value K for variable IS_PCOND_CONST(K): ", K
          stop
      end select
      P_COND(K) = P_COND(K) * (P / PRESS_START)
    else
      P_COND(K) = P_COND(K) * (P / P_OLD)
    end if
    C_COND(K) = P_COND(K) * 10.0_dp * MOLAR_MASS_SOLUTES(K) / (RGAS_CGS * TABS_OLD)
  end do
  
  ! Scale particle concentrations according to change of absolute pressure
  if (PART_PRESENT) then
    do K = KULIM_H2O, KOLIM_H2O
      Z_H2O(K) = Z_H2O(K) * (P / P_OLD)
    end do
    do K = KULIM_ICE, KOLIM_ICE
      Z_ICE(K) = Z_ICE(K) * (P / P_OLD)
    end do
    do K = KULIM_IN, KOLIM_IN
      Z_IN(K) = Z_IN(K) * (P / P_OLD)
    end do
  end if
  
  ! Count ice crystals formed in previous time step
  Z_ICE_HET_SUM = Z_ICE_HET_SUM + Z_ICE_NEW_HET_STEP
  Z_ICE_NEW_HET_STEP = 0.0_dp
  Z_ICE_HOM_SUM = Z_ICE_HOM_SUM + Z_ICE_NEW_HOM_STEP
  Z_ICE_NEW_HOM_STEP = 0.0_dp
  Z_ICE_FORMED_SUM = Z_ICE_FORMED_SUM + Z_ICE_NEW_STEP
  Z_ICE_NEW_STEP = 0.0_dp
  do K = 1, N_BINS
    Z_ICE_NEW(K) = 0.0_dp
    Z_ICE_NEW_HET(K) = 0.0_dp
    Z_ICE_NEW_HOM(K) = 0.0_dp
  end do
  
  
  ! Apply particle processes
  ! - condensatinon / evaporation between liquid and vapor phase
  select case (PART_PRESENT)
    
    case (.true.)  ! (PART_PRESENT)
      do K = 2, N_SOLUTES
        AV_VEL(K) = sqrt(8.0_dp * RGAS_CGS * TABS_OLD / (MOLAR_MASS_SOLUTES(K) * PI))
        DC_OUT(K) = 0.0_dp
      end do
      
      ! Treat liquid particles
      if (IS_H2O_PRESENT) then
        
        select case (IS_H2O_PURE)
        
          case (.true.)  ! (IS_H2O_PURE)
            ! Pure liquid water particles
            
            ! NOTE: This call used to be commented, since reactivation it was not tested.
            ! Be cautious about DMASS_EVAP and EVAP which have intent `inout` and are also called regarding ice crystals!
            call condensation_water_particle(RAD_H2O, PMASS_H2O, Z_H2O, ZERO_H2O, &
              &                              RHO_H2O, DIFF_COEFS(2), TABS_OLD, &
              &                              AV_VEL(2), P_H2O_SAT_LIQ, P_COND(2), DC_OUT, LATENT_HEAT_PROCESS, DT_STEP, &
              &                              KULIM_H2O, KOLIM_H2O, N_SOLUTES_LBOUND, N_SOLUTES, N_PCOND, IS_H2O_PRESENT, &
              &                              DMASS_EVAP, EVAP)
            LATENT_HEAT_STEP = LATENT_HEAT_STEP + LATENT_HEAT_PROCESS
            
            do K = KULIM_H2O, KOLIM_H2O
              WATER_ACTIVITY_SOL(K) = P_COND(2) / P_H2O_SAT_LIQ
              VOL_PART(K) = PMASS_H2O(0, K) / RHO_H2O
            end do
        
          case (.false.)  ! (IS_H2O_PURE)
          ! Aqueous solution particles
            if (CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL .gt. 0.0_dp) then
            ! value of TRAJ_PCOND given as pressure
              do K = 2, N_PCOND
                if (TRAJ_COLIDX_PCOND(K) .lt. 0) cycle
                P_COND(K) = CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL * TRAJ_PCOND(3, K)
                C_COND(K) = P_COND(K) * 10.0_dp * MOLAR_MASS_SOLUTES(K) / (RGAS_CGS * TABS_OLD)
              end do
            else
            ! value of TRAJ_PCOND given as mixing ratio (ppmv or ppbv)
              do K = 2, N_PCOND
                if (TRAJ_COLIDX_PCOND(K) .lt. 0) cycle
                P_COND(K) = -CONVERSION_FACTOR_TRAJ_PCOND_TO_PASCAL * TRAJ_PCOND(3, K) * P
                C_COND(K) = P_COND(K) * 10.0_dp * MOLAR_MASS_SOLUTES(K) / (RGAS_CGS * TABS_OLD)
              end do
            end if

            ! Condensation subroutine for H2SO4/H2O solutions
            call condensation_solution_particle(RAD_H2O, PMASS_H2O, Z_H2O, ZERO_H2O,  &
              &                                 P_SAT_SOLUTE_H2O, RAD_EQ, PMASS_EQ,  &
              &                                 TABS_OLD, LATENT_HEAT_PROCESS, DT_STEP,  &
              &                                 KULIM_H2O, KOLIM_H2O, N_SOLUTES, N_PCOND,  &
              &                                 DO_NOT_CALC_EQUILIBRIUM, IS_H2O_PRESENT,  &
              &                                 IS_PCOND_CONST, DIFF_COEFS, AV_VEL, DC_OUT, P_COND)
            LATENT_HEAT_STEP = LATENT_HEAT_STEP + LATENT_HEAT_PROCESS
            
            if (.not. DO_NOT_CALC_EQUILIBRIUM) then
              ! For equilibrium calculations
              do KK = 1, 2
                do K = KULIM_H2O, KOLIM_H2O
                  RAD_H2O(K) = RAD_EQ(K)
                  PMASS_H2O(KK, K) = PMASS_EQ(KK, K)
                end do
                PMASS_H2O(0, K) = PMASS_EQ(0, K)
              end do
            end if
            
            if (.not. IS_H2O_PRESENT) then
            ! In case all liquid particles evaporated in the current timestep
              PART_PRESENT = (IS_ICE_PRESENT .or. (sum(Z_IN) .gt. CUTOFF_NUMBER_CONC))
              do K = 1, N_SOLUTES
                WT_AVH2O(K) = 0.0_dp
              end do
              Z_H2O_TOT = 0.0_dp
              RG_H2O = 0.0_dp
            end if
            
            ! Calculate water activity of solution particles
            do K = KULIM_H2O, KOLIM_H2O
              VOL_PART(K) = 4.0_dp / 3.0_dp * PI * RAD_H2O(K)**3
              do KK = 1, N_SOLUTES
                X_WT(KK) = PMASS_H2O(KK, K) / PMASS_H2O(0, K)
              end do
              
              select case (KPAR_AW_P)
                case (1)
                  WATER_ACTIVITY_SOL(K) = P_SAT_SOLUTE_H2O(K) / P_H2O_SAT_LIQ
                  
                case (2)
                  H2SO4_1 = X_WT(1) / (X_WT(1) + (1.0_dp - X_WT(1))  &
                    &                  * MOLAR_MASS_SOLUTES(1) / MOLAR_MASS_SOLUTES(2))
                  WATER_ACTIVITY_SOL(K) = water_activity_sol_shi2001(TABS, H2SO4_1)
                case (3)
                  WATER_ACTIVITY_SOL(K) = water_activity_sol_schneider2021(TABS, P_H2O_SAT_LIQ, P_H2O_SAT_ICE)
              end select  ! (KPAR_AW_P)
            end do
        end select  ! (IS_H2O_PURE)
      end if  ! (IS_H2O_PRESENT)
      
      if (Z_ICE_NEW_STEP .lt. (10.0_dp * CUTOFF_NUMBER_CONC)) then
        do K = KULIM_H2O, KOLIM_H2O
          DMASS_TRAP_SUM(K) = 0.0_dp
        end do
      end if
      
      
      ! ------------------------------------------------------------------
      !
      ! Ice formation
      !
      ! Notes: * No nucleation in case of pre-existing ice.
      !        * DELTA_AW boundaries:
      !            - homogeneous: 0.26 < DELTA_AW < 0.34
      !            - heterogeneous: 0.06 < DELTA_AW < 0.34
      !
      ! ------------------------------------------------------------------
      if (.not. (SUPPRESS_ICE) .and. (SAT_ICE .gt. 1.01_dp)) then
        LATENT_HEAT_PROCESS = 0.0_dp
        
        ! Implementation of freezing mechanism is selected by PAR_HET_FREEZE
        select case (PAR_HET_FREEZE)
          case ('KL2003')
          ! Heterogeneous immersion freezing
          ! Literature:
          !   * B. Kärcher and U. Lohmann, “A parameterization of cirrus cloud formation: Heterogeneous freezing,”
          !     Journal of Geophysical Research, vol. 108, no. D14, 2003, doi: 10.1029/2002jd003220
          !   * H. R. Pruppacher and J. D. Klett, "Microphysics of Clouds and Precipitation",
          !     vol. 18. Springer Netherlands, 2010, p.211
            
            do K = KULIM_H2O, KOLIM_H2O
              DELTA_AW(K) = WATER_ACTIVITY_SOL(K) - WATER_ACTIVITY_ICE
            end do
            
            ! Heterogeneous immersion freeing
            if ((N_NUCL_INI .eq. 2) .and. (Z_ICE_HET_SUM .lt. Z_IN_TOT)) N_NUCL = 2  ! Reactivate het freezing if not all INPs are activated
            if (N_NUCL .eq. 2) then
              call ice_form_het_immersion_KL2003( &
                & IN_TYPE, TABS, DT_STEP, WATER_ACTIVITY_ICE, DELTA_AW, &
                & Z_H2O, RAD_H2O, PMASS_H2O, VOL_PART, ZERO_H2O, KULIM_H2O, KOLIM_H2O, &
                & Z_IN_TOT, Z_ICE_HET_SUM, N_NUCL, &
                & Z_ICE_NEW_HET, LATENT_HEAT_PROCESS, VOLRATIO)
              LATENT_HEAT_STEP = LATENT_HEAT_STEP + LATENT_HEAT_PROCESS
            end if
            
            ! Homogeneous freezing
            call ice_form_hom_Koop2000( &
              & (Z_H2O - Z_ICE_NEW_HET), &  ! Note: apply only on those droplets which didn't freeze heterogeneously already
              & PMASS_H2O, VOL_PART, ZERO_H2O, KULIM_H2O, KOLIM_H2O, DT_STEP, DELTA_AW, &  ! intent(in)
              & STAY_LIQUID_PROBABILITY, Z_ICE_NEW_HOM, & ! intent(inout)
              & LATENT_HEAT_PROCESS, &  ! intent(out)
              & PAR_HET_FREEZE, VOLRATIO)  ! optional, intent(in)
            LATENT_HEAT_STEP = LATENT_HEAT_STEP + LATENT_HEAT_PROCESS
            
        case ('SOOT_ULLRICH2017', 'DUST_ULLRICH2017', 'DUST_ULLRICH2017_UPDATED', &
          &   'ATD_STEINKE2015', 'FUMED_SILICA_SCHORR', 'QUARTZ_SCHORR', 'CALCIUM_CARBONATE_SCHORR')
        ! Heterogeneous deposition freezing according to ice-nucleating active site (INAS) density approach.
        ! You must choose the parameterization through `PAR_HET_FREEZE`.
                    
          ! Heterogeneous deposition freezing
          if ((Z_IN_TOT - Z_ICE_HET_SUM) .gt. CUTOFF_NUMBER_CONC) then
            
            ! calculate the number of frozen INPs at current conditions
            FROZEN_INP_OLD = FROZEN_INP
            call ice_form_het_deposition_ns( &
              & TABS_OLD, SAT_ICE, &
              & Z_IN, Z_IN_INI, SURF_IN, &
              & NS, FROZEN_INP, PAR_HET_FREEZE)
              
            if (any(FROZEN_INP(:) .gt. FROZEN_INP_OLD(:))) then
              ! if new het. ice crystals have formed
              
              if (RAD_ICE(NEW_HET_ICE_BIN) .gt. RAD_THRESHOLD_NEW_HET_ICE) then
                ! use new empty bin if bin if the radius of the previous bin grew too big
                  if (NEW_HET_ICE_BIN .lt. N_BINS) then
                    
                    NEW_HET_ICE_BIN = NEW_HET_ICE_BIN + 1
                    
                    ! initialize next ice bin to be filled with new het ice
                    Z_ICE(NEW_HET_ICE_BIN)            = 0.0_dp
                    ZERO_ICE(NEW_HET_ICE_BIN)         = .true.
                    
                    RAD_ICE(NEW_HET_ICE_BIN)          = R_IN
                    PMASS_ICE(0, NEW_HET_ICE_BIN)     = 4.0_dp / 3.0_dp * PI * R_IN**3 * RHO_ICE
                    PMASS_ICE(1, NEW_HET_ICE_BIN)     = 0.0_dp
                    PMASS_ICE(2, NEW_HET_ICE_BIN)     = 4.0_dp / 3.0_dp * PI * R_IN**3 * RHO_ICE
                    PMASS_ICE(3, NEW_HET_ICE_BIN)     = 0.0_dp
                    PMASS_ICE(4, NEW_HET_ICE_BIN)     = 0.0_dp
                    
                    ! NOTE: mass of new ice will be deterimined by corresponding water mass bin. This logic originates from immersion freezing.
                    Z_H2O(NEW_HET_ICE_BIN)            = 0.0_dp
                    ZERO_H2O(NEW_HET_ICE_BIN)         = .true.
                    RAD_H2O(NEW_HET_ICE_BIN)          = R_IN
                    PMASS_H2O(0, NEW_HET_ICE_BIN)     = 4.0_dp / 3.0_dp * PI * R_IN**3 * RHO_H2O
                    PMASS_H2O(1, NEW_HET_ICE_BIN)     = 0.0_dp
                    PMASS_H2O(2, NEW_HET_ICE_BIN)     = 4.0_dp / 3.0_dp * PI * R_IN**3 * RHO_H2O
                    PMASS_H2O(3, NEW_HET_ICE_BIN)     = 0.0_dp
                    PMASS_H2O(4, NEW_HET_ICE_BIN)     = 0.0_dp
                    STAY_LIQUID_PROBABILITY(NEW_HET_ICE_BIN) = 1.0_dp    
                    
                  else
                    if (COMPILED_WITH_G) then
                      print*, "WARNING: No empty bin available for new heterogeneous ice crystals. Consider increasing N_BINS."
                    end if
                  end if
                end if
                
              ! subtract new ice crystals from INP population and add to counter of het ice
              do K = 1, KMAX
                if ( (Z_IN_INI(K) - FROZEN_INP(K)) .lt. Z_IN(K) ) then
                  Z_IN(K) = Z_IN_INI(K) - FROZEN_INP(K)  ! remaining INPs
                  Z_ICE_NEW_HET(NEW_HET_ICE_BIN) = Z_ICE_NEW_HET(NEW_HET_ICE_BIN) + (FROZEN_INP(K) - FROZEN_INP_OLD(K))  ! frozen INPs compared to previous timestep
                end if
              end do
              
              ZERO_ICE(NEW_HET_ICE_BIN) = .false.
              
            end if  ! any(FROZEN_INP(:) .gt. FROZEN_INP_OLD(:))
          end if  ! ((Z_IN_TOT - Z_ICE_HET_SUM) .gt. CUTOFF_NUMBER_CONC)
          
          
          ! Homogeneous freezing
          do K = KULIM_H2O, KOLIM_H2O
            DELTA_AW(K) = WATER_ACTIVITY_SOL(K) - WATER_ACTIVITY_ICE
          end do
          call ice_form_hom_Koop2000( &
            & Z_H2O, PMASS_H2O, VOL_PART, ZERO_H2O, KULIM_H2O, KOLIM_H2O, DT_STEP, DELTA_AW, &  ! intent(in)
            & STAY_LIQUID_PROBABILITY, Z_ICE_NEW_HOM, &  ! intent(inout)
            & LATENT_HEAT_PROCESS)  ! intent(out)
          
          LATENT_HEAT_STEP = LATENT_HEAT_STEP + LATENT_HEAT_PROCESS
          
        end select  ! (PAR_HET_FREEZE), KL2003 or INAS-based
      end if  ! if (.not. (SUPPRESS_ICE) .and. (SAT_ICE .gt. 1.01_dp))
      
      Z_ICE_NEW = Z_ICE_NEW_HOM + Z_ICE_NEW_HET
      Z_ICE_NEW_HOM_STEP = sum(Z_ICE_NEW_HOM)
      Z_ICE_NEW_HET_STEP = sum(Z_ICE_NEW_HET)
      Z_ICE_NEW_STEP = sum(Z_ICE_NEW)
      
      if (((Z_ICE_FORMED_SUM + Z_ICE_NEW_STEP) .gt. (Z_IN_TOT + Z_ICE_HET_SUM)) .and. (N_NUCL .eq. 2)) then 
        N_NUCL = 1
      end if
      
      
      if (Z_ICE_NEW_STEP == 0.0_dp) then
        ICE_NUCLEATION_MODE = 'none'
      else if ((Z_ICE_NEW_HET_STEP > 0.0_dp) .and. (Z_ICE_NEW_HOM_STEP > 0.0_dp)) then
        ICE_NUCLEATION_MODE = 'mix'
      else if ((Z_ICE_NEW_HET_STEP > 0.0_dp) .and. (Z_ICE_NEW_HOM_STEP == 0.0_dp)) then
        ICE_NUCLEATION_MODE = 'het'
      else if ((Z_ICE_NEW_HET_STEP == 0.0_dp) .and. (Z_ICE_NEW_HOM_STEP > 0.0_dp)) then
        ICE_NUCLEATION_MODE = 'hom'
      else
        print*, "Could not specify ICE_NUCLEATION_MODE, please check this condition!"
      end if
      
      ! End ice formation
      
      
      if (IS_ICE_PRESENT) then
        do K = KULIM_ICE, KOLIM_ICE
          EVAP(K) = .false.
          DMASS_EVAP(K) = 0.0_dp
        end do
        
        ! Calculate evaporation/growth of ice from vapor phase, like condensation/growth of liquid particles
        call condensation_water_particle(RAD_ICE, PMASS_ICE, Z_ICE, ZERO_ICE,  &
          &                              RHO_ICE, DIFF_COEFS(2), TABS_OLD,  &
          &                              AV_VEL(2), P_H2O_SAT_ICE, P_COND(2), DC_OUT,  &
          &                              LATENT_HEAT_PROCESS, DT_STEP, KULIM_ICE, KOLIM_ICE,  &
          &                              N_SOLUTES_LBOUND, N_SOLUTES, N_PCOND, IS_ICE_PRESENT,  &
          &                              DMASS_EVAP, EVAP)
        LATENT_HEAT_STEP = LATENT_HEAT_STEP + LATENT_HEAT_PROCESS
        
        ! Calculate trapping of HNO3 in ice crystals
        call HNO3_TRAP(RAD_ICE, PMASS_ICE, Z_ICE, ZERO_ICE, EVAP,  &
          &            DMASS_EVAP, TABS, P_COND(3), LATENT_HEAT_PROCESS, DT_STEP,  &
          &            KULIM_ICE, KOLIM_ICE, N_SOLUTES,  &
          &            DMASS_TRAP_SUM, DIFF_COEFS, AV_VEL, DC_OUT)
        LATENT_HEAT_STEP = LATENT_HEAT_STEP + LATENT_HEAT_PROCESS
      end if
      
      
      ! increase KOLIM if a new ice bin was populated for deposition freezing
      if (NEW_HET_ICE_BIN .gt. KOLIM_ICE) KOLIM_ICE = NEW_HET_ICE_BIN
      
      ! Redistribute bin population in size distributions in case of new ice crystals (Increase ice crystal bins, reduce liquid particle bins)
      ! TODO: Check that liquid particle SD is not reduced in case of deposition freezing
      if (Z_ICE_NEW_STEP .gt. (10.0_dp * CUTOFF_NUMBER_CONC)) then
        
        do K = KULIM_H2O, KOLIM_ICE
          ! Increase ice number size distribution with freshly formed ice crystals
          Z_ICE_OLD = Z_ICE(K)
          Z_ICE(K) = Z_ICE(K) + Z_ICE_NEW(K)
          
          if (Z_ICE(K) .gt. CUTOFF_NUMBER_CONC) then
            ! Reduce liquid particle number wrt immersion freezing and homogeneous freezing
            Z_H2O(K) = Z_H2O(K) * STAY_LIQUID_PROBABILITY(K)
            
            ! Recalibrate ice particle radius wrt ice particle mass
            PMASS_ICE(1, K) = PMASS_H2O(1, K)
            PMASS_ICE(0, K) = PMASS_ICE(1, K)
            do KK = 2, N_SOLUTES
              PMASS_ICE(KK, K) = (PMASS_ICE(KK, K) * Z_ICE_OLD + PMASS_H2O(KK, K) * Z_ICE_NEW(K)) &
                &                / (Z_ICE_OLD + Z_ICE_NEW(K))
              PMASS_ICE(0, K) = PMASS_ICE(0, K) + PMASS_ICE(KK, K)
            end do
            RAD_ICE(K) = (PMASS_ICE(0, K) / (RHO_ICE * 4.0_dp / 3.0_dp * PI))**(1.0_dp / 3.0_dp)
          else
            Z_ICE(K) = 0.0_dp
            PMASS_ICE(1, K) = PMASS_H2O(1, K)
            PMASS_ICE(0, K) = PMASS_H2O(0, K)
          end if
        end do
        
        ! Check uppper and lower  bins for liq. particles and ice particles.
        KULIM_H2O = 1
        KOLIM_H2O = KOLIM
        FOUND_POPULATED_BIN_H2O = .false.
        do K = 1, KMAX
          if (FOUND_POPULATED_BIN_H2O) KULIM_H2O = KULIM_H2O + 1
          if (Z_H2O(K) .gt. CUTOFF_NUMBER_CONC) then
            KOLIM_H2O = K
            ZERO_H2O(K) = .false.
            FOUND_POPULATED_BIN_H2O = .true.
          else
            Z_H2O(K) = 0.0_dp
            ZERO_H2O(K) = .true.
          end if
        end do
        KULIM_H2O = KMAX - KULIM_H2O + 1
        
        KULIM_ICE = 1
        KOLIM_ICE = KOLIM
        FOUND_POPULATED_BIN_ICE = .false.
        do K = 1, N_BINS
          if (FOUND_POPULATED_BIN_ICE) KULIM_ICE = KULIM_ICE + 1
          if (Z_ICE(K) .gt. CUTOFF_NUMBER_CONC) then
            KOLIM_ICE = K
            ZERO_ICE(K) = .false.
            FOUND_POPULATED_BIN_ICE = .true.
          else
            Z_ICE(K) = 0.0_dp
            ZERO_ICE(K) = .true.
          end if
        end do
        KULIM_ICE = N_BINS - KULIM_ICE + 1
        IS_ICE_PRESENT = .true.
      end if
      
      
      ! Balance of partial vapor pressure of the condensable gases
      do K = 2, N_PCOND
        C_COND(K) = C_COND(K) - DC_OUT(K)
        P_COND(K) = C_COND(K) * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(K)) + DELTA_P_COND(K)
      end do
      
      PART_PRESENT = (IS_H2O_PRESENT .or. IS_ICE_PRESENT .or. (sum(Z_IN) .gt. CUTOFF_NUMBER_CONC))
    
    case (.false.)  ! (PART_PRESENT)
      do K = 2, N_PCOND
        P_COND(K) = P_COND(K) - DELTA_P_COND(K)
      end do
      
  end select  ! (PART_PRESENT)
  
  
  ! Melting of ice crystals
  if (SAT_ICE .le. 1.0_dp .and. IS_ICE_PRESENT) then
    call apply_ice_melting(LATENT_HEAT_PROCESS,  &  ! intent(out)
    &                      Z_H2O, Z_ICE, ZERO_H2O, ZERO_ICE,  &  ! intent(inout)
    &                      PMASS_H2O, PMASS_ICE, RAD_H2O, IS_H2O_PRESENT, IS_ICE_PRESENT,  &  ! intent(inout)
    &                      KULIM_ICE, KOLIM_ICE, KULIM_H2O, KOLIM_H2O,  &  ! intent(inout)
    &                      TABS, N_SOLUTES, XWT_H2SO4_H2O_INI, X,  &  ! intent(in)
    &                      N_NUCL, N_NUCL_INI)  ! intent(inout), intent(in)
    LATENT_HEAT_STEP = LATENT_HEAT_STEP + LATENT_HEAT_PROCESS
  end if
  
  
  ! Sedimentation of ice crystals
  if (IS_ICE_PRESENT .and. DO_SEDIMENTATION) then
    call apply_sedimentation(TABS, P, DENSITY_AIR, PMASS_ICE, Z_ICE,  &
      &                      KULIM_ICE, KOLIM_ICE, DT_STEP, DZ, SEDFACTOR)
  end if


  ! Get the total ice number concentration
  if (IS_ICE_PRESENT) then
    Z_ICE_TOT = 0.0_dp
    do K = KULIM_ICE, KOLIM_ICE
      Z_ICE_TOT = Z_ICE_TOT + Z_ICE(K)
    end do
  end if
  
  
  ! Write to data output file
  if (int(ELAPSED_TIME_S / DT_OUT) .ge. LINES_WRITTEN_OUT) then
    LINES_WRITTEN_OUT = LINES_WRITTEN_OUT + 1
    
    select case (OUTPUT_FORMAT_DATA)
      case ('TIME_PRESSURE_TABS')  ! (OUTPUT_FORMAT_DATA)
        write(FILEUNIT_OUTPUT_DATA,  &
          &   '(1X, F9.3, 4(2X, 1PE11.4))')  &
          &  ELAPSED_TIME_M, (P * 0.01_dp), TABS, SAT_ICE, Z_ICE_NEW_STEP
        
      case ('EFFIZIENZ')  ! (OUTPUT_FORMAT_DATA)
        KULIM = min(KULIM_ICE, KULIM_H2O)
        KOLIM = max(KOLIM_ICE, KOLIM_H2O)
        KULIM_H2O = 0
        KOLIM_H2O = KOLIM
        KULIM_ICE = 0
        KOLIM_ICE = KOLIM
        FOUND_POPULATED_BIN_H2O = .false.
        FOUND_POPULATED_BIN_ICE = .false.
        do K = KULIM, KOLIM
          if (Z_ICE(K) .gt. CUTOFF_NUMBER_CONC) then
            KOLIM_ICE = K
            ZERO_ICE(K) = .false.
            FOUND_POPULATED_BIN_ICE = .true.
          else
            Z_ICE(K) = 0.0_dp
            ZERO_ICE(K) = .true.
          end if
          
          if (Z_H2O(K) .gt. CUTOFF_NUMBER_CONC) then
            KOLIM_H2O = K
            ZERO_H2O(K) = .false.
            FOUND_POPULATED_BIN_H2O = .true.
          else
            Z_H2O(K) = 0.0_dp
            ZERO_H2O(K) = .true.
          end if
          if (FOUND_POPULATED_BIN_H2O) KULIM_H2O = KULIM_H2O + 1
          if (FOUND_POPULATED_BIN_ICE) KULIM_ICE = KULIM_ICE + 1
          
          write(FILEUNIT_OUTPUT_DATA, '(F9.4, 1X, I5, 4(2X, 1PE11.4))') &
            &  ELAPSED_TIME_M, K, Z_H2O(K), RAD_H2O(K) * 1e4_dp, Z_ICE(K), RAD_ICE(K)*1e4_dp
            
        end do
        if (FOUND_POPULATED_BIN_H2O) then
          KULIM_H2O = KOLIM - KULIM_H2O + 1
          IS_H2O_PRESENT = .true.
        else
          KULIM_H2O = 1
          KOLIM_H2O = 1
        end if
        if (FOUND_POPULATED_BIN_ICE) then
          KULIM_ICE = KOLIM - KULIM_ICE + 1
        else
          IS_ICE_PRESENT = .false.
          KULIM_ICE = 1
          KOLIM_ICE = 1
        end if
        
      case ('ANZAHL_PART')  ! (OUTPUT_FORMAT_DATA)
        KULIM = min(KULIM_ICE, KULIM_H2O)
        KOLIM = max(KOLIM_ICE, KOLIM_H2O)
        KULIM_H2O = 0
        KOLIM_H2O = KOLIM
        KULIM_ICE = 0
        KOLIM_ICE = KOLIM
        FOUND_POPULATED_BIN_H2O = .false.
        FOUND_POPULATED_BIN_ICE = .false.
        do K = KULIM, KOLIM
          if (Z_ICE(K) .gt. CUTOFF_NUMBER_CONC) then
            KOLIM_ICE = K
            ZERO_ICE(K) = .false.
            FOUND_POPULATED_BIN_ICE = .true.
          else
            Z_ICE(K) = 0.0_dp
            ZERO_ICE(K) = .true.
          end if
          
          if (Z_H2O(K) .gt. CUTOFF_NUMBER_CONC) then
            KOLIM_H2O = K
            ZERO_H2O(K) = .false.
            FOUND_POPULATED_BIN_H2O = .true.
          else
            Z_H2O(K) = 0.0_dp
            ZERO_H2O(K) = .true.
          end if
          if (FOUND_POPULATED_BIN_H2O) KULIM_H2O = KULIM_H2O + 1
          if (FOUND_POPULATED_BIN_ICE) KULIM_ICE = KULIM_ICE + 1
          if (K .eq. 28) then
            write(FILEUNIT_OUTPUT_DATA, '(1X, F9.4, 5(2X, 1PE11.4))')  &
              & ELAPSED_TIME_M, Z_H2O(K), RAD_H2O(K), Z_ICE(K), RAD_ICE(K)
          end if
        end do
        if (FOUND_POPULATED_BIN_H2O) then
          KULIM_H2O = KOLIM - KULIM_H2O + 1
          IS_H2O_PRESENT = .true.
        else
          KULIM_H2O = 1
          KOLIM_H2O = 1
        end if
        if (FOUND_POPULATED_BIN_ICE) then
          KULIM_ICE = KOLIM - KULIM_ICE + 1
        else
          IS_ICE_PRESENT = .false.
          KULIM_ICE = 1
          KOLIM_ICE = 1
        end if
      
      case ('KRAEMER')  ! (OUTPUT_FORMAT_DATA)
        
        ! Liquid particles
        RG_H2O = -0.99_dp
        P_P2 = -99.0_dp
        P_P3 = -99.0_dp
        if (IS_H2O_PRESENT) then
          TOTAL1 = 0.0_dp
          ALNRG = 0.0_dp
          Z_H2O_TOT = 0.0_dp
          P_P2 = 0.0_dp
          P_P3 = 0.0_dp
          do K = 1, N_SOLUTES
            WT_AVH2O(K) = 0.0_dp
          end do
          do K = KULIM_H2O, KOLIM_H2O
            TOTAL1 = TOTAL1 + PMASS_H2O(0, K) * Z_H2O(K)
            P_P2 = P_P2 + PMASS_H2O(2, K) * Z_H2O(K)
            P_P3 = P_P3 + PMASS_H2O(3, K) * Z_H2O(K)
            do KK = N_SOLUTES_LBOUND, N_SOLUTES
              WT_AVH2O(KK) = WT_AVH2O(KK) + PMASS_H2O(KK, K) * Z_H2O(K)
            end do
            ALNRG = ALNRG + log(RAD_H2O(K)) * Z_H2O(K)
            Z_H2O_TOT = Z_H2O_TOT + Z_H2O(K)
          end do
          do K = N_SOLUTES_LBOUND, N_SOLUTES
            WT_AVH2O(K) = WT_AVH2O(K) / TOTAL1
          end do
          ALNRG = ALNRG / Z_H2O_TOT
          RG_H2O = exp(ALNRG)
          P_P2 = P_P2 * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(2))
          P_P2 = 1e6_dp * P_P2 / P
          P_P3 = P_P3 * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(3))
          P_P3 = 1e9_dp * P_P3 / P
        end if
        
        ! Ice particles
        P_P2_ICE = 0.0_dp
        P_P3_ICE = 0.0_dp
        if (IS_ICE_PRESENT) then
          do K = KULIM_ICE, KOLIM_ICE
            P_P2_ICE = P_P2_ICE + PMASS_ICE(2, K) * Z_ICE(K)
            P_P3_ICE = P_P3_ICE + PMASS_ICE(3, K) * Z_ICE(K)
          end do
          P_P2_ICE = P_P2_ICE * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(2))
          P_P2_ICE = 1e6_dp * P_P2_ICE / P
          P_P3_ICE = P_P3_ICE * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(3))
          P_P3_ICE = 1e9_dp * P_P3_ICE / P
        end if
        
        write(FILEUNIT_OUTPUT_DATA, '(7(2X, 1PE12.5))')  &
          & ELAPSED_TIME_M, (1e9_dp * P_COND(3) / P), P_P3, P_P3_ICE,  &
          & (1e6_dp * P_COND(2) / P), P_P2, P_P2_ICE
        
      case ('MAJOR_OUTPUT')  ! (OUTPUT_FORMAT_DATA)
        P_P2 = -99.0_dp
        P_P3 = -99.0_dp
        if (IS_H2O_PRESENT) then
          TOTAL1 = 0.0_dp
          ALNRG = 0.0_dp
          Z_H2O_TOT = 0.0_dp
          P_P2 = 0.0_dp
          P_P3 = 0.0_dp
          do K = KULIM_H2O, KOLIM_H2O
            TOTAL1 = TOTAL1 + PMASS_H2O(0, K) * Z_H2O(K)
            P_P2 = P_P2 + PMASS_H2O(2, K) * Z_H2O(K)
            P_P3 = P_P3 + PMASS_H2O(3, K) * Z_H2O(K)
            ALNRG = ALNRG + log(RAD_H2O(K)) * Z_H2O(K)
            Z_H2O_TOT = Z_H2O_TOT + Z_H2O(K)
          end do
          ALNRG = ALNRG / Z_H2O_TOT
          RG_H2O = exp(ALNRG)
          P_P2 = P_P2 * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(2))
          P_P2 = 1e6_dp * P_P2 / P
          P_P3 = P_P3 * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(3))
          P_P3 = 1e9_dp * P_P3 / P
        else if (.not. IS_H2O_PRESENT) then
          RG_H2O = 0.0_dp
        end if
        
        P_P2_ICE  = 0.0_dp
        P_P3_ICE  = 0.0_dp
        RG_ICE    = 0.0_dp
        R_ice_eff = 0.0_dp
        R_ice_mm  = 0.0_dp
        SIGMA_ICE = 0.0_dp
        Z_ICE_TOT = 0.0_dp
        WC_ICE_ABS= 0.0_dp
        EXT_ICE   = 0.0_dp
        
        ! sums for effective r_ice_eff and mass mean radius r_ice_mm
        sum_N_R_2 = 0.0_dp
        sum_N_R_3 = 0.0_dp
        sum_N_R_4 = 0.0_dp
        if (IS_ICE_PRESENT) then
          ALNRG = 0.0_dp
          TOTAL1 = 0.0_dp
          do K = KULIM_ICE, KOLIM_ICE
            TOTAL1 = TOTAL1 + PMASS_ICE(0, K) * Z_ICE(K)
            ALNRG = ALNRG + log(RAD_ICE(K)) * Z_ICE(K)
            Z_ICE_TOT = Z_ICE_TOT + Z_ICE(K)
            sum_N_R_2 = sum_N_R_2 + Z_ICE(K) * RAD_ICE(K)**2
            sum_N_R_3 = sum_N_R_3 + Z_ICE(K) * RAD_ICE(K)**3
            sum_N_R_4 = sum_N_R_4 + Z_ICE(K) * RAD_ICE(K)**4
          end do
          ALNRG = ALNRG / Z_ICE_TOT
          SIGMA = 0.0_dp
          do K = KULIM_ICE, KOLIM_ICE
            SIGMA = SIGMA + Z_ICE(K) * (ALNRG - log(RAD_ICE(K)))**2
          end do
          SIGMA = sqrt(SIGMA / Z_ICE_TOT)
          SIGMA_ICE = exp(SIGMA)

          RG_ICE = exp(ALNRG)
          R_ice_eff = sum_N_R_3 / sum_N_R_2
          R_ice_mm = sum_N_R_4 / sum_N_R_3

          do K = KULIM_ICE, KOLIM_ICE
            P_P2_ICE = P_P2_ICE + PMASS_ICE(2, K) * Z_ICE(K)
            P_P3_ICE = P_P3_ICE + PMASS_ICE(3, K) * Z_ICE(K)
          end do
          
          P_P2_ICE = P_P2_ICE * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(2))
          P_P2_ICE = 1e6_dp * P_P2_ICE / P
          P_P3_ICE = P_P3_ICE * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(3))
          P_P3_ICE = 1e9_dp * P_P3_ICE / P

          ! cr -> calculate extinction after Gayet 2006
          WC_ICE_ABS = 1e7_dp * (P_P2_ICE * P / (1e6_dp + P_P2_ICE)) / (RGAS_CGS / MOLAR_MASS_SOLUTES(2) * TABS)
          EXT_ICE = 3.0_dp * WC_ICE_ABS / (R_ice_eff * 1e4_dp * 2.0_dp * RHO_ICE)
        else if (.not. IS_ICE_PRESENT) then
          RG_ICE = 0.0_dp
          EXT_ICE = 0.0_dp
        end if
        
        118 format(F10.4, 4(1X, F8.3), 1X, E11.4, 3(3X, F6.2), 1X, (1X, F8.3), 7(1X, E11.3), 1X, F2.0)
        write(FILEUNIT_OUTPUT_DATA, 118)  &
          & ELAPSED_TIME_M,  &
          & (T0 + TGAS),  &
          & (P * 0.01_dp),  &
          & Z_H2O_TOT,  &
          & (RG_H2O * 1e4_dp),  &
          & Z_ICE_TOT,  &
          & (RG_ICE * 1e4_dp),  &
          & (R_ice_eff * 1e4_dp),  &
          & (R_ice_mm * 1e4_dp),  &
          & SAT_ICE,  &
          & (1e9_dp * P_COND(3) / P),  &
          & P_P3,  &
          & P_P3_ICE,  &
          & (1e6_dp * P_COND(2) / P),  &
          & P_P2,  &
          & P_P2_ICE,  &
          & EXT_ICE,  &
          & real(N_NUCL)
          
      case ('MAJOR_OUTPUT_STEP')  ! (OUTPUT_FORMAT_DATA)
        RG_ICE = 0.0_dp
        P_P2_ICE = 0.0_dp
        if (IS_ICE_PRESENT) then
          Z_ICE_TOT = 0.0_dp
          ALNRG = 0.0_dp
          do K = KULIM_ICE, KOLIM_ICE
            ALNRG = ALNRG + log(RAD_ICE(K)) * Z_ICE(K)
            Z_ICE_TOT = Z_ICE_TOT + Z_ICE(K)
          end do
          ALNRG = ALNRG / Z_ICE_TOT
          RG_ICE = exp(ALNRG)
          
          do K = KULIM_ICE, KOLIM_ICE
            P_P2_ICE = P_P2_ICE + PMASS_ICE(2, K) * Z_ICE(K)
          end do
          P_P2_ICE = P_P2_ICE * RGAS_CGS * TABS / (10.0_dp * MOLAR_MASS_SOLUTES(2))
          P_P2_ICE = 1e6_dp * P_P2_ICE / P          
        end if
        
        250 format(1X, F10.3, 2(1X, F10.2), 1X, E11.4, 1X, F6.2, 1X, F10.3, 1X, E12.4, 1X, F2.0)
        write(FILEUNIT_OUTPUT_DATA, 250)  &
          & ELAPSED_TIME_M,  &
          & TABS,  &
          & (P * 0.01_dp),  &
          & Z_ICE_TOT,  &
          & (RG_ICE * 1e4_dp),  &
          & (SAT_ICE * 100.0_dp),  &
          & P_P2_ICE,  &
          & real(N_NUCL)
          
        ! debug output
        if (COMPILED_WITH_G) then
          if (Z_ICE_TOT .gt. 0.0_dp) print*, 'hom=1 het=2 ', N_NUCL
          
          117 format(A, F8.2, A, F7.2, A, F7.2, A, E12.3, A, F7.2)
          print 117, 'Zeit:', ELAPSED_TIME_M,  &
            &        ' T: ', T0 + TGAS,  &
            &        ' Sat_ice:', SAT_ICE,  &
            &        '  N_ice:', Z_ICE_TOT,  &
            &        '  R_ice:', RG_ICE * 1e4_dp
        end if
      
      case ('AWICIT')  ! (OUTPUT_FORMAT_DATA)
        RG_ICE = get_geometric_mean_radius(RAD_ICE, Z_ICE)
        GWC = 1e6_dp * P_COND(2) / P
        LWC = sum(PMASS_H2O(2, :) * Z_H2O(:)) * RGAS_CGS * TABS / MOLAR_MASS_SOLUTES(2) / 10.0_dp / P * 1e6_dp
        IWC = sum(PMASS_ICE(2, :) * Z_ICE(:)) * RGAS_CGS * TABS / MOLAR_MASS_SOLUTES(2) / 10.0_dp / P * 1e6_dp
        
        write(FILEUNIT_OUTPUT_DATA, "(f0.1, 2(1x, f0.3), 1x, f0.3, 7(1x, e0.4), 1(1x, a4))") &
          & ELAPSED_TIME_S, TABS, P, &
          & SAT_ICE, GWC, LWC, IWC, &
          & RG_ICE * 1e4_dp, sum(Z_ICE), &
          & Z_ICE_HET_SUM, Z_ICE_HOM_SUM, &
          & ICE_NUCLEATION_MODE
          
        if (COMPILED_WITH_G) then
          print "(f0.1, 2(1x, f0.3), 1x, f0.3, 7(1x, e0.4), 1(1x, a4))", &
          & ELAPSED_TIME_S, TABS, P, &
          & SAT_ICE, GWC, LWC, IWC, &
          & RG_ICE * 1e4_dp, sum(Z_ICE), &
          & Z_ICE_HET_SUM, Z_ICE_HOM_SUM, &
          & ICE_NUCLEATION_MODE
        end if
        
    end select  ! (OUTPUT_FORMAT_DATA)
    
    
    ! Write to size distribution output file(s)
    
    ! Write size distribution of liquid particles
    if (OUTPUT_FORMAT_SD_LIQ .eq. 'YES') then
      do K = 2, KMAX
        RAD_LEFT_LOG = log10(RAD_H2O(K - 1))
        RAD_RIGHT_LOG = log10(RAD_H2O(K))
        
        RAD_CENTER_LOG = (RAD_LEFT_LOG + RAD_RIGHT_LOG) / 2.0_dp ! in center of bin bin between K=1 amd K=2 in log space
        RAD_CENTER_LOG_TO_LINEAR = 10**(RAD_CENTER_LOG)  ! bin center radius in linear space
        
        Z_BIN_AVG = (Z_H2O(K - 1) + Z_H2O(K)) / 2.0_dp
        
        write(FILEUNIT_OUTPUT_SD_LIQ, '(F10.4, 1X, I4, 5(1X, E11.4))')  &
          & ELAPSED_TIME_S, K, (RAD_H2O(K) * 1e4_dp), Z_H2O(K),  &
          & (RAD_CENTER_LOG_TO_LINEAR * 1e4_dp), Z_BIN_AVG, (Z_BIN_AVG / (RAD_RIGHT_LOG - RAD_LEFT_LOG))
      end do
    end if
    
    ! Write size distribution of ice crystals
    if (OUTPUT_FORMAT_SD_ICE .eq. 'YES') then
      do K = 2, N_BINS
        RAD_LEFT_LOG = log10(RAD_ICE(K - 1))
        RAD_RIGHT_LOG = log10(RAD_ICE(K))
        
        RAD_CENTER_LOG = (RAD_LEFT_LOG + RAD_RIGHT_LOG) / 2.0_dp ! in center of bin bin between K=1 amd K=2 in log space
        RAD_CENTER_LOG_TO_LINEAR = 10**(RAD_CENTER_LOG)  ! bin center radius in linear space
        
        Z_BIN_AVG = (Z_ICE(K - 1) + Z_ICE(K)) / 2.0_dp
        
        write(FILEUNIT_OUTPUT_SD_ICE, '(F10.4, 1X, I4, 5(1X, E11.4))')  &
          & ELAPSED_TIME_S, K, (RAD_ICE(K) * 1e4_dp), Z_ICE(K),  &
          & (RAD_CENTER_LOG_TO_LINEAR * 1e4_dp), Z_BIN_AVG, (Z_BIN_AVG / abs(RAD_RIGHT_LOG - RAD_LEFT_LOG))
      end do
    end if
    
    ! Write size distribution of ice-nucleating particles
    if (OUTPUT_FORMAT_SD_IN .eq. 'YES') then
      do K = 2, KMAX
        RAD_LEFT_LOG = log10(RAD_IN(K - 1))
        RAD_RIGHT_LOG = log10(RAD_IN(K))
        
        RAD_CENTER_LOG = (RAD_LEFT_LOG + RAD_RIGHT_LOG) / 2.0_dp ! in center of bin bin between K=1 amd K=2 in log space
        RAD_CENTER_LOG_TO_LINEAR = 10**(RAD_CENTER_LOG)  ! bin center radius in linear space
        
        Z_BIN_AVG = (Z_IN(K - 1) + Z_IN(K)) / 2.0_dp
        
        write(FILEUNIT_OUTPUT_SD_IN, '(F10.4, 1X, I4, 5(1X, E11.4))')  &
          & ELAPSED_TIME_S, K, (RAD_IN(K) * 1e4_dp), Z_IN(K),  &
          & (RAD_CENTER_LOG_TO_LINEAR * 1e4_dp), Z_BIN_AVG, (Z_BIN_AVG / (RAD_RIGHT_LOG - RAD_LEFT_LOG))
      end do
    end if  ! (OUTPUT_FORMAT_SD_IN .eq. 'YES')
    
  end if  ! (mod(ELAPSED_TIME_S, DT_OUT) .gt. LINES_WRITTEN_OUT)
  
  
  ! Check if end criteria for TLOOP is reached
  select case (TRAJECTORY_END_CRITERIA)
    case ('DURATION')
      TLOOP = ELAPSED_TIME_S .le. DURATION
    case ('CONDITION')
      TLOOP = (P .ge. PRESS_FINAL) .and. (TABS .ge. TABS_FINAL)
    case ('FROMFILE')
      ! This case is covered in the block where .traj file values are read from file
  end select
  
  
  ! In case the model runtime is prolonged for RUNTIME_EXTENSION seconds.
  ! The model will continue with constant pressure, allowing variables to equilibrate
  if ((.not. TLOOP) .and. (TRAJECTORY_END_CRITERIA .ne. 'FROMFILE')) then
    IS_DURING_RUNTIME_EXTENSION = .true.
    TLOOP = .true.
  end if
  
  ! If updraft fluctuations are used: catch runs which escape with warming/downdraft at some point
  if ((USE_GRAVITY_WAVE_FORCING) .and. (TABS .gt. 250.0_dp)) then
    ! The threshold is kind of arbitrary, but should not be set too low to allow runs where temporary warming reverts to cooling again
    TLOOP = .false.
  end if
  
  if (IS_DURING_RUNTIME_EXTENSION) then
    if (RUNTIME_EXTENSION .gt. 0.0_dp) then
      TLOOP = .true.
    else
      TLOOP = .false.
    end if
    
    RUNTIME_EXTENSION = RUNTIME_EXTENSION - DT_STEP
  end if

  
end do  ! WHILE (TLOOP)

if (READ_PRES) close(FILEUNIT_TRAJ)


end program MAID
