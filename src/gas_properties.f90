module gas_properties
  !! Procedures calculating gas properties.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: specific_heat_dry_air_const_pres

  
contains


  function specific_heat_dry_air_const_pres(T) result (CP_DRY_AIR)
    !! Calculate specific heat capacity of dry air in kJ kg⁻¹ K⁻¹.
    !!
    !! TODO: Literature?
    !!
    use constants, only: T0
    real(dp), intent(in) :: T  !! in K, gas temperature
    real(dp) :: CP_DRY_AIR  !! in kJ kg⁻¹ K⁻¹, specific heat capacity of dry air at constant pressure

    CP_DRY_AIR = 1.00507_dp - 5.18275e-6_dp * (T - T0) + 8.90574e-07_dp * (T - T0)**2
  end function specific_heat_dry_air_const_pres


end module gas_properties
