module masstransport_gas_liquid
  !! Procedures describing the mass transport between liquid and gas phase (of water).
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: condensation_water_particle, condensation_solution_particle
  
  
contains

  
  subroutine condensation_water_particle(RAD, PMASS, ZAHL, ZERO, RHO_PART, &
      &                                  DIFF_COEF_H2O, TABS, AVSPEED, P_H2O_SAT_FLAT, &
      &                                  P_H2O, DC_OUT, LATENT_HEAT_PROCESS, DT_STEP, &
      &                                  KULIM, KOLIM, N_SOLUTES_LBOUND, N_SOLUTES, N_PCOND, PRES, DMASS_EVAP, EVAP)
    !! Calculate condensation/evaporation of water in aqueous solution particles.
    !!
    !! ### Literature:
    !! This approach uses the approach by Dahneke. For a comparison to other approaches see also Seinfeld & Pandis.
    !!
    !! * B. Dahneke, "Simple Kinetic Theory of Brownian Diffusion in Vapors and Aerosols",
    !!   Theory of Dispersed Multiphase Flow, Elsevier, 1983, 97-133,
    !!   doi:10.1016/b978-0-12-493120-6.50011-8
    !! * Seinfeld & Pandis, "Atmospheric Chemistry and Physics", edition 3, 2016, Chapter 12.1.3, p. 497-500
    !!
    use PARAMETERS, only: N_COMPONENTS, N_BINS
    use CONSTANTS, only: PI, RGAS_CGS, MOLAR_MASS_SOLUTES, SURFACE_TENSION_H2O_SC, ACCOMODATION_COEF_SOLUTES
    use water_properties, only: enthalpy_of_vaporization_water
    implicit none
    
    ! input variables
    real(dp), intent(inout) :: RAD(*)  !! in cm, particle radius per bin
    real(dp), intent(inout) :: PMASS(0:N_COMPONENTS, *)  !! in g, mass of compound KK in a single particle in size bin K
    real(dp), intent(inout) :: ZAHL(*)  !! in #/cm³, number concentration of particles per size bin
    real(dp), intent(inout) :: DC_OUT(N_COMPONENTS)  !! in g/cm³, total change of mass concentration of condensable components in the particles
    real(dp), intent(inout) :: DMASS_EVAP(N_BINS)  !! in ???, measure for the amount of evaporated water, used in subroutine HNO3_TRAP
    real(dp), intent(out) :: LATENT_HEAT_PROCESS  !! in J/cm³, latent heat due to condensation/evaporation
    real(dp), intent(in) :: RHO_PART  !! in g/cm³, density of the partilces
    real(dp), intent(in) :: DIFF_COEF_H2O  !! in cm²/s, diffusion coefficient of water in gas
    real(dp), intent(in) :: TABS  !! in K, temperature
    real(dp), intent(in) :: AVSPEED  !! in cm/s, mean thermal velocity of water molecules in gas phase, based on Maxwells theory
    real(dp), intent(in) :: P_H2O_SAT_FLAT  !! in Pa, water vapor saturation pressure over a flat water surface (without Kelvin effect)
    real(dp), intent(in) :: P_H2O  !! in Pa, ambient water vapor pressure at start of internal time loop
    real(dp), intent(in) :: DT_STEP  !! in sec, timestep of external time loop
    
    integer, intent(in) :: N_SOLUTES_LBOUND  !! first value to iterate to N_SOLUTES; default value is 1, only in case of pure water it is N_SOLUTES_LBOUND=N_SOLUTES=2
    integer, intent(in) :: N_SOLUTES  !! number of compounds in the solution particle (1=H2SO4, 2=+H2O, 3=+HNO3...)
    integer, intent(in) :: N_PCOND  !! number of condensable components
    integer, intent(in) :: KULIM, KOLIM  !! indeices of smallest/biggest populated size bins
    
    logical, intent(inout) :: PRES  !! .true. if any bin is filled, .false. if all bins are empty
    logical, intent(inout) :: ZERO(*)  !! .true. for each bin that is empty, .false. for each bin that has ZAHL(K)>0
    logical, intent(inout) :: EVAP(*)  !! .true. for each bin if evaporation happened (DMASS_EVAP(K)<0), else undefined.
    
    integer :: K, KK  !! Bin index, solute index
    integer :: NZERO  !! Counter of bins between KULIM and KOLIM that are zero
    logical:: IS_PURE  !! .true. if particle consists only of water
    
    real(dp), parameter :: EPS = 5e-3_dp  !! dimensionless, "epsilon" --> empirical factor ???
    real(dp) :: C_FAKT  !! Factor to transform Pa to mass concentration g/cm³
    
    real(dp) :: MEAN_FREE_PATH  !! in cm, mean free path, see Dahneke 1983, eq. 3.6
    real(dp) :: KN  !! dimensionless, Knudsen number, KN=MEAN_FREE_PATH/RAD=2*D/AVSPEED/RAD, see Dahneke 1983, p. 104)
    real(dp) :: LATENT_HEAT_WATER  !! in J/mol, specific latent heat of water
    
    real(dp) :: DT_COND  !! in sec, own timesteps in this function, smaller than the timestep DT_STEP from TLOOP
    real(dp) :: ZEIT_COND  !! in sec, sum of all `DT_COND` timesteps, must not grow bigger than DT_STEP
    
    real(dp) :: P_AKT  !! in Pa, water vapor pressure during internal time loop
    real(dp) :: PMASS_H2O_PRE_COND  !! in g, value of PMASS(2, K) at start of internal timestep (before condensation/evaporation)
    real(dp) :: VAP_PART  !! in Pa, effective water vapor saturation pressure which the particle exhibits
    real(dp) :: DPH2O  !! in Pa, difference of water vapor pressure between bulk and particle surface
    real(dp) :: DC  !! in g/cm³, difference of water vapor mass concentration between bulk and particle surface
    real(dp) :: RCOND  !! in #/sec, condensation rate
    real(dp) :: BETA  !! dimensionless, correction factor to compensate unperfect accomodation, see Dahnke 1983, eq. 5.5
    real(dp) :: DMASS(N_BINS)  !! in g, mass change of PMASS(2, K) during internal time loop
    real(dp) :: DC_KAN(N_BINS)  !! in g/cm³, aggregated change of H2O mass concentration over internal time loop
    
    real(dp) :: DC_DT  !! in g/cm³, change in H2O mass concentration during internal timestep, aggregated over all bins
    real(dp) :: DP_DT  !! in Pa, = DC_DT / C_FAKT
    real(dp) :: LATENT_HEAT_DT  !! in J/cm³, aggregated latent heat during internal timestep, due to condensation/evaporation
    
    
    C_FAKT = 10.0_dp * MOLAR_MASS_SOLUTES(2) / (RGAS_CGS * TABS)  ! Note: Factor 10 transforms pressure in Pa to CGS unit Ba (1Pa=10Ba)
    MEAN_FREE_PATH = 2.0_dp * DIFF_COEF_H2O / AVSPEED
    LATENT_HEAT_WATER = enthalpy_of_vaporization_water(TABS)
    P_AKT = P_H2O
    IS_PURE = N_SOLUTES_LBOUND .EQ. N_SOLUTES
    
    ! initialize aggreated variables
    ZEIT_COND = 0.0_dp
    LATENT_HEAT_PROCESS = 0.0_dp
    do K = KULIM, KOLIM
      DC_KAN(K) = 0.0_dp
    end do
    
    do while(ZEIT_COND .LT. DT_STEP)
      DT_COND = DT_STEP
      
      ! Determine internal condensation timestep and mass growth
      do K = KULIM, KOLIM
        if (.not. ZERO(K)) then
          ! Calc difference in water vapor mass concentration between particle surface and bulk with respect to Kelvin effect
          VAP_PART = P_H2O_SAT_FLAT * exp(2.0_dp * SURFACE_TENSION_H2O_SC * MOLAR_MASS_SOLUTES(2) & 
            &                             / (RHO_PART * RGAS_CGS * TABS) / RAD(K))  ! in Pa, water vapor saturation pressure at particle surface
          DPH2O = P_AKT - VAP_PART
          DC = DPH2O * C_FAKT
          
          ! Calculate mass growth rate
          KN = MEAN_FREE_PATH / RAD(K)
          BETA = (KN + 1.0_dp) / (2.0_dp * KN * (KN + 1.0_dp) / ACCOMODATION_COEF_SOLUTES(2) + 1.0_dp)
          RCOND = 4.0_dp * PI * DIFF_COEF_H2O * RAD(K) * BETA  ! in cm³/sec, yet to be multiplied by (n_inf - n_surf), see Dahneke 1983, eq. 5.4
          DMASS(K) = RCOND * DC  ! in g/sec, mass growth rate, yet to be multiplied with condensation timestep to determine absolute mass growth
          
          ! Remark: the loop ends when the condensed mass is low
          DT_COND = min(EPS * abs(PMASS(0, K) / DMASS(K)), &  ! smallest possible timestep
            &           DT_COND, &                            ! biggest possible timestep
            &           DT_STEP - ZEIT_COND)                  ! timestep<DT_COND if DT_COND would exceed boundary condition ZEIT_COND<=DT_STEP
        end if
      end do
      
      ZEIT_COND = ZEIT_COND + DT_COND
      DC_DT = 0.0_dp
      LATENT_HEAT_DT = 0.0_dp
      NZERO = count(ZERO(KULIM: KOLIM))
      
      do K = KULIM, KOLIM
        if(.not. ZERO(K)) then
          PMASS_H2O_PRE_COND  = PMASS(2, K)
          DMASS(K) = DMASS(K) * DT_COND  ! in g
          DC_KAN(K) = DC_KAN(K) + DMASS(K) * ZAHL(K)
          PMASS(2, K) = PMASS(2, K) + DMASS(K)
          PMASS(0, K) = PMASS(0, K) + DMASS(K)
          if (IS_PURE) PMASS(0, K) = PMASS(2, K)
          RAD(K) = (PMASS(0, K) / (RHO_PART * (4.0_dp * PI / 3.0_dp)))**(1.0_dp / 3.0_dp)

          if (PMASS(2, K) .LT. 1e-24_dp) then
            ! if water content evaporates completely
            DMASS(K) = -PMASS_H2O_PRE_COND
            PMASS(2, K) = 0.0_dp
            ZERO(K) = .TRUE.
            NZERO = NZERO + 1
            
            do KK = 3, N_PCOND
              ! Note: only H2SO4 remains, but all the other components (HNO3) are supposed to also evaporate completely
              DC_OUT(KK)   = DC_OUT(KK) - PMASS(KK,K) * ZAHL(K)
              PMASS(KK, K) = 0.0_dp
            end do
            ZAHL(K) = 0.0_dp
          end if
          
          DC_DT = DC_DT + DMASS(K) * ZAHL(K)
          LATENT_HEAT_DT = LATENT_HEAT_DT + DMASS(K) * ZAHL(K) * LATENT_HEAT_WATER / MOLAR_MASS_SOLUTES(2)
          
        else if (ZERO(K)) then
          RAD(K) = 1.0_dp  ! Prepares the calculation of ln(R)
        end if
        
      end do
      
      if (IS_PURE) NZERO = NZERO + 1
      
      ! Case: if all particles evaporated
      if(NZERO .gt. KOLIM-KULIM) then
        PRES      = .FALSE.
        ZEIT_COND = DT_STEP * 2.0_dp  ! ensures to exit the internal time loop
      end if
      
      DP_DT = DC_DT / C_FAKT  ! in Pa, change in gasphase water mass concentration during internal timestep
      P_AKT = P_AKT - DP_DT  ! in Pa, new ambient water vapor pressure during internal timestep
      DC_OUT(2) = DC_OUT(2) + DC_DT  ! in g/cm³, change of mass concentration of water in the particle
      LATENT_HEAT_PROCESS = LATENT_HEAT_PROCESS + LATENT_HEAT_DT  ! in J/cm³
    
    end do  ! END WHILE(ZEIT_COND .LT. DT_STEP)
    
    ! track if bins were affected by evaporation
    do K = KULIM, KOLIM
      if (.not. ZERO(K)) then
        if (DC_KAN(K) .LT. 0.0_dp) then
          DMASS_EVAP(K) = DC_KAN(K) / PMASS(2, K) * ZAHL(K)
          EVAP(K)       = .TRUE.
        end if
      end if
    end do
    
  end subroutine condensation_water_particle
  
  
  subroutine condensation_solution_particle(RAD, PMASS, ZAHL, ZERO, VAP_PH2O, RAD_EQ,  &
      &                                     PMASS_EQ, TABS, LATENT_HEAT_PROCESS, DT_STEP, KULIM, KOLIM,  &
      &                                     N_SOLUTES, N_PCOND, DO_NOT_CALC_EQUILIBRIUM, PRES, PCOND_CONST, &
      &                                     DIFF_COEFS, AV_VEL, DC_OUT, P_COND)
    !! Calculate condensation/evaporation of water between H2SO4/H2O solution particles and ambient air.
    !!
    !! ### Literature:
    !! This approach uses the approach by Dahneke. For a comparison to other approaches see also Seinfeld & Pandis.
    !!
    !! * B. Dahneke, "Simple Kinetic Theory of Brownian Diffusion in Vapors and Aerosols",
    !!   Theory of Dispersed Multiphase Flow, Elsevier, 1983, 97-133,
    !!   doi:10.1016/b978-0-12-493120-6.50011-8
    !! * Seinfeld & Pandis, "Atmospheric Chemistry and Physics", edition 3, 2016, Chapter 12.1.3, p. 497-500
    !!
    use PARAMETERS, only: N_COMPONENTS, N_BINS, KPAR_AW_P
    use CONSTANTS, only: RGAS_CGS, PI, T0, MOLAR_MASS_SOLUTES, ACCOMODATION_COEF_SOLUTES
    use solution_particle, only: calc_H2SO4solution_surface_tension_with_derivatives,  &
      &                          calc_H2SO4solution_density_with_derivatives,  &
      &                          get_solution_density, get_solution_surface_tension,  &
      &                          get_p_h2o_sat_over_H2SO4_solution, get_p_sat_solutes
    implicit none
    
    real(dp), intent(inout) :: RAD(*)  !! in cm, particle radius
    real(dp), intent(inout) :: PMASS(0:N_COMPONENTS, *)  !! in g, mass of compound KK in a particle in size bin K
    real(dp), intent(inout) :: ZAHL(*)  !! in #/cm³, number concentration of particles per size bin
    logical, intent(inout) :: ZERO(*)  !! .true. for each bin that is empty, .false. for each bin that has ZAHL(K)>0
    real(dp), intent(out) :: VAP_PH2O(*)  !! in Pa, water vapor saturation pressure over the curved particle surface
    real(dp), intent(out) :: RAD_EQ(*)  !! in cm, particle radius in equilibrium
    real(dp), intent(out) :: PMASS_EQ(0:N_COMPONENTS, *)  !! in g, mass of particle components in equilibrium
    real(dp), intent(in) :: TABS  !! in K, ambient temperature
    real(dp), intent(out) :: LATENT_HEAT_PROCESS  !! in J/cm³, latent heat released to environment
    real(dp), intent(in) :: DT_STEP  !! in sec, timestep of main program time loop
    integer, intent(in) :: KULIM, KOLIM  !! dimensionless, lower and upper bin boundary of initially populated bins
    integer, intent(in) :: N_SOLUTES, N_PCOND  !! dimensionless
    logical, intent(in) :: DO_NOT_CALC_EQUILIBRIUM  !! .true. if EQUILIBRIUM option is set to 'false'
    logical, intent(inout) :: PRES  !! .false. if all particles evaporated, then no particles are PRESent anymore
    logical, intent(in) :: PCOND_CONST(*)  !! if .true. then P_AKT(KK) is prevented to change due to condensation/evaporation
    real(dp), intent(in) :: DIFF_COEFS(N_COMPONENTS)  !! in cm²/s, diffusion coefficients of condensable components in gas
    real(dp), intent(in) :: AV_VEL(N_COMPONENTS)  !! in cm/s, mean thermal velocity of a gas phase molecule, acc. to Maxwells theory
    real(dp), intent(inout) :: DC_OUT(N_COMPONENTS)  !! in g/cm³, total change of mass concentration of condensable components in the gas phase
    real(dp), intent(in) :: P_COND(N_COMPONENTS)  !! in Pa, vapor pressures of condensable components
    
    logical :: IS_DEFAULT  !! is evaluated to .true. for invalid XSS_EQ values, then XSS_EQ=X_DEFAULT
    logical :: IS_INCONSISTENT  !! controls loop until condensation/evaporation doesn't leads to inconsistency
    
    integer :: K, KK  !! index for bin, condensable component
    integer :: ICOUNT  !! count number of iterations in a WHILE loop as an exit criterium
    integer :: NZERO  !! Counter of bins between KULIM and KOLIM that are zero
    
    real(dp), parameter :: EPS = 5.0e-3_dp  !! ??? dimensionless? abbrev. for "epsilon"? an empirical factor? maybe E... Per Second in s⁻¹?
    real(dp) :: DUMMY1, DUMMY2  !! placeholder for unused subroutine argument
    real(dp) :: C_FAKT(N_COMPONENTS)  !! Factor to transform pressure in Pa to mass concentration in g/cm³
    
    real(dp) :: DT_COND  !! in seconds, internal timestep
    real(dp) :: ZEIT_COND  !! in seconds, internal elapsed time during external timestep DT_STEP
    
    real(dp) :: PMASS_INI(0:N_COMPONENTS, N_BINS)  !! in g, initial values of PMASS
    real(dp) :: PM_STORE(0:N_COMPONENTS)  !! in g, stored value of PMASS(KK, K) for a specific bin K
    
    real(dp) :: P_AKT(N_COMPONENTS)  !! in Pa, vapor pressure of the condensable components at start of subroutine
    real(dp) :: P_SAT_SOLUTES(N_COMPONENTS)  !! in Pa, saturation vapor pressure of all condensable components in the particle, calculated in subroutine get_p_sat_solutes
    real(dp) :: VAP_PART(N_COMPONENTS)  !! in Pa, water vapor saturation pressure over the solution particle, derived from P_SAT_SOLUTES with respect to Kelvin effect
    
    real(dp) :: RHO_PART  !! in g/cm³, particle density
    real(dp) :: SIG_PART  !! in mN/m, particles surface tension
    
    real(dp) :: MEAN_FREE_PATH(N_COMPONENTS)  !! in cm, mean free path
    real(dp) :: BETA  !! dimensionless, correction factor to compensate unperfect accomodation, see Dahnke 1983, eq. 5.5
    real(dp) :: KN  !! dimensionless, Knudsen number, KN=MEAN_FREE_PATH/RAD=2*DIFF_COEFS/AV_VEL/RAD, see Dahneke 1983, p. 104)
    
    real(dp) :: RCOND  !! in #/sec, condensation rate
    real(dp) :: DP_COND  !! in Pa, difference between vapor pressure of condensable component in bulk and over particle surface
    real(dp) :: DC_COND  !! in g/cm³, difference between gas phase mass concentration of condensable component in bulk and over particle surface
    real(dp) :: DC_DT(N_COMPONENTS)  !! in g/cm³, change of condensable components mass concentration during internal timestep, aggregated over all bins
    real(dp) :: DC_STORE(N_COMPONENTS)  !! in in g/cm³, stored value of DC_DT
    
    real(dp) :: DMASS(N_COMPONENTS, N_BINS)  !! in g, the mass change of condensable component KK for a particle in bin K due to condensation/evaporation during the internal timestep
    real(dp) :: DM_DT  !! in g/sec, interim value of DMASS(KK, K) to determine DT_COND, before being multiplied with internal timestep
    real(dp) :: DM_TOT  !! in g, sum of DMASS over all condensable components for a specific bin K during internal timestep
    real(dp) :: SUM_KOND(N_COMPONENTS, N_BINS)  !! in g, sum of DMASS over internal time loop, aggregated mass growth over all condensable components
    real(dp) :: SUM_STORE(N_COMPONENTS)  !! in g, stored value of SUM_KOND for a specific bin K
    
    real(dp) :: SPECIFIC_LATENT_HEAT_SOLUTES(N_COMPONENTS)  !! in J/g, specific latent heat of vaporization of the solutes
    real(dp) :: LATENT_HEAT_DUMMY(N_COMPONENTS)  !! placeholder for unused subroutine argument
    real(dp) :: SPECIFIC_LATENT_HEAT_SOLUTES_BINNED(N_COMPONENTS, N_BINS)  !! in J/g, latent heat for condensable components for all bins (aggregated SPECIFIC_LATENT_HEAT_SOLUTES(KK))
    real(dp) :: LATENT_HEAT_DT  !! in J/cm³, latent heat per internal timestep
    real(dp) :: LATENT_HEAT_DT_STORE  !! in J/cm³, stored value of LATENT_HEAT_DT
    
    real(dp) :: DX  !! dimensionless, distance on XSS axis from previous value to new value during iterative search for XSS_EQ
    real(dp) :: F1  !! dimensionless, expression which must evaluate to zero at equilibrium (XSS=XSS_EQ)
    real(dp) :: DF1X  !! dimensionless, slope dF1/dXSS, used to determine DX through Newtons method for finding roots
    real(dp) :: dlnPH2OSOLSAT_dX_WT_H2SO4  !! dimensionless, same as DF1X, but derived through parameterization of water vapor saturation pressure over H2SO4 solutions
    
    real(dp) :: PSATXX  !! in Pa, interim value of water vapor saturation pressure over particle surface during iteration of finding XSS_EQ
    real(dp) :: RXX  !! in cm, interim value of particles radius during iteration of finding XSS_EQ
    real(dp) :: DRXX  !! in cm, first derivative of RXX
    real(dp) :: RHOXX  !! in g/cm³, interim value of particle density during iteration of finding XSS_EQ
    real(dp) :: DRHO  !! in g/cm³/sec, first derivative of RHOXX
    real(dp) :: SIGXX  !! in mN/m, interim value of particles surface tension during iteration of finding XSS_EQ
    real(dp) :: DSIG  !! in mN/m/sec, first derivative of SIGXX
    
    real(dp) :: X_WT(N_COMPONENTS)  !! dimensionless, mass fractions of condensable components in a particle of a specific size bin K
    real(dp) :: XSS_EQ  !! dimensionless, mass fraction of H2SO4 in equilibrium with ambient water vapor pressure
    real(dp) :: X_DEFAULT  !! dimensionless, fallback value for XSS_EQ without consideration of Kelvin effect if no new valid XSS_EQ value can be found by taking Kelvin effect into account
    
    ! For each component some factors, in analogy to subroutine condensation_water_particle
    do KK = 2, N_PCOND
      C_FAKT(KK) = 10.0_dp * MOLAR_MASS_SOLUTES(KK) / (RGAS_CGS * TABS)
      P_AKT(KK) = P_COND(KK)
      MEAN_FREE_PATH(KK) = 2.0_dp * DIFF_COEFS(KK) / AV_VEL(KK)   ! in cm, see Dahneke 1983, eq. 3.6
    end do
    
    ! initialization of some variables
    LATENT_HEAT_PROCESS = 0.0_dp
    ZEIT_COND = 0.0_dp
    
    do K = KULIM, KOLIM
      PMASS_INI(0, K) = PMASS(0, K)
      do KK = 1, N_SOLUTES
        SUM_KOND(KK, K) = 0.0_dp
        PMASS_INI(KK, K) = PMASS(KK, K)
      end do
    end do
    
    ! Start internal time loop
    do while(ZEIT_COND .LT. DT_STEP)
      
      ! Determine internal timestep DT_COND and mass flux rate DMASS
      DT_COND = DT_STEP  ! initialization with biggest possible value
      binloop1: do K = KULIM, KOLIM
        if(ZERO(K)) cycle binloop1
          
        do KK = 1, N_SOLUTES
          X_WT(KK) = PMASS(KK, K) / PMASS(0, K)
        end do
        
        RHO_PART = get_solution_density(TABS, X_WT)
        SIG_PART = get_solution_surface_tension(TABS, X_WT)
        call get_p_sat_solutes(TABS, X_WT, P_SAT_SOLUTES, SPECIFIC_LATENT_HEAT_SOLUTES)  ! calculates P_SAT_SOLUTES(N_COMPONENTS) and SPECIFIC_LATENT_HEAT_SOLUTES(N_COMPONENTS)
        RAD(K) = (PMASS(0, K) / (RHO_PART * (4.D0 * PI / 3.D0)))**(1.D0 / 3.D0)
        
        do KK = 2, N_PCOND
          KN             = MEAN_FREE_PATH(KK) / RAD(K)
          BETA           = (KN + 1.0_dp) / (2.0_dp * KN * (KN + 1.0_dp) / ACCOMODATION_COEF_SOLUTES(KK) + 1.0_dp)
          RCOND          = 4.D0 * PI * DIFF_COEFS(KK) * RAD(K) * BETA  ! in cm³/sec, yet to be multiplied by (n_inf - n_surf), see Dahneke 1983, eq. 5.4
          SPECIFIC_LATENT_HEAT_SOLUTES_BINNED(KK, K) = SPECIFIC_LATENT_HEAT_SOLUTES(KK)
          VAP_PART(KK)   = P_SAT_SOLUTES(KK) &
            &              * exp(2.0_dp * MOLAR_MASS_SOLUTES(KK) / (RGAS_CGS * TABS) * SIG_PART / (RHO_PART * RAD(K)))
          DP_COND        = P_AKT(KK) - VAP_PART(KK)
          DC_COND        = DP_COND * C_FAKT(KK)
          DMASS(KK, K)   = RCOND * DC_COND  ! in g/second, to be multiplied with DT_COND in a later step
          DM_DT = max(abs(DMASS(KK, K)), &
            &         1.D-3 * EPS * PMASS(0, K))
          DT_COND = min(DT_COND, &
            &           EPS * abs(PMASS(0, K) / DM_DT), &
            &           DT_STEP - ZEIT_COND)
        end do  ! KK = 2, N_PCOND
        
        VAP_PH2O(K) = VAP_PART(2)
      end do binloop1 ! K=KULIM,KOLIM
          
      NZERO = count(ZERO(KULIM:KOLIM))
      ZEIT_COND = ZEIT_COND + DT_COND
      
      do KK = 2, N_PCOND
        DC_DT(KK) = 0.0_dp
      end do
      
      LATENT_HEAT_DT = 0.0_dp
      binloop2: do K = KULIM, KOLIM
        if(ZERO(K)) cycle binloop2
        
        ! store variables in case the value of DMASS leads to inconsistency
        PM_STORE(0) = PMASS(0, K)
        PM_STORE(1) = PMASS(1, K)
        LATENT_HEAT_DT_STORE = LATENT_HEAT_DT
        do KK = 2, N_PCOND
          PM_STORE(KK) = PMASS(KK, K)
          SUM_STORE(KK) = SUM_KOND(KK, K)
          DC_STORE(KK) = DC_DT(KK)
        end do
        
        IS_INCONSISTENT = .true.  ! initialization of variable to start while loop
        do while (IS_INCONSISTENT)  ! loop if condensation/evaporation leads to inconsistency
          DM_TOT = 0.0_dp
          do KK = 2, N_PCOND
            DMASS(KK, K) = DMASS(KK, K) * DT_COND  ! in g
            if(DMASS(KK, K) + PMASS(KK, K) .LT. 0.0_dp) DMASS(KK, K) = -PMASS(KK, K)  ! ensure mass won't get negative
            PMASS(KK, K) = PMASS(KK, K) + DMASS(KK, K)
            SUM_KOND(KK, K) = SUM_KOND(KK, K) + DMASS(KK, K)
            DM_TOT = DM_TOT + DMASS(KK, K)
            DC_DT(KK) = DC_DT(KK) + DMASS(KK,K) * ZAHL(K)
            LATENT_HEAT_DT = LATENT_HEAT_DT + DMASS(KK, K) * ZAHL(K) * SPECIFIC_LATENT_HEAT_SOLUTES_BINNED(KK, K)
          end do
          
          PMASS(0, K) = PMASS(0, K) + DM_TOT
          
          if(PMASS(0, K) .LT. 4.2e-24_dp .AND. DM_TOT .LT. 0.0_dp) then
            ! Particles smaller than 0.1 nm and evaporation (DM_TOT < 0) are said to evaporate completely
            do KK = 2, N_PCOND
              DC_DT(KK) = DC_DT(KK) - PMASS(KK, K) * ZAHL(K)
              LATENT_HEAT_DT = LATENT_HEAT_DT - PMASS(KK, K) * ZAHL(K) * SPECIFIC_LATENT_HEAT_SOLUTES_BINNED(KK, K)
              PMASS(KK, K) = 0.0_dp
            end do
            ZAHL(K) = 0.0_dp
            ZERO(K) = .TRUE.
            NZERO = NZERO + 1
            cycle binloop2
          end if
          
          do KK = 1, N_SOLUTES
            X_WT(KK) = PMASS(KK, K) / PMASS(0, K)
          end do
          
          RHO_PART = get_solution_density(TABS, X_WT)
          RAD(K) = (PMASS(0, K) / (RHO_PART * (4.0_dp * PI / 3.0_dp)))**(1.0_dp / 3.0_dp)
          SIG_PART = get_solution_surface_tension(TABS, X_WT)
          call get_p_sat_solutes(TABS, X_WT, P_SAT_SOLUTES, LATENT_HEAT_DUMMY)
          
          ! check for inconsistency: DMASS(KK, K) and DP_COND have to have the same sign
          IS_INCONSISTENT = .FALSE.
          do KK = 2, N_PCOND
            VAP_PART(KK) = P_SAT_SOLUTES(KK) &
              &            * exp(2.0_dp * MOLAR_MASS_SOLUTES(KK) / (RGAS_CGS * TABS) * SIG_PART / (RHO_PART * RAD(K)))
            DP_COND = P_AKT(KK) - VAP_PART(KK)
            
            if (int(sign(1.0_dp, DMASS(KK, K))) .ne. int(sign(1.0_dp, DP_COND))) then  ! clumsy expression because comparison of reals is not recommended
              ! Consistency check:
              !   DP_COND > 0 leads to condensation with DMASS > 0
              !   DP_COND < 0 leads to evaporation with DMASS < 0
              IS_INCONSISTENT = .TRUE.
            end if
          end do
          
          if (IS_INCONSISTENT) then
            ! if IS_INCONSISTENT: restore previous values and try again with reduced DMASS
            PMASS(0, K) = PM_STORE(0)
            PMASS(1, K) = PM_STORE(1)
            LATENT_HEAT_DT = LATENT_HEAT_DT_STORE
            
            do KK = 2, N_PCOND
              DMASS(KK, K) = DMASS(KK, K) * 0.9_dp
              PMASS(KK, K) = PM_STORE(KK)
              SUM_KOND(KK, K) = SUM_STORE(KK)
              DC_DT(KK) = DC_STORE(KK)
            end do
          end if
        end do  ! WHILE (IS_INCONSISTENT)
      end do binloop2  ! K = KULIM, KOLIM
      
      if(NZERO .GE. (KOLIM - KULIM)) then
        ! Case: if whole particle evaporated
        PRES = .FALSE.
        ZEIT_COND = DT_STEP * 2.0_dp  ! criteria to exit internal time loop
      end if
      
      do KK = 2, N_PCOND
        if(.not. PCOND_CONST(KK)) P_AKT(KK) = P_AKT(KK) - (DC_DT(KK) / C_FAKT(KK))
        DC_OUT(KK) = DC_OUT(KK) + DC_DT(KK)
      end do
      LATENT_HEAT_PROCESS = LATENT_HEAT_PROCESS + LATENT_HEAT_DT
    end do  ! WHILE(ZEIT_COND .LT. DT_STEP)
      
    do K = KULIM, KOLIM
      DM_TOT = 0.0_dp
      do KK = 2, N_PCOND
        PMASS(KK, K) = PMASS_INI(KK, K) + SUM_KOND(KK, K)
        DM_TOT = DM_TOT + SUM_KOND(KK, K)
      end do
      
      PMASS(0, K) = PMASS_INI(0, K) + DM_TOT
      
      do KK = 1, N_SOLUTES
        X_WT(KK) = PMASS(KK, K) / PMASS(0, K)
      end do
      
      RHO_PART = get_solution_density(TABS, X_WT)
      RAD(K) = (PMASS(0, K) / (RHO_PART * (4.0_dp * PI / 3.0_dp)))**(1.0_dp / 3.0_dp)
    end do

    ! select equilibrium- or non-equilibrium calculation
    select case (DO_NOT_CALC_EQUILIBRIUM)
    
      case (.true.)  ! non-equilibrium calculation
        do K = KULIM, KOLIM
          do KK = 1, N_COMPONENTS
            PMASS_EQ(KK, K) = PMASS(KK, K)
          end do
          
          PMASS_EQ(0, K) = PMASS(0, K)
          RAD_EQ(K) = 1.0_dp
        end do
      
      case (.false.)  ! equilibrium calculation
        if (KPAR_AW_P .ne. 1) then
          ! Raise warning, because calculation of dlnPH2OSOLSAT_dX_WT_H2SO4 is only implemented with Tabazadeh/Carslaw approach
          stop "ERROR: Equilibrium calculation works only with `KPAR_AW_P=1` (Tabazadeh/Carslaw). " //  &
            & "For other KPAR_AW_P, please implement the calculation of `dlnPH2OSOLSAT_dX_WT_H2SO4`."
        end if
        
        ! Use Newtons method for finding roots to determine the equilibrium mixing ratio `XSS_EQ` of the H2SO4/H2O solution.
        ! The control function `F1 = ln(P_COND(2) / PSATXX)` hast to be zero in equilibrium.
        
        ! Determine a default value for XSS_EQ without considering the Kelvin effect
        XSS_EQ = PMASS(1, KULIM) / PMASS(0, KULIM)  ! mass fraction of H2SO4 in smallest bin
        DX     = XSS_EQ  ! only initialized to start following loop, will be overwritten
        F1     = 1.D0    ! only initialized to start following loop, will be overwritten
        ICOUNT = 0
        do while((abs(DX) .GT. 1.D-9 * XSS_EQ) .OR. (F1 .GT. 1.D-9))
          call get_p_h2o_sat_over_H2SO4_solution(TABS, XSS_EQ, PSATXX, dlnPH2OSOLSAT_dX_WT_H2SO4, LATENT_HEAT_DUMMY)  ! in Pa, saturation water vapor pressure over solution
          
          F1 = log(P_COND(2) / PSATXX)  ! control function F1 must evaluate to zero in equilibrium
          ! Note: d/dXSS F1 = d/dXSS ln(P_COND(2) / PSATXX) 
          !                 = d/dXSS (ln(P_COND(2)) - ln(PSATXX))
          !                 = d/dXSS ln(P_COND(2)) - d/dXSS ln(PSATXX)
          !                 = 0 - d/dXSS ln(PSATXX)
          !                 = - dlnPH2OSOLSAT_dX_WT_H2SO4
          DF1X   = -dlnPH2OSOLSAT_dX_WT_H2SO4  ! d/dXSS F1
          DX     = F1 / DF1X  ! derive dX interval from linear approximation (differential coefficient)
          XSS_EQ = XSS_EQ - DX
          ICOUNT = ICOUNT + 1
          if((ICOUNT .GT. 1000) .OR. (XSS_EQ .LT. 0.D0) .OR. (XSS_EQ .GT. 1.D0)) then
            print*, "Could not calculate equilibrium radius (XSS_EQ=,", XSS_EQ, &
              &     " after ", ICOUNT, " iterations for bin ", K
            return
          end if
        end do
        X_DEFAULT = XSS_EQ
        
        ! Determine XSS_EQ with taking the Kelvin effect into account and derive PMASS_EQ(KK, K) and RAD_EQ(K)
        binloop3: do K = KOLIM, KULIM, -1
          DX         = XSS_EQ  ! initialization with biggest possible value of DX, DX<=XSS_EQ
          IS_DEFAULT = .FALSE.
          F1         = 1.D0
          
          ICOUNT = 0
          whileloop: do while((abs(DX) .GT. (1.D-9 * XSS_EQ)) .OR. (F1 .GT. 1.D-9))
            call calc_H2SO4solution_surface_tension_with_derivatives(SIGXX, 'mass', XSS_EQ, TABS-T0, DSIG, DUMMY1)
            call calc_H2SO4solution_density_with_derivatives(RHOXX, 'mass', XSS_EQ, DRHO, DUMMY1)

            call get_p_h2o_sat_over_H2SO4_solution(TABS, XSS_EQ, PSATXX, dlnPH2OSOLSAT_dX_WT_H2SO4, LATENT_HEAT_DUMMY)  ! in Pa
            RXX    = (PMASS(1, K) / (XSS_EQ * RHOXX))**(1.D0 / 3.D0)  ! in cm
            DRXX   = -(3.D0 / (4.D0 * PI))**(1.D0 / 3.D0) / 3.D0 * PMASS(2, K) * (RHOXX + XSS_EQ * DRHO) / (RXX * XSS_EQ * RHOXX)**2  ! in cm
            RXX    = (3.D0 / (4.D0 * PI))**(1.D0 / 3.D0) * RXX  ! in cm
            
            ! Note: derive the function F1 to be zero in equilibrium:
            ! F1 = ln(P_COND(2) / PSATXX_SURF)
            !    = ln(P_COND(2) / (PSATXX_INF * KELVIN_CORR))
            !    = ln(P_COND(2)) - ln(PSATXX_INF * KELVIN_CORR)
            !    = ln(P_COND(2)) - (ln(PSATXX_INF) + ln(KELVIN_CORR))
            !    = ln(P_COND(2)) - ln(PSATXX_INF) - ln(KELVIN_CORR)
            !    = ln(P_COND(2) / PSATXX_INF) - (2 * MOLAR_MASS_SOLUTES(2) / RGAS_CGS / TABS * SIG / RHO / RAD)
            F1 = log(P_COND(2) / PSATXX) - 2 * MOLAR_MASS_SOLUTES(2) / RGAS_CGS / TABS * SIGXX / (RXX * RHOXX)
            ! functions of XSS:     ^                                             ^       ^      ^
            
            DF1X = -dlnPH2OSOLSAT_dX_WT_H2SO4 &
              &    - 2 * MOLAR_MASS_SOLUTES(2) / RGAS_CGS / TABS * (DSIG / (RXX * RHOXX) &
              &                                                     - SIGXX * DRXX / (RXX**2 * RHOXX) &
              &                                                     - SIGXX * DRHO / (RXX * RHOXX**2))
            DX = F1 / DF1X
            XSS_EQ = XSS_EQ - DX
            ICOUNT = ICOUNT + 1
            
            if((ICOUNT .GT. 1000) .OR. (XSS_EQ .LT. 0.D0) .OR. (XSS_EQ .GT. 1.D0)) then
              print*, "Could not further calculate equilibrium radius (XSS_EQ=,", XSS_EQ, &
              &     " after ", ICOUNT, " iterations for bin ", K
              print*, "Using default value without considering Kelvin effect."
              IS_DEFAULT = .TRUE.
              exit whileloop
            end if
          end do whileloop
          
          if (IS_DEFAULT) XSS_EQ = X_DEFAULT
          
          call calc_H2SO4solution_density_with_derivatives(RHO_PART, 'mass', XSS_EQ, DUMMY1, DUMMY2)
          PMASS_EQ(1, K) = PMASS(1, K)
          PMASS_EQ(0, K) = PMASS(1, K) / XSS_EQ
          PMASS_EQ(2, K) = PMASS_EQ(0, K) * (1.D0 - XSS_EQ)
          RAD_EQ(K)      = (PMASS_EQ(0, K) / (RHO_PART * (4.D0 * PI / 3.D0)))**(1.D0 / 3.D0)
          
          if (IS_DEFAULT) print*, K, XSS_EQ, RAD_EQ(K)
          
        end do binloop3
    end select
  end subroutine condensation_solution_particle

end module masstransport_gas_liquid
