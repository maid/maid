module water_properties
  !! Procedures about water properties.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: get_p_h2o_sat_liq, enthalpy_of_vaporization_water
  

contains


  function get_p_h2o_sat_liq(T) result (P_H2O_SAT_LIQ)
    !! Water vapor saturation pressure over supercooled water in Pa.
    !!
    !! Takes variable `KPAR_H2OS` from module `parameters` to determine parameterization.
    !!
    use PARAMETERS, only: KPAR_H2OS
    use ice_properties, only: get_p_h2o_sat_ice
    implicit none
    real(dp), intent(in) :: T  !! in K, temperature
    real(dp) :: P_H2O_SAT_LIQ  !! in Pa, water vapor saturation pressure over a (supercooled) aqueous liquid.
    
    real(dp) :: P1(4) = [18.452406985, 3505.1578807, &              ! See Tabazadeh et al. (1997), eq. 1
      &                  330918.55082, 12725068.262], &
      &         P2(10)= [54.842763, 6763.22, 4.21, &                ! See Murphy & Koop (2005), eq. 10
      &                  0.000367, 0.0415, 218.8, &
      &                  53.878, 1331.22, 9.44523, 0.014025], &
      &         P3(4) = [74.8727, 7167.40548, 7.77107, 0.00505], &  ! See Nachbar et al. (2019), eq. 7
      &         P4(5) = [6096.9385, 16.635794, 0.02711193, &        ! See Sonntag et al. (1994)
      &                  0.00001673952, 2.433502], &
      &         P5(2) = [-1.49274, 437.73026], &                    ! Fit to yet unpublished experiments by Julia Schneider
      &         P6(6) = [5800.2206, 1.3914993, 0.048640239, &       !  See Hyland & Wexler (1983)
      &                  0.000041764768, 0.000000014452093, 6.5459673]
      !! Parameters to calculate water vapor saturation pressure
    
    select case (KPAR_H2OS)
        case (1)  ! (KPAR_H2OS)
        ! ---  TABAZADEH ET AL. (1997):
        P_H2O_SAT_LIQ = 100.0_dp * exp(P1(1) - P1(2) / T - P1(3) / T**2 + P1(4) / T**3)
        
        case (2)  ! (KPAR_H2OS)
        ! --- MURPHY AND KOOP (2005):
        P_H2O_SAT_LIQ = exp(P2(1) - P2(2) / T - P2(3) * log(T) + P2(4) * T &
            &         + tanh(P2(5) * (T - P2(6))) &
            &           * (P2(7) - P2(8) / T - P2(9) * log(T) + P2(10) * T))
            
        case (3)  ! (KPAR_H2OS)
        ! --- NACHBAR ET AL. (2019):
        P_H2O_SAT_LIQ = exp(P3(1) - (P3(2) / T) - P3(3) * log(T) + P3(4) * T)
        
        case (4)  ! (KPAR_H2OS)
        ! --- SONNTAG (1994):
        P_H2O_SAT_LIQ = 100.0_dp * exp(-P4(1) / T + P4(2) + T * (-P4(3) + P4(4) * T) + P4(5) * log(T))
        
        case (5)  ! (KPAR_H2OS)
        ! --- SCHNEIDER (unpublished), based on DEHS aerosol experiments: 
        P_H2O_SAT_LIQ= exp(P5(1) + P5(2) / T) * get_p_h2o_sat_ice(T)
        
        case (6)  ! (KPAR_H2OS)
        ! --- HYLAND AND WEXLER (1983):
        P_H2O_SAT_LIQ = exp(-P6(1) / T + P6(2) - P6(3) * T + P6(4) * T**2 - P6(5) * T**3 + P6(6) * log(T))
        
        case default
        stop "ERROR: invalid value for variable KPAR_H2OS in function `get_p_h2o_sat_liq()`."
        
    end select

  end function get_p_h2o_sat_liq
  
  
  function enthalpy_of_vaporization_water(T) result(DELTA_H_VAP)
    !! Enthalpy of vaporization (specific latent heat) of water in in J/mol.
    !! Derived from Clausius-Clapeyron equation.
    !!
    !! Using the relation d(ln(pw))/dT = WL / (RGAS * T**2)
    !! with pw as water vapor saturation pressure over supercooled liquid water
    !!
    use CONSTANTS, only: RGAS_SI
    use PARAMETERS, only: KPAR_H2OS, KPAR_ICE
    implicit none
    real(dp), intent(in) :: T  !! in K, temperature
    real(dp) :: DELTA_H_VAP  !! in J/mol, 
    real(dp) :: dlnPW_dT  !! derivative of the ln of water vapor saturation pressure with respect to water
    real(dp) :: PWi  !! water vapor saturation pressure with respect to ice
    real(dp) :: dPWi_dT  !! derivative of the ln of water vapor saturation pressure with respect to ice
    
    ! Parameters for parameterizations
    real(dp) :: P1(4)  = [18.452406985, 3505.1578807, 330918.55082, 12725068.262]  !! See Tabazadeh et al. (1997), eq. 1
    real(dp) :: P2(10) = [54.842763, 6763.22, 4.21, 0.000367, 0.0415, 218.8, &
      &                 53.878, 1331.22, 9.44523, 0.014025]
      !! See Murphy & Koop (2005), eq. 10
    real(dp) :: P3(4)  = [74.8727, 7167.40548, 7.77107, 0.00505]  !! See Nachbar et al. (2019), eq. 7
    real(dp) :: P4(5)  = [6096.9385, 16.635794, 0.02711193, &
      &                 0.00001673952, 2.433502]
      !! See Sonntag et al. (1994)
    real(dp) :: P5(2)  = [-1.49274, 437.73026]  !! Fit to yet unpublished experiments by Julia Schneider
    real(dp) :: P6(6)  = [5800.2206, 1.3914993, 0.048640239, 0.000041764768, &
      &                 0.000000014452093, 6.5459673]
      !! See Hyland & Wexler (1983)
    real(dp) :: MM(2)  = [-6132.935395, 28.867509]  !! See Marti & Mauersberger (1993)
    real(dp) :: MK_P(4)= [9.550426, -5723.265, 3.53068, -0.00728332]  !! See Murphy & Koop (2005))
        
    select case (KPAR_H2OS)
      case (1)  ! (KPAR_H2OS)
        ! ---  TABAZADEH ET AL. (1997):
        DELTA_H_VAP = RGAS_SI * T**2 &
          &           * (P1(2) / T**2 + 2.0_dp * P1(3) / T**3 - 3.0_dp * P1(4) / T**4)
        
      case (2)  ! (KPAR_H2OS)
        ! --- MURPHY AND KOOP (2005):
        dlnPW_dT = P2(2) / T**2 &
          &        - P2(3) / T &
          &        + P2(4) &
          &        + 1.0_dp / cosh(P2(5) * (T - P2(6)))**2 * P2(5) &
          &          * (P2(7) - P2(8) / T - P2(9) * log(T) + P2(10) * T) &
          &        + tanh(P2(5) * (T - P2(6))) &
          &          * (P2(8) / T**2 - P2(9) / T + P2(10))
        DELTA_H_VAP = RGAS_SI * T**2 * dlnPW_dT
        
      case (3)  ! (KPAR_H2OS)
        ! --- NACHBAR ET AL. (2019):
        dlnPW_dT = P3(2) / T**2 - P3(3) / T + P3(4)
        DELTA_H_VAP = RGAS_SI * T**2 * dlnPW_dT
        
      case (4)  ! (KPAR_H2OS)
        ! --- SONNTAG (1994):
        dlnPW_dT = P4(1) / T**2 - P4(3) + 2.0_dp * P4(4) * T + P4(5) / T
        DELTA_H_VAP = RGAS_SI * T**2 * dlnPW_dT
        
      case (5)  ! (KPAR_H2OS)
        ! --- SCHNEIDER (unpublished), based on DEHS aerosol experiments:
        
        select case (KPAR_ICE)
          case (1)
            ! --- MARTI AND MAUSBERGER (1993):
            PWi = exp(MM(1) / T + MM(2))
            dPWi_dT = exp(MM(1) / T + MM(2)) * (-1) * MM(1) / T**2      
          case (2)
            ! --- MURPHY AND KOOP (2005):
            PWi = exp(MK_P(1) + MK_P(2) / T + MK_P(3) * log(T) + MK_P(4) * T)
            dPWi_dT = exp(MK_P(1) + MK_P(2) / T + MK_P(3) * log(T) + MK_P(4) * T) &
              &       * (- MK_P(2) / T**2 + MK_P(3) / T + MK_P(4))
          case default
            stop "ERROR: invalid value for variable KPAR_ICE in function `enthalpy_of_vaporization_water()`."
            
        end select
        
        dlnPW_dT = - P5(2) / T**2 + 1.0_dp / PWi * dPWi_dT
        DELTA_H_VAP = RGAS_SI * T**2 * dlnPW_dT
        
      case (6)  ! (KPAR_H2OS)
        ! --- HYLAND AND WEXLER (1983):
        dlnPW_dT = P6(1) / T**2 + P6(3) + 2.0_dp * P6(4) - 3.0_dp * P6(5) * T**2 + P6(6) / T
        DELTA_H_VAP = RGAS_SI * T**2 * dlnPW_dT
      
      case default
        stop "ERROR: invalid value for variable KPAR_H2OS in function `enthalpy_of_vaporization_water()`."
    
    end select  ! (KPAR_H2OS)
    
  end function enthalpy_of_vaporization_water


end module water_properties
