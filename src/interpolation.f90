module interpolation
  !! Interpolation methods.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: CSPLIN
  
contains


  subroutine CSPLIN(KULIM, KOLIM, X, Y, A1, A2, A3)
    !! Calculate cubic spline interpolation.
    !!
    !! TODO: Literature/implementation reference needed!
    !!
    implicit none
    integer, intent(in) :: KULIM, KOLIM
    real(dp), intent(in) :: X(*), Y(*)
    real(dp), intent(out) :: A1(*), A2(*), A3(*)
    
    real(dp) :: H(600), Q(600), U(600), M(600)
    real(dp) :: MYK, LAMK, DNK, XX, PK
    integer :: K, IDX
    
    if ((KOLIM - KULIM) .LT. 2) then
      stop "ERROR in CSPLIN: Can not interpolate within less than 2 bins.)"
    end if
    
    do K = KULIM + 1, KOLIM
      H(K) = X(K) - X(K - 1)
    end do
    
    Q(1) = -0.5_dp
    U(1) = 0.0_dp
    
    do K = KULIM + 1, KOLIM - 1
      XX = H(K) + H(K + 1)
      LAMK = H(K + 1) / XX
      MYK = H(K) / XX
      DNK = 6.0_dp / XX * ((Y(K + 1) - Y(K)) / H(K + 1) - (Y(K) - Y(K - 1)) / H(K))
      PK = MYK * Q(K - 1) + 2.0_dp
      Q(K) = -LAMK / PK
      U(K) = (DNK - MYK * U(K - 1)) / PK
    end do
    
    M(KOLIM) = -U(KOLIM - 1) / (Q(KOLIM - 1) + 2.0_dp)
    
    do K = KULIM, KOLIM - 1
      IDX = KOLIM - K
      M(IDX) = Q(IDX) * M(IDX + 1) + U(IDX)
    end do
    
    do K = KULIM, KOLIM - 1
      A2(K) = 0.5_dp * M(K)
      A1(K) = (Y(K + 1) - Y(K)) / H(K + 1) - (2.0_dp * M(K) + M(K + 1)) * H(K + 1) / 6.0_dp
      A3(K) = (M(K + 1) - M(K)) / 6.0_dp / H(K + 1)
    end do
    
  end subroutine CSPLIN


end module interpolation
