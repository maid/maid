module constants
  !! Collection of constants and static coefficients.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  use parameters, only: N_COMPONENTS
  implicit none
  public
  
  real(dp), parameter :: PI = 4.0_dp * atan(1.0_dp)  !! mathematical constant, ratio of a circles circumference to its diameter
  
  real(dp), parameter :: RGAS_CGS = 8.31446261815324e7_dp  !! in erg/mol/K, universal gas constant in CGS units
  real(dp), parameter :: RGAS_SI = 8.31446261815324_dp  !! in J/mol/K, universal gas constant in SI units
  real(dp), parameter :: T0 = 273.15_dp  !! in K, Temperature at 0°C
  real(dp), parameter :: P0 = 101325_dp  !! in Pa, atmospheric pressure at sea level
  real(dp), parameter :: G = 9.81_dp  !! in m/s², gravitational acceleration on Earth
  
  real(dp), parameter :: RHO_ICE = 0.91_dp  !! in g/cm³, density of ice at ???
  real(dp), parameter :: RHO_H2O = 1.0_dp  !! in g/cm³, density of water
  real(dp), parameter :: SURFACE_TENSION_H2O_SC = 80.0_dp  !! in mN/m, surface tension of pure supercooled water
  real(dp), parameter :: ENTHALPY_OF_FUSION_H2O = 333.55_dp  !! in J/g, enthalpy of fusion for water
  
  real(dp), parameter :: adiabatic_index_dry_air = 1.404_dp  !! dimensionless, gamma=(C_P / C_V) adiabatic index of air at (-15°C)
  real(dp), parameter :: RGAS_SPECIFIC_DRY_AIR = 287.052874_dp  !! in J/kg/K, specific gas constant for dry air, https://en.wikipedia.org/wiki/Gas_constant#Specific_gas_constant on 2023-07-01
  real(dp), parameter :: MOLAR_MASS_DRY_AIR = 28.964917_dp  !! in g/mol, molar mass of dry air
  real(dp), parameter :: DENSITY_AIR_273K = 1.2754_dp  !! in kg/m³, density of dry air at 0°C
  real(dp), parameter :: DENSITY_H2SO4 = 1.8302_dp  !! in g cm⁻³, density of liquid sulfuric acid
  
  real(dp), parameter :: MOLAR_MASS_SOLUTES(N_COMPONENTS) = [ &  !! in g/mol, molar masses of soluble components 1:NV
    & 98.07848_dp, & !! H2SO4
    & 18.01528_dp, & !! H2O
    & 63.01284_dp, & !! HNO3
    & 36.46094_dp, & !! HCl
    & 80.91194_dp]   !! HBr
  
  real(dp), parameter :: ACCOMODATION_COEF_SOLUTES(N_COMPONENTS) = [ &  !! Accommodation coefficients of condensable components 1:NV
    & 0.3_dp, &  !! H2SO4
    & 1.0_dp, &  !! H2O
    & 0.3_dp, &  !! HNO3
    & 1.0_dp, &  !! HCL
    & 1.0_dp]    !! HBR
  
  real(dp), parameter :: EARTH_ROTATION_RATE = 7.2921e-5_dp  !! in rad/s, rotation rate of the Earth
  
end module constants
