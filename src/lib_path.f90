module lib_path
  !! Collection of procedures to handle paths.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  private
  public :: is_unix_or_win_path, is_unix_path, is_win_path
  public :: is_rel_or_abs_path, is_absolute_path, is_relative_path
  public :: is_dir
  public :: convert_abspath_to_relpath, convert_relpath_to_abspath
  public :: realpath
  public :: max_pathlength
  
  integer, parameter :: max_pathlength = 255  !! upper limit for character length of paths

  
contains 
  
  
  function is_dir(path)
    !! Determine if path represents a directory or a file.
    implicit none
    character(len=*), intent(in) :: path  !! path to a file or directory
    character(len=1) :: dir_separator  !! character to separate elements of a path
    logical :: is_dir  !! flag to indicate if path points to a directory
    
    if (is_unix_path(path)) then
      dir_separator = '/'
    else if (is_win_path(path)) then
      dir_separator = '\'
    end if
    
    if (index(path, dir_separator, back=.true.) == len_trim(path)) then
      is_dir = .true.
    else
      is_dir = .false.
    end if
  end function is_dir
  
  
  function is_unix_path(path)
    !! Check if a path matches unix type.
    implicit none
    character(len=*) :: path  !! path to a file or directory
    logical :: is_unix_path  !! flag to indicate if path has a unix path pattern
    
    if (index(path, '/') .gt. 0) then
      is_unix_path = .true.
    else
      is_unix_path = .false.
    end if
  end function is_unix_path
  
  
  function is_win_path(path)
    !! Check if a path matches windows type.
    implicit none
    character(len=*) :: path  !! path to a file or directory
    logical :: is_win_path  !! flag to indicate if path has a windows path pattern
    character(len=1) :: backslash = '\'  !! backslash as character variable
    
    if (index(path, backslash) .gt. 0) then
      is_win_path = .true.
    else
      is_win_path = .false.
    end if
  end function is_win_path
  
  
  function is_unix_or_win_path(path) result(path_os_type)
    !! Check if a path matches a unix or windows path type.
    implicit none
    character(len=*) :: path  !! path to a file or directory
    character(len=7) :: path_os_type  !! specifies operating system type
    
    if (is_unix_path(path)) then
      path_os_type = 'unix'
    else if (is_win_path(path)) then
      path_os_type = 'win'
    else
      STOP "Could not determine if path is unix or windows type: " // path
    end if
  end function is_unix_or_win_path
  
  
  function is_absolute_path(path)
    !! Check if a path is absolute.
    implicit none
    character(len=*), intent(in) :: path  !! path to a file or directory
    logical :: is_absolute_path  !! flag to indicate if path is absolute
        
    is_absolute_path = .true.
    
    if (is_unix_path(path)) then
      if (index(path, '/') .eq. 1) then
        is_absolute_path = .true.
      else
        is_absolute_path = .false.
      end if
    else if (is_win_path(path)) then
      STOP "NotImplementedError: Function `is_absolute_path` is not implemented for windows paths: " // path
    end if
  end function is_absolute_path
  
  
  function is_relative_path(path)
    !! Check if a path is relative.
    implicit none
    character(len=*), intent(in) :: path  !! path to a file or directory
    logical :: is_relative_path!! flag to indicate if path is relative
    
    if (is_unix_path(path)) then
      if (.not. index(path, '/') .eq. 1) then
        is_relative_path = .true.
      else if (index(path, '/') .eq. 1) then
        is_relative_path = .false.
      end if
    else if (is_win_path(path)) then
      STOP "NotImplementedError: Function `is_relative_path` is not implemented for windows paths: " // path
    end if
  end function is_relative_path
  
  
  function is_rel_or_abs_path(path) result(path_type)
    !! Check if a path is relative or absolute.
    implicit none
    character(len=*), intent(in) :: path  !! path to a file or directory
    character(len=3) :: path_type  !! indicates if path type is relative ('rel') or absolute ('abs')
    
    if (is_absolute_path(path)) then
      path_type = 'abs'
    else if (is_relative_path(path)) then
      path_type = 'rel'
    else
      stop "Could not determine if path is relative or absolute: " // path
    end if
  end function is_rel_or_abs_path
  
  
  function convert_relpath_to_abspath(relpath) result(abspath)
    !! Convert a relative path to an absolute path.
    implicit none
    character(len=*), intent(in) :: relpath  !! relative path to a file or directory
    character(len=:), allocatable :: abspath  !! absolute path to a file or directory
    character(len=max_pathlength) :: cwd  !! current working directory
    
    call getcwd(cwd)
    allocate(character(len=len_trim(cwd)+len_trim(relpath)+1) :: abspath)
    abspath = trim(cwd) // "/" // trim(relpath)
  end function convert_relpath_to_abspath
  
  
  function convert_abspath_to_relpath(abspath, relative_to) result(relpath)
    !! Convert an absolute path to a relative path.
    use lib_string, only: count_substrings, list_substring_indices
    implicit none
    character(len=*), intent(in) :: abspath  !! absolute path to a file or directory
    character(len=*), intent(in) :: relative_to  !! base to which the relative output path must refer to
    character(len=:), allocatable :: relative_to_  !! same as relative_to, but ensured, that it is an absolute path type
    character(len=:), allocatable :: relpath  !! relative path to a file or directory
    character(len=:), allocatable :: abspath_base  !! parent directory of relpath
    
    integer, allocatable :: level_indices_abspath(:)  !! counts number of available directory levels
    integer ::  cutoff_level_idx  !! iteration index to determine `levels_up`
    integer :: levels_up  !! counter for number of directory levels between `abspath_base` and `relative_to_`
    
    ! ensure `relative_to` is an directory
    if (.not. is_dir(relative_to)) then
      STOP "Keyword argument `relative_to` must be a directory (to end with path separator): " // relative_to
    endif
    
    ! ensure `relative_to` is an absolute path
    if (is_relative_path(relative_to)) then
      allocate(character(len=len(convert_relpath_to_abspath(relative_to))) :: relative_to_)
      relative_to_ = convert_relpath_to_abspath(relative_to)
    else
      allocate(character(len=len(relative_to)) :: relative_to_)
      relative_to_ = relative_to
    end if
    
    ! determine number of directories to go up, until both paths have a common base
    allocate(integer :: level_indices_abspath(size(list_substring_indices(abspath, '/'))))
    level_indices_abspath = list_substring_indices(abspath, '/')
    
    allocate(character(len=len(abspath)) :: abspath_base)
    levels_up = 0
    do cutoff_level_idx = size(level_indices_abspath), 1, -1
      abspath_base = abspath(:level_indices_abspath(cutoff_level_idx) - 1)
      if (index(relative_to_, abspath_base) .eq. 1) exit
      levels_up = levels_up + 1
    end do
    
     allocate(character(len=len(abspath(level_indices_abspath(size(level_indices_abspath)-levels_up)+1:))) :: relpath)
     relpath = abspath(level_indices_abspath(size(level_indices_abspath)-levels_up)+1:)
    
  end function convert_abspath_to_relpath
  

end module lib_path
