module size_distribution
  !! Procedures regarding size distributions.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: get_geometric_mean_radius
  public :: get_geometric_standard_deviation
  public :: get_logspace_1D
  public :: initialize_psd
  

contains
  
  
  function get_geometric_standard_deviation(RAD, Z, RADIUS_MEAN_GEOMETRIC) result(SIGMA_G)
    !! Get the geometric standard deviation, if RAD and Z represent a lognormal size distribution.
    use parameters, only: CUTOFF_NUMBER_CONC
    implicit none
    real(dp) :: RAD(:)  !! in cm, particle radius
    real(dp) :: Z(:)  !! in cm⁻³, particle number concentration
    real(dp), optional :: RADIUS_MEAN_GEOMETRIC  !! in cm, geometric mean radius
    real (dp) :: SIGMA_G  !! dimensionless, geometric standard deviation
    
    logical, allocatable :: mask(:)  !! mask only bins with particles
    real(dp) :: RG  !! in cm, dummy variable for optional RADIUS_MEAN_GEOMETRIC
    
    allocate(mask(size(Z)))
    mask = Z(:) .gt. 0.0_dp
    
    if (present(RADIUS_MEAN_GEOMETRIC)) then
      RG = RADIUS_MEAN_GEOMETRIC
    else
      RG = get_geometric_mean_radius(pack(RAD, mask), pack(Z, mask))
    end if
    
    SIGMA_G = (sqrt( sum( (log(pack(RAD, mask)) - log(RG))**2 * pack(Z, mask) ) / sum(pack(Z, mask)) ))
  end function get_geometric_standard_deviation
  
  
  function get_geometric_mean_radius(RAD, Z) result(RADIUS_MEAN_GEOMETRIC)
    !! Get the geometric mean radius, if RAD and Z represent a lognormal size distribution.
    use parameters, only: CUTOFF_NUMBER_CONC
    use ieee_arithmetic, only: ieee_is_finite
    implicit none
    real(dp) :: RAD(:)  !! in cm, particle radius
    real(dp) :: Z(:)  !! in cm⁻³, particle number concentration
    real(dp) :: RADIUS_MEAN_GEOMETRIC  !! in cm, geometric mean radius
    logical, allocatable :: mask(:)  !! mask only bins with particles
    
    allocate(mask(size(Z)))
    mask = Z(:) .gt. CUTOFF_NUMBER_CONC
    
    RADIUS_MEAN_GEOMETRIC = exp( sum(log(pack(RAD, mask)) * pack(Z, mask)) / sum(pack(Z, mask)) )
    
    if (.not. ieee_is_finite(RADIUS_MEAN_GEOMETRIC)) then  ! catch case of empty bins
      RADIUS_MEAN_GEOMETRIC = 0.0_dp
    end if
  end function
  
  
  function get_logspace_1D(XMIN, XMAX, N_VALUES) result (X)
    !! Get an 1D-array of log10-spaced values
    implicit none
    real(dp) :: XMIN  !! first value of the log10-spaced array X
    real(dp) :: XMAX  !! last value of the log10-spaced array X
    integer :: N_VALUES  !! number of values in the log10-spaced array X
    real(dp) :: X(N_VALUES)  !! output array containing N_VALUES log10-spaced values
    
    real(dp) :: BIN_WIDTH_LOG  !! distance of two values on a log10 axis (e.g. log10(b) - log10(a))
    integer :: K  !! index for bin iteration
    
    BIN_WIDTH_LOG = (log10(XMAX) - log10(XMIN)) / (N_VALUES - 1)
    
    X(1) = log10(XMIN)
    X(N_VALUES) = log10(XMAX)
    do K = 2, (N_VALUES-1)
      X(K) = X(K-1) + BIN_WIDTH_LOG
    end do
    
    X = 10.0_dp**X  ! convert from log10 space to linear space
  end function get_logspace_1D
  
  
  elemental pure function lognormal_distribution(X, AMPLITUDE, X0, SIGMA) result(Y)
    !! Get ordinates of a lognormal distribution for abscissa values and defined lognormal parameters.
    use constants, only: PI
    implicit none
    real(dp), intent(in) :: X  !! abscissa for the lognormal distribution
    real(dp), intent(in) :: AMPLITUDE  !! scaling parameter for the lognormal distribution
    real(dp), intent(in) :: X0  !! geometric mean parameter 
    real(dp), intent(in) :: SIGMA  !! geometric standard deviation parameter
    real(dp) :: Y  !! Lognormal distribution
    
    Y = AMPLITUDE / log10(SIGMA) / sqrt(2.0_dp * PI) * exp( -(log10(X / X0))**2 / (2.0_dp * log10(SIGMA)**2) )
  end function lognormal_distribution
  
  
  function initialize_psd(X, AMPLITUDE, RAD_MEAN_GEOM, SIGMA_GEO, KMAX) result (Z)
    !! Initialize a the number concentration of a particle size distribution
    use parameters, only: CUTOFF_NUMBER_CONC
    implicit none
    
    ! define psd ordinate
    real(dp) :: AMPLITUDE  !! Scaling parameter, typically the total number concentration of an aerosol population
    real(dp) :: RAD_MEAN_GEOM  !! in cm, geometric mean diameter
    real(dp) :: SIGMA_GEO  !! geometric standard deviation
    integer :: KMAX  !! total number of size bins to be used for the lognormal distribution
    
    real(dp) :: X(:)  !! in cm, logarithmically distributed radii
    real(dp) :: Y(size(X))  !! Lognormal distribution
    real(dp) :: Z(size(X))  !! Total number of aerosol particles per size bin
    real(dp) :: BIN_WIDTH_LOG  !! distance of two values on a log10 axis (e.g. log10(b) - log10(a))
    integer :: K  !! index to iterate through size bins
    
    Y = lognormal_distribution(X, AMPLITUDE, RAD_MEAN_GEOM, SIGMA_GEO)
    
    ! scale Y to make sum_K(Y(K)) fit AMPLITUDE
    BIN_WIDTH_LOG = (log10(X(KMAX)) - log10(X(1))) / (KMAX - 1)
    do K = 1, size(X)
      Z(K) = Y(K) * BIN_WIDTH_LOG
      if (Z(K) .lt. CUTOFF_NUMBER_CONC) Z(K) = 0.0_dp
    end do
  end function initialize_psd
  
  
end module size_distribution
