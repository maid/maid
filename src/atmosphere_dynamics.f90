module atmosphere_dynamics
  !! This module contains processes describing atmospheric dynamics.
  !!
  !!
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  
  private
  public :: get_dry_adiabatic_lapse_rate
  public :: get_buoyancy_frequency
  public :: get_coriolis_frequency
  
  
  ! -----------------------------------------------------------------------------------------------
  !
  ! Interfaces for procedures from submodule `atmosphere_dynamics_gravity_wave_fluctuations`
  !
  public :: get_gravity_wave_forcing_autocorrelation_time
  public :: get_gravity_wave_updraft_forcing_sample
  public :: get_gravity_wave_fluctuations
  
  
  interface get_gravity_wave_forcing_autocorrelation_time_interface
    !! Interface to function `get_gravity_wave_forcing_autocorrelation_time()` from
    !! submodule `atmosphere_dynamics_gravity_wave_fluctuations`.
    module function get_gravity_wave_forcing_autocorrelation_time(BUOYANCY_FREQ) result (TIME_AUTOCORR)
      !! Derive autocorrelation time for updraft fluctuations as the inverse value of the Brunt‐Väisälä frequency.
      !! Acts as a timescale for the gravity wave driven fluctuations to change.
      !!
      !! Note:
      !!  The input argument BUOYANCY_FREQ should be calculated using the function `get_buoyancy_frequency`.
      !!  However, this value only leads to reasonable values for stable atmospheric stratification.
      !!  If this is not the case (e.g. for adiabatic updraft trajectories (neutral atmosphere)) you can
      !!  deliberate about wheather a pre-defined constant autocorrelation time is applicable.
      !!
      implicit none
      real(dp), intent(in) :: BUOYANCY_FREQ  !! in s⁻¹, Brunt‐Väisälä frequency
      real(dp) :: TIME_AUTOCORR  !! in s, autocorrelation time
    end function get_gravity_wave_forcing_autocorrelation_time
  end interface get_gravity_wave_forcing_autocorrelation_time_interface
  
  
  interface get_gravity_wave_updraft_forcing_sample_interface
    !! Interface to function `get_gravity_wave_updraft_forcing_sample()` from
    !! submodule `atmosphere_dynamics_gravity_wave_fluctuations`.
    module function get_gravity_wave_updraft_forcing_sample(SIGMA_DELTA_W, N_VALUES, N_BINS_PDF) result(DELTA_W_SAMPLE)
      !! Create dataset of values sampled from updraft fluctuation PDF
      implicit none
      real(dp) :: SIGMA_DELTA_W  !! cm s⁻¹, standard deviation of vertical wind speed
      integer :: N_VALUES  !! size of PDF_VALUES
      integer :: N_BINS_PDF  !! number of bins to split PDF
      
      real(dp) :: PDF_BIN_PROBABILITY(N_BINS_PDF)  !! Probability of a vertical wind speed fluctuation
      real(dp) :: PDF_BIN_UPPER_BOUND(N_BINS_PDF)  !! in cm s⁻¹, upper bin boundary along an axis of vertical wind speed fluctuations
      real(dp) :: DELTA_W_SAMPLE(N_VALUES) !! in cm s⁻¹, values of vertical wind speed fluctuations sampled from a probability distribution
    
      real(dp), parameter :: PDF_BIN_UPPER_BOUND_MAX = 100.0_dp  !! in cm s⁻¹, upper limit of DELTA_W axis, NOTE: This value is arbitraryly chosen, since the PDF is close to 0 theore
      real(dp), parameter :: PDF_BIN_UPPER_BOUND_MIN = 0.1_dp  !! in cm s⁻¹, lower limit of DELTA_W axis
      real(dp) :: LOG_BIN_WIDTH  !! in cm s⁻¹, width of bin on DELTA_W axis
      real(dp) :: LOWER_BIN_BOUNDARY  !! in cm s⁻¹, lower bin boundary of the current bin
      integer :: COUNTER_FILLED_VALUES  !! number values in DELTA_W_SAMPLE being already filled
      real(dp) :: RANDOM  !! a random real number
      real(dp) :: PLUS_OR_MINUS_SIGN  !! A factor being either +1 or -1, depending on value of RANDOM
      integer :: IDX, N  !! indices to be used in loop
    end function get_gravity_wave_updraft_forcing_sample
  end interface get_gravity_wave_updraft_forcing_sample_interface
  
  
  interface get_gravity_wave_fluctuations_interface
    !! Interface to function `get_gravity_wave_fluctuations()` from submodule `atmosphere_dynamics_gravity_wave_fluctuations`.
    module subroutine get_gravity_wave_fluctuations(DELTA_W, DELTA_TEMP, DELTA_W_SAMPLE, TEMP, TEMP_DAMPING_FACTOR, DELTA_TIME)
      !! Get a new set of DELTA_W and DELTA_TEMP based on a random element of DELTA_W_SAMPLE
      implicit none
      real(dp), intent(inout) :: DELTA_W  !! in cm s⁻¹, vertical wind velocity fluctuation
      real(dp), intent(inout) :: DELTA_TEMP  !! in K, temperature fluctuation forced by DELTA_W
      real(dp), intent(in) :: DELTA_W_SAMPLE(:)  !! in cm s⁻¹, a set of values for DELTA_W sampled from a probability distribution
      real(dp), intent(in) :: TEMP  !! in K, air temperature
      real(dp), intent(in) :: TEMP_DAMPING_FACTOR  !! damping factor for DELTA_TEMP
      real(dp), intent(in) :: DELTA_TIME  !! in seconds, time period between DELTA_TEMP and DELTA_TEMP_OLD
    end subroutine get_gravity_wave_fluctuations
  end interface get_gravity_wave_fluctuations_interface
  
  
contains
  
  
  function get_dry_adiabatic_lapse_rate(TEMP) result(DRY_ADIABATIC_LAPSE_RATE)
    !! Calculate dry adiabatic lapse rate in K cm⁻³
    use constants, only: G
    use gas_properties, only: specific_heat_dry_air_const_pres
    implicit none
    
    real(dp) :: TEMP !! in K, air temperature
    real(dp) :: CP_DRY_AIR  !! in kJ/kg/K, heat capacity of air
    real(dp) :: DRY_ADIABATIC_LAPSE_RATE  !! in K cm⁻¹
    
    CP_DRY_AIR = specific_heat_dry_air_const_pres(TEMP)
    DRY_ADIABATIC_LAPSE_RATE = G / (CP_DRY_AIR * 1e3_dp)  ! in K / m
    DRY_ADIABATIC_LAPSE_RATE = DRY_ADIABATIC_LAPSE_RATE / 100.0_dp  ! in K / cm
  end function get_dry_adiabatic_lapse_rate
  
  
  function get_buoyancy_frequency(TEMP, PRESS, TEMP_GRADIENT, PRESS_GRADIENT) result(N)
    !! Calculate Brunt‐Väisälä frequency (buoyancy frequency) in s⁻¹.
    !! A measure of the stability of a fluid to vertical displacements and
    !! an upper limit constraining gravity wave propagation.
    !!
    !! Note: The buoyancy frequency can only be calculated for a stable atmosphere.
    !!   With the potential temperature dT_pot/dz the atmosphere is (see e.g. [SP2016]):
    !!     - dT_pot/dz > 0 : stable
    !!     - dT_pot/dz = 0 : neutral (adiabatic vertical profile)
    !!     - dT_pot/dz < 0 : unstable
    !!   So this function should not be used when the model runs along an adiabatic trajectory!
    !!
    !! Literature:
    !!  * [SP2016] Seinfeld & Pandis, "Atmospheric chemistry and physics", 3rd edition,
    !!    2016, John Wiley & Sons, Inc.
    !!  * [Hantel2013] Michael Hantel, "Einführung in die theoretische Meteorologie",
    !!    2013, Springer Spektrum, DOI 10.1007/978-3-8274-3056-4
    !!
    use constants, only: G, RGAS_SPECIFIC_DRY_AIR
    use gas_properties, only: specific_heat_dry_air_const_pres
    implicit none
    
    real(dp) :: N  !! in s⁻¹, Brunt‐Väisälä frequency
    real(dp), intent(in) :: TEMP  !! in K, air temperature
    real(dp), intent(in) :: PRESS  !! in Pa, air pressure
    real(dp), intent(in) :: TEMP_GRADIENT  !! in K cm⁻1, vertical temperature gradient
    real(dp), intent(in) :: PRESS_GRADIENT  !! in Pa cm⁻¹, vertical air pressure gradient
    
    real(dp) :: KAPPA  !! factor describing the ratio of the specific gas constant and the cpecific heat capacity at constant pressure
    real(dp) :: CP_DRY_AIR  !! Specific heat capacity of dry air in (kJ kg⁻¹ K⁻¹)
    
    CP_DRY_AIR = specific_heat_dry_air_const_pres(TEMP)
    KAPPA = RGAS_SPECIFIC_DRY_AIR / (CP_DRY_AIR * 1e3_dp)   !  see [Hantel2013], eq. (6.75)
    
    N = sqrt((G * 100.0_dp) * (TEMP_GRADIENT / TEMP - KAPPA * PRESS_GRADIENT / PRESS))  ! see [Hantel2013], eq. (13.22)
  end function get_buoyancy_frequency
  
  
  function get_coriolis_frequency(LATITUDE_ANGLE) result(CORIOLIS_FREQ)
    !! Calculate coriolis frequncy in s⁻¹.
    !! Frequency of inertial oscillations on the surface of the Earth.
    use constants, only: EARTH_ROTATION_RATE, PI
    implicit none
    
    real(dp), intent(in) :: LATITUDE_ANGLE  !! in degree, latitude angular coordinate
    real(dp) :: CORIOLIS_FREQ  !! in s⁻¹, coriolis frequency
    
    if (abs(LATITUDE_ANGLE) .gt. 90.0_dp) stop "Invalid value for LATITUDE_ANGLE."  ! check input argument
    
    CORIOLIS_FREQ = 2.0_dp * EARTH_ROTATION_RATE * sin(LATITUDE_ANGLE * PI / 180.0_dp)  ! angle converted to rad
  end function get_coriolis_frequency
  
  
end module


! ==================================================================================================


submodule (atmosphere_dynamics) atmosphere_dynamics_gravity_wave_fluctuations
  !! Module to calculate forcing of updraft velocity fluctuations caused by gravity waves on air temperature.
  !! 
  !! Literature:
  !!
  !!  * [KJL2019] B. Kärcher, E. J. Jensen, and U. Lohmann, “The Impact of Mesoscale Gravity Waves on
  !!    Homogeneous Ice Nucleation in Cirrus Clouds,” Geophysical Research Letters,
  !!    vol. 46, no. 10, pp. 5556–5565, May 2019, doi: 10.1029/2019gl082437.
  !!  * [KP2019] B. Kärcher and A. Podglajen, “A Stochastic Representation of Temperature Fluctuations
  !!    Induced by Mesoscale Gravity Waves,” Journal of Geophysical Research: Atmospheres,
  !!    vol. 124, no. 21, pp. 11506–11529, Nov. 2019, doi: 10.1029/2019jd030680.
  !!
  !! Usage:
  !!
  !! 1.  
  !!     - Ensure you know the standard deviation of updraft velocity fluctuations SIGMA_DELTA_W.
  !!     - Derive an autocorrelation time, which determines the frequency of fluctuations to be updated, from the BUOYANCY_FREQUENCY.
  !!     - Ensure you know a latitude angle corresponding to a certain location, in order to calculate CORIOLIS_FREQUENCY, which is needed to calculate a damping factor against the forced temperature fluctuations.
  !! 2. Create a set of vertical wind speed fluctuation values.
  !!     - Choose the size N_VALUES of an array to be filled with values from the probability distribution of DELTA_W
  !!     -  Choose the number of bins (resolution) to divide the probability density function of DELTA_W.
  !!     -  Use function `get_gravity_wave_updraft_forcing_sample` to sample values
  !! 3.  
  !!     - In the model run check periodically if a multiple of the autocorrelation time has passed.
  !!     - If this is the case, call the subroutine `get_gravity_wave_fluctuations` to update the values of updraft velocity fluctuaion and temperature fluctuation.
  !!     -  Add the vertical wind speed fluctuation value to the previous absolute vertical wind speed (in order to get a 1st order autocorrelation) once it has been updated. The resulting sum (W0 + DELTA_W1 + DELTA_W2 ...) is applied in each timestep.
  !!     -  Apply the temperature fluctuation (derived in 3b)) in each timestep.
  implicit none

  
contains
  
  
  module function updraft_fluctuation_pdf(DELTA_W, SIGMA_DELTA_W) result(P_L)
    !! Calculate the probability of DELTA_W from the probability distribution of vertical wind speed fluctuations caused by gravity waves.
    !! The probability distribution is a Laplacian double exponential function.
    !!
    !! Literature:
    !!  * [KP2019] B. Kärcher and A. Podglajen, “A Stochastic Representation of Temperature Fluctuations
    !!    Induced by Mesoscale Gravity Waves,” Journal of Geophysical Research: Atmospheres,
    !!    vol. 124, no. 21, pp. 11506–11529, Nov. 2019, doi: 10.1029/2019jd030680.
    implicit none
    real(dp) :: DELTA_W  !! in cm s⁻¹, vertical wind speed fluctuation
    real(dp) :: SIGMA_DELTA_W !! in cm s⁻¹, standard deviation of vertical wind speed
    real(dp) :: P_L  !! Probability of a vertical wind speed fluctuation DELTA_W
    
    real(dp) :: MU_W  !! mean value taken over the one-sided (updraft) statistic
    
    MU_W = SIGMA_DELTA_W / 2.0_dp**(1.0_dp / 2.0_dp)
    P_L = exp(-abs(DELTA_W) / MU_W) / (2.0_dp * MU_W)  ! See [KP2019], eq. (8)
  end function updraft_fluctuation_pdf
  
  
  module function get_gravity_wave_updraft_forcing_sample(SIGMA_DELTA_W, N_VALUES, N_BINS_PDF) result(DELTA_W_SAMPLE)
    !! Create a dataset of values sampled from updraft fluctuation PDF
    implicit none
    real(dp) :: SIGMA_DELTA_W  !! cm s⁻¹, standard deviation of vertical wind speed
    integer :: N_VALUES  !! size of PDF_VALUES
    integer :: N_BINS_PDF  !! number of bins to split PDF
    
    real(dp) :: PDF_BIN_PROBABILITY(N_BINS_PDF)  !! Probability of a vertical wind speed fluctuation
    real(dp) :: PDF_BIN_UPPER_BOUND(N_BINS_PDF)  !! in cm s⁻¹, upper bin boundary along an axis of vertical wind speed fluctuations
    real(dp) :: DELTA_W_SAMPLE(N_VALUES) !! in cm s⁻¹, values of vertical wind speed fluctuations sampled from a probability distribution
  
    real(dp), parameter :: PDF_BIN_UPPER_BOUND_MAX = 100.0_dp  !! in cm s⁻¹, upper limit of DELTA_W axis, NOTE: This value is arbitraryly chosen, since the PDF is close to 0 theore
    real(dp), parameter :: PDF_BIN_UPPER_BOUND_MIN = 0.1_dp  !! in cm s⁻¹, lower limit of DELTA_W axis
    real(dp) :: LOG_BIN_WIDTH  !! in cm s⁻¹, width of bin on DELTA_W axis
    real(dp) :: LOWER_BIN_BOUNDARY  !! in cm s⁻¹, lower bin boundary of the current bin
    integer :: COUNTER_FILLED_VALUES  !! number values in DELTA_W_SAMPLE being already filled
    real(dp) :: RANDOM  !! a random real number
    real(dp) :: PLUS_OR_MINUS_SIGN  !! A factor being either +1 or -1, depending on value of RANDOM
    integer :: IDX, N  !! indices to be used in loop
    
    ! create logarithmically distributet upper bin boundaries
    LOG_BIN_WIDTH = (log10(PDF_BIN_UPPER_BOUND_MAX) - log10(PDF_BIN_UPPER_BOUND_MIN)) / (N_BINS_PDF - 1)
    PDF_BIN_UPPER_BOUND(1) = PDF_BIN_UPPER_BOUND_MIN
    do IDX = 2, N_BINS_PDF
      PDF_BIN_UPPER_BOUND(IDX) = 10.0_dp**(log10(PDF_BIN_UPPER_BOUND(IDX-1)) + LOG_BIN_WIDTH)
    end do
    
    ! calculate probability of each bin from area under the pdf
    DELTA_W_SAMPLE(:) = 0.0_dp  ! initialize content
    COUNTER_FILLED_VALUES = 0
    LOWER_BIN_BOUNDARY = 0.0_dp
    loopbins: do IDX = 1, N_BINS_PDF
      ! calculate probability of a DELTA_W value
      PDF_BIN_PROBABILITY(IDX) = (updraft_fluctuation_pdf(PDF_BIN_UPPER_BOUND(IDX), SIGMA_DELTA_W) &
        &                         + updraft_fluctuation_pdf(LOWER_BIN_BOUNDARY, SIGMA_DELTA_W)) / 2.0_dp &
        &                        * (PDF_BIN_UPPER_BOUND(IDX) - LOWER_BIN_BOUNDARY)
        
      ! Note, the sum over all probabilities adds up to 0.5, since we use only on the right wing of the Laplace distribution
      ! The quantity of a certain probability in DELTA_W_SAMPLE can therefore be upscaled with a factor of N_VALUES/0.5
      N = nint(PDF_BIN_PROBABILITY(IDX) * real(N_VALUES, kind=dp)/0.5_dp * 0.995_dp)  ! factor 0.995 just to have a little buffer for rounding errors
      
      ! Add DELTA_W value as often as its probability indicates to pool of values
      if (COUNTER_FILLED_VALUES+N .le. size(DELTA_W_SAMPLE)) then
        DELTA_W_SAMPLE(COUNTER_FILLED_VALUES+1 : COUNTER_FILLED_VALUES+N) = (PDF_BIN_UPPER_BOUND(IDX) + LOWER_BIN_BOUNDARY) / 2
      else
        DELTA_W_SAMPLE(COUNTER_FILLED_VALUES+1 : size(DELTA_W_SAMPLE)) = (PDF_BIN_UPPER_BOUND(IDX) + LOWER_BIN_BOUNDARY) / 2
        print*, "WARNING: array DELTA_W_SAMPLE is already filled at bin", IDX, "of total", N_BINS_PDF, &
          &     "bins. Consider to reduce the buffer factor a little bit!"
        exit loopbins
      end if
      
      COUNTER_FILLED_VALUES = COUNTER_FILLED_VALUES + N
      LOWER_BIN_BOUNDARY = PDF_BIN_UPPER_BOUND(IDX)  ! prepares lower bin boundary for next iteration
    end do loopbins
    
    ! minor correction: also fill those indices of the output array which were not filled due to rounding errors
    ! Using random values from the filled indices.
    if (COUNTER_FILLED_VALUES .lt. N_VALUES) then
      do IDX = COUNTER_FILLED_VALUES + 1, N_VALUES
        call random_number(RANDOM)
        DELTA_W_SAMPLE(IDX) = DELTA_W_SAMPLE(floor(RANDOM * COUNTER_FILLED_VALUES))
      end do
    end if
    
    ! Represent whole Laplace distribution by adding random sign, since
    ! DELTA_W_SAMPLE were sampled only from the right wing of the symmetrical Laplace distribution
    do idx = 1, COUNTER_FILLED_VALUES
      call random_number(RANDOM)
      if (nint(RANDOM) .eq. 0) then
        PLUS_OR_MINUS_SIGN = - 1.0_dp
      else if (nint(RANDOM) .eq. 1) then
        PLUS_OR_MINUS_SIGN = 1.0_dp
      end if
      
      DELTA_W_SAMPLE(IDX) = DELTA_W_SAMPLE(IDX) * PLUS_OR_MINUS_SIGN
    end do
  end function get_gravity_wave_updraft_forcing_sample
  
  
  module function get_gravity_wave_forcing_autocorrelation_time(BUOYANCY_FREQ) result (TIME_AUTOCORR)
    !! Derive autocorrelation time for updraft fluctuations as the inverse value of the Brunt‐Väisälä frequency.
    !! Acts as a timescale for the gravity wave driven fluctuations to change.
    !!
    !! Note:
    !!  The input argument BUOYANCY_FREQ should be calculated using the function `get_buoyancy_frequency`.
    !!  However, this value only leads to reasonable values for stable atmospheric stratification.
    !!  If this is not the case (e.g. for adiabatic updraft trajectories (neutral atmosphere)) you can
    !!  deliberate about wheather a pre-defined constant autocorrelation time is applicable.
    !!
    implicit none
    real(dp), intent(in) :: BUOYANCY_FREQ  !! in s⁻¹, Brunt‐Väisälä frequency
    real(dp) :: TIME_AUTOCORR  !! in s, autocorrelation time
    
    TIME_AUTOCORR = 1.0_dp / BUOYANCY_FREQ
  end function get_gravity_wave_forcing_autocorrelation_time
  
  
  module function get_gravity_wave_temperature_fluctuation(DELTA_W, DELTA_TEMP_OLD, DELTA_TIME, &
    &                                                      TEMP, TEMP_DAMPING_FACTOR) result(DELTA_TEMP)
    !! Temperature fluctuation due to gravity wave driven fluctuation of vertical wind velocity.
    implicit none
    real(dp) :: DELTA_W  !! in cm s⁻¹, vertical wind velocity fluctuation
    real(dp) :: DELTA_TEMP_OLD  !! in K, DELTA_TEMP value from the previous timestep
    real(dp) :: DELTA_TIME  !! in seconds, time period between DELTA_TEMP and DELTA_TEMP_OLD
    real(dp) :: TEMP  !! in K, air temperature
    real(dp) :: TEMP_DAMPING_FACTOR  !! damping factor for DELTA_TEMP
    
    real(dp) :: DELTA_TEMP  !! in K, temperature fluctuation due to vertical wind velocity fluctuations
    
    DELTA_TEMP = - (get_dry_adiabatic_lapse_rate(TEMP) * DELTA_W + TEMP_DAMPING_FACTOR * DELTA_TEMP_OLD) &
      &          / (1.0_dp + TEMP_DAMPING_FACTOR * DELTA_TIME)  ! see Literature: [KP2019], eq. (25)
  end function get_gravity_wave_temperature_fluctuation
  
  
  module subroutine get_gravity_wave_fluctuations(DELTA_W, DELTA_TEMP, DELTA_W_SAMPLE, TEMP, TEMP_DAMPING_FACTOR, DELTA_TIME)
    !! Get a new set of DELTA_W and DELTA_TEMP based on a random element of DELTA_W_SAMPLE
    implicit none
    real(dp), intent(inout) :: DELTA_W  !! in cm s⁻¹, vertical wind velocity fluctuation
    real(dp), intent(inout) :: DELTA_TEMP  !! in K, temperature fluctuation forced by DELTA_W
    real(dp), intent(in) :: DELTA_W_SAMPLE(:)  !! in cm s⁻¹, a set of values for DELTA_W sampled from a probability distribution
    real(dp), intent(in) :: TEMP  !! in K, air temperature
    real(dp), intent(in) :: TEMP_DAMPING_FACTOR  !! damping factor for DELTA_TEMP
    real(dp), intent(in) :: DELTA_TIME  !! in seconds, time period between DELTA_TEMP and DELTA_TEMP_OLD
    
    real(dp) :: RANDOM  !! a random real number
    
    ! forcing: variations in updraft wind speed
    call random_number(RANDOM)
    DELTA_W = DELTA_W + DELTA_W_SAMPLE(nint(RANDOM * size(DELTA_W_SAMPLE)))  ! first order autocorrelation with respect to previous value
    
    ! response: variation in temperature
    DELTA_TEMP = get_gravity_wave_temperature_fluctuation(DELTA_W, DELTA_TEMP, DELTA_TIME, TEMP, TEMP_DAMPING_FACTOR)
  end subroutine get_gravity_wave_fluctuations
  
end submodule atmosphere_dynamics_gravity_wave_fluctuations
