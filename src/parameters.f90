module parameters
  !! Central module to define parameters making them accessible from other modules/programs.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  public
  
  integer, parameter :: N_BINS = 200  !! Size of the size distribution arrays (max channel number)
  integer, parameter :: N_COMPONENTS = 5  !! Total number of soluble components
  
  integer :: KPAR_H2OS  !! Parametrization identifier for water vapor saturation pressure with respect to water, defined through [PAR-PSAT-WATER]
  integer :: KPAR_ICE   !! Parametrization identifier for water vapor saturation pressure with respect to ice, defined through [PAR-PSAT-ICE]
  integer :: KPAR_AW_P  !! Parametrization identifier for water activity, defined through [PAR-WATER-ACTIVITY]
  
  integer :: KMAX  !! highest channel number to be populated by the lognormal size distribution
  
  real(dp) :: CUTOFF_NUMBER_CONC = 1.0e-12_dp  !! in cm⁻³, lower threshold for number concentration
  
  ! Gravity wave driven mesoscale fluctuations
  integer, parameter :: GRAVITY_WAVE_UPDRAFT_FORCING_SAMPLE_SIZE = 1e4  !! number of values to be sampled from updraft fluctuation PDF
  integer, parameter :: DELTA_W_PDF_N_BINS = 1e2  !! number of bins to split updraft fluctuation probability density function

end module parameters
