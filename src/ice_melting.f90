module ice_melting
  !! Procedures desribing melting of ice in case the gas temperature exceeds melting point.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: apply_ice_melting
  
  
contains
  
  
  subroutine apply_ice_melting(LATENT_HEAT_PROCESS,  &
    &                          Z_H2O, Z_ICE, ZERO_H2O, ZERO_ICE,  &
    &                          PMASS_H2O, PMASS_ICE, RAD_H2O, IS_H2O_PRESENT, IS_ICE_PRESENT,  &
    &                          KULIM_ICE, KOLIM_ICE, KULIM_H2O, KOLIM_H2O,  &
    &                          TABS, N_SOLUTES, XWT_H2SO4_H2O_INI, X,  &
    &                          N_NUCL, N_NUCL_INI)
    !! Calculates the melting of ice crystals.
    !!
    !! TODO: Is there literature to be referenced?
    !!
    !! NOTE: The melting temperature depends on the parameterization for water vapor 
    !!       saturation pressure with respect to ice by Marti and Mauersberger (1993),
    !!       even if another parameterization was selected.
    !!
    use constants, only: PI, ENTHALPY_OF_FUSION_H2O
    use parameters, only: CUTOFF_NUMBER_CONC, N_COMPONENTS, N_BINS, KPAR_ICE
    use solution_particle, only: get_p_sat_solutes, get_solution_density
    use gas_properties, only: specific_heat_dry_air_const_pres
    implicit none
    
    real(dp), intent(out) :: LATENT_HEAT_PROCESS  !! in J cm⁻³, latent heat effect from a certain process
    real(dp), intent(inout) :: Z_H2O(*)  !! in cm⁻³, number concentration of liquid particles per channel
    real(dp), intent(inout) :: Z_ICE(*)  !! in cm⁻³, number concentration of ice crystals per channel
    logical, intent(inout) :: ZERO_H2O(*)  !! flag indicates if a channel in Z_H2O is empty
    logical, intent(inout) :: ZERO_ICE(*)  !! flag indicates if a channel in Z_ICE is empty
    real(dp), intent(inout) :: PMASS_H2O(0:N_COMPONENTS, *)  !! in g, mass of component KK in a single liquid particle within channel K
    real(dp), intent(inout) :: PMASS_ICE(0:N_COMPONENTS, *)  !! in g, mass of component KK in a single ice crystal within channel K
    real(dp), intent(inout) :: RAD_H2O(*)  !! in cm, radius of the liquid particles
    logical, intent(inout) :: IS_H2O_PRESENT  !! flag indicates if any liquid particles are present
    logical, intent(inout) :: IS_ICE_PRESENT  !! flag indicates if any ice crystals are present
    integer, intent(inout) :: KULIM_ICE, KOLIM_ICE, KULIM_H2O, KOLIM_H2O  !! lower and upper boundaries of populated channels
    
    real(dp), intent(in) :: TABS  !! in K, temperature
    integer, intent(in) :: N_SOLUTES  !! number of compounds in the solution particle (1=H2SO4, 2=+H2O, 3=+HNO3...)
    real(dp), intent(in) :: XWT_H2SO4_H2O_INI  !! initial mass fraction of sulfuric acid in solution particles
    real(dp), intent(in) :: X(N_BINS)  !! abscissa values for the generation of a particle size distribution
    
    integer, intent(inout) :: N_NUCL  !! index to distinguish between heterogeneous and homogeneous ice mode; het=2, hom=1
    integer, intent(in) :: N_NUCL_INI  !! initial value of N_NUCL
    
    real(dp) :: LATENT_HEAT_DUMMY(N_COMPONENTS)  !! in J g⁻¹, latent heat dummy argument for function/subroutine get_p_sat_solutes and VAPH2O
    real(dp) :: X_WT_SOLUTES(N_COMPONENTS)  !! Mass fraction of the solutes
    real(dp) :: Z_H2O_STORE  !! in cm⁻³, store the most recent value of Z_H2O(K)
    real(dp) :: Z_ICE_STORE  !! in cm⁻³, store the most recent value of Z_ICE(K)
    real(dp) :: P_SAT_SOLUTES(N_COMPONENTS)  !! in Pa, saturation vapor pressures of condensable components in the liquid particle
    real(dp) :: MASS_ICE_MELT  !! in g cm⁻³, mass of water to melt 
    real(dp) :: TEMP_MELT  !! in K, melting temperature of the ice crystal
    real(dp) :: RHO_PART  !! in g cm⁻³, density of solution particles
    
    integer :: K  !! index to iterate through size bins
    integer :: KULIM, KOLIM  !! lowest/highest index of populated size bins
    integer :: KK  !! index to iterate through solutes
    logical :: FOUND_POPULATED_BIN_H2O, FOUND_POPULATED_BIN_ICE  !! flag to count populated bins during bin iteration
    
    real(dp), parameter :: PARAM_MM_PW_ICE(2) = [- log(10.0_dp) * 2663.5_dp, log(10.0_dp) * 12.527_dp] 
      !! parameters from Marti & Mauersberger (1993), eq. 1
    
    if (KPAR_ICE .ne. 1) then
      print*, "WARNING: Melting of ice crystals uses the parameterization for water vapor&
        & saturation pressure with respect to ice by Marti and Mauersberger (1993), but you selected another parameterization."
    end if
    
    
    KULIM = min(KULIM_ICE, KULIM_H2O)
    KOLIM = max(KOLIM_ICE, KOLIM_H2O)
    KULIM_H2O = 0
    KOLIM_H2O = KOLIM
    KULIM_ICE = 0
    KOLIM_ICE = KOLIM
    FOUND_POPULATED_BIN_H2O = .false.
    FOUND_POPULATED_BIN_ICE = .false.
    MASS_ICE_MELT = 0.0_dp
    
    do K = KULIM, KOLIM
      if (.not. ZERO_ICE(K)) then
        Z_H2O_STORE = Z_H2O(K)
        Z_ICE_STORE = Z_ICE(K)
        do KK  =  1, N_SOLUTES
          X_WT_SOLUTES(KK) = PMASS_ICE(KK, K) / PMASS_ICE(0, K)
        end do
        
        call get_p_sat_solutes(TABS, X_WT_SOLUTES, P_SAT_SOLUTES, LATENT_HEAT_DUMMY)
        TEMP_MELT = PARAM_MM_PW_ICE(1) / (log(P_SAT_SOLUTES(2)) - PARAM_MM_PW_ICE(2))
        if (TABS .gt. TEMP_MELT) then
          MASS_ICE_MELT = MASS_ICE_MELT + Z_ICE(K) * PMASS_ICE(2, K)
          Z_H2O(K) = Z_H2O(K) + Z_ICE(K)
          Z_ICE(K) = 0.0_dp
          ZERO_ICE(K) = .true.
          N_NUCL = N_NUCL_INI

          if (Z_H2O(K) .gt. CUTOFF_NUMBER_CONC) then
            ZERO_H2O(K) = .false.
            PMASS_H2O(0, K) = PMASS_H2O(1, K)
            do KK = 2, N_SOLUTES
              PMASS_H2O(KK, K) = (PMASS_H2O(KK, K) * Z_H2O_STORE + PMASS_ICE(KK, K) * Z_ICE_STORE) / Z_H2O(K)
              PMASS_H2O(0, K) = PMASS_H2O(0, K) + PMASS_H2O(KK, K)
            end do
          end if  ! (Z_H2O(K) .gt. CUTOFF_NUMBER_CONC)
        end if  ! (TABS .gt. TEMP_MELT)
      end if  ! (.not. ZERO_ICE(K))
      
      if (Z_ICE(K) .gt. CUTOFF_NUMBER_CONC) then
        KOLIM_ICE = K
        ZERO_ICE(K) = .false.
        FOUND_POPULATED_BIN_ICE = .true.
      else
        Z_ICE(K) = 0.0_dp
        ZERO_ICE(K) = .true.
        PMASS_ICE(0, K) = exp(X(K))
        PMASS_ICE(1, K) = XWT_H2SO4_H2O_INI * PMASS_ICE(0, K)
      end if
      
      if (Z_H2O(K) .gt. CUTOFF_NUMBER_CONC) then
        KOLIM_H2O = K
        ZERO_H2O(K) = .false.
        FOUND_POPULATED_BIN_H2O = .true.
      else
        Z_H2O(K) = 0.0_dp
        ZERO_H2O(K) = .true.
        PMASS_H2O(0, K) = exp(X(K))
        PMASS_H2O(1, K) = XWT_H2SO4_H2O_INI * PMASS_H2O(0, K)
      end if
      
      X_WT_SOLUTES(1) = PMASS_H2O(1, K) / PMASS_H2O(0, K)
      X_WT_SOLUTES(3) = PMASS_H2O(3, K) / PMASS_H2O(0, K)
      RHO_PART = get_solution_density(TABS, X_WT_SOLUTES)
      RAD_H2O(K) = (PMASS_H2O(0, K) / (RHO_PART * 4.0_dp / 3.0_dp * PI))**(1.0_dp / 3.0_dp)
      if (FOUND_POPULATED_BIN_H2O) KULIM_H2O = KULIM_H2O + 1
      if (FOUND_POPULATED_BIN_ICE) KULIM_ICE = KULIM_ICE + 1
    end do
    
    if (FOUND_POPULATED_BIN_H2O) then
      KULIM_H2O = KOLIM - KULIM_H2O + 1
      IS_H2O_PRESENT = .true.
    else
      KULIM_H2O = 1
      KOLIM_H2O = 1
    end if
    
    if (FOUND_POPULATED_BIN_ICE) then
      KULIM_ICE = KOLIM - KULIM_ICE + 1
    else
      IS_ICE_PRESENT = .false.
      KULIM_ICE = 1
      KOLIM_ICE = 1
    end if
    
    LATENT_HEAT_PROCESS = MASS_ICE_MELT * ENTHALPY_OF_FUSION_H2O * (-1)  ! sign must be opposite to freezing process
    
  end subroutine apply_ice_melting

end module ice_melting
