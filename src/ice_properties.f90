module ice_properties
  !! Properties of ice, such as the water vapor saturation pressure over ice.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: get_p_h2o_sat_ice
  

contains


  function get_p_h2o_sat_ice(T) result(P_H2O_SAT_ICE)
    !! Calculate water vapor saturation pressure over ice, in Pa.
    !! The parameterization to use is defined by the global variable KPAR_ICE.
    !!
    !! Literature:
    !!
    !! * Marti & Mauersberger (1993): "A survey and new measurements of ice vapor pressure at temperatures between
    !!   170 and 250K", Geophysical Research Letters , Vol. 20, No. 5, p. 363-366, doi:10.1029/93gl00105
    !! * Murphy & Koop (2005): "Review of the vapour pressures of ice and supercooled water for atmospheric applications",
    !!   Quarterly Journal of the Royal Meteorological Society, Vol. 131, No. 608, doi:10.1256/qj.04.94
    !!
    !! Annotation for Marti & Mauersberger (1993):
    !!
    !!     P_H2O_SAT_ICE = EXP(MM(1) / T + MM(2))
    !!     MM(1) = 6132.935395 = 2663.5 * LN(10)
    !!     MM(2) = 28.867509 = 12.537 * LN(10)
    !!
    use CONSTANTS, only: RGAS_SI
    use PARAMETERS, only: KPAR_ICE
    implicit none
    real(dp) :: T  !! in K, temperature
    real(dp), dimension(2) :: MM    = [-6132.935395, 28.867509]  !! parameters from Marti & Mauersberger (1993)
    real(dp), dimension(4) :: MK_P  = [9.550426, -5723.265, 3.53068, -0.00728332]  !! See Murphy & Koop (2005), eq. 7
    real(dp) :: P_H2O_SAT_ICE  !! in Pa, water vapor saturation pressure over ice
    
    select case (KPAR_ICE)
      case (1)  ! Marti & Mauersberger (1993)
        P_H2O_SAT_ICE = exp(MM(1) / T + MM(2))
      case (2)  ! Murphy & Koop (2005)
        P_H2O_SAT_ICE = exp(MK_P(1) + MK_P(2) / T + MK_P(3) * log(T) + MK_P(4) * T)
      case default
        stop "ERROR: invalid value for variable KPAR_ICE in function get_p_h2o_sat_ice()."
    end select
    
  end function get_p_h2o_sat_ice

  
end module ice_properties
