module gas_kinetics
  !! Procedures on gas kinetics.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: diff_coef_h2o, diff_coef_hno3
  
  
contains

  
  function diff_coef_h2o(T, P) result(D_H2O)
    !! Calculate diffusion coefficient of water molecules in air.
    !!
    !! Literature:
    !!   * H. R. Pruppacher and J. D. Klett, Microphysics of Clouds and Precipitation,
    !!     3rd ed., vol. 18. Springer Netherlands, 2010., p. 503, eq. 13-3
    !!
    use constants, only: T0, P0
    implicit none
    real(dp), intent(in) :: T  !! in K, temperature
    real(dp), intent(in) :: P  !! in Pa, pressure
    real(dp) :: D_H2O  !! in cm² s⁻¹, diffusion coefficient of water vapor
    
    real(dp)  :: D0 = 0.211_dp  !! in cm² s⁻¹
    
    D_H2O = D0 * (T / T0)**1.94_dp * (P0 / P)
    
  end function diff_coef_h2o
  
  
  function diff_coef_hno3(T, P) result(D_HNO3)
    !! Calculate diffusion coefficient of HNO3 molecules in air.
    !!
    !! Literature:
    !!   * VDI-Wärmeatlas, p. DA33, eq. 87
    !!
    use constants, only: T0, P0
    implicit none
    real(dp), intent(in) :: T  !! in K, temperature
    real(dp), intent(in) :: P  !! in Pa, pressure
    real(dp) :: D_HNO3  !! in cm² s⁻¹, diffusion coefficient of HNO3 molecules in air
    
    real(dp) :: D0 = 0.13129_dp  !! in cm² s⁻¹
    
    D_HNO3 = D0 * (T / T0)**1.75_dp * (P0 / P)
    
  end function diff_coef_hno3

  
end module gas_kinetics
