module ice_formation_hom
  !! Procedures about homogeneous freezing of (solution) droplets.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  
  private
  public :: ice_form_hom_Koop2000

contains
  
  subroutine ice_form_hom_Koop2000( &
    & Z_H2O, PMASS_H2O, VOL_PART, ZERO_H2O, KULIM_H2O, KOLIM_H2O, DT_STEP, DELTA_AW, &  ! intent(in)
    & STAY_LIQUID_PROBABILITY, Z_ICE_NEW_HOM, & ! intent(inout)
    & LATENT_HEAT_PROCESS, &  ! intent(out)
    & PAR_HET_FREEZE, VOLRATIO)  ! optional, intent(in)
    !! Calculate homogeneous ice nucleation for a given size distribution of solution particles.
    !!
    !! Literature: 
    !! ---
    !!
    !! * T. Koop, B. Luo, A. Tsias, and T. Peter, “Water activity as the determinant for homogeneous
    !!   ice nucleation in aqueous solutions,” Nature, vol. 406, no. 6796, pp. 611–614, Aug. 2000,
    !!   doi: 10.1038/35020537.
    use parameters, only: N_BINS, N_COMPONENTS
    use constants, only: ENTHALPY_OF_FUSION_H2O
    implicit none
    
    real(dp), intent(in) :: Z_H2O(N_BINS)  !! in #/cm³, number concentration of liquid particles per channel
    real(dp) :: PMASS_H2O(0: N_COMPONENTS, N_BINS)  !! in g, mass of component KK in a single liquid particle within channel K
    logical, intent(in) :: ZERO_H2O(N_BINS)  !! flag indicates if a channel in Z_H2O is empty
    real(dp), intent(in) :: VOL_PART(N_BINS)  !! in cm³, volume of a liquid particle per channel
    integer, intent(in) :: KULIM_H2O, KOLIM_H2O  !! boundaries of filled liquid channels
    real(dp), intent(in) :: DT_STEP  !! in seconds, model-internal time step during TLOOP
    real(dp), intent(in) :: DELTA_AW(N_BINS)  !! difference of water activity in solution and pure water
    real(dp), intent(inout) :: STAY_LIQUID_PROBABILITY(N_BINS)  !! water-ice conversion rate, derived from EXP(-RATE_ICE)
    real(dp), intent(inout) :: Z_ICE_NEW_HOM(N_BINS)  !! in #/cm³, number of new homogeneously formed ice crystals per channel.
    real(dp), intent(out) :: LATENT_HEAT_PROCESS  !! in J/cm³, latent heat effect from a certain process
    
    character(len=*), intent(in), optional :: PAR_HET_FREEZE  !! paramtrization for heterogeneous ice formation
    real(dp), intent(in), optional :: VOLRATIO  !! Scaling ratio for nucleation rates in mixed freezing approach according
    
    real(dp) :: NUCL_RATE_COEF_HOM(N_BINS)  !! Nucleation rate coefficient for homogeneous freezing
    integer :: K  !! index for size bin iteration
    integer :: KJ  !! index to iterate through parameters and polynom orders
    real(dp), parameter :: PARAM_KOOP_LOGJ(0:3) = [ &  !! Parameters from Koop et al. (2000), eq. 7
      & -log(10.0_dp) * 906.7_dp, &
      & log(10.0_dp) * 8502_dp, &
      & -log(10.0_dp) * 26924_dp, &
      & log(10.0_dp) * 29180_dp]
    
    LATENT_HEAT_PROCESS = 0.0_dp
    
    do K = KULIM_H2O, KOLIM_H2O
      if (ZERO_H2O(K)) cycle
      ! Nucleation rates for homogeneous freezing from Koop et al. 2000:
      if (DELTA_AW(K) .gt. 0.34_dp) then
        NUCL_RATE_COEF_HOM(K) = 1e19_dp
      else if (DELTA_AW(K) .lt. 0.26_dp) then
        NUCL_RATE_COEF_HOM(K) = 0.0_dp
      else
        NUCL_RATE_COEF_HOM(K) = PARAM_KOOP_LOGJ(0)
        do KJ = 1, 3
          NUCL_RATE_COEF_HOM(K) = NUCL_RATE_COEF_HOM(K) + PARAM_KOOP_LOGJ(KJ) * DELTA_AW(K)**KJ
        end do
        NUCL_RATE_COEF_HOM(K) = exp(NUCL_RATE_COEF_HOM(K))
      end if
      
      if (present(PAR_HET_FREEZE)) then
        ! This is only necessary when mixed freezing (het+hom) occurs simultaneously
        if (PAR_HET_FREEZE .eq. 'KL2003') NUCL_RATE_COEF_HOM(K) = VOLRATIO * NUCL_RATE_COEF_HOM(K)
      end if
      
      STAY_LIQUID_PROBABILITY(K) = exp(-VOL_PART(K) * NUCL_RATE_COEF_HOM(K) * DT_STEP)
      Z_ICE_NEW_HOM(K) = Z_H2O(K) * (1.0_dp - STAY_LIQUID_PROBABILITY(K))
      
      LATENT_HEAT_PROCESS = LATENT_HEAT_PROCESS + PMASS_H2O(1, K) * Z_ICE_NEW_HOM(K) * ENTHALPY_OF_FUSION_H2O
    end do  ! do K = KULIM_H2O, KOLIM_H2O
    
  end subroutine ice_form_hom_Koop2000
  
end module ice_formation_hom
