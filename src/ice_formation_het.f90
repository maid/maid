module ice_formation_het
  !! Procedures calculating heterogeneous ice formation
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  private
  
  public :: ice_form_het_deposition_ns
  public :: ice_form_het_immersion_KL2003
  
  
  interface ice_form_het_immersion_KL2003_interface
    !! Interface to subroutine `ice_form_het_immersion_KL2003` from submodule `ice_formation_het_immersion`.
    module subroutine ice_form_het_immersion_KL2003( &
      & IN_TYPE, TABS, DT_STEP, WATER_ACTIVITY_ICE, DELTA_AW, &
      & Z_H2O, RAD_H2O, PMASS_H2O, VOL_PART, ZERO_H2O, KULIM_H2O, KOLIM_H2O, &
      & Z_IN_TOT, Z_ICE_HET_SUM, N_NUCL, &
      & Z_ICE_NEW_HET, LATENT_HEAT_PROCESS, VOLRATIO)
      !! Calculate heterogeneous ice formation (immersion freezing) of liquid solution aerosol particles.
      !!
      !! Literature:
      !! ---
      !!
      !! * B. Kärcher and U. Lohmann, “A parameterization of cirrus cloud formation: Heterogeneous freezing,”
      !!   Journal of Geophysical Research, vol. 108, no. D14, 2003, doi: 10.1029/2002jd003220.
      !! * B. Kärcher and C. Voigt, “Formation of nitric acid/water ice particles in cirrus clouds”,
      !!   Geophysical Research Letters, vol. 33, no. 8, 2006, doi: 10.1029/2006gl025927.
      !!
      use parameters, only: N_BINS, N_COMPONENTS
      use constants, only: ENTHALPY_OF_FUSION_H2O
      implicit none
      character(len=*), intent(in) :: IN_TYPE  !! specifies the freezing behavior for immersion freezing with PAR_HET_FREEZE=KL2003
      real(dp), intent(in) :: TABS  !! in K, air temperature
      real(dp), intent(in) :: DT_STEP  !! in seconds, model-internal time step during TLOOP
      real(dp), intent(in) :: WATER_ACTIVITY_ICE  !! ice activity, ratio of water vapor saturation pressure over ice and water vapor saturation pressure over water
      real(dp), intent(in) :: DELTA_AW(N_BINS)  !! difference of water activity in solution and pure water
      real(dp), intent(in) :: Z_H2O(N_BINS)  !! in #/cm³, number concentration of liquid particles per channel
      real(dp), intent(in) :: RAD_H2O(N_BINS)  !! in cm, radius of the liquid particles
      real(dp), intent(in) :: PMASS_H2O(0: N_COMPONENTS, N_BINS)  !! in g, mass of component KK in a single liquid particle within channel K
      real(dp), intent(in) :: VOL_PART(N_BINS)  !! in cm³, volume of a liquid particle per channel
      logical, intent(in) :: ZERO_H2O(N_BINS)  !! flag indicates if a channel in Z_H2O is empty
      integer, intent(in) :: KULIM_H2O, KOLIM_H2O  !! boundaries of filled liquid channels
      
      real(dp), intent(in) :: Z_IN_TOT  !! in #/cm³, total number concentration of ice-nucleating particles
      real(dp), intent(in) :: Z_ICE_HET_SUM  !! in cm⁻3, sum of all heterogeneously formed ice crystals
      integer, intent(inout) :: N_NUCL  !! index to distinguish between heterogeneous and homogeneous ice mode; het=2, hom=1
      
      real(dp), intent(inout) :: Z_ICE_NEW_HET(N_BINS)  !! in #/cm³, number of new heterogeneously formed ice crystals per channel.
      real(dp), intent(out) :: LATENT_HEAT_PROCESS  !! in J/cm³, latent heat effect from a certain process
      real(dp), intent(out) :: VOLRATIO  !! Scaling ratio for nucleation rates in mixed freezing approach
      
      real(dp) :: DELTA_AW_SHIFT  !! shifted water activity to calculate heterogeneous freezing according to Kärcher & Lohmann et. al (2003)
      real(dp) :: SAT_ICE_CRIT_HET  !! critical water vapor saturation ratio for heterogeneous freezing to happen, according to Kärcher & Lohmann et. al (2003)
      real(dp) :: SAT_ICE_CRIT_HOM  !! critical water vapor saturation ratio with respect to ice for homogeneous freezing to happen
      real(dp) :: NUCL_RATE_COEF_HET(N_BINS)  !! in cm⁻³ s⁻¹, heterogeneous nucleation rate coefficient
      integer :: K  !! index for size bin iteration
      integer :: KJ  !! index to iterate through parameters and polynom orders
      real(dp), parameter :: PARAM_KOOP_LOGJ(0:3) = [ &  !! Parameters from Koop et al. (2000), eq. 7
        & -log(10.0_dp) * 906.7_dp, &
        & log(10.0_dp) * 8502_dp, &
        & -log(10.0_dp) * 26924_dp, &
        & log(10.0_dp) * 29180_dp]
    end subroutine ice_form_het_immersion_KL2003
  end interface ice_form_het_immersion_KL2003_interface
  
  
  interface ice_form_het_deposition_ns_interface
    !! Interface to subroutine `ice_form_het_deposition_ns` from submodule `ice_formation_het_ns`.
    module subroutine ice_form_het_deposition_ns( &
      & TABS, SAT_ICE, &
      & Z_IN, Z_IN_INI, SURF_IN, &
      & NS, FROZEN_INP, PAR_HET_FREEZE)
      !! Calculate heterogeneous deposition freezing according ice-nucleating active site (INAS) density parameterizations.
      use parameters, only: CUTOFF_NUMBER_CONC, N_BINS
      implicit none
      real(dp), intent(in) :: TABS  !! in K, air temperature
      real(dp), intent(in) :: SAT_ICE  !! water vapor saturation ratio with respect to ice
      real(dp), intent(in) :: Z_IN(N_BINS)  !! in cm⁻³, number concentration of ice-nucleating particles per channel
      real(dp), intent(in) :: Z_IN_INI(N_BINS)  !! in cm⁻³, initial number concentration of ice-nucleating particles per channel
      real(dp), intent(in) :: SURF_IN(N_BINS)  !! in cm², surface of an ice-nucleating particle in channel K
      real(dp), intent(inout) :: NS  !! in m⁻², ice nucleating active site density
      real(dp), intent(out) :: FROZEN_INP(N_BINS)  !! in cm⁻³, number concentration from initial INPs to freeze heterogeneously at current time step
      character(len=40), intent(in) :: PAR_HET_FREEZE  !! paramtrization for heterogeneous ice formation
      
      real(dp) :: FROZEN_INP_OLD(N_BINS)  !! in #/cm³, number concentration from initial INPs to be frozen heterogeneously at previous time step
      real(dp) :: NS_OLD  !! in m⁻², previous ice nucleating active site density
      integer :: K  !! index to iteratate though size bins
    end subroutine ice_form_het_deposition_ns
  end interface ice_form_het_deposition_ns_interface
  
  
end module ice_formation_het


submodule (ice_formation_het) ice_formation_het_immersion
  !! Procedures about heterogeneous immersion freezing.
  implicit none
  
contains

  module subroutine ice_form_het_immersion_KL2003( &
    & IN_TYPE, TABS, DT_STEP, WATER_ACTIVITY_ICE, DELTA_AW, &
    & Z_H2O, RAD_H2O, PMASS_H2O, VOL_PART, ZERO_H2O, KULIM_H2O, KOLIM_H2O, &
    & Z_IN_TOT, Z_ICE_HET_SUM, N_NUCL, &
    & Z_ICE_NEW_HET, LATENT_HEAT_PROCESS, VOLRATIO)
    !! Calculate heterogeneous ice formation (immersion freezing) of liquid solution aerosol particles.
    !!
    !! Literature:
    !! ---
    !!
    !! * B. Kärcher and U. Lohmann, “A parameterization of cirrus cloud formation: Heterogeneous freezing,”
    !!   Journal of Geophysical Research, vol. 108, no. D14, 2003, doi: 10.1029/2002jd003220.
    !! * B. Kärcher and C. Voigt, “Formation of nitric acid/water ice particles in cirrus clouds”,
    !!   Geophysical Research Letters, vol. 33, no. 8, 2006, doi: 10.1029/2006gl025927.
    !!
    use parameters, only: N_BINS, N_COMPONENTS
    use constants, only: ENTHALPY_OF_FUSION_H2O
    implicit none
    character(len=*), intent(in) :: IN_TYPE  !! specifies the freezing behavior for immersion freezing with PAR_HET_FREEZE=KL2003
    real(dp), intent(in) :: TABS  !! in K, air temperature
    real(dp), intent(in) :: DT_STEP  !! in seconds, model-internal time step during TLOOP
    real(dp), intent(in) :: WATER_ACTIVITY_ICE  !! ice activity, ratio of water vapor saturation pressure over ice and water vapor saturation pressure over water
    real(dp), intent(in) :: DELTA_AW(N_BINS)  !! difference of water activity in solution and pure water
    real(dp), intent(in) :: Z_H2O(N_BINS)  !! in #/cm³, number concentration of liquid particles per channel
    real(dp), intent(in) :: RAD_H2O(N_BINS)  !! in cm, radius of the liquid particles
    real(dp), intent(in) :: PMASS_H2O(0: N_COMPONENTS, N_BINS)  !! in g, mass of component KK in a single liquid particle within channel K
    real(dp), intent(in) :: VOL_PART(N_BINS)  !! in cm³, volume of a liquid particle per channel
    logical, intent(in) :: ZERO_H2O(N_BINS)  !! flag indicates if a channel in Z_H2O is empty
    integer, intent(in) :: KULIM_H2O, KOLIM_H2O  !! boundaries of filled liquid channels
    
    real(dp), intent(in) :: Z_IN_TOT  !! in #/cm³, total number concentration of ice-nucleating particles
    real(dp), intent(in) :: Z_ICE_HET_SUM  !! in cm⁻3, sum of all heterogeneously formed ice crystals
    integer, intent(inout) :: N_NUCL  !! index to distinguish between heterogeneous and homogeneous ice mode; het=2, hom=1
    
    real(dp), intent(inout) :: Z_ICE_NEW_HET(N_BINS)  !! in #/cm³, number of new heterogeneously formed ice crystals per channel.
    real(dp), intent(out) :: LATENT_HEAT_PROCESS  !! in J/cm³, latent heat effect from a certain process
    real(dp), intent(out) :: VOLRATIO  !! Scaling ratio for nucleation rates in mixed freezing approach
    
    real(dp) :: DELTA_AW_SHIFT  !! shifted water activity to calculate heterogeneous freezing according to Kärcher & Lohmann et. al (2003)
    real(dp) :: SAT_ICE_CRIT_HET  !! critical water vapor saturation ratio for heterogeneous freezing to happen, according to Kärcher & Lohmann et. al (2003)
    real(dp) :: SAT_ICE_CRIT_HOM  !! critical water vapor saturation ratio with respect to ice for homogeneous freezing to happen
    real(dp) :: NUCL_RATE_COEF_HET(N_BINS)  !! in cm⁻³ s⁻¹, heterogeneous nucleation rate coefficient
    integer :: K  !! index for size bin iteration
    integer :: KJ  !! index to iterate through parameters and polynom orders
    real(dp), parameter :: PARAM_KOOP_LOGJ(0:3) = [ &  !! Parameters from Koop et al. (2000), eq. 7
      & -log(10.0_dp) * 906.7_dp, &
      & log(10.0_dp) * 8502_dp, &
      & -log(10.0_dp) * 26924_dp, &
      & log(10.0_dp) * 29180_dp]
    
    LATENT_HEAT_PROCESS = 0.0_dp
    
    ! Select ice-nucleating particle type from variable IN_TYPE
    select case (IN_TYPE)
    ! TODO: Literature references? Parameterizations were added by Margot Hildebrand.
      case ('COATED_SOOT')  ! H2SO4 Coated Soot
        SAT_ICE_CRIT_HET = 2.30_dp - (0.0043333_dp * TABS)
      case ('SOOT')  ! Soot
        SAT_ICE_CRIT_HET = 2.06_dp - (0.004_dp * TABS)
      case ('AMMONIUMSULFAT')  ! Ammonium sulfate
        SAT_ICE_CRIT_HET = 2.24_dp - (0.0048333_dp * TABS)
      case ('MINERAL_DUST')  ! Mineral Dust
        SAT_ICE_CRIT_HET = 1.34_dp - (0.00102_dp * TABS)
      case ('VOLCANIC_ASH_LOW')  ! VOLCANIC_ASH_LOW
        SAT_ICE_CRIT_HET = 1.29_dp - (0.00102_dp * TABS)
      case ('VOLCANIC_ASH_HIGH')  ! VOLCANIC_ASH_HIGH
        SAT_ICE_CRIT_HET = 1.43_dp - (0.00102_dp * TABS)
      case ('SOOT_ACTIVATED')  ! SOOT ACTIVATED (CR-AVE? 2005 Costa Rica – Aura Validation Experiment, Popp et al., 2007)
        SAT_ICE_CRIT_HET = 2.25_dp - (0.0043_dp * TABS)
      case default  ! Custom freezing threshold, if IN_TYPE is a number
        read(IN_TYPE, *) SAT_ICE_CRIT_HET
        SAT_ICE_CRIT_HET = SAT_ICE_CRIT_HET / 100.0_dp
    end select
    
    VOLRATIO = (1.6048754_dp - SAT_ICE_CRIT_HET) / 0.50829226_dp
    ! TODO: Literature reference for this function?
    ! VOLRATIO(SAT_ICE_CRIT_HET) is decreasing from ~1.18@SICE=1.0 linearly down to 0.0@~SICE=1.6
    
    SAT_ICE_CRIT_HOM = 2.418_dp - TABS / 245.68_dp  ! Kärcher & Lohmann 2003, eq. 5, WARNING: This parameterization is specified only for one certrain size
    if (SAT_ICE_CRIT_HOM .gt. (1.0_dp / WATER_ACTIVITY_ICE)) SAT_ICE_CRIT_HOM = 1.0_dp / WATER_ACTIVITY_ICE  ! Check if Homogeneous freezing treshold is above water saturation
    
    DELTA_AW_SHIFT = (SAT_ICE_CRIT_HOM - SAT_ICE_CRIT_HET) * WATER_ACTIVITY_ICE - 0.04_dp  ! TODO: Why minus 0.04?
    
    do K = KULIM_H2O, KOLIM_H2O
      if (ZERO_H2O(K)) cycle
      
      ! Calculate nucleation rate coefficient for heterogeneous/mixed freezing
      NUCL_RATE_COEF_HET(K) = 0.0_dp

      ! Calculate nucleation rate coefficient according to Koop (2000)
      NUCL_RATE_COEF_HET(K) = PARAM_KOOP_LOGJ(0)
      do KJ = 1, 3
        NUCL_RATE_COEF_HET(K) = NUCL_RATE_COEF_HET(K) + PARAM_KOOP_LOGJ(KJ) * (DELTA_AW(K) + DELTA_AW_SHIFT)**KJ
      end do
      NUCL_RATE_COEF_HET(K) = exp(NUCL_RATE_COEF_HET(K))
      
      ! edge cases for large/small shifted water activity
      if ((DELTA_AW(K) + DELTA_AW_SHIFT) .gt. 0.34_dp) then
        NUCL_RATE_COEF_HET(K) = 1e19_dp
      end if
      if ((DELTA_AW(K) + DELTA_AW_SHIFT) .lt. 0.06_dp) then
        NUCL_RATE_COEF_HET(K) = 0.0_dp
      end if
      
      ! Nucleation rate coefficients for mixed/heterogeneous freezing (Kärcher & Lohmann 2003)
      if (DELTA_AW(K) .gt. 0.26_dp) then
      ! Mixed freezing
        NUCL_RATE_COEF_HET(K) = (1.0_dp - VOLRATIO)**(2.0_dp / 3.0_dp)  &
          &                     * 3e-6_dp * NUCL_RATE_COEF_HET(K) * 3.0_dp / RAD_H2O(K)  ! V=4/3*pi*r³ | * 3/r → 4*pi*r² = surface
      else
      !	Pure Heterogeneous freezing
        NUCL_RATE_COEF_HET(K) = 3e-6_dp * NUCL_RATE_COEF_HET(K) * 3.0_dp / RAD_H2O(K)
      end if
      
      ! Calc number of new heterogeneously formed ice
      Z_ICE_NEW_HET(K) = Z_H2O(K) * (1.0_dp - exp(- VOL_PART(K) * NUCL_RATE_COEF_HET(K) * DT_STEP))
      if ((Z_ICE_HET_SUM + sum(Z_ICE_NEW_HET)) .gt. Z_IN_TOT) then
      ! limit heterogeneous freezing if all ice nuclei are consumed
        Z_ICE_NEW_HET(K) = Z_IN_TOT - Z_ICE_HET_SUM - sum(Z_ICE_NEW_HET(:K-1))
        N_NUCL = 1
      end if
      
      LATENT_HEAT_PROCESS = LATENT_HEAT_PROCESS + PMASS_H2O(1, K) * Z_ICE_NEW_HET(K) * ENTHALPY_OF_FUSION_H2O
      
    end do  ! do K = KULIM_H2O, KOLIM_H2O
  end subroutine ice_form_het_immersion_KL2003

end submodule




submodule (ice_formation_het) ice_formation_het_ns
  !! Procedures about heterogeneous deposition freezing.
  implicit none

  
contains


  module subroutine ice_form_het_deposition_ns( &
    & TABS, SAT_ICE, &
    & Z_IN, Z_IN_INI, SURF_IN, &
    & NS, FROZEN_INP, PAR_HET_FREEZE)
    !! Calculate heterogeneous deposition freezing according ice-nucleating active site (INAS) density parameterizations.
    use parameters, only: KMAX
    use parameters, only: CUTOFF_NUMBER_CONC, N_BINS
    implicit none
    real(dp), intent(in) :: TABS  !! in K, air temperature
    real(dp), intent(in) :: SAT_ICE  !! water vapor saturation ratio with respect to ice
    real(dp), intent(in) :: Z_IN(N_BINS)  !! in cm⁻³, number concentration of ice-nucleating particles per channel
    real(dp), intent(in) :: Z_IN_INI(N_BINS)  !! in cm⁻³, initial number concentration of ice-nucleating particles per channel
    real(dp), intent(in) :: SURF_IN(N_BINS)  !! in cm², surface of an ice-nucleating particle in channel K
    real(dp), intent(inout) :: NS  !! in m⁻², ice nucleating active site density
    real(dp), intent(out) :: FROZEN_INP(N_BINS)  !! in cm⁻³, number concentration from initial INPs to freeze heterogeneously at current time step
    character(len=40), intent(in) :: PAR_HET_FREEZE  !! paramtrization for heterogeneous ice formation
    
    real(dp) :: FROZEN_INP_OLD(N_BINS)  !! in #/cm³, number concentration from initial INPs to be frozen heterogeneously at previous time step
    real(dp) :: NS_OLD  !! in m⁻², previous ice nucleating active site density
    integer :: K  !! index to iteratate though size bins
    
    FROZEN_INP(:) = 0.0_dp
    NS_OLD = NS
    
    NS = get_ns(TABS, SAT_ICE, PAR_HET_FREEZE) / (100.0_dp**2) ! in cm**-2
    
    ! form new het. ice from INPs
    if (NS .gt. NS_OLD) then
      do K = 1, KMAX
        if (Z_IN(K) .gt. CUTOFF_NUMBER_CONC) then
          FROZEN_INP(K) = ns_het_ice(Z_IN_INI(K), SURF_IN(K), ns)
        end if
      end do
    end if  ! (NS .gt. NS_OLD)
  
  end subroutine ice_form_het_deposition_ns

  
  module function get_ns(TEMP, SAT_ICE, PARAMETERIZATION) result(NS)
    !! Calculates ice-nucleating active site (INAS) density according to a given
    !! parameterization at given ambient conditions.
    !!
    use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
    use CONSTANTS, only: PI
    implicit none
    
    real(dp) :: TEMP  !! in K, air temperature
    real(dp) :: SAT_ICE  !! dimensionless, water vapor saturation ratio with respect to ice
    character(len=*) :: PARAMETERIZATION  !! parameterization name for ice-nucleating active site density
    real(dp) :: NS  !! in m⁻², ice-nucleating active site density
    
    real(dp) :: A, B, G, K, L  !! alpha, betta, gamma, kappa, lambda in Ullrich2017

    select case(PARAMETERIZATION)
      case ('SOOT_ULLRICH2017')
          A = 46.021_dp
          B = 0.011_dp
          G = 248.560_dp
          K = 0.148_dp
          L = 237.570_dp
          NS = exp(A * (SAT_ICE - 1.0_dp)**(1.0_dp / 4.0_dp) &
            &      * cos(B * (TEMP - G))**2 * (PI / 2.0_dp - atan(K * (TEMP - L))) / PI)
      case ('DUST_ULLRICH2017')
          A = 285.692_dp
          B = 0.017_dp
          G = 256.692_dp
          K = 0.080_dp
          L = 200.745_dp
          NS = exp(A * (SAT_ICE - 1.0_dp)**(1.0_dp / 4.0_dp) &
            &      * cos(B * (TEMP - G))**2 * (PI / 2.0_dp - atan(K * (TEMP - L))) / PI)
      case ('DUST_ULLRICH2017_UPDATED')
          A = 297.521_dp
          B = 0.0088_dp
          G = 320.525_dp
          K = 0.042_dp
          L = 201.424_dp
          NS = exp(A * (SAT_ICE - 1.0_dp)**(1.0_dp / 4.0_dp) &
            &      * cos(B * (TEMP - G))**2 * (PI / 2.0_dp - atan(K * (TEMP - L))) / PI)
      case ('ATD_STEINKE2015')
          A = -(TEMP - 273.2_dp) + (SAT_ICE - 1.0_dp) * 100.0_dp  ! "x_therm"
          NS = 1.88_dp * 1e5_dp * exp(0.2659_dp * A)
          
      ! Adding yet unpublished preliminary parameterizations from PhD work of Tobias Schorr (as of state @ 01/2023)
      case ('FUMED_SILICA_SCHORR')
          A = 5.663_dp
          B = 5.383_dp
          NS = 10.0_dp**(A + SAT_ICE * B)
      case ('QUARTZ_SCHORR')
          A = 5.96_dp
          B = 4.318_dp
          NS = 10.0_dp**(A + SAT_ICE * B)
      case ('CALCIUM_CARBONATE_SCHORR')
          ! NOTE: This parameterization refers to CaCO3 which was in an environment with sulfuric acid!
          A = 4.424_dp
          B = 4.85_dp
          NS = 10.0_dp**(A + SAT_ICE * B)
      case default
          stop "ERROR: invalid value for PARAMETERIZATION in function get_ns: " // PARAMETERIZATION
    end select
    
  end function get_ns


  module function ns_het_ice(NUM, AE_SURFACE, NS) result(N_ICE_HET)
    !! Calculate the number of heterogeneously frozen aerosol particles based on
    !! the number of aerosol particles with a certain particle surface.
    !!
    !! Note: AE_SURFACE relates to the surface of a single aerosol particle.
    !!
    use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
    implicit none
    real(dp) :: NUM  !! in in cm⁻³, number (concentration) of binned particles
    real(dp) :: AE_SURFACE  !! in cm², surface of a single particles in each bin
    real(dp) :: NS   !! in in # cm⁻², ice-nucleating active site density
    
    real(dp) :: N_ICE_HET  !! in in cm⁻³, number concentration of heterogeneously frozen aerosol particles
    
    N_ICE_HET = NUM * (1.0_dp - exp(- AE_SURFACE * NS))
  end function ns_het_ice
  
  
end submodule ice_formation_het_ns
