module sedimentation
  !! Procedures on sedimentation.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  use PARAMETERS, only: N_COMPONENTS
  implicit none
  private
  public :: apply_sedimentation
  
  
contains


  subroutine apply_sedimentation(T_ABS, P, DENSITY_AIR, PMASS_ICE, Z_ICE, KULIM_ICE, KOLIM_ICE, DT_STEP, DZ, SEDFACTOR)
    !! Calculate sedimentation of ice crystals and apply changes on PMASS_ICE and Z_ICE.
    !!
    !! ### Literature:
    !!
    !! * P. Spichtinger & D. Cziczo, Impact of heterogeneous ice nuclei on homogeneous
    !!   freezing events in cirrus clouds, JGR, 2010, https://doi.org/10.1029/2009JD012168
    !!
    !! ---
    !! **Note** about sedimentation factor:
    !!
    !!     sedfactor = flux through top / flux through bottom
    !!
    !! sedfactor           | effect
    !! ------------------- | --------------------------------------------------------
    !! 0 < sedfactor < 0.6 | regions with contineous and strong nucleation
    !! sedfactor = 0       | no sedimentation input in the box (top of cirrus cloud)
    !! sedfactor = 1       | without sedimentation
    !!
    implicit none
    
    real(dp), intent(in) :: T_ABS
    real(dp), intent(in) :: P
    real(dp), intent(in) :: DENSITY_AIR
    real(dp), intent(inout) :: PMASS_ICE(0: N_COMPONENTS, *)
    real(dp), intent(inout) :: Z_ICE(*)
    integer, intent(in) :: KULIM_ICE, KOLIM_ICE
    real(dp), intent(in) :: DT_STEP
    real(dp), intent(in) :: DZ
    real(dp), intent(in) :: SEDFACTOR
    
    real(dp) :: RFM  !! Mass weighted flux from above inside the box through the upper boundary
    real(dp) :: RFN  !! Number weighted flux from above inside the box through the upper boundary
    real(dp) :: VTM  !! Mass weighted terminal Velocity
    real(dp) :: VTN  !! Number weighted terminal Velocity
    real(dp) :: ALPHAM, ALPHAN
    real(dp) :: SED_RATIO
    real(dp) :: QIT, AIT, RHOICT, ANZICT, IWCKG, PMASS_ICE_OLD
    integer :: K, KK

    binloop: do K = KULIM_ICE, KOLIM_ICE
      if ((Z_ICE(K) .gt. 0.0_dp) .AND. (PMASS_ICE(2, K) .gt. 0.0_dp)) then
        ANZICT = Z_ICE(K) * 1e6_dp  ! Conversion to [1/m3]
        RHOICT = PMASS_ICE(2, K) * Z_ICE(K) * 1e3_dp ! Conversion to [kg/m3]
        IWCKG = RHOICT

        QIT = RHOICT / DENSITY_AIR ! Conversion to [kg/kg (dry air)]
        AIT = ANZICT / DENSITY_AIR ! Conversion to [1/kg (dry air)]

        RFM = 0.0_dp
        RFN = 0.0_dp
        VTM = 0.0_dp
        VTN = 0.0_dp

        call TERM_VEL_ICE_NEW_CONS(IWCKG, ANZICT, T_ABS, P, VTM, VTN, DT_STEP, DZ)

        ! Convert only for RFM and RFN
        VTM = -1.0_dp * VTM
        VTN = -1.0_dp * VTN

        ALPHAM = VTM * DT_STEP / DZ
        ALPHAN = VTN * DT_STEP / DZ

        RFM = DENSITY_AIR * VTM * QIT * SEDFACTOR
        RFN = DENSITY_AIR * VTN * AIT * SEDFACTOR

        if (ALPHAM .gt. 1e-20_dp) then
          QIT = QIT * exp(-ALPHAM) + RFM * (1.0_dp - exp(-ALPHAM)) / (DENSITY_AIR * VTM)
        end if
        if (ALPHAN .gt. 1e-20_dp) then
          AIT = AIT * exp(-ALPHAN) + RFN * (1.D0 - exp(-ALPHAN)) / (DENSITY_AIR * VTN)
        end if

        PMASS_ICE_OLD = PMASS_ICE(2, K)
        PMASS_ICE(2, K) = QIT * DENSITY_AIR / (Z_ICE(K) * 1e3_dp)
        Z_ICE(K) = AIT * DENSITY_AIR / 1e6_dp

        SED_RATIO =  PMASS_ICE(2, K) / PMASS_ICE_OLD
        PMASS_ICE(1, K) = PMASS_ICE(1, K) * SED_RATIO
        PMASS_ICE(3, K) = PMASS_ICE(3, K) * SED_RATIO
        PMASS_ICE(4, K) = PMASS_ICE(4, K) * SED_RATIO
        PMASS_ICE(5, K) = PMASS_ICE(5, K) * SED_RATIO

        PMASS_ICE(0, K) = 0.0_dp
        
        do KK = 1, N_COMPONENTS
          PMASS_ICE(0, K) = PMASS_ICE(0, K) + PMASS_ICE(KK, K)
        end do
      end if  ! (Z_ICE(K) .gt. 0.D0 .AND. PMASS_ICE(2,K) .gt. 0.D0)
    end do binloop
  end subroutine apply_sedimentation


  subroutine TERM_VEL_ICE_NEW_CONS(QICC, ANICC, TEMP, PC, VM, VN, DT_STEP, DZ)
    !! Calculate terminal velocity for sedimentation.
    !!
    !! ### Literature:
    !!
    !! * Spichtinger & Cziczo (2010), "Impact of heterogeneous ice nuclei
    !!   on homogeneous freezing events in cirrus clouds",
    !!   Journal of Geophysical Research, vol. 115, D14208,
    !!   doi:10.1029/2009JD012168
    !!
    implicit none
    real(dp), intent(in) :: QICC
    real(dp), intent(in) :: ANICC
    real(dp), intent(in) :: TEMP
    real(dp), intent(in) :: PC
    real(dp), intent(out) :: VM
    real(dp), intent(out) :: VN
    real(dp), intent(in) :: DT_STEP
    real(dp), intent(in) :: DZ
    
    real(dp), parameter :: L0 = 0.01_dp, &
      &                    V0 = 0.01_dp, &
      &                    M0 = 1e-3_dp, &
      &                    L00 = 1e-3_dp, &
      &                    RHOB = 0.81_dp * 1.e3_dp
    real(dp), parameter :: A1 = 1.649e-3_dp
    real(dp), parameter :: B1 = 2.2_dp
    real(dp), parameter :: X1 = 3086.0_dp, X2 = 538.0_dp, X3 = 1.20_dp
    real(dp), parameter :: Y1 = 1.26_dp, Y2 = 0.68_dp, Y3 = 0.21_dp
    real(dp) :: ALPHA1, ALPHA2, BETA1, BETA2
    real(dp) :: XX1, XX2, XX3, YY1, YY2, YY3
    real(dp) :: MMEAN, N, MCRIT1, MCRIT2, MCRIT3, LCRIT3
    real(dp) :: AV1, AV2, AV3, AV4
    
    real(dp) :: BV1, BV2, BV3, BV4
    real(dp) :: CC, CMGICC, R
    
    if (ANICC .gt. 0.0_dp) then
      MMEAN = QICC / ANICC
      
      if (MMEAN .lt. 1.e-15) then
        VN = 0.0_dp
        VM = 0.0_dp
        return
      end if
      
      N = ANICC

      ! translate to SI units
      ALPHA2 = A1 * M0 / (L0**B1)
      BETA2 = B1

      ALPHA1 = sqrt(27.0_dp) * RHOB / 8.0_dp
      BETA1 = 3.0_dp

      CC = TPCORR_VT(270.0_dp, 815.0_dp)

      XX1 = V0 * X1 / (L0**Y1)
      XX2 = V0 * x2 / (L0**Y2)
      XX3 = X3 / (L00**Y3)
      XX3 = XX3 / CC

      YY1 = Y1
      YY2 = Y2
      YY3 = Y3

      ! constants for terminal velocities
      BV1 = YY1 / BETA1
      BV2 = YY1 / BETA2
      BV3 = YY2 / BETA2
      BV4 = YY3 / BETA2

      AV1 = XX1 / (ALPHA1**BV1)
      AV2 = XX1 / (ALPHA2**BV2)
      AV3 = XX2 / (ALPHA2**BV3)
      AV4 = XX3 / (ALPHA2**BV4)

      !	Transition Masses [kg]
      MCRIT1 = (ALPHA1**(1.0_dp / BETA1) / ALPHA2**(1.0_dp / BETA2))**(1.0_dp / (1.0_dp / BETA1 - 1.0_dp / BETA2))
      MCRIT2 = ML(490e-6_dp, ALPHA2, BETA2)
      LCRIT3 = (XX2 / XX3)**(1.0_dp / (YY3 - YY2))
      MCRIT3 = ML(LCRIT3, ALPHA2, BETA2)

      CMGICC  =  3.0_dp
      R = CMGICC  ! fixed ratio: (m_pre / m_mean) = r 

      VN = (AV1 * FMUTR(0.0_dp, MCRIT1, BV1, MMEAN, R, N) &
        &         + AV2 * FMUTR(MCRIT1, MCRIT2, BV2, MMEAN, R, N) &
        &         + AV3 * FMUTR(MCRIT2, MCRIT3, BV3, MMEAN, R, N) &
        &         + AV4 * FMUTR(MCRIT3, 10000.0_dp, BV4, MMEAN, R, N)) / N

      VM = (AV1 * FMUTR(0.0_dp, MCRIT1, BV1 + 1.0_dp, MMEAN, R, N) &
        &         + AV2 * FMUTR(MCRIT1, MCRIT2, BV2 + 1.0_dp, MMEAN, R, N) &
        &         + AV3 * FMUTR(MCRIT2, MCRIT3, BV3 + 1.0_dp, MMEAN, R, N) &
        &         + AV4 * FMUTR(MCRIT3, 10000.0_dp, BV4 + 1.0_dp, MMEAN, R, N)) &
        &         / FMU(1.0_dp, MMEAN, R, N)

      VN = VN * TPCORR_VT(TEMP, PC)
      VM = VM * TPCORR_VT(TEMP, PC)

      VN = min(VN, DZ / DT_STEP)
      VM = min(VM, DZ / DT_STEP)
    else
      VN = 0.0_dp
      VM = 0.0_dp
    end if
    
    VN = -1.0_dp * VN
    VM = -1.0_dp * VM
  end subroutine TERM_VEL_ICE_NEW_CONS


  real(dp) function TPCORR_VT(T, P)
    !! Correction factor for terminal velocity, temperature and pressure correction.
    !!
    !! LITERATURE: ??? TODO
    !!
    implicit none
    real(dp) :: T  ! in K, temperature
    real(dp) :: P  ! in Pa, pressure
    
    real(dp), parameter :: TCORR = 233.0_dp
    real(dp), parameter :: PCORR = 30000.0_dp
    real(dp), parameter :: EXPP = -0.178_dp
    real(dp), parameter :: EXPT = -0.394_dp

    TPCORR_VT = ((P / PCORR)**EXPP) * ((T / TCORR)**EXPT)
  end function TPCORR_VT


  real(dp) function ML(L, AA, BB)
    implicit none
    real(dp) :: L, AA, BB
    
    ML = AA * L**BB
  end function ML


  real(dp) function FMU(K, MMEAN, R, N)
    implicit none
    real(dp) :: K, MMEAN, R, N
    real(dp) :: EXPK
    
    EXPK = (K * K - K) * 0.5_dp
    FMU = N * (MMEAN**K) * (R**EXPK)
  end function FMU
  
  
  real(dp) function FMUTR(X1, X2, K, MMEAN, R, N)
    use CONSTANTS, only: PI
    implicit none
    real(dp) :: X1, X2, MMEAN, R, N
    real(dp) :: K
    real(dp) :: Z1, Z2

    if (X2 .le. 0.0_dp) then
      ! WARNING: X2<=0 should not happen; return FMUTR=0'
      FMUTR = 0.0_dp
    else
      Z2 = TRANSFZ(X2, MMEAN, R, K)
      if (X1 .gt. 0.0_dp) then
        Z1 = TRANSFZ(X1, MMEAN, R, K)
        FMUTR = 1.0_dp / sqrt(PI) * FMU(K, MMEAN, R, N) * (FUNCINT(Z2) - FUNCINT(Z1))
      else
        FMUTR = 1.0_dp / sqrt(PI) * FMU(K, MMEAN, R, N) * FUNCINT(Z2)
      end if
    end if
  end function FMUTR
  
  
  real(dp) function TRANSFZ(X, MMEAN, R, K)
    implicit none
    real(dp) :: X, MMEAN, R, K
    real(dp) :: C1, C2, C3

    C1 = sqrt(R)
    C2 = sqrt(2.0_dp * log(R))
    C3 = sqrt(log(R) * 0.5_dp)

    TRANSFZ = (log(C1 * X / MMEAN) / C2) - K * C3
  end function TRANSFZ


  real(dp) function FUNCINT(X)
    use CONSTANTS, only: PI
    implicit none
    real(dp) :: X
    real(dp) :: SUMS

    if (X .lt. 0.0_dp) then
      SUMS = 1.0_dp - ERF_SPPM(abs(X))
    else
      SUMS = 1.0_dp + ERF_SPPM(X)
    end if
    FUNCINT = SUMS * 0.5_dp * sqrt(PI)
  end function FUNCINT


  real(dp) function ERF_SPPM(X)
    !! Computes approximation of the error function that is
    !! better than 5.0e-4 everywhere and that doesn't need a call of exp.
    implicit none
    real(dp) :: X
    real(dp), parameter :: A1 = 0.278398_dp, A2 = 0.230389_dp, &
      &                  A3 = 0.000972_dp, A4 = 0.078108_dp
    real(dp) :: POLY

    POLY = 1.0_dp + X * (A1 + X * (A2 + X * (A3 + X * A4)))
    ERF_SPPM = 1.0_dp - 1.0_dp / POLY**4.0_dp
  end function ERF_SPPM

  
end module sedimentation
