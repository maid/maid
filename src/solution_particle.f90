module solution_particle
  !! Procedures regarding solution particles.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr, Christian Rolf, Martina Krämer, Iulia Gensch and Helmut Bunz
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  use, intrinsic :: iso_fortran_env, only: sp=>real32, dp=>real64
  implicit none
  
  private
  public :: water_activity_sol_shi2001, water_activity_sol_schneider2021
  public :: calc_H2SO4solution_density_with_derivatives, calc_H2SO4solution_surface_tension_with_derivatives
  public :: get_solution_density, get_solution_surface_tension
  public :: get_p_h2o_sat_over_H2SO4_solution
  public :: get_p_sat_solutes
  
contains
  
  
  real(dp) function water_activity_sol_shi2001(T, X_WT) result(WATER_ACTIVITY_SOL)
    !! Water activity of H2SO4/H2O solution according to Shi et al. (2001).
    !!
    !! ### Literature
    !!
    !! * Q. Shi, J. T. Jayne, C. E. Kolb, D. R. Worsnop, and P. Davidovits,
    !!   “Kinetic model for reaction of ClONO2with H2O and HCl and HOCl with HCl in sulfuric acid solutions”,
    !!   Journal of Geophysical Research: Atmospheres, vol. 106, no. D20, pp. 24259–24274, Oct. 2001, doi: 10.1029/2000jd000181.
    !!
    implicit none
    real(dp) :: T  !! in K, temperature
    real(dp) :: X_WT  !! Molefraction of H2SO4 in solution with water.
    real(dp) :: P(5) = [-69.775_dp, -18253.7_dp, 31072.2_dp, -25668.8_dp, -26.9033_dp]  !! Parameters according to Shi et al. (2001)
    
    if (X_WT .gt. 1) then
      WATER_ACTIVITY_SOL = 0.0_dp
    else
      WATER_ACTIVITY_SOL = exp((P(1) * X_WT &
        &                       + P(2) * X_WT**2 &
        &                       + P(3) * X_WT**3 &
        &                       + P(4) * X_WT**4) &
        &                      * (1.0_dp / T + P(5) / T**2))
    end if
  end function water_activity_sol_shi2001

  
  real(dp) function water_activity_sol_schneider2021(T, PW_SAT_LIQ, PW_SAT_ICE) result(WATER_ACTIVITY_SOL)
    !! Water activity of H2SO4/H2O solution according to Schneider et al. (2021).
    !! Parametrization for water activity indirectly from S_ice_crit.
    !!
    !! ### Literature
    !!
    !! * J. Schneider et al., “High homogeneous freezing onsets of sulfuric acid aerosol at cirrus temperatures”,
    !!   vol. 21, no. 18, pp. 14403–14425, Sep. 2021, doi: 10.5194/acp-21-14403-2021.
    !!
    implicit none
    real(dp) :: T  !! in K, temperature
    real(dp) :: PW_SAT_LIQ  !! water vapor pressure with respect to water, in Pa
    real(dp) :: PW_SAT_ICE  !! water vapor pressure with respect to ice, in Pa
    real(dp) :: S_ICE_CRIT  !! critical water saturation ratio with respect to ice, where homogeneous freezing is observed
    
    real(dp), parameter :: A1 = -1.4_dp, A2 = 390.0_dp  !! Fit Parameters according to Schneider et al. (2021)
    
    S_ICE_CRIT = exp(A1 + A2 / T)
    WATER_ACTIVITY_SOL = S_ICE_CRIT * PW_SAT_ICE / PW_SAT_LIQ
  end function water_activity_sol_schneider2021

  
  subroutine calc_H2SO4solution_density_with_derivatives(RHO, MODE, XI, DRHO, D2RHO)
    !! Calculate the density of a H2SO4/H2O solution with its first and second derivative.
    use CONSTANTS, only: MOLAR_MASS_SOLUTES
    implicit none
    
    real(dp), intent(out) :: RHO  !! in g/cm³, density of H2SO4/H2O solution
    character(*), intent(in) :: MODE  !! specify 'mass' or 'mole'
    real(dp), intent(in) :: XI  !! mass fraction of H2SO4 in the aqueous aerosol
    real(dp), intent(out) :: DRHO  !! first derivative d/dXI RHO
    real(dp), intent(out) :: D2RHO  !! second derivative d²/dXI² RHO
    
    integer :: IDX, K, I, J, L  !! iteration indices
    real(dp) :: H(101), Q(101), U(101), M(101)
    real(dp) :: A1(101, 2), A2(101, 2), A3(101, 2), X(101, 2)
    real(dp) :: MYK, LAMK, DNK, PK, XX
    
    real(dp) :: RHOIN(101) = reshape([  &
      & 1.0, 1.0051, 1.0118, 1.0184, 1.025, 1.0317, 1.0385, 1.0453, &
      & 1.0522, 1.0591, 1.0661, 1.0731, 1.0802, 1.0874, 1.0947, 1.102, 1.1094, &
      & 1.1168, 1.1243, 1.1318, 1.1394, 1.1471, 1.1548, 1.1626, 1.1704, 1.1783, &
      & 1.1862, 1.1942, 1.2023, 1.2104, 1.2185, 1.2267, 1.2349, 1.2432, 1.2515, &
      & 1.2599, 1.2684, 1.2769, 1.2855, 1.2941, 1.3028, 1.3116, 1.3205, 1.3294, &
      & 1.3384, 1.3476, 1.3569, 1.3663, 1.3758, 1.3854, 1.3951, 1.4049, 1.4148, &
      & 1.4248, 1.435, 1.4453, 1.4557, 1.4662, 1.4768, 1.4875, 1.4983, 1.5091, &
      & 1.52, 1.531, 1.5421, 1.5533, 1.5646, 1.576, 1.5874, 1.5989, 1.6105, &
      & 1.6221, 1.6338, 1.6456, 1.6574, 1.6692, 1.681, 1.6927, 1.7043, 1.7158, &
      & 1.7272, 1.7383, 1.7491, 1.7594, 1.7693, 1.7786, 1.7872, 1.7951, 1.8022, &
      & 1.8087, 1.8144, 1.8195, 1.824, 1.8279, 1.8312, 1.8337, 1.8355, 1.8364, &
      & 1.8361, 1.8342, 1.8305], shape(RHOIN))
      
    do K = 1, 101
      X(K, 2) = real(K - 1, kind=dp) * 0.01_dp
      X(K, 1) = MOLAR_MASS_SOLUTES(2) * X(K, 2)  &
        &       / (MOLAR_MASS_SOLUTES(1) - (MOLAR_MASS_SOLUTES(1) - MOLAR_MASS_SOLUTES(2)) * X(K, 2))
    end do
    
    X(101, 1) = X(101, 1) * 1.00001_dp
    X(101, 2) = X(101, 2) * 1.00001_dp
    
    do I = 1, 2
      do K = 2, 101
        H(K) = X(K, I) - X(K - 1, I)
      end do
      
      Q(1) = - 0.5_dp
      U(1) = 0.0_dp
      
      do K = 2, 100
        XX = H(K) + H(K + 1)
        LAMK = H(K + 1) / XX
        MYK = H(K) / XX
        DNK = 6.0_dp / XX * ((RHOIN(K + 1) - RHOIN(K)) / H(K + 1) - (RHOIN(K) - RHOIN(K - 1)) / H(K))
        PK = MYK * Q(K - 1) + 2.0_dp
        Q(K) = - LAMK / PK
        U(K) = (DNK - MYK * U(K - 1)) / PK
      end do
      
      M(101) = - U(100) / (Q(100) + 2.0_dp)
      
      do K = 1, 100
        IDX = 101 - K
        M(IDX) = Q(IDX) * M(IDX + 1) + U(IDX)
      end do
      
      do K = 1, 100
        A2(K, I) = 0.5_dp * M(K)
        A1(K, I) = (RHOIN(K + 1) - RHOIN(K)) / H(K + 1)  &
          &        - (2.0_dp * M(K) + M(K + 1)) * H(K + 1) / 6.0_dp
        A3(K, I) = (M(K + 1) - M(K)) / 6.0_dp / H(K + 1)
      end do
    end do

    select case (MODE)
      case ('mole')
        J = 1
        do while (XI .gt. X(J + 1, 1))
          J = J + 1
        end do
        L = 1
      case ('mass')
        J = int(XI * 100.0_dp) + 1
        L = 2
      case default
        print*, "ERROR: invalid value for variable MODE in subroutine calc_H2SO4solution_density_with_derivatives: ", MODE
        stop
    end select

    XX = XI - X(J, L)
    RHO = RHOIN(J) + XX * (A1(J, L) + XX * (A2(J, L) + XX * A3(J, L)))
    DRHO = A1(J, L) + XX * (2.0_dp * A2(J, L) + XX * 3.0_dp * A3(J, L))
    D2RHO = 2.0_dp * A2(J, L) + XX * 6.0_dp * A3(J, L)
    
  end subroutine calc_H2SO4solution_density_with_derivatives

  
  subroutine calc_H2SO4solution_surface_tension_with_derivatives(SIGMA, MODE, XI, TEMP, DSIG, D2SIG)
    !! Surface tension for H2SO4/H2O solution with its first and second derivative.
    !!
    !! ### Literature
    !! * SABININA, L., TERPUGOW, L. (1935): DIE OBERFLÄCHENSPANNUNG DES SYSTEMS H2SO4-H2O, Z.PHYSIKAL. CHEMIE A173 P. 237-241
    !! * STOER, J. (2004): EINFUEHRUNG IN DIE NUMERISCHE..., SPRINGER VERLAG, BAND 1: P. 80, DEF. C
    !!
    !! ---
    !! **Notes**:
    !! ```text
    !! INTERPOLATION T-REF : LINEAR
    !! MOLEC(MASS)FRACTION-REF : SPLINE
    !! SIGMAI - TO INITIATE THE INTERPOLATION
    !! SIGMOL - IF XI AS MOLECULAR FRACTION
    !! SIGMAS - IF XI AS MASS FRACTION
    !! ```
    !!
    use CONSTANTS, only: MOLAR_MASS_SOLUTES
    implicit none
    real(dp), intent(out):: SIGMA  !! Surface tension of H2SO4/H2O solution
    character(len=*), intent(in) :: MODE  ! specify if XI is mole fraction or mass fraction
    real(dp), intent(in) :: XI  !! current mixing fraction of H2SO4/H2O, with respect to mass or mole, depending on `MODE`
    real(dp), intent(in) :: TEMP  !! Temperature in °C
    real(dp), intent(out):: DSIG  !! derivative of `SIGMA`
    real(dp), intent(out):: D2SIG  !! second derivative of `SIGMA`
    
    real(dp) :: H(15), Q(15), U(15), SIGT(15), &
      &         A1(15, 2, 2), A2(15, 2, 2), A3(15, 2, 2), &
      &         B1(15, 2), B2(15, 2), B3(15, 2), &
      &         M(15), &
      &         XX, MYK, LAMK, DNK, PK, &
      &         TT, B1T, B2T
    integer :: INDEXM(0:1000)
    real(dp) :: X(15, 2) = reshape(  &
      & [0., .0543, .1013, .1117, .1322, .1712, .1912, .215, .2649, .333, &  !! mole fraction
      &  .415, .5285, .6712, .885, 1., &
      &  0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], &  !! mass fraction
      &  shape(X))
    real(dp) :: SIG(15, 2) = reshape(  &
      & [74.01, 75.57, 77.1, 77.25, 77.4,  &  !! mole fraction
      &  77.18, 76.79, 76.4, 75.5, 73.98, 71.8, 68., 62.26, 55.22, 52.92, 67.8,  &
      &  71.55, 73.55, 73.9, 74.33, 74.99, 75.11, 74.94, 74.2, 72.95, 70.73, 66.92,  &  !! mass fraction
      &  61.15, 54., 51.7],  &
      &  shape(SIG))
    integer :: IDX, IND1, IND2, K, KK, J
    integer :: I, L  !! index over mole or mass fraction
    
    do K = 1, 14
      X(K, 2) = X(K, 1) * MOLAR_MASS_SOLUTES(1) / (MOLAR_MASS_SOLUTES(2) * (1 - X(K, 1)) + MOLAR_MASS_SOLUTES(1) * X(K, 1))
    end do
    X(15, 2) = 1.0_dp
    
    INDEXM(0) = 1
    IND1 = 1
    do K = 2, 14
      IND2 = int(X(K, 2) * 1000.0_dp)
      do KK = IND1, IND2
        INDEXM(KK) = K - 1
      end do
      IND1 = IND2 + 1
    end do
    
    do K = IND1, 1000
      INDEXM(K) = 14
    end do
    
    ! CHANGING MOLEC. FRACTION (I=1) TO MASS FRACTION (I=2) (9TH LOOP)
    do I = 1, 2
      do K = 2, 15
        H(K) = X(K, I) - X(K-1, I)  ! DELTA_X
      end do
      Q(1) = -0.5_dp
      U(1) = 0.0_dp
      
      ! INTERPOLATING FOR 10 RESP. 50°C (2ND LOOP)
      do L = 1, 2
        do K = 2, 14
          XX = H(K) + H(K+1)  ! 2 * DELTA_X
          LAMK = H(K+1) / XX  ! upper ratio to XX
          MYK = H(K) / XX       ! lower ratio to XX
          DNK = 6.0_dp / XX * ((SIG(K+1, L) - SIG(K, L)) / H(K+1) - (SIG(K, L) - SIG(K-1, L)) / H(K))
          PK = MYK * Q(K - 1) + 2.0_dp
          Q(K) = -LAMK / PK
          U(K) = (DNK - MYK * U(K - 1)) / PK
        end do
        
        M(15) = -U(14) / (Q(14) + 2.0_dp)
        do K = 1, 14
          IDX = 15 - K
          M(IDX) = Q(IDX) * M(IDX+1) + U(IDX)
        end do
        
        do K = 1, 14
          A2(K, L, I) = 0.5_dp * M(K)
          A1(K, L, I) = (SIG(K+1, L) - SIG(K, L)) / H(K+1) - (2.0_dp * M(K) + M(K+1)) * H(K+1) / 6.0_dp
          A3(K, L, I) = (M(K + 1) - M(K)) / 6.0_dp / H(K+1)
        end do
      end do
    end do
    !
    ! ------------------------------------------------------------------
    !
    ! ANNOTATION: IF CONSTANT TEMPERATURE (AIDA) INTERPOLATION
    !               COEFFICIENTS ARE SET
    !
    ! ------------------------------------------------------------------
    !
    TT = TEMP
    if (TEMP .gt. 50.0_dp) TT = 50.0_dp
    if (TEMP .lt. 0.0_dp) TT = 0.0_dp
    
    B1T = (50.0_dp - TT) / 40.0_dp
    B2T = (TT - 10.0_dp) / 40.0_dp
    SIGT(15) = B1T * SIG(15, 1) + B2T * SIG(15, 2)
    do K = 1, 14
      SIGT(K) = B1T * SIG(K, 1) + B2T * SIG(K, 2)
      do I = 1, 2
        B1(K, I) = B1T * A1(K, 1, I) + B2T * A1(K, 2, I)
        B2(K, I) = B1T * A2(K, 1, I) + B2T * A2(K, 2, I)
        B3(K, I) = B1T * A3(K, 1, I) + B2T * A3(K, 2, I)
      end do
    end do

    
    select case (MODE)
      case ('mole')
        J = 1
        L = 1  ! use mole fraction, not mass fractioop
      case ('mass')
        J = INDEXM(int(XI * 1000.0_dp))
        L = 2  ! use mass fraction, not mole fraction
      case default
        print*, "ERROR: invalid value for MODE in subroutine calc_H2SO4solution_surface_tension_with_derivatives: ", MODE
        stop
    end select
    
    do while (XI .gt. X(J+1, L))
      J = J + 1
    end do
    XX = XI - X(J, L)
    SIGMA = SIGT(J) + XX * (B1(J, L) + XX*(B2(J, L) + XX * B3(J, L)))
    DSIG = B1(J, L) + XX * (2.0_dp * B2(J, L) + XX * 3.0_dp * B3(J, L))
    D2SIG = B2(J, L) + XX * 6.0_dp * B3(J, L)
      
  end subroutine calc_H2SO4solution_surface_tension_with_derivatives

  
  real(dp) function get_solution_density(T, X_WT) result(DENSITY)
    !! Calculate the density of H2SO4/HNO3/H2O solutions in (g cm⁻³).
    !!
    !! ### Literature:
    !!
    !! * Martin et al. (2000), " Densities and surface tensions
    !!   of H2SO4/HNO3/H2O solutions", Geophysical Research Letters ,
    !!   Vol. 27, No. 2, American Geophysical Union (AGU),
    !!   p. 197-200, doi: 10.1029/1999gl010892
    !!
    !! ---
    !! **Note**: Based on measurements between 253 and 293 Kelvin, then further extrapolated for 10 K to both sides.
    !!
    !! index | solute
    !! ----- | ------
    !! 1     | H2SO4
    !! 2     | HNO3 (MAINPROGRAM 3)
    implicit none
    real(dp), intent(in) :: T  !! Input temperature in K
    real(dp), dimension(*), intent(in) :: X_WT  !! mass fraction of solutes
    
    real(dp) :: T_AKT  !! Temperature to be used for density calculation, in K
    real(dp), dimension(2) :: DEN  !! Density of solution at 253 and 293 K
    real(dp) :: A1, A2  !! Parameters for linear interpolation/extrapolation
    integer :: I, J, K  !! Loop indices
    real(dp), dimension(0:4, 0:3, 2), parameter :: RHO_IJK = &
    & reshape([ &
      ! Parameters for 253 K
      & 1.0015_dp,     9.6589e-3_dp,  -1.1562e-4_dp, 2.6848e-6_dp,  -1.6015e-8_dp, &
      & 9.7509e-3_dp,  -3.9433e-5_dp, 4.3442e-6_dp,  -3.6871e-8_dp, 0.0_dp, &
      & -1.834e-4_dp,  3.8149e-6_dp,  -7.0749e-8_dp, 0.0_dp,        0.0_dp, &
      & 2.9113e-6_dp,  -6.3144e-8_dp, 0.0_dp,        0.0_dp,        0.0_dp, &
      ! Parameters for 293 K
      & .9982_dp,      7.9119e-3_dp,  -7.6431e-5_dp, 2.2885e-6_dp,  -1.4651e-8_dp, &
      & 5.8487e-3_dp,  2.9369e-5_dp,  2.8093e-6_dp,  -2.1422e-8_dp, 0.0_dp, &
      & -2.3873e-5_dp, 1.8949e-6_dp,  -6.4247e-8_dp, 0.0_dp,        0.0_dp, &
      & 7.9815e-7_dp,  -3.6905e-8_dp, 0.0_dp,        0.0_dp,        0.0_dp], shape(RHO_IJK))
        
    do K = 1, 2  ! for temperatures 253K and 293 K
      DEN(K) = 0.0_dp
      do J = 0, 3
        do I = 0, (4 - J)
          DEN(K) = DEN(K) + RHO_IJK(I, J, K) * (X_WT(1) * 100.0_dp)**I * (X_WT(3) * 100.0_dp)**J
        end do
      end do
    end do
    
    T_AKT=T
    ! Extrapolation 10K around the valid range.
    ! If temperature exceeds (t_valid +/- 10 K) extrapolation is not extended.
    if (T .lt. 243.0_dp) T_AKT = 243.0_dp
    if (T .gt. 303.0_dp) T_AKT = 303.0_dp
    
    ! linear interpolation between 253K and 293K
    A1 = (293.0_dp - T_AKT) / 40.0_dp
    A2 = (T_AKT - 253.0_dp) / 40.0_dp
    DENSITY = A1 * DEN(1) + A2 * DEN(2)

  end function get_solution_density
  
  
  real(dp) function get_solution_surface_tension(T, X_WT) result(SURFACE)
    !! Surface tension of H2O/H2SO4/HNO3 solution in (mN m⁻¹) or (dyn cm⁻1).
    !!
    !! Literature:
    !! ---
    !!
    !! * Martin et al. (2000), " Densities and surface tensions
    !!   of H2SO4/HNO3/H2O solutions", Geophysical Research Letters ,
    !!   Vol. 27, No. 2, American Geophysical Union (AGU),
    !!   p. 197-200, doi: 10.1029/1999gl010892
    !!
    !! ---
    !! **Note**: Based on measurements between 253 and 293 Kelvin, then further extrapolated for 10 K to both sides.
    !!
    !! index | solute
    !! ----- | ------
    !! 1     | H2SO4
    !! 2     | HNO3 (MAINPROGRAM 3)
    implicit none
    real(dp) :: T  !! in K, temperature
    real(dp) :: X_WT(*)  !! mass fraction of compunds in the particle
    
    real(dp) :: T_AKT  !! in K, temperature between T_LOWER and T_UPPER on which to evaluate the parameterization
    real(dp) :: SIG(2)  !! surface tension calculated for T_UPPER and T_LOWER
    real(dp) :: X_INT(2)  !! wt% of H2SO4 and HNO3
    real(dp) :: A1, A2  !! parameters for linear interpolation
    integer :: K, I, J, II  !! loop indices
    
    real(dp), parameter :: T_UPPER = 293.0_dp  !! upper temperature with parameterization
    real(dp), parameter :: T_LOWER = 253.0_dp  !! lower temperature with parameterization
    
    real(dp) :: SIG_IJ(0:5, 0:5, 2) = reshape( &
      ! Values for 253 K
      &    [3.601e7_dp,  8.127e5_dp, 2.194e4_dp, -4.554e2_dp, 7.115_dp,   -4.483e-2_dp,  &
      &     5.894e5_dp,  0.0_dp,     0.0_dp,     0.0_dp,     -3.648e-2_dp, 0.0_dp,  &
      &     5.397e4_dp,  0.0_dp,     0.0_dp,     8.724e-2_dp, 0.0_dp,      0.0_dp,  &
      &     -2.994e3_dp, 0.0_dp,     -9.681e-2_dp, 0.0_dp,    0.0_dp,      0.0_dp,  &
      &     6.919e1_dp,  1.001e-2_dp,0.0_dp,     0.0_dp,      0.0_dp,      0.0_dp,  &
      &     -5.648e-1_dp,0.0_dp,     0.0_dp,     0.0_dp,      0.0_dp,      0.0_dp,  &
      ! Values for 293 K
      &     7.201e7_dp,  1.073e6_dp, 8.58e3_dp,  -1.036e2_dp, 2.27_dp,    -2.333e-2_dp,  &
      &     3.893e6_dp,  0.0_dp,     0.0_dp,     0.0_dp,      4.895e-3_dp, 0.0_dp,  &
      &     9.736e4_dp,  0.0_dp,     0.0_dp,     -1.866e-1_dp,0.0_dp,      0.0_dp,  &
      &     -1.832e3_dp, 0.0_dp,     3.358e-1_dp,0.0_dp,      0.0_dp,      0.0_dp,  &
      &     1.282e1_dp, -2.811e-1_dp,0.0_dp,     0.0_dp,      0.0_dp,      0.0_dp,  &
      &     1.076e-1_dp, 0.0_dp,     0.0_dp,     0.0_dp,      0.0_dp,      0.0_dp],  &
      & shape(SIG_IJ))
      !! Values from Martin et al. (2000), table IV
      
    
    real(dp) :: GAM(0:2, 2) = reshape([6.726e2_dp, 9.692_dp, 8.276_dp, &             ! 253 K
      &                                9.949e2_dp, 7.321_dp, 29.17_dp], shape(GAM))  ! 293 K
      !! Values from Martin et al. (2000), table IV (lower)
    
    
    X_INT(1) = X_WT(1) * 100.0_dp  ! wt% of H2SO4
    X_INT(2) = X_WT(3) * 100.0_dp  ! wt% of HNO3
    
    if (X_INT(1) .lt. 1.e-20_dp) X_INT(1) = 0.0_dp
    if (X_INT(2) .lt. 1.e-20_dp) X_INT(2) = 0.0_dp
    
    T_AKT = T
    if (T .lt. T_LOWER) T_AKT = T_LOWER
    if (T .gt. T_UPPER) T_AKT = T_UPPER
    A1 = (T_UPPER - T_AKT) / (T_UPPER - T_LOWER)
    A2 = (T_AKT - T_LOWER) / (T_UPPER - T_LOWER)
    
    do K = 1, 2
      SIG(K) = SIG_IJ(0, 0, K)
      
      do I = 1, 5
      SIG(K) = SIG(K) + SIG_IJ(I, 0, K) * X_INT(1)**I
      end do
      
      do J = 1, 5
      SIG(K) = SIG(K) + SIG_IJ(0, J, K) * X_INT(2)**J
      end do
      
      do II = 1, 4
      SIG(K) = SIG(K) + SIG_IJ(5 - II, II, K) * X_INT(1)**(5 - II) * X_INT(2)**II
      end do
      
      SIG(K) = SIG(K) / (GAM(0, K) + GAM(1, K) * X_INT(1) + GAM(2, K) * X_INT(2))**2
    end do
    SURFACE = A1 * SIG(1) + A2 * SIG(2)
    
    ! Note: to eliminate negative values of the surface tension in case of concentrated HNO3 solutions
    if (SURFACE .lt. 1e-20_dp) SURFACE = 0.0_dp
    
  end function get_solution_surface_tension

  
  subroutine get_p_h2o_sat_over_H2SO4_solution(T, X_WT_H2SO4,  &
    &                                          P_H2O_SAT_OVER_H2SO4_SOLUTION, dlnPH2OSOLSAT_dX_WT_H2SO4, LATENT_HEAT_SOLUTES)
    !! Calculate water vapor saturation pressure over H2O/H2SO4 solution and the derivative of its ln for changes in X_WT.
    !!
    !! NOTE: This procedure is only used to calculate equilibrium states between solution droplets and environment.
    !!
    use CONSTANTS, only: RGAS_SI, MOLAR_MASS_SOLUTES
    use PARAMETERS, only: N_COMPONENTS, KPAR_AW_P
    use interpolation, only: CSPLIN
    use water_properties, only: get_p_h2o_sat_liq
    use ice_properties, only: get_p_h2o_sat_ice
    implicit none
    real(dp), intent(in) :: T  !! in K, Temperature
    real(dp), intent(in) :: X_WT_H2SO4  !! mass fraction of H2SO4 in the aqueous solution
    real(dp), intent(out) :: P_H2O_SAT_OVER_H2SO4_SOLUTION  !! in Pa, water vapor saturation pressure over H2O/H2SO4 solution
    real(dp), intent(out) :: dlnPH2OSOLSAT_dX_WT_H2SO4  !! Change of ln(`P`) for a deviation of `epsilon_X_WT_H2SO4` in `X_WT_H2SO4`, in ln(DP[Pa]))
    real(dp), intent(out) :: LATENT_HEAT_SOLUTES(N_COMPONENTS)  !! current (AKTuelle) latent heat in ???
    
    real(dp) :: ln_PH2OSOLSAT_AKT  !! ln of current water vapor saturation pressure over solution
    real(dp) :: PH2OSOLSAT_0, PH2OSOLSAT_10, PH2OSOLSAT_80, PH2OSOLSAT_100  !! Water vapor pressure over aqueous H2SO4 solution for different wt%
    real(dp) :: epsilon_X_WT_H2SO4  !! Difference between input X_WT_H2SO4 and closest X_WT_H2SO4 value in parameterization
    real(dp) :: ln_PH2OSOLSAT(15)  !! natural logarithm of water vapor pressure in mbar
    real(dp) :: A1(15), A2(15), A3(15)  !! interpolation parameters for water vapor pressure `P`
    real(dp) :: WL (15)  !! in ???, latent heat
    real(dp) :: B1(15), B2(15), B3(15)  !! interpolation parameters for latent heat `WL`
    real(dp) :: DX_WT_H2SO4, DPRES, DPRES_DX_WT_H2SO4  !! interpolate edge cases, where Tabazadeh 1997 is unspecified
    
    integer, parameter :: KU = 1, KO = 15  !! lower and upper bin from 10 %wt to 80 %wt in 5%-steps
    integer :: K  !! iterate `K` from `KU` to `KO`
    integer :: CLOSEST_IDX  !! the value of K in [KU, KO] best fitting `X_WT_H2SO4`
    
    real(dp) :: X(15) = [0.1_dp, 0.15_dp, 0.2_dp, 0.25_dp, 0.3_dp, 0.35_dp, 0.4_dp, &
      &                  0.45_dp, 0.5_dp, 0.55_dp, 0.6_dp, 0.65_dp, 0.7_dp, 0.75_dp, 0.8_dp]
      !! H2SO4 mass fraction bins
    real(dp) :: COEFF(3, 15) = reshape( &
      !  a       ,  b        ,  c                    
      & [19.726_dp,-4364.8_dp, -147620.0_dp,  &  ! 10 wt%; a, b, c; based on Clegg and Brimblecombe, 1995
      & 19.747_dp, -4390.9_dp, -144690.0_dp,  &  ! 15 wt%; based on Clegg and Brimblecombe, 1995
      & 19.761_dp, -4414.7_dp, -142940.0_dp,  &  ! 20 wt%; --"--
      & 19.794_dp, -4451.1_dp, -140870.0_dp,  &  ! 25 wt%
      & 19.883_dp, -4519.2_dp, -136500.0_dp,  &  ! 30 wt%
      & 20.078_dp, -4644.0_dp, -127240.0_dp,  &  ! ...
      & 20.379_dp, -4828.5_dp, -112550.0_dp,  &
      & 20.637_dp, -5011.5_dp, -98811.0_dp,   &
      & 20.682_dp, -5121.3_dp, -94033.0_dp,  &
      & 20.555_dp, -5177.6_dp, -96984.0_dp,  &
      & 20.405_dp, -5252.1_dp, -100840.0_dp,  &
      & 20.383_dp, -5422.4_dp, -97966.0_dp,  &
      & 20.585_dp, -5743.8_dp, -83701.0_dp,  &   ! ...
      & 21.169_dp, -6310.6_dp, -48396.0_dp,  &   ! 70 wt%; based on Clegg and Brimblecombe, 1995
      & 21.808_dp, -6985.9_dp, -12170.0_dp],  &  ! 80 wt%; based on Giauque et al., 1960
      & shape(COEFF))
      !! Coefficients from Tabazadeh et al. (1997), table 1
    
    real(dp) :: X_MOLEFRAC, WATER_ACTIVITY_SOL  !! variables used when following Shi et al., 2001
    real(dp) :: P_SAT_ICE, P_SAT_LIQ  !! variables used when following Schneider, 2021
    
    select case (KPAR_AW_P)
      case (1)  ! (KPAR_AW_P)
        !! Using the original implementation, following Carslaw (see the paper of Tabazadeh).
        !!
        !! Literature
        !! ---
        !!
        !! * A. Tabazadeh, O. B. Toon, S. L. Clegg, and P. Hamill, “A new parameterization of H2SO4/H2O aerosol
        !!   composition: Atmospheric implications,” Geophysical Research Letters, vol. 24, no. 15, pp. 1931–1934,
        !!   Aug. 1997, doi: 10.1029/97gl01879.
        !!
        !! ---
        !! #### Notes
        !!
        !! * THE FORMULA FOR WATER VAPOR PRESSURE OVER H2SO4 SOLUTIONS IS VALID FOR WT% BETWEEN 10%-80%
        !! * FOR VALUES BETWEEN PARAMETERIZED POINTS USE INTERPOLATION BASED ON SUBR. CSPLIN
        !! * FOR LOWER OR HIGHER X_WT_H2SO4: LINEAR INTERPOLATION BETWEEN BOUNDARY CONDITION AND PARAMETERIZATION
        
        iter_xss: do K = 1, 15
          ln_PH2OSOLSAT(K) = COEFF(1, K) + COEFF(2, K) / T + COEFF(3, K) / T**2
          WL(K)= - RGAS_SI * (COEFF(2, K) + 2.0_dp * COEFF(3, K) / T)
        end do iter_xss
        
        call CSPLIN(KU, KO, X, ln_PH2OSOLSAT, A1, A2, A3)
        call CSPLIN(KU, KO, X, WL, B1, B2, B3)
        
        ! boundary conditions for water vapor pressure over H2SO4 solution embracing the undefined intervals
        PH2OSOLSAT_0   = get_p_h2o_sat_liq(T)  ! at 0 wt% H2SO4, in Pa
        PH2OSOLSAT_10  = 100.0_dp * exp(COEFF(1, 1) + COEFF(2, 1) / T + COEFF(3, 1) / T**2)  ! at 10 wt% H2SO4, in Pa
        PH2OSOLSAT_80  = 100.0_dp * exp(COEFF(1, 15) + COEFF(2, 15) / T + COEFF(3, 15) / T**2)  ! at 80 wt% H2SO4, in Pa
        PH2OSOLSAT_100 = 0.0_dp  ! at 100 wt% H2SO4, in Pa
        
        
        if (X_WT_H2SO4 .gt. 0.1_dp .and. X_WT_H2SO4 .lt. 0.8_dp) then
        ! case: most typical H2SO4 concentrations
          CLOSEST_IDX = int((X_WT_H2SO4 - X(1)) * 20.0_dp) + 1  ! get current bin (1<CLOSEST_IDX<15) representing 0.1<X_WT_H2SO4<0.8
          epsilon_X_WT_H2SO4 = X_WT_H2SO4 - X(CLOSEST_IDX)  ! get delta between input X_WT_H2SO4 and parameterized data points
          ln_PH2OSOLSAT_AKT = ln_PH2OSOLSAT(CLOSEST_IDX) &  ! correct X_WT_H2SO4-deviation `XD` based on CPLINE-based fit, in mbar
            &                 + A1(CLOSEST_IDX) * epsilon_X_WT_H2SO4 &
            &                 + A2(CLOSEST_IDX) * epsilon_X_WT_H2SO4**2 &
            &                 + A3(CLOSEST_IDX) * epsilon_X_WT_H2SO4**3
          P_H2O_SAT_OVER_H2SO4_SOLUTION = 100.0_dp * exp(ln_PH2OSOLSAT_AKT)  ! transform from ln(p_w) in mbar to p_w in Pa
          
          dlnPH2OSOLSAT_dX_WT_H2SO4 = 1.0_dp * A1(CLOSEST_IDX) &  ! Derivative d(ln(P))/d(X_WT_H2SO4) at epsilon_X_WT_H2SO4
            &                         + 2.0_dp * A2(CLOSEST_IDX) * epsilon_X_WT_H2SO4 &
            &                         + 3.0_dp * A3(CLOSEST_IDX) * epsilon_X_WT_H2SO4**2
            
          LATENT_HEAT_SOLUTES = WL(CLOSEST_IDX) &  ! Latent heat, J/mol
            &                   + B1(CLOSEST_IDX) * epsilon_X_WT_H2SO4 &
            &                   + B2(CLOSEST_IDX) * epsilon_X_WT_H2SO4**2 &
            &                   + B3(CLOSEST_IDX) * epsilon_X_WT_H2SO4**3
        
        else if (X_WT_H2SO4 .le. 0.1_dp) then
        ! case: low H2SO4 mass fraction
          DX_WT_H2SO4 = 0.1_dp - 0.0_dp  ! width of unparameterized X_WT_H2SO4 interval at lower boundary
          DPRES  = PH2OSOLSAT_10 - PH2OSOLSAT_0  ! water vapor pressure difference over lower undefined interval
          DPRES_DX_WT_H2SO4 = DPRES / DX_WT_H2SO4  ! difference quotient (slope), in dPa/(d(mass fraction))
          P_H2O_SAT_OVER_H2SO4_SOLUTION = PH2OSOLSAT_0 + X_WT_H2SO4 * DPRES_DX_WT_H2SO4  ! Offset + linear interpolation
          dlnPH2OSOLSAT_dX_WT_H2SO4 = DPRES_DX_WT_H2SO4 / P_H2O_SAT_OVER_H2SO4_SOLUTION  ! derivative d/dX_WT_H2SO4 ln(P_H2O_SAT_OVER_H2SO4_SOLUTION)
          LATENT_HEAT_SOLUTES = WL(1)
        
        else if (X_WT_H2SO4 .ge. 0.8_dp) then
        ! case: high H2SO4 mass fraction
          DX_WT_H2SO4 = 1.0_dp - 0.8_dp  ! width of unparameterized X_WT_H2SO4 interval at upper boundary
          DPRES  = PH2OSOLSAT_100 - PH2OSOLSAT_80  ! water vapor pressure difference over upper undefined interval
          DPRES_DX_WT_H2SO4 = DPRES / DX_WT_H2SO4  ! difference quotient (slope), in dPa/(d(mass fraction))
          P_H2O_SAT_OVER_H2SO4_SOLUTION = PH2OSOLSAT_80 + (X_WT_H2SO4 - 0.8_dp) * DPRES_DX_WT_H2SO4  ! Offset + linear interpolation
          dlnPH2OSOLSAT_dX_WT_H2SO4 = DPRES_DX_WT_H2SO4 / P_H2O_SAT_OVER_H2SO4_SOLUTION  ! derivative d/dX_WT_H2SO4 ln(P_H2O_SAT_OVER_H2SO4_SOLUTION)
          LATENT_HEAT_SOLUTES = WL(15)
        else
          stop "ERROR: Invalid value for X_WT_H2SO4 in get_p_h2o_sat_over_H2SO4_solution()."
        end if

      case (2)  ! (KPAR_AW_P)
        ! Using the parameterization of a_w by Shi to derive P_H2O_SAT_OVER_H2SO4_SOLUTION.
        ! From `a_w = p_sol/p_sat` we get `p_sol = a_w * p_sat`,
        ! with a_w according to Shi et al. (2001).
        P_SAT_LIQ = get_p_h2o_sat_liq(T)
        X_MOLEFRAC = X_WT_H2SO4 / (X_WT_H2SO4 + (1.0_dp - X_WT_H2SO4) * (MOLAR_MASS_SOLUTES(1) / MOLAR_MASS_SOLUTES(2)))
        WATER_ACTIVITY_SOL = water_activity_sol_shi2001(T, X_MOLEFRAC)
        P_H2O_SAT_OVER_H2SO4_SOLUTION = WATER_ACTIVITY_SOL * P_SAT_LIQ
      
      case (3)  ! (KPAR_AW_P)
        ! Derive water vapor saturation pressure over H2SO4 solution
        ! from the relation `p_sol = a_w * p_sat`.
        ! The hom. freezing curve of Schneider et al. (ACP, 2021) is used
        ! under the assumption of thermodynamic equilibrium to derive a_w.
        P_SAT_ICE    = get_p_h2o_sat_ice(T)  ! saturation water vapor pressure over ice, in Pa
        P_SAT_LIQ    = get_p_h2o_sat_liq(T)  ! saturation water vapor pressure over water, in Pa
        P_H2O_SAT_OVER_H2SO4_SOLUTION = water_activity_sol_schneider2021(T, P_SAT_LIQ, P_SAT_ICE) * P_SAT_LIQ
        
      case default
        stop "ERROR: Invalid value for KPAR_AW_P in get_p_h2o_sat_over_H2SO4_solution()."
        
        
    end select
    
  end subroutine get_p_h2o_sat_over_H2SO4_solution
  

  subroutine get_p_sat_solutes(T, X_WT, P_SAT_SOLUTES, SPECIFIC_LATENT_HEAT_SOLUTES)
    !! Saturation vapor pressures P_SAT_SOLUTES of H2O, HNO3, HCl and HBr over solution, in Pa.
    !! Specific latent heat of vaporization SPECIFIC_LATENT_HEAT_SOLUTES of H2O, HNO3, HCl and HBr, in J/g.
    !!
    !! Note: Intent of P_SAT_SOLUTES and SPECIFIC_LATENT_HEAT_SOLUTES is `inout` instead of `out` to not
    !!       overwrite an eventually set value for H2SO4 on index 1
    !!
    !! LITERATURE: Luo et al. (1995), "Vapour pressures of H2SO4/HNO3/HCl/HBr/H2O
    !!             solutions to low stratospheric temperatures",
    !!             Geophysical research letters, vol. 22, no. 3, p. 247-250
    !!             doi:10.1029/94gl02988
    !!
    !! Notes: * "H2SO4/HNO3/H20 ternary: [...] below 40 wt% H2SO4 there is agreement
    !!          between measurements of HNO3 vapor pressures and model values within 40%"
    !!        * Regarding latent heat: "Although this functional form provides a relibale
    !!          fit we do not claim the resulting parameters have a physical meaning in
    !!          terms of **enthalpies** or entropies."
    !!        * Vapor pressure relations are applicable within (185K < T < 235K) and (0 < W_1+W_2 < 0.7).
    use PARAMETERS, only : N_COMPONENTS
    use CONSTANTS, only : RGAS_SI, MOLAR_MASS_SOLUTES
    implicit none
    
    real(dp), intent(in) :: T  !! in K, ambient temperature
    real(dp), intent(in) :: X_WT(N_COMPONENTS)  !! dimensionless, mass mixing ratio of all condensable components, (0<=X_WT<=1)
    real(dp), intent(inout) :: P_SAT_SOLUTES(N_COMPONENTS)  !! in Pa, Saturation vapor pressure of all condensable components in the particle
    real(dp), intent(inout) :: SPECIFIC_LATENT_HEAT_SOLUTES(N_COMPONENTS)  !! in J/g, specific latent heat of vaporization of the solutes
    
    real(dp) :: A_SOLUTE(2:5)  !! dimensionless, parameter evaluated for a specific solute
    real(dp) :: B_SOLUTE(2:5)  !! in K, parameter evaluated for a specific solute
    real(dp) :: W_1, W_2, W_3, W_4, W_H  !! used for calulation of water vapor saturation pressure
    integer :: K  !! index for solutes, 2=H2O, 3=HNO3, 4=HCl, 5=HBr
    
    real(dp) :: A(0:7, 3:5) = reshape( &
      & [22.74_dp, 29.0_dp, -0.457_dp, -5.03_dp, 1.447_dp, -29.72_dp, -13.9_dp, 6.1_dp, &  ! HNO3
      &  21.0_dp, 46.61_dp, 4.069_dp, -4.837_dp, 2.186_dp, -63.0_dp, -40.17_dp, -1.571_dp, &  ! HCl
      &  17.83_dp, 1.02_dp, -1.08_dp, 3.9_dp, 4.38_dp, -8.87_dp, -17.0_dp, 3.73_dp], &  ! HBr
      & shape(A))
      !! see Luo et al. (1995), table 1 (A-rows)
    real(dp) :: B(0:7, 3:5) = reshape ( &
      & [-7689.8_dp, -2896.1_dp, 2859.8_dp, -274.2_dp, -389.5_dp, 7281.0_dp, 6475.0_dp, 801.0_dp,  &  ! HNO3
      &  -7437.0_dp, -8327.8_dp, 1300.9_dp, 1087.2_dp, -242.71_dp, 18749.0_dp, 18500.0_dp, 5632.0_dp, &  ! HCl
      &  -8220.5_dp, -362.76_dp, 658.93_dp, -914.0_dp, -955.3_dp, 9976.6_dp, 19778.5_dp, 7680.0_dp], &  ! HBr
      & shape(B))
      !! See Luo et al. (1995), table 1 (B-rows)
    real(dp) :: C(12) = [23.306_dp, -4.5261_dp, -5.3465_dp, 7.451_dp, 12.0_dp, -4.0_dp, -8.19_dp, &
      &                  -5814.0_dp, 1033.0_dp, 928.9_dp, -2309.0_dp, -1876.7_dp]
      !! See Luo et al. (1995), for H2O, see eq. 7
    
    ! define parameters labeled as in Luo et al. (1995)
    W_1 = X_WT(3)  ! dimensionless, mass fraction of HNO3
    W_2 = X_WT(1)  ! dimensionless, mass fraction of H2SO4
    W_3 = X_WT(4)  ! dimensionless, mass fraction of HCl
    W_4 = X_WT(5)  ! dimensionless, mass fraction of HBr
    W_H = W_1 + W_2 * 1.4408_dp  ! See Luo et al. (1995), below eq. 7
    
    do K = 3, 5
      A_SOLUTE(K) = A(0, K) + A(1, K) * W_1 &  ! dimensionless
          &        + A(2, K) * W_2 + A(3, K) * sqrt(W_1) &
          &        + A(4, K) * sqrt(W_2) + A(5, K) * W_1**2 &
          &        + A(6, K) * W_1 * W_2 + A(7, K) * W_2**2
          
      B_SOLUTE(K) = B(0, K) + B(1, K) * W_1  &  ! in K
        &        + B(2, K) * W_2 + B(3, K) * sqrt(W_1) &
        &        + B(4, K) * sqrt(W_2) + B(5, K) * W_1**2 &
        &        + B(6, K) * W_1 * W_2 + B(7, K) * W_2**2
    end do
    
    do K = 2, 5
      ! Calculate saturation vapor pressures of H2O, HNO3, HCl, HBr
      ! over solution surface and latent heat acc. to CC-equation
      
      select case (K)
      
        case (2)  ! H2O, see  Luo et al. (1995), eq. 7
          P_SAT_SOLUTES(K) = 100.0_dp * exp(C(1) + C(2) * W_1 + C(3) * W_2 &  ! in Pa
            &                               + W_H * (C(4) * W_1 + C(5)* W_2) &
            &                               + W_H**2 * (C(6) * W_1 + C(7) * W_2) &
            &                               + 1.0_dp / T * (C(8) + C(9) * W_1 + C(10) * W_2 &
            &                                          + W_H * (C(11) * W_1 + C(12) * W_2)))
          SPECIFIC_LATENT_HEAT_SOLUTES(K) = -RGAS_SI / MOLAR_MASS_SOLUTES(K)  &
            &                               * (C(8) + C(9) * W_1 + C(10) * W_2  &
            &                                  + W_H * (C(11) * W_1 + C(12) * W_2))  ! in J/g
            
        case (3)  ! HNO3, see Luo et al. (1995), eq. 4
          P_SAT_SOLUTES(K) = 100.0_dp * exp(A_SOLUTE(K) + B_SOLUTE(K) / T) * W_1 * (W_1 + 0.09_dp * W_2)  ! in Pa
          SPECIFIC_LATENT_HEAT_SOLUTES(K) = -RGAS_SI * B_SOLUTE(K) / MOLAR_MASS_SOLUTES(K)  ! in J/g
          
        case (4)  ! HCl, see Luo et al. (1995), eq. 5
          P_SAT_SOLUTES(K) = 100.0_dp * exp(A_SOLUTE(K) + B_SOLUTE(K) / T) * W_3 * (W_3 + W_1 + 0.61_dp * W_2)  ! in Pa
          SPECIFIC_LATENT_HEAT_SOLUTES(K) = -RGAS_SI * B_SOLUTE(K) / MOLAR_MASS_SOLUTES(K)  ! in J/g
          
        case (5)  ! HBr, see Luo et al. (1995), eq. 6
          P_SAT_SOLUTES(K) = 100.0_dp * exp(A_SOLUTE(K) + B_SOLUTE(K) / T) * W_4 * (W_4 + W_1 + 0.41_dp * W_2)  ! in Pa
          SPECIFIC_LATENT_HEAT_SOLUTES(K) = -RGAS_SI * B_SOLUTE(K) / MOLAR_MASS_SOLUTES(K)  ! in J/g
          
      end select
    end do

  end subroutine get_p_sat_solutes

end module solution_particle
