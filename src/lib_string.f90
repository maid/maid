module lib_string
  !! Collection of procedures to manipulate and analyze strings/characters.
  !
  ! Copyright notice:
  !
  ! This file is part of MAID, "Model for Aerosol and Ice Dynamics",
  ! Copyright (C) 2023, Tobias Schorr
  !
  ! This program is free software: you can redistribute it and/or modify
  ! it under the terms of the GNU Affero General Public License as
  ! published by the Free Software Foundation, either version 3 of the
  ! License, or (at your option) any later version.
  !
  ! This program is distributed in the hope that it will be useful,
  ! but WITHOUT ANY WARRANTY; without even the implied warranty of
  ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ! GNU Affero General Public License for more details.
  !
  ! In the projects base directory you can find a copy of the
  ! GNU Affero General Public License or check <https://www.gnu.org/licenses/>.
  !
  implicit none
  private
  
  public :: count_substrings
  public :: list_substring_indices
  public :: to_uppercase
  public :: character_to_logical

  
contains


  function count_substrings(string, substring) result(num_substrings)
    !! counts occurances of a substring in a string
    implicit none
    character(len=*), intent(in) :: string  !! reference input string
    character(len=*), intent(in) :: substring  !! character expression to be found in `string`
    integer :: num_substrings  !! number of occurances of a substring in a string
    
    character(len=:), allocatable :: partial_string  !! a subset of `string`
    integer :: idx  !! index of a character expression in `string`
    
    ! initialization
    allocate(character(len=len_trim(string)) :: partial_string)
    num_substrings = 0
    
    partial_string = trim(string)  ! trucated part of string
    do while (.true.)
      idx = index(partial_string, trim(substring))
      if (idx > 0) then
        num_substrings = num_substrings + 1
        partial_string = partial_string(idx+1:)
      else if (idx == 0) then
        exit
      end if
    end do      
  end function count_substrings

  
  function list_substring_indices(string, substring) result(substring_indices)
    !! Return a list of integer indices defining the start of a substring in string.
    implicit none
    character(len=*), intent(in) :: string  !! reference input string
    character(len=*), intent(in) :: substring  !! character expression to be found in `string`
    integer, allocatable :: substring_indices(:)  !! list of indices where `substring` occurred in `string`
    
    character(len=:), allocatable :: partial_string  !! a subset of `string`
    integer :: idx, last_idx  !! index of a character expression in `string`
    integer :: n  !! index to iterate through `num_substrings`
    integer :: num_substrings  !! number of occurances of a substring in a string
    
    allocate(character(len=len_trim(string)) :: partial_string)
    num_substrings = count_substrings(string, substring)
    allocate(integer :: substring_indices(num_substrings))
    
    partial_string = trim(string)
    last_idx = 0
    do n = 1, num_substrings
      idx = index(partial_string, trim(substring))
      partial_string = partial_string(idx+1:)
      
      substring_indices(n) = last_idx + idx
      last_idx = last_idx + idx
    end do
  end function list_substring_indices
  
  
  function to_uppercase(STRING) result (STRING_UPCASE)
    !! Convert characters from lower case to upper case.
    character(len=*), intent(in) :: STRING  !! input string to be converted to uppercase only
    character(len=len(STRING))   :: STRING_UPCASE  !! uppercase-only version of `STRING`
    
    integer, parameter :: UP_LOW_OFFSET = iachar("a") - iachar("A")  !! offset in number representation between lowercase and uppercase characters
    integer :: j  !! index to iterate through each character of `STRING`
    
    STRING_UPCASE = STRING
    do j = 1, len(STRING_UPCASE)
      select case(STRING_UPCASE(j:j))
        case("a":"z")
            STRING_UPCASE(j:j) = achar(iachar(STRING_UPCASE(j:j)) - UP_LOW_OFFSET)
      end select
    end do
  end function to_uppercase

  
  function character_to_logical(string) result(bool)
    !! Convert character expression to logical value.
    implicit none
    character(len=*) :: string  !! input character expression
    logical :: bool  !! logical interpretation of input `string`
    
    string = to_uppercase(trim(string))
    
    if ((string .eq. '.TRUE.') .or. &
      &  (string .eq. 'TRUE') .or. &
      &  (string .eq. 'T') .or. &
      &  (string .eq. 'YES') .or. &
      &  (string .eq. 'Y')) then
      bool = .true.
    else if ((string .eq. '.FALSE.') .or. &
      &  (string .eq. 'FALSE') .or. &
      &  (string .eq. 'F') .or. &
      &  (string .eq. 'NO') .or. &
      &  (string .eq. 'N')) then
      bool = .false.
    else
      stop "ERROR: Invalid string in `character_to_logical(string)`!"
    end if
  end function character_to_logical

end module lib_string
